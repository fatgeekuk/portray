Light
   Position ( -50 100 -100 )
   Colour ( 1 1 1 )
   shape sphere
      center (-50 100 -100)
      radius 30
   ;
   samples 100
   min-samples 10
;
camera
   look-at ( 0 3.5 0 )
   drop-line (0 -1 0)
   origin ( 0 8 -17 )
   depth 0.8
   x-size 0.64
   y-size 0.48
   filter adaptive quick ;
//   filter standard
;

background ( 0 0 1 )

image
   xsize  640
   ysize  480
;

object
   box
      a (-10 0 -10)
      b (-11 7  10)
   ;
;

object
   box
      a (10 0 -10)
      b (11 7  10)
   ;
;

object
   box
      a (-10 0 10)
      b ( 10 7 11)
   ;
;
object
   plane
      position (0 0 0)
      normal (0 1 0)
      x-axis (1 0 0)
      y-axis (0 0 1)
   ;
   material
      colour (.1 .1 .1)
      spec-colour (1 1 1)
      diffuse-reflection 0.6
      specular-reflection 0.4
      specular-power 100
      gloss 0.1
      gloss-samples 10
   ;
;

object
   sphere
      center (0 6.5 0)
      radius 1.4
   ;
   material
      colour (1 .1 .1)
      spec-colour (1 .9 .9)
      diffuse-reflection 0.6
      specular-reflection 0.4
      specular-power 100
      gloss 0.1
      gloss-samples 10
   ;
;

object
   cylinder
      from (0 3 0)
      to   (0 0 0)
      radius1 1.5
      radius2 0
   ;
   material
      colour (.1 1 .1)
      spec-colour (.9 1 .9)
      diffuse-reflection 0.6
      specular-reflection 0.4
      specular-power 100
      gloss 0.1
      gloss-samples 10
   ;
;

object
   box
      a (-1 -1 -1)
      b (1 1 1)
   ;
   y-rotate 45.0
   x-rotate 45.0
   translate (0 4.4 0)
   material
      colour (.1 .1 1)
      spec-colour (.9 .9 1)
      diffuse-reflection 0.6
      specular-reflection 0.4
      specular-power 100
      gloss 0.1
      gloss-samples 10
   ;
;
