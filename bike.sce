
camera
  look-at (1.0 1.1 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (2.0 2.0 -4)
  depth 0.022
  x-size 0.032
  y-size 0.024
//  filter standard
  filter adaptive quick
  ;
;

image
  xsize 1024
  ysize 768
;

light
  colour (1.0 1.0 1.0)
  position (0.0 100 -30)
;

object
   plane
      position (0 0 0)
      normal (0 1 0)
   ;
;
object
   torus
   ;
   material
     diff-colour (0 0 0)
     spec-colour (1 1 1)
     diffuse-reflection 0.4
     specular-reflection 0.6
     specular-power 4
     reflection 0.2
   ;
   x-rotate 90.0
   translate (0.0 1.2 0)
;

object
   torus
   ;
   material
     diff-colour (0 0 0)
     spec-colour (1 1 1)
     diffuse-reflection 0.4
     specular-reflection 0.6
     specular-power 4
     reflection 0.2
   ;
   x-rotate 90.0
   translate (4 1.2 0)
;

define
   object name rim
     object
       cylinder from (0 0 0.1) to   (0 0 -0.1) radius 0.3 ;
       material named chrome
         diff-colour (0 0 0) spec-colour (1 1 1)
         diffuse-reflection 0.4 specular-reflection 0.6
         specular-power 8 reflection 0.8
       ;
     ;
     object cylinder from (0.2 0.0 0.2) to   (0 1.0 0.2) radius 0.1 ; material called chrome ; ;
     object z-rotate 60.0 cylinder from (0.2 0.0 0.2) to   (0 1.0 0.2) radius 0.1 ; material called chrome ; ;
     object z-rotate 120.0 cylinder from (0.2 0.0 0.2) to   (0 1.0 0.2) radius 0.1 ; material called chrome ; ;
     object z-rotate 180.0 cylinder from (0.2 0.0 0.2) to   (0 1.0 0.2) radius 0.1 ; material called chrome ; ;
     object z-rotate 240.0 cylinder from (0.2 0.0 0.2) to   (0 1.0 0.2) radius 0.1 ; material called chrome ; ;
     object z-rotate 300.0 cylinder from (0.2 0.0 0.2) to   (0 1.0 0.2) radius 0.1 ; material called chrome ; ;

     object
       torus radius 0.9 norm-radius 0.25 plane-radius 0.25 ;
       x-rotate 90.0
       material called chrome ;
     ;
  ;
  object name frontforks
    object
       cylinder from (0.0 0.0 0.5) to (0.0 0.0 -0.5) radius 0.05 ;
    ;
    object
      csg
        union
        operand1
          cylinder from (0.0 0.0 0.4) to (0.0 0.0 -0.4) radius 0.1 ;
        ;
        operand2
          csg
            subtract
            operand1
              box a (-0.1 -0.1 -0.4) b (0.1 0.3 0.4) ;
            ;
            operand2
              box a (0 0 -0.5) b (0.2 -0.2 0.5) ;
            ;
          ;
        ;
      ;
    ;
  ;
;
object frontforks ; translate (0 1.2 0) ;
object rim ; translate (4 1.2 0) ;
object rim ; translate (0 1.2 0) ;
