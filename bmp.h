struct BMPFileHeader
{
  char Ident0, Ident1;
  long filesize;
  int Fill1;
  int Fill2;
  long PixStart;
} BMPFileHeader;

struct BMPInfoHeader
{
  long InfoLen;
  long Width;
  long height;
  int Setto1;
  int BitsPerPix;
  long CompScheme;
  long BitsLen;
  long HPixperMetre;
  long VPixperMetre;
  long ColsInImage;
  long ImpColsImage;
} BMPInfoHeader;
