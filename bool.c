/*
  *************************************************************************
 **                                                                       **
 **			      CSG Section				  **
 **                                                                       **
  *************************************************************************
*/

#include "rend_8.h"

char   *CSGName();
CSG    *CSGRead();
void	CSGPrint();
ItemEntry *CSGIntersect(ray *Ray, Object *AnObj);
Vec    *CSGNormal();
Vec    *CSGInverse();
Box    *CSGBound();
int	CSGInOut();
Vec    *CSGCentre();
Vec    *CSGRandom();

void InitCSG(Prim *APrim)
{
  APrim->Name	   = CSGName;
  APrim->Read	   = (void *)CSGRead;
  APrim->Print	   = CSGPrint;
  APrim->Intersect = CSGIntersect;
  APrim->Bound	   = CSGBound;
  APrim->InOut	   = CSGInOut;
}

char *CSGName()
{
  return "CSG";
}

CSG *CSGRead(FILE *inputfile)
{
  CSG *Csg;
  char var[20];
  Csg = (CSG *)AllocHeap(sizeof(CSG));
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "UNION") == ZERO)
    {
      Csg->ThisOp = UNION;
    }
    else if (strcmp(var, "INTERSECT") == ZERO)
    {
      Csg->ThisOp = COMMON;
    }
    else if (strcmp(var, "SUBTRACT") == ZERO)
    {
      Csg->ThisOp = SUBTRACT;
    }
    else if (strcmp(var, "OPERAND1") == ZERO)
    {
      Csg->Op1 = ReadObjectInfo(inputfile);
    }
    else if (strcmp(var, "OPERAND2") == ZERO)
    {
      Csg->Op2 = ReadObjectInfo(inputfile);
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Csg\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);

  return(Csg);
}

void CSGPrint(CSG *Csg)
{
}

ItemEntry *CSGIntersect(ray *Ray, Object *AnObj)
{
  ItemEntry *A1, *A2, *Ans;
  CSG *Csg;

  Csg = (CSG *)AnObj->kdata;
  if (Csg->ThisOp == UNION)
  {
    A1 = IntersectObject(Ray, Csg->Op1, 1, 99999999.999);
    A2 = IntersectObject(Ray, Csg->Op2, 1, 99999999.999);
    Ans = IntMerge(A1, A2, Csg->ThisOp);
  }
  else if (Csg->ThisOp == COMMON)
  {
    A1 = IntersectObject(Ray, Csg->Op1, 1, 99999999.999);
    if (A1->Next->Data != NULL)
    {
      A2 = IntersectObject(Ray, Csg->Op2, 1, 99999999.999);
      if (A2->Next->Data != NULL)
      {
        Ans = IntMerge(A1, A2, Csg->ThisOp);
      }
      else
      {
        FreeIntList(A1);
        Ans = A2;
      }
    }
    else
    {
      Ans = A1;
    }
  }
  else
  {
    A1 = IntersectObject(Ray, Csg->Op1, 1, 99999999.999);
    if (A1->Next->Data != NULL)
    {
      A2 = IntersectObject(Ray, Csg->Op2, 1, 99999999.999);
      if (A2->Next->Data == NULL)
      {
        Ans = A1;
        FreeIntList(A2);
      }
      else
      {
        Ans = IntMerge(A1, A2, Csg->ThisOp);
      }
    }
    else
    {
      Ans = A1;
    }
  }

  return (Ans);
}

Box *CSGBound(CSG *Csg, Box *Bound)
{
  Box BoundA, BoundB;
  if (Csg->ThisOp == COMMON)
  {
    BoundObject(Csg->Op1, &BoundA);
    BoundObject(Csg->Op2, &BoundB);

    if (BoundA.A.x > BoundB.A.x)
      Bound->A.x = BoundA.A.x;
    else
      Bound->A.x = BoundB.A.x;

    if (BoundA.A.y > BoundB.A.y)
      Bound->A.y = BoundA.A.y;
    else
      Bound->A.y = BoundB.A.y;

    if (BoundA.A.z > BoundB.A.z)
      Bound->A.z = BoundA.A.z;
    else
      Bound->A.z = BoundB.A.z;

    if (BoundA.B.x < BoundB.B.x)
      Bound->B.x = BoundA.B.x;
    else
      Bound->B.x = BoundB.B.x;

    if (BoundA.B.y < BoundB.B.y)
      Bound->B.y = BoundA.B.y;
    else
      Bound->B.y = BoundB.B.y;

    if (BoundA.B.z < BoundB.B.z)
      Bound->B.z = BoundA.B.z;
    else
      Bound->B.z = BoundB.B.z;

  }
  else if (Csg->ThisOp == UNION)
  {
    BoundObject(Csg->Op1, &BoundA);
    BoundObject(Csg->Op2, &BoundB);

    if (BoundA.A.x < BoundB.A.x)
      Bound->A.x = BoundA.A.x;
    else
      Bound->A.x = BoundB.A.x;

    if (BoundA.A.y < BoundB.A.y)
      Bound->A.y = BoundA.A.y;
    else
      Bound->A.y = BoundB.A.y;

    if (BoundA.A.z < BoundB.A.z)
      Bound->A.z = BoundA.A.z;
    else
      Bound->A.z = BoundB.A.z;

    if (BoundA.B.x > BoundB.B.x)
      Bound->B.x = BoundA.B.x;
    else
      Bound->B.x = BoundB.B.x;

    if (BoundA.B.y > BoundB.B.y)
      Bound->B.y = BoundA.B.y;
    else
      Bound->B.y = BoundB.B.y;

    if (BoundA.B.z > BoundB.B.z)
      Bound->B.z = BoundA.B.z;
    else
      Bound->B.z = BoundB.B.z;
  }
  else
  {
    BoundObject(Csg->Op1, Bound);
  }

  return(Bound);
}

int CSGInOut(CSG *Csg, Vec *A)
{
    return 0; /* outside sphere */
}
