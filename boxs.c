/*
  *************************************************************************
 **                                                                       **
 **                          Box Section                                  **
 **                                                                       **
  *************************************************************************
*/

char      *BoxName();
void      *BoxRead();
void       BoxPrint();
ItemEntry *BoxIntersect(ray *Ray, Object *AnObj);
int        BoxHit(ray *Ray, Box *ABox, Flt *Dist);
Vec       *BoxNormal();
Vec       *BoxInverse();
Box       *BoxBound();
int        BoxInOut();
Vec       *BoxCentre();
Vec       *BoxRandom();

void InitBox(Prim *APrim)
{
  APrim->Name      = BoxName;
  APrim->Read      = BoxRead;
  APrim->Print     = BoxPrint;
  APrim->Intersect = BoxIntersect;
  APrim->Normal    = BoxNormal;
  APrim->Inverse   = BoxInverse;
  APrim->Bound     = BoxBound;
  APrim->InOut     = BoxInOut;
  APrim->Centre    = BoxCentre;
  APrim->Random    = BoxRandom;
}

char *BoxName()
{
  return "BOX";
}

void *BoxRead(FILE *inputfile)
{
  Box *ABox;
  Vec A, B;
  char var[20];
  ABox = (Box *)AllocHeap(sizeof(Box));
  SetVector(0.0, 0.0, 0.0, &(ABox->A));
  SetVector(1.0, 1.0, 1.0, &(ABox->B));
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "A") == ZERO)
    {
      ReadVec(inputfile,&A);
    }
    else if (strcmp(var, "B") == ZERO)
    {
      ReadVec(inputfile,&B);
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Plane\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);

  if (A.x < B.x)
  {
    ABox->A.x = A.x;
    ABox->B.x = B.x;
  }
  else
  {
    ABox->A.x = B.x;
    ABox->B.x = A.x;
  }

  if (A.y < B.y)
  {
    ABox->A.y = A.y;
    ABox->B.y = B.y;
  }
  else
  {
    ABox->A.y = B.y;
    ABox->B.y = A.y;
  }

  if (A.z < B.z)
  {
    ABox->A.z = A.z;
    ABox->B.z = B.z;
  }
  else
  {
    ABox->A.z = B.z;
    ABox->B.z = A.z;
  }
  ABox->Centre.x = (ABox->A.x + ABox->B.x) / 2;
  ABox->Centre.y = (ABox->A.y + ABox->B.y) / 2;
  ABox->Centre.z = (ABox->A.z + ABox->B.z) / 2;
  ABox->XArea = sqrt((ABox->A.y - ABox->B.y)*(ABox->A.y - ABox->B.y) +
		     (ABox->A.z - ABox->B.z)*(ABox->A.z - ABox->B.z));

  ABox->YArea = sqrt((ABox->A.x - ABox->B.x)*(ABox->A.x - ABox->B.x) +
		     (ABox->A.z - ABox->B.z)*(ABox->A.z - ABox->B.z));

  ABox->ZArea = sqrt((ABox->A.x - ABox->B.x)*(ABox->A.x - ABox->B.x) +
		     (ABox->A.y - ABox->B.y)*(ABox->A.y - ABox->B.y));

  ABox->Area  = ABox->XArea + ABox->YArea + ABox->ZArea;

  if (DEBUG == ON)
  {
    BoxPrint(ABox);
  }
  return(ABox);
}

void BoxPrint(Box *ABox)
{
  printf("Box with Corner A (" FltFmt ", " FltFmt ", " FltFmt ") \n                B (" FltFmt ", " FltFmt ", " FltFmt ") \n",
   ABox->A.x,
   ABox->A.y,
   ABox->A.z,
   ABox->B.x,
   ABox->B.y,
   ABox->B.z);
}

ItemEntry *BoxIntersect(ray *Ray, Object *AnObj)
{
  Box *ABox;
  ItemEntry *Ans=CreateList(), *Ans1=NULL;
  IntRec *AnInt;
  char Inter;
  Flt Imin, Imax, t, t1, t2;
  ABox = (Box *)AnObj->kdata;
  Imin = -9999.99;
  Imax =  9999.99;
  Inter = TRUE;
  if (Ray->Direction.x == 0.0)
  {
    if (Ray->Origin.x < ABox->A.x || Ray->Origin.x > ABox->B.x)
      Inter = FALSE;
  }
  else
  {
    t1 = (ABox->A.x - Ray->Origin.x)/Ray->Direction.x;
    t2 = (ABox->B.x - Ray->Origin.x)/Ray->Direction.x;
    if (t1 > t2)
    {
      t = t1;
      t1 = t2;
      t2 = t;
    }
    if (t1>Imin) Imin = t1;
    if (t2<Imax) Imax = t2;
    if (Imin > Imax) Inter = FALSE;
  }
  if (Inter == TRUE)
  {

    if (Ray->Direction.y == 0.0)
    {
      if (Ray->Origin.y < ABox->A.y || Ray->Origin.y > ABox->B.y)
	Inter = FALSE;
    }
    else
    {
      t1 = (ABox->A.y - Ray->Origin.y)/Ray->Direction.y;
      t2 = (ABox->B.y - Ray->Origin.y)/Ray->Direction.y;
      if (t1 > t2)
      {
	t = t1;
	t1 = t2;
	t2 = t;
      }
      if (t1>Imin) Imin = t1;
      if (t2<Imax) Imax = t2;
      if (Imin > Imax) Inter = FALSE;
    }
  }
  if (Inter == TRUE)
  {

    if (Ray->Direction.z == 0.0)
    {
      if (Ray->Origin.z < ABox->A.z || Ray->Origin.z > ABox->B.z)
	Inter = FALSE;
    }
    else
    {
      t1 = (ABox->A.z - Ray->Origin.z)/Ray->Direction.z;
      t2 = (ABox->B.z - Ray->Origin.z)/Ray->Direction.z;
      if (t1 > t2)
      {
	t = t1;
	t1 = t2;
	t2 = t;
      }
      if (t1>Imin) Imin = t1;
      if (t2<Imax) Imax = t2;
      if (Imin > Imax) Inter = FALSE;
    }
  }
  
  if (Inter == TRUE)
  {
    if (Imin < -999.9 && Imax > 999.99)
    {
      printf("R=A(%f %f %f) + nB(%f %f %f)\n", Ray->Origin.x, Ray->Origin.y, Ray->Origin.z, Ray->Direction.x, Ray->Direction.y, Ray->Direction.z);
    }
    AnInt = AllocInt();
    Ans1 = LinkInToList(AnInt, Ans, AnInt->Parent);


    VecAdds(Imin, &(Ray->Direction), &(Ray->Origin), &(AnInt->ModelHit));
    AnInt->Dirn      = ENTRY;
    AnInt->Dist      = Imin;
    AnInt->HitObj	 = AnObj;
    AnInt->WorldHit  = AnInt->ModelHit;

    AnInt = AllocInt();
    LinkInToList(AnInt, Ans1, AnInt->Parent);
    VecAdds(Imax, &(Ray->Direction), &(Ray->Origin), &(AnInt->ModelHit));
    AnInt->Dirn	    = EXIT;
    AnInt->Dist	    = Imax;
    AnInt->HitObj   = AnObj;
    AnInt->WorldHit = AnInt->ModelHit;
  }
  return(Ans);
}

int BoxHit(ray *Ray, Box *ABox, Flt *Dist)
{
  char Inter;
  int Ans=0;
  Flt Imin, Imax, t, t1, t2;
  Imin = -9999.99;
  Imax =  9999.99;
  Inter = TRUE;

  if (Ray->Direction.x == 0.0)
  {
    if (Ray->Origin.x < ABox->A.x || Ray->Origin.x > ABox->B.x)
      Inter = FALSE;
  }
  else
  {
    t1 = (ABox->A.x - Ray->Origin.x)/Ray->Direction.x;
    t2 = (ABox->B.x - Ray->Origin.x)/Ray->Direction.x;
    if (t1 > t2)
    {
      t = t1;
      t1 = t2;
      t2 = t;
    }
    if (t1>Imin) Imin = t1;
    if (t2<Imax) Imax = t2;
    if (Imin > Imax) Inter = FALSE;
  }
  if (Inter == TRUE)
  {

    if (Ray->Direction.y == 0.0)
    {
      if (Ray->Origin.y < ABox->A.y || Ray->Origin.y > ABox->B.y)
	Inter = FALSE;
    }
    else
    {
      t1 = (ABox->A.y - Ray->Origin.y)/Ray->Direction.y;
      t2 = (ABox->B.y - Ray->Origin.y)/Ray->Direction.y;
      if (t1 > t2)
      {
        t = t1;
        t1 = t2;
        t2 = t;
      }
      if (t1>Imin) Imin = t1;
      if (t2<Imax) Imax = t2;
      if (Imin > Imax) Inter = FALSE;
    }
  }
  if (Inter == TRUE)
  {

    if (Ray->Direction.z == 0.0)
    {
      if (Ray->Origin.z < ABox->A.z || Ray->Origin.z > ABox->B.z)
	Inter = FALSE;
    }
    else
    {
      t1 = (ABox->A.z - Ray->Origin.z)/Ray->Direction.z;
      t2 = (ABox->B.z - Ray->Origin.z)/Ray->Direction.z;
      if (t1 > t2)
      {
        t = t1;
        t1 = t2;
        t2 = t;
      }
      if (t1>Imin) Imin = t1;
      if (t2<Imax) Imax = t2;
      if (Imin > Imax) Inter = FALSE;
    }
  }
  if (Inter == TRUE)
  {
    *Dist = Imin;
    Ans = 1;
  }
  return(Ans);
}

Vec *BoxNormal(IntRec *A, Box *ABox)
{
  SetVector(0.0, 0.0, 0.0, &(A->Normal));
  if (fabs(A->ModelHit.x - (ABox->B.x)) <= EFFECTIVE_ZERO) A->Normal.x =  1.0;
  if (fabs(A->ModelHit.x - (ABox->A.x)) <= EFFECTIVE_ZERO) A->Normal.x = -1.0;
  if (fabs(A->ModelHit.y - (ABox->B.y)) <= EFFECTIVE_ZERO) A->Normal.y =  1.0;
  if (fabs(A->ModelHit.y - (ABox->A.y)) <= EFFECTIVE_ZERO) A->Normal.y = -1.0;
  if (fabs(A->ModelHit.z - (ABox->B.z)) <= EFFECTIVE_ZERO) A->Normal.z =  1.0;
  if (fabs(A->ModelHit.z - (ABox->A.z)) <= EFFECTIVE_ZERO) A->Normal.z = -1.0;
  VecUnit(&(A->Normal), &(A->Normal));
  return (&(A->Normal));
}

Vec *BoxInverse(IntRec *B, Box *ABox, Flt *u, Flt *v, Vec *Ans)
{
  Vec *A;

  A = &(B->ModelHit);

  if (fabs(ABox->A.x - A->x) < EFFECTIVE_ZERO || fabs(ABox->B.x - A->x) < EFFECTIVE_ZERO)
  {
    *u = A->y;
    *v = A->z;
  } else if (fabs(ABox->A.y - A->y) < EFFECTIVE_ZERO || fabs(ABox->B.y - A->y) < EFFECTIVE_ZERO)
  {
    *u = A->x;
    *v = A->z;
  }
  else
  {
    *u = A->x;
    *v = A->y;
  }
  return(A);
}

Box *BoxBound(Box *ABox, Box *Bound)
{
  *Bound = *ABox;
  return (ABox);
}

int BoxInOut(Box *ABox, Vec *A)
{
  if ((ABox->A.x >= A->x && ABox->B.x <= A->x) &&
      (ABox->A.y >= A->y && ABox->B.y <= A->y) &&
      (ABox->A.z >= A->z && ABox->B.z <= A->z))
  {
    return -1;
  }
  else
  {
    return 0;
  }
}

Vec *BoxCentre(Box *ABox, Vec *Cen)
{
  Cen->x = ABox->Centre.x;
  Cen->y = ABox->Centre.y;
  Cen->z = ABox->Centre.z;
  return(Cen);
}

Vec *BoxRandom(Box *ABox, Vec *Random)
{
  Flt a, XRange, YRange, ZRange, XOffset, YOffset, ZOffset;
  a = (ABox->Area * 2.0 * (Flt)rand()) / (Flt)RAND_MAX;
  XOffset = ABox->A.x;
  YOffset = ABox->A.y;
  ZOffset = ABox->A.z;
  XRange  = ABox->B.x - ABox->A.x;
  YRange  = ABox->B.y - ABox->A.y;
  ZRange  = ABox->B.z - ABox->A.z;

  if (a < ABox->XArea)
  {
    /* First X Side */
    XRange = 0.0;
  }
  else
  {
    a -= ABox->XArea;
    if (a < ABox->XArea)
    {
      /* Second X Side */
      XRange = 0.0;
      XOffset = ABox->B.x;
    }
    else
    {
      a -= ABox->XArea;
      if (a < ABox->YArea)
      {
	/* First Y Side */
	YRange = 0.0;
      }
      else
      {
	a -= ABox->YArea;
	if (a < ABox->YArea)
	{
	  /* Second Y Side */
	  YRange = 0.0;
	  YOffset = ABox->B.y;
	}
	else
	{
	  a -= ABox->YArea;
	  if (a < ABox->ZArea)
	  {
	    /* First Z Side */
	    ZRange = 0.0;
	  }
	  else
	  {
	    /* Second Z Side, No other options left! */
	    ZRange = 0.0;
	    ZOffset = ABox->B.z;
	  }
	}
      }
    }
  }
  Random->x = XOffset + (XRange * rand() / RAND_MAX);
  Random->y = YOffset + (YRange * rand() / RAND_MAX);
  Random->z = ZOffset + (ZRange * rand() / RAND_MAX);
  return (Random);
}
