include colours.inc

camera
  look-at (0.0 0.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (0.0 0.0 -10)
  depth 0.022
  x-size 0.032
  y-size 0.024
//  filter standard
  filter adaptive quick
  ;
;

image
  xsize 1024
  ysize 768
;

light
  colour (1.0 1.0 1.0)
  position (20.0 100 -100)
;

object sphere centre (-3 3 0) radius 0.5 ; material colour aliceblue ; ;
object sphere centre (-2 3 0) radius 0.5 ; material colour antiquewhite ; ;
object sphere centre (-1 3 0) radius 0.5 ; material colour aqua ; ;
object sphere centre (0 3 0) radius 0.5 ; material colour aquamarine ; ;
object sphere centre (1 3 0) radius 0.5 ; material colour azure ; ;
object sphere centre (2 3 0) radius 0.5 ; material colour beige ; ;
object sphere centre (3 3 0) radius 0.5 ; material colour bisque ; ;
object sphere centre (-3 2 0) radius 0.5 ; material colour black ; ;
object sphere centre (-2 2 0) radius 0.5 ; material colour blanchedalmond ; ;
object sphere centre (-1 2 0) radius 0.5 ; material colour blue ; ;
object sphere centre (0 2 0) radius 0.5 ; material colour blueviolet ; ;
object sphere centre (1 2 0) radius 0.5 ; material colour brown ; ;
object sphere centre (2 2 0) radius 0.5 ; material colour burlywood ; ;
object sphere centre (3 2 0) radius 0.5 ; material colour cadetblue ; ;
object sphere centre (-3 1 0) radius 0.5 ; material colour chartreuse ; ;
object sphere centre (-2 1 0) radius 0.5 ; material colour chocolate ; ;
object sphere centre (-1 1 0) radius 0.5 ; material colour coral ; ;
object sphere centre (0 1 0) radius 0.5 ; material colour cornflowerblue ; ;
object sphere centre (1 1 0) radius 0.5 ; material colour cornsilk ; ;
object sphere centre (2 1 0) radius 0.5 ; material colour crimson ; ;
object sphere centre (3 1 0) radius 0.5 ; material colour cyan ; ;
object sphere centre (-3 0 0) radius 0.5 ; material colour darkblue ; ;
object sphere centre (-2 0 0) radius 0.5 ; material colour darkcyan ; ;
object sphere centre (-1 0 0) radius 0.5 ; material colour darkgoldenrod ; ;
object sphere centre (0 0 0) radius 0.5 ; material colour darkgray ; ;
object sphere centre (1 0 0) radius 0.5 ; material colour darkgreen ; ;
object sphere centre (2 0 0) radius 0.5 ; material colour darkkhaki ; ;
object sphere centre (3 0 0) radius 0.5 ; material colour darkmagenta ; ;
object sphere centre (-3 -1 0) radius 0.5 ; material colour darkolivegreen ; ;
object sphere centre (-2 -1 0) radius 0.5 ; material colour darkorange ; ;
object sphere centre (-1 -1 0) radius 0.5 ; material colour blanchedalmond ; ;
object sphere centre (0 -1 0) radius 0.5 ; material colour darkorchid ; ;
object sphere centre (1 -1 0) radius 0.5 ; material colour darkred ; ;
object sphere centre (2 -1 0) radius 0.5 ; material colour darksalmon ; ;
object sphere centre (3 -1 0) radius 0.5 ; material colour darkseagreen ; ;
object sphere centre (-3 -2 0) radius 0.5 ; material colour darkslateblue ; ;
object sphere centre (-2 -2 0) radius 0.5 ; material colour darkslategray ; ;
object sphere centre (-1 -2 0) radius 0.5 ; material colour darkturquoise ; ;
object sphere centre (0 -2 0) radius 0.5 ; material colour darkviolet ; ;
object sphere centre (1 -2 0) radius 0.5 ; material colour deeppink ; ;
object sphere centre (2 -2 0) radius 0.5 ; material colour deepskyblue ; ;
object sphere centre (3 -2 0) radius 0.5 ; material colour dimgray ; ;
object sphere centre (-3 -3 0) radius 0.5 ; material colour dodgerblue ; ;
object sphere centre (-2 -3 0) radius 0.5 ; material colour firebrick ; ;
object sphere centre (-1 -3 0) radius 0.5 ; material colour floralwhite ; ;
object sphere centre (0 -3 0) radius 0.5 ; material colour forestgreen ; ;
object sphere centre (1 -3 0) radius 0.5 ; material colour fuchsia ; ;
object sphere centre (2 -3 0) radius 0.5 ; material colour gainsboro ; ;
object sphere centre (3 -3 0) radius 0.5 ; material colour ghostwhite ; ;

