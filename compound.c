/*
  *************************************************************************
 **                                                                       **
 **			      CSG Section				  **
 **                                                                       **
  *************************************************************************
*/

#include "rend_8.h"

char     *CompName();
Compound *CompRead();
void 	 CompPrint();
ItemEntry *CompIntersect(ray *Ray, Object *AnObj);
Vec     *CompNormal();
Vec     *CompInverse();
Box     *CompBound();
int	 CompInOut();
Vec     *CompCentre();
Vec     *CompRandom();

void InitComp(Prim *APrim)
{
  APrim->Name	   = CompName;
  APrim->Read	   = (void *)CompRead;
  APrim->Print	   = CompPrint;
  APrim->Intersect = CompIntersect;
  APrim->Bound	   = CompBound;
  APrim->InOut	   = CompInOut;
}

char *CompName()
{
  printf("In Compound name\n");
  return "COMPOUND";
}

Compound *CompRead(FILE *inputfile)
{
  Compound *Comp;
  char var[20];
  Comp = (Compound *)AllocHeap(sizeof(Compound));

  printf("In CompRead\n");
  Comp->objList = CreateList();
  Comp->Model = NULL;
  Comp->Inverse = NULL;

  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "OBJECT") == ZERO)
    {
      AddToList((void *)ReadObjectInfo(inputfile), Comp->objList);
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Comp\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);

  return(Comp);
}

void CompPrint(Compound *Comp)
{
}

ItemEntry *CompIntersect(ray *Ray, Object *AnObj)
{
  ItemEntry *List, *A1, *A2;
  Compound *Comp;

  Comp = (Compound *)AnObj->kdata;

  A1 = NULL;

  for (List = Comp->objList->Next; List->Data != NULL; List = List->Next)
  {
    Object *Obj = List->Data;

    if (A1 == NULL)
    {
      A1 = IntersectObject(Ray, Obj, 1, 99999999.999);
    }
    else
    {
      A2 = IntersectObject(Ray, Obj, 1, 99999999.999);
      A1 = IntMerge(A1, A2, UNION);
    }
  }
  
  return (A1);
}

Box *CompBound(Compound *Comp, Box *Bound)
{
  Box BoundA, BoundB;
  ItemEntry *List;
  int Done1 = 0;
  
 
  for (List = Comp->objList->Next; List->Data != NULL; List = List->Next)
  {
    Object *Obj;

    Obj = List->Data;

    if (Done1 == 0)
    {
      BoundObject(Obj, &BoundA);
      Done1 = 1;
    }
    else
    {
      BoundObject(Obj, &BoundB);

      BoundA.A.x = (BoundA.A.x < BoundB.A.x) ? BoundA.A.x : BoundB.A.x;
      BoundA.A.y = (BoundA.A.y < BoundB.A.y) ? BoundA.A.y : BoundB.A.y;
      BoundA.A.z = (BoundA.A.z < BoundB.A.z) ? BoundA.A.z : BoundB.A.z;
      BoundA.B.x = (BoundA.B.x < BoundB.B.x) ? BoundB.B.x : BoundA.B.x;
      BoundA.B.y = (BoundA.B.y < BoundB.B.y) ? BoundB.B.y : BoundA.B.y;
      BoundA.B.z = (BoundA.B.z < BoundB.B.z) ? BoundB.B.z : BoundA.B.z;
    }
  }

  *Bound = BoundA;

  return(Bound);
}

int CompInOut(Compound *Comp, Vec *A)
{
    return 0; /* outside sphere */
}
