include materials.inc

camera
  look-at (0.0 4.1 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (6.0 4.2 -20)
  depth 0.030
  x-size 0.032
  y-size 0.024
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 1024
  ysize 768
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
  shape sphere centre (-30 100 -50) radius 10 ;
  samples 1000
  min-samples 10
;


object
   plane
      position (0 0 0)
      normal (0 1 0)
   ;
;

define
   material named leaf-stuff
      colour green
   ;
   material named branch-stuff
      colour brown
   ;
;
include tree.inc

object tree at (-4 0 0) ; ;
object tree y-rotate -60 at (4 0 10) ; ;
object tree y-rotate 30 at (0 0 16) ; ;
object tree y-rotate 30 at (-10 0 20) ; ;
object tree y-rotate 30 at (6 0 -3) ; ;
