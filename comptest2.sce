include materials.inc

camera
  look-at (0.0 0.1 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (6.0 4.2 -20)
  depth 0.300
  width 0.032
  height 0.024
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 1024
  ysize 768
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;


object
   plane
      position (0 0 0)
      normal (0 1 0)
   ;
;

define
   material named leaf-stuff
      colour green
   ;
   material named branch-stuff
      colour brown
   ;
;

object
   translate (0 0.72 0)
   compound
      object
         box a (-0.12 -0.18 -0.06)
             b ( 0.12  0.18  0.06)
         ;
      ;
      object
         translate (0 0.18 0)
         box a (-0.06 0.0  -0.05)
             b ( 0.06 0.18  0.05)
         ;
      ;
      object
         translate (-0.16 0.16 0)
         box a (-0.02 -0.30 -0.03) b (0.02 0.02 0.03) ;
      ;
      object
         translate (0.16 0.16 0)
         x-rotate -90
         box a (-0.02 -0.30 -0.03) b (0.02 0.02 0.03) ;
      ;
      object
         translate (0 -0.18 0)
         compound
            object
               box a (-0.12 -0.12 -0.06)
                   b ( 0.12  0.0  0.08)
               ;
            ;
            object
               translate (-0.07 -0.06 0)
               compound
                  object
                     box a (-0.05 -0.44 -0.05)
                         b ( 0.05  0.0   0.05) ;
                  ; 
               ;
            ;
            object
               translate (0.07 -0.06 0)
               x-rotate -90
               compound
                  object
                     box a (-0.05 -0.44 -0.05)
                         b ( 0.05  0.0   0.05) ;
                  ; 
               ;
            ;
         ;
      ;
   ;
;
