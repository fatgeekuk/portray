/*
  *************************************************************************
 **                                                                       **
 **			    Cylinder Section				  **
 **                                                                       **
  *************************************************************************
*/

char   *CylName();
Cyl    *CylRead();
void    CylPrint();
ItemEntry *CylIntersect(ray *Ray, Object *AnObj);
Vec    *CylNormal();
Vec    *CylInverse();
Box    *CylBound();
int     CylInOut();
Vec    *CylCentre();
Vec    *CylRandom();

void InitCylinder(Prim *APrim)
{
  APrim->Name	   = CylName;
  APrim->Read	   = (void *)CylRead;
  APrim->Print	   = CylPrint;
  APrim->Intersect = CylIntersect;
  APrim->Normal    = CylNormal;
  APrim->Inverse   = CylInverse;
  APrim->Bound	   = CylBound;
  APrim->InOut	   = CylInOut;
  APrim->Centre	   = CylCentre;
  APrim->Random    = CylRandom;
}

char *CylName()
{
  return "CYLINDER";
}

Cyl *CylRead(FILE *inputfile)
{
  Vec From, To, YAxis, XAxis;
  Cyl *Cylinder;
  char var[20];
  Vec Sub1;
  Flt Rad1=1.0, Rad2=1.0;
  Cylinder = (Cyl *)AllocHeap(sizeof(Cyl));
  Cylinder->Radius = 1.0;
  SetVector(0.0, 0.0, 0.0, &From);
  SetVector(0.0, 1.0, 0.0, &To);
  MIdentity(&(Cylinder->Model));
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "FROM") == ZERO)
    {
      ReadVec(inputfile,&From);
    }
    else if (strcmp(var, "CAPPED") == ZERO)
    {
    }
    else if (strcmp(var, "RADIUS") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(Cylinder->Radius));
      Rad1 = Cylinder->Radius;
      Rad2 = Cylinder->Radius;
    }
    else if (strcmp(var, "RADIUS1") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Rad1);
    }
    else if (strcmp(var, "RADIUS2") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Rad2);
    }
    else if (strcmp(var, "TO") == ZERO)
    {
      ReadVec(inputfile, &To);
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Cylinder\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);


  /* Now Fill up the Required Variables in the Structure */
  VecSub(&To, &From, &Sub1);
  VecUnit(&Sub1, &YAxis);
  Cylinder->Length = VecLen(&Sub1);
  Cylinder->c = Rad1;
  Cylinder->m = (Rad2 - Rad1) / Cylinder->Length;
  Cylinder->sint = -sin(atan(Cylinder->m));
  Cylinder->cost = cos(atan(Cylinder->m));
  Cylinder->Rad1Sq = Rad1 * Rad1;
  Cylinder->Rad2Sq = Rad2 * Rad2;
  if (Rad1 > Rad2)
     Cylinder->Radius = Rad1;
  else
     Cylinder->Radius = Rad2;

  SetVector(0.0, 0.0, 1.0, &Sub1);
  if (fabs(VecDot(&YAxis, &Sub1)) > 0.8)
  {
    SetVector(1.0, 0.0, 0.0, &Sub1);
  }

  VecUnit(VecProduct(&YAxis, &Sub1, &XAxis), &XAxis);
  MAxis(&XAxis, &YAxis, &From, &(Cylinder->Model), &(Cylinder->Inverse));
  if (DEBUG == ON)
  {
    CylPrint(Cylinder);
  }
  return(Cylinder);
}

void CylPrint(Cyl *Cylinder)
{
  printf("Cylinder with length " FltFmt ", radius " FltFmt "\n",
   Cylinder->Length,
   Cylinder->Radius);
}

ItemEntry *CylIntersect(ray *Ray, Object *AnObj)
{
  ItemEntry *Ans, *Ans1 = NULL;
  IntRec *A1, *A2, *A3, *A4;
  Cyl *Cylinder;
  Vec A, B, Hit;
  Flt a, b, c1, d, Ay, By, i;

  Ans = CreateList();
  A1 = NULL;

  Cylinder = (Cyl *)AnObj->kdata;
  /* Rotate Ray Direction into Model Space */
  MProjectVector(&(Ray->Origin),    &(Cylinder->Model), &A);
  MProjectNormal(&(Ray->Direction), &(Cylinder->Model), &B);
  Ay = A.y;
  By = B.y;
  A.y = 0.0;
  B.y = 0.0;
  a  = 2.0 * (VecDot(&B, &B) - Cylinder->m * Cylinder->m * By * By);
  b  = 2.0 * (VecDot(&A, &B) - (Cylinder->m * Cylinder->m * Ay * By + Cylinder->m * Cylinder->c * By));
  c1 = VecDot(&A, &A) - (2.0 * Cylinder->m * Cylinder->c * Ay + Cylinder->m * Cylinder->m * Ay * Ay + Cylinder->c * Cylinder->c);
  d  = b * b - 2.0 * a * c1;
  A.y = Ay;
  B.y = By;
  if (d > 0.0)
  {
    d = sqrt(d);

    /* entry Point Calculations */

    if (a == 0.0)
    {
      printf("Cyl - Warning, a == 0.0\n");
      exit(1);
    }
    i = (-b - d) / a;

    VecAdds(i, &B, &A, &Hit);

    if (Hit.y > Cylinder->Length || Hit.y < 0.0)
    {
      A1 = NULL;
    }
    else
    {
      A1 = AllocInt();

      VecAdds(i, &(Ray->Direction), &(Ray->Origin), &(A1->ModelHit));
      A1->Dist = i;
      A1->Normal = Hit;
      A1->Normal.y = 0.0;

      VecUnit(&(A1->Normal), &(A1->Normal));
      A1->Normal.x = Cylinder->cost * A1->Normal.x;
      A1->Normal.z = Cylinder->cost * A1->Normal.z;
      A1->Normal.y = Cylinder->sint;
    }

    i = (-b + d) / a;

    VecAdds(i, &B, &A, &Hit);

    if (Hit.y > Cylinder->Length || Hit.y < 0.0)
    {
      A2 = NULL;
    }
    else
    {
      A2 = AllocInt();

      VecAdds(i, &(Ray->Direction), &(Ray->Origin), &(A2->ModelHit));
      A2->Dist = i;
      A2->Normal = Hit;
      A2->Normal.y = 0.0;

      VecUnit(&(A2->Normal), &(A2->Normal));
      A2->Normal.x = Cylinder->cost * A2->Normal.x;
      A2->Normal.z = Cylinder->cost * A2->Normal.z;
      A2->Normal.y = Cylinder->sint;
    }

    if (fabs(By) > EFFECTIVE_ZERO)
    {
      i = Ay / (-By);
    }
    else
    {
      i = 10000000.0;
    }

    VecAdds(i, &B, &A, &Hit);

    if ((Hit.x * Hit.x + Hit.z * Hit.z) >= Cylinder->Rad1Sq)
    {
      A3 = NULL;
    }
    else
    {
      A3 = AllocInt();
      VecAdds(i, &(Ray->Direction), &(Ray->Origin), &(A3->ModelHit));
      A3->Dist	  = i;
      SetVector(0.0, -1.0, 0.0, &(A3->Normal));

    }
    if (fabs(By) > EFFECTIVE_ZERO)
    {
      i = (Ay - Cylinder->Length) / (-By);
    }
    else
    {
      i = 10000000.0;
    }

    VecAdds(i, &B, &A, &Hit);

    if ((Hit.x * Hit.x + Hit.z * Hit.z) >= Cylinder->Rad2Sq)
    {
      A4 = NULL;
    }
    else
    {
      A4 = AllocInt();
      VecAdds(i, &(Ray->Direction), &(Ray->Origin), &(A4->ModelHit));
      A4->Dist	  = i;
      SetVector(0.0, 1.0, 0.0, &(A4->Normal));

    }
    /* This is HORRIBLE!!!!!!!!!!!!!!!!!!!!!!! */
    if (A1 != NULL || A2 != NULL || A3 != NULL || A4 != NULL)
    {
      if (A1 == NULL)
      {
        if (A3 != NULL)
        {
          A1 = A3;
          A3 = NULL;
        }
        else if (A4 != NULL)
        {
          A1 = A4;
          A4 = NULL;
        }
      }

      if (A2 == NULL)
      {
        if (A3 != NULL)
        {
          A2 = A3;
          A3 = NULL;
        }
        else if (A4 != NULL)
        {
          A2 = A4;
          A4 = NULL;
        }
      }


      if (A1 == NULL || A2 == NULL)
      {
        if (A1 != NULL) FreeInt(A1);
        if (A2 != NULL) FreeInt(A2);
        A1 = NULL;
        A2 = NULL;
      }
      else
      {
        if (A2->Dist < A1->Dist)
        {
          A3 = A2;
          A2 = A1;
          A1 = A3;
        }
        if (VecDot(&(A1->Normal), &B) < 0.0)
        {
          A1->Dirn = ENTRY;
        }
        else
        {
          A1->Dirn = EXIT;
        }

        if (VecDot(&(A2->Normal), &B) < 0.0)
        {
          A2->Dirn = ENTRY;
        }
        else
        {
          A2->Dirn = EXIT;
        }


        MInvertNormal(&(A1->Normal), &(Cylinder->Model), &(A1->Normal));
        MInvertNormal(&(A2->Normal), &(Cylinder->Model), &(A2->Normal));
        A1->HitObj = AnObj;
        A1->WorldHit = A1->ModelHit;
        A2->HitObj = AnObj;
        A2->WorldHit = A2->ModelHit;

        Ans1 = LinkInToList(A1, Ans, A1->Parent);
        LinkInToList(A2, Ans1, A2->Parent);
      }
    }
  }
  return (Ans);
}

Vec *CylNormal(IntRec *A, Cyl *Cylinder)
{
  return (&(A->Normal));
}

Vec *CylInverse(IntRec *B, Cyl *Cylinder, Flt *u, Flt *v, Vec *Ans)
{
  Vec mA;
  Flt lA, tY;
  Vec *A;

  A = &(B->ModelHit);

  MProjectVector(A, &(Cylinder->Model), &mA);
  *v = mA.y;
  tY = mA.y;
  *v += Cylinder->Radius;
  mA.y = 0;
  lA = VecLen(&mA);
  if (lA > 0.0)
  {
    VecUnit(&mA, &mA);
    *u = acos(mA.z);
    if (mA.x > 0.0)
       *u = -(*u);
    *u += 1.0;
    *u *= Cylinder->Radius;
  }
  else
    *u = 0.0;
  if (tY < EFFECTIVE_ZERO)
  {
    /* We are on the bottom cap */
    *v = lA;
  }
  if (Cylinder->Length - tY < EFFECTIVE_ZERO)
  {
    /* We are on the top cap */
    *v += (Cylinder->Radius - lA);
  }

  return(Ans);
}

void swap(Flt *A, Flt *B)
{
  Flt t;
  t = *A;
  *A = *B;
  *B = t;
}

Box *CylBound(Cyl *Cylinder, Box *Bound)
{
  Vec B1, B2, B3;

  B2.x = 999999999.9;
  B2.y = B2.x;
  B2.z = B2.y;
  B3.x = -999999999.9;
  B3.y = B3.x;
  B3.z = B3.y;

  /* I KNOW, this IS brute force, and it IS clumsy, but to force this into
     a LOOP would have slowed it down, taken a LOT longer to code, Made it
     LESS easy to see what is going on, and MORE difficult to maintain
     So if you want to recode as a loop, feel free, and good riddence,
     I have a LOT better things to do that polishing a simple bit of code
     like this - I have the JOY of writing the manual for this sucker
     Best Regards PM '93 */


  B1.x = - Cylinder->Radius;
  B1.y = 0.0;
  B1.z = - Cylinder->Radius;

  MInvertVector(&B1, &(Cylinder->Inverse), &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  B1.x = Cylinder->Radius;
  B1.y = 0.0;
  B1.z = - Cylinder->Radius;

  MInvertVector(&B1, &(Cylinder->Inverse), &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  B1.x = - Cylinder->Radius;
  B1.y = 0.0;
  B1.z = Cylinder->Radius;

  MInvertVector(&B1, &(Cylinder->Inverse), &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  B1.x = Cylinder->Radius;
  B1.y = 0.0;
  B1.z = Cylinder->Radius;

  MInvertVector(&B1, &(Cylinder->Inverse), &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  B1.x = - Cylinder->Radius;
  B1.y =   Cylinder->Length;
  B1.z = - Cylinder->Radius;

  MInvertVector(&B1, &(Cylinder->Inverse), &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  B1.x =   Cylinder->Radius;
  B1.y =   Cylinder->Length;
  B1.z = - Cylinder->Radius;

  MInvertVector(&B1, &(Cylinder->Inverse), &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  B1.x = - Cylinder->Radius;
  B1.y =   Cylinder->Length;
  B1.z =   Cylinder->Radius;

  MInvertVector(&B1, &(Cylinder->Inverse), &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  B1.x = Cylinder->Radius;
  B1.y = Cylinder->Length;
  B1.z = Cylinder->Radius;

  MInvertVector(&B1, &(Cylinder->Inverse), &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  /* Now pack the answer back into the output bounding box and get going */
  Bound->A.x = B2.x;
  Bound->A.y = B2.y;
  Bound->A.z = B2.z;
  Bound->B.x = B3.x;
  Bound->B.y = B3.y;
  Bound->B.z = B3.z;
  return(Bound);
}

int CylInOut(Cyl *Cylinder, Vec *A)
{
    return 0; /* outside cylinder - This NEEDS changing but it can wait
		 until the CSG code goes in at which time it will no longer
		 be needed */
}

Vec *CylCentre(Cyl *Cylinder, Vec *Cen)
{
  Cen->x = 0.0;
  Cen->y = Cylinder->Length / 2.0;
  Cen->z = 0.0;
  MInvertVector(Cen, &(Cylinder->Inverse), Cen);
  return(Cen);
}

Vec *CylRandom(Cyl *Cylinder, Vec *Random)
{
    Flt u, v;
    u = rand();
    u /= RAND_MAX;
    u *= (2.0 * PI);
    v = rand();
    v /= RAND_MAX;
    Random->x = Cylinder->Radius * sin(u);
    Random->y = Cylinder->Length * v;
    Random->z = Cylinder->Radius * cos(u);
    MInvertVector(Random, &(Cylinder->Inverse), Random);
    return (Random);
}
