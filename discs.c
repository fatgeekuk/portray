/*
  *************************************************************************
 **                                                                       **
 **			      Disc Section				  **
 **                                                                       **
  *************************************************************************
*/

char      *DscName();
Dsc       *DscRead();
void       DscPrint();
ItemEntry *DscIntersect(ray *Ray, Object *AnObj);
Vec       *DscNormal();
Vec       *DscInverse();
Box       *DscPlaBound();
int        DscInOut();
Vec       *DscCentre();
Box       *DscBound(Dsc *Disk, Box *Bound);

void InitDisk(Prim *APrim)
{
  APrim->Name	   = DscName;
  APrim->Read	   = (void *)DscRead;
  APrim->Print	   = DscPrint;
  APrim->Intersect = DscIntersect;
  APrim->Normal    = DscNormal;
  APrim->Inverse   = DscInverse;
  APrim->Bound	   = DscBound;
  APrim->InOut	   = DscInOut;
  APrim->Centre    = DscCentre;
}

char *DscName()
{
  return "DISK";
}

Dsc *DscRead(FILE *inputfile)
{
  Dsc *Disk;
  Vec Sub1;
  char var[20];
  Disk = (Dsc *)AllocHeap(sizeof(Dsc));
  SetVector(0.0, 0.0, 0.0, &(Disk->Position));
  SetVector(0.0, 1.0, 0.0, &(Disk->Normal));
  SetVector(1.0, 0.0, 0.0, &(Disk->XAxis));
  SetVector(0.0, 0.0, 1.0, &(Disk->YAxis));
  Disk->Radius = 1.0;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "POSITION") == ZERO)
    {
      ReadVec(inputfile, &(Disk->Position));
    }
    else if (strcmp(var, "NORMAL") == ZERO)
    {
      ReadVec(inputfile, &(Disk->Normal));
    }
    else if (strcmp(var, "X-AXIS") == ZERO)
    {
      VecUnit(ReadVec(inputfile, &Sub1), &(Disk->XAxis));
    }
    else if (strcmp(var, "Y-AXIS") == ZERO)
    {
       VecUnit(ReadVec(inputfile, &Sub1), &(Disk->YAxis));
    }
    else if (strcmp(var, "RADIUS") == ZERO)
    {
       fscanf(inputfile, " " FltFmt " ", &(Disk->Radius));
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Plane\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);
  VecUnit(&(Disk->Normal),&(Disk->Normal));
  if (DEBUG == ON)
  {
    DscPrint(Disk);
  }
  return(Disk);
}

void DscPrint(Dsc *Disk)
{
  printf("Disk with position at (" FltFmt ", " FltFmt ", " FltFmt ") and normal of (" FltFmt ", " FltFmt ", " FltFmt ") and radius of " FltFmt "\n",
   Disk->Position.x,
   Disk->Position.y,
   Disk->Position.z,
   Disk->Normal.x,
   Disk->Normal.y,
   Disk->Normal.z);
}

ItemEntry *DscIntersect(ray *Ray, Object *AnObj)
{
  ItemEntry *Ans=CreateList();
  IntRec *AnInt;
  Dsc *Disk;
  Flt t1;
  Vec Normal, Sub1;

  Disk = (Dsc *)AnObj->kdata;
  Normal = Disk->Normal;
  t1 = -VecDot(&(Disk->Normal), &(Ray->Direction));
  if (fabs(t1) > EFFECTIVE_ZERO)
  {
    AnInt = AllocInt();

    if (t1 < EFFECTIVE_ZERO)
    {
      AnInt->Dirn = EXIT;
    }
    else
    {
      AnInt->Dirn = ENTRY;
    }
    VecSub(&(Ray->Origin), &(Disk->Position),&Sub1);
    AnInt->Dist   = VecDot(&(Disk->Normal), &Sub1) / t1;
    VecAdds(AnInt->Dist, &(Ray->Direction), &(Ray->Origin), &(AnInt->ModelHit));
    AnInt->HitObj   = AnObj;
    AnInt->WorldHit = AnInt->ModelHit;
    VecSub(&(AnInt->ModelHit), &(Disk->Position), &Sub1)
    if (VecLen(&Sub1) > Disk->Radius)
    {
      FreeInt(AnInt);
    }
    else
    {
      LinkInToList(AnInt, Ans, AnInt->Parent);
    }

  }
  return(Ans);
}

Vec *DscNormal(IntRec *A, Dsc *Disk)
{
  A->Normal = Disk->Normal;
  return (&(A->Normal));
}

Vec *DscInverse(IntRec *B, Dsc *Disk, Flt *u, Flt *v, Vec *Ans)
{
  Vec Sub1;
  Vec *A;

  A = &(B->ModelHit);
  VecSub(A, &(Disk->Position),&Sub1);
  *u = VecDot(&(Disk->YAxis), &Sub1);
  *v = VecDot(&(Disk->XAxis), &Sub1);
  return(Ans);
}

Box *DscBound(Dsc *Disk, Box *Bound)
{
  Bound->A.x = -999.99;
  Bound->A.y = -999.99;
  Bound->A.z = -999.99;
  Bound->B.x =  999.99;
  Bound->B.y =  999.99;
  Bound->B.z =  999.99;
  return(Bound);
}

int DscInOut(Dsc *Disk, Vec *A)
{
  Vec Sub1;
  VecSub(A, &(Disk->Position),&Sub1);
  if (VecDot(&(Disk->Normal), &Sub1) > 0.0)
  {
    return 0;
  }
  else
  {
    return -1;
  }
}

Vec *DscCentre(Dsc *Disk, Vec *Cen)
{
  Cen->x = Disk->Position.x;
  Cen->y = Disk->Position.y;
  Cen->z = Disk->Position.z;
  return(Cen);
}
