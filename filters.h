typedef struct UseFilt
{
  struct UseFilt *NextUsed;
  Filter *ThisFilter;
  void	 *fdata;
} UseFilt;

typedef struct SuperFiltVars
{
  int XSamples, YSamples;
} SuperFiltVars;

typedef struct DiskFiltVars
{
  int NoOfSamples;
  Flt Radius;
} DiskFiltVars;

typedef struct LensFiltVars
{
  int NoOfSamples;
  Flt Radius;
  Flt FocalLength;
} LensFiltVars;

typedef struct AdapCacheItem
{
  ItemEntry *Used;
  ItemEntry *Hash;
  long int FPx, FPy;
  colour Answer;
  Flt Area;
} AdapCacheItem;


typedef struct AdapFiltVars
{
  int MaxDepth, CacheSize;
  Flt Threshold, Relax, SubThresh;
  int SubSam;
  ItemEntry *UsedList;

  ItemEntry *HashTable[256];
  /*
  AdapCacheItem *(RayCache);
  AdapCacheItem *HeadPtr, *TailPtr;
  */
} AdapFiltVars;
