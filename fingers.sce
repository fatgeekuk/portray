
camera
  look-at (0.0 2.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (-20.0 3.2 -30)
  depth 0.080
  y-size 0.0320
  x-size 0.0240
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 480
  ysize 640
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;

object 
   implicit
      epsilon 0.05
      crosspoint 0.5
      bound
         A (-6 -6 -5)
         B (6 8 5)
      ;
      tube from (-1.84 0 0) to (3.68 0 0) height 1.0 zradius 1.0 ;
      tube from (-1.84 0 0) to (0 -4.32 0) height 1.0 zradius 1.0 ;
      tube from (0 0 0) to (0 -4.32 0) height 1.0 zradius 1.0 ;
      tube from (1.84 0 0) to (0 -4.32 0) height 1.0 zradius 1.0 ;
      tube from (3.68 0 0) to (0 -4.32 0) height 1.0 zradius 1.0 ;

      sum
         translate (0 -4.32 0)
         rotate-z -45
         rotate-y 60
         sum
            rotate-x 9
            polar centre (0 0 0) zradius 1.0 height -1.0 ;
            tube from (0 0 0) to (0 3.11 0) height 1.0 zradius 1.0 ;
            sum
               rotate-x 9
               translate (0 3.11 0)
               polar centre (0 0 0) zradius 1.0 height -1.0 ;
               tube from (0 0 0) to (0 2.59 0) height 1.0 zradius 1.0 ;
               sum
                  rotate-x 9
                  translate (0 2.59 0)
                  polar centre (0 0 0) zradius 1.0 height -1.0 ;
                  tube from (0 0 0) to (0 1.75 0) height 1.0 zradius 1.0 ;
               ;
            ;
         ;
      ;

      sum
         rotate-x 9
         translate (-1.84 0 0)       
         polar centre (0 0 0) zradius 1.0 height -1.0 ;
         tube from (0 0 0) to (0 3.14 0) height 1.0 zradius 1.0 ;
         sum
            rotate-x 9
            translate (0 3.14 0)
            polar centre (0 0 0) zradius 1.0 height -1.0 ;
            tube from (0 0 0) to (0 1.75 0) height 1.0 zradius 1.0 ;
            sum
               rotate-x 9
               translate (0 1.75 0)
               polar centre (0 0 0) zradius 1.0 height -1.0 ;
               tube from (0 0 0) to (0 1.41 0) height 1.0 zradius 1.0 ;
            ;
         ;
      ;

      sum
         rotate-x 11
         polar centre (0 0 0) zradius 1.0 height -1.0 ;
         tube from (0 0 0) to (0 3.74 0) height 1.0 zradius 1.0 ;
         sum
            rotate-x 11
            translate (0 3.74 0)
            polar centre (0 0 0) zradius 1.0 height -1.0 ;
            tube from (0 0 0) to (0 2.17 0) height 1.0 zradius 1.0 ;
            sum
               rotate-x 11
               translate (0 2.17 0)
               polar centre (0 0 0) zradius 1.0 height -1.0 ;
               tube from (0 0 0) to (0 1.27 0) height 1.0 zradius 1.0 ;
            ;
         ;
      ;

      sum
         rotate-x 13
         translate (1.84 0 0)       
         polar centre (0 0 0) zradius 1.0 height -1.0 ;
         tube from (0 0 0) to (0 3.34 0) height 1.0 zradius 1.0 ;
         sum
            rotate-x 13
            translate (0 3.34 0)
            polar centre (0 0 0) zradius 1.0 height -1.0 ;
            tube from (0 0 0) to (0 2.05 0) height 1.0 zradius 1.0 ;
            sum
               rotate-x 13
               translate (0 2.05 0)
               polar centre (0 0 0) zradius 1.0 height -1.0 ;
               tube from (0 0 0) to (0 1.27 0) height 1.0 zradius 1.0 ;
            ;
         ;
      ;

      sum
         rotate-x 15
         translate (3.68 0 0)       
         polar centre (0 0 0) zradius 1.0 height -1.0 ;
         tube from (0 0 0) to (0 2.44 0) height 1.0 zradius 1.0 ;
         sum
            rotate-x 15
            translate (0 2.44 0)
            polar centre (0 0 0) zradius 1.0 height -1.0 ;
            tube from (0 0 0) to (0 1.40 0) height 1.0 zradius 1.0 ;
            sum
               rotate-x 15
               translate (0 1.40 0)
               polar centre (0 0 0) zradius 1.0 height -1.0 ;
               tube from (0 0 0) to (0 1.22 0) height 1.0 zradius 1.0 ;
            ;
         ;
      ;
   ;
;
