
camera
  look-at (0.0 1.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (0.0 1.2 -5)
  depth 0.022
  y-size 0.024
  x-size 0.032
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 640
  ysize 480
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;
object 
   implicit
      epsilon 0.005
      crosspoint 0.5
      bound
         A (-2 3 -2)
         B (2 -1 2)
      ;
      combi
         planar
            origin (0 2 0)
            direction (-1 1 -1)
            width 0.4
            height 1.0
         ;
         tube
            height 5.0
            from (0 0 0)
            to   (0 2 0)
            zradius 1.0
         ;
         linear
            origin (0 -1 0)
            direction (0 0.5 0)
         ;
      ;
   ;
;
