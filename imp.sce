
camera
  look-at (0.0 0.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (2.0 2.2 -12)
  depth 0.042
  x-size 0.032
  y-size 0.024
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 512
  ysize 384
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;

object 
   implicit
      bound
         a (-2.5 -2.5 -2.5)
         b (2.5 2.5 2.5)
      ;
      epsilon 0.002
      crosspoint 0.1
      sum
        polar power 0.2 centre (0 0 0) zradius 2.0 height 1.0 ;
      	torus power 2.0 amplitude -2.0 centre (0 0 0) axis (0 1 0) size 2.0 diameter 0.1 ;
      	torus power 2.0 amplitude -2.0 centre (0 0 0) axis (1 0 0) size 2.0 diameter 0.1 ;
      	torus power 2.0 amplitude -2.0 centre (0 0 0) axis (0 0 1) size 2.0 diameter 0.1 ;
      ;
   ;
;
