
camera
  look-at (0.0 1.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (0.0 1.2 -5)
  depth 0.022
  y-size 0.024
  x-size 0.032
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 640
  ysize 480
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;
object sphere centre (-4 3 -4) radius 0.1 ; material colour (1 0 0) ; ;
object sphere centre (4 3 4) radius 0.1 ; material colour (1 0 0) ; ;
object sphere centre (4 3 -4) radius 0.1 ; material colour (1 0 0) ; ;
object sphere centre (-4 3 4) radius 0.1 ; material colour (1 0 0) ; ;
object sphere centre (-4 -1 -4) radius 0.1 ; material colour (1 0 0) ; ;
object sphere centre (4 -1 4) radius 0.1 ; material colour (1 0 0) ; ;
object sphere centre (4 -1 -4) radius 0.1 ; material colour (1 0 0) ; ;
object sphere centre (-4 -1 4) radius 0.1 ; material colour (1 0 0) ; ;
object 
   implicit
      epsilon 0.005
      crosspoint 0.2
      bound
         A (-1000 3 -1000)
         B (1000 -1 1000)
      ;
      polar
         centre (-3 -0.5 0)
         zradius 1.4
         height 2.0
      ;
      polar
         centre (-1 0 0)
         zradius 1.4
         height 2.0
      ;
      polar
         centre (1 0.5 0)
         zradius 1.4
         height 2.0
      ;
      polar
         centre (3 1 0)
         zradius 1.4
         height 2.0
      ;
      linear
         origin (0 0 0)
         direction (0 -1 0)
   ;
;
