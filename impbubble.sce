
camera
  look-at (0.0 0.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (2.0 2.2 -12)
  depth 0.042
  x-size 0.032
  y-size 0.024
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 1024
  ysize 768
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;

object 
   implicit
      bound
         a (-2.5 -2.5 -2.5)
         b (2.5 2.5 2.5)
      ;
      epsilon 0.002
      crosspoint 0.4
      sum
        polar centre (0 0 0) zradius 2.0 height 1.0 ;
	noise terms 7 origin 0.0 amp 0.2 freq 1.0 ;
      ;
   ;
   material
   	specular-reflection 0.3
	specular-power 20
	reflection 0.2
	diffuse-reflection 0.7
	spec-colour (1 0 0)
	diff-colour (0 0 1)
   ;
;

object
   plane
      position (0 -2 0) normal (0 1 0)
      x-axis (1 0 0) y-axis (0 0 1)
   ;
   material
      chequers
      	material-a
		colour (1 1 1)
	;
	material-b
		colour (0 0 0)
	;
   ;
;
