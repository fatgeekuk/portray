include materials.inc

camera
  look-at (0.0 0.1 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (0.0 4.2 -12)
  depth 0.030
  x-size 0.032
  y-size 0.024
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 1024
  ysize 768
;

light
  colour (1.0 1.0 1.0)
  position (-10.0 10 5000)
;

define
   object
      name impcube
      object 
         inherit
         implicit
            bound
               a (-3 -3 -3)
               b (3 3 3)
            ;
            epsilon 0.01
            crosspoint 0.3
            combi
	       noise origin 1.0 amp 0.4 freq 2 terms 6 ;
               planar origin (0 -1 0) direction (0 -1 0) height 1.0 width 1.8 ;
               planar origin (0 1 0) direction (0 1 0) height 1.0 width 1.8 ;
               planar origin (-1 0 0) direction (-1 0 0) height 1.0 width 1.8 ;
               planar origin (1 0 0) direction (1 0 0) height 1.0 width 1.8 ;
               planar origin (0 0 -1) direction (0 0 -1) height 1.0 width 1.8 ;
               planar origin (0 0 1) direction (0 0 1) height 1.0 width 1.8 ;
            ;
         ;
      ;
   ;
;

object
   implicit
      bound
         a (-200 -4 -200)
         b (200 4 200)
      ;
      epsilon 0.01
      crosspoint 0.4
      combi
         noise origin 1.0 amp 0.5 freq 0.2 terms 2 ;
         planar origin (0 0 0) direction (0 1 0) height 1.0 width 6.0 ;
      ;
   ;
   material
      specular-reflection 0.7
      diffuse-reflection 0.3
      spec-colour (0.5 0.7 1)
      diff-colour (1 1 1)
   ;
;
