
camera
  look-at (0.0 0.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (-3.0 4.2 -15)
  depth 0.022
  y-size 0.024
  x-size 0.032
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 640
  ysize 480
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;
object 
   implicit
      epsilon 0.005
      crosspoint 0.2
      bound
         A (-10 10 -10)
         B (10 -1 10)
      ;
      combi
         planar height 1.0 position (0 0 -1) direction (0 0 -1) ;
         planar height 1.0 position (0 0 -1) direction (0 -1 0) ;
         planar height 1.0 position (0 0 -1.4) direction (-0.8 0.1 -0.2) ;
         planar height 1.0 position (0 0 -1.4) direction (0.8 0.1 -0.2) ;
      ;
   ;
;
