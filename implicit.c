/*
  *************************************************************************
 **                                                                       **
 **                      Implicit Surface Section                         **
 **                                                                       **
  *************************************************************************
*/


char   *ImpName();
Imp    *ImpRead();
void    ImpPrint();
ItemEntry *ImpIntersect(ray *Ray, Object *AnObj);
Vec    *ImpNormal();
Vec    *ImpInverse();
Box    *ImpBound();
int     ImpInOut();
Vec    *ImpCentre();
Vec    *ImpRandom();

Flt combiFunc(Vec *A, ifCombi *theCombi);
Flt sumFunc(Vec *A, ifCombi *theCombi);

void InitImplicit(Prim *APrim)
{
  APrim->Name      = ImpName;
  APrim->Read	   = (void *)ImpRead;
  APrim->Print     = ImpPrint;
  APrim->Intersect = ImpIntersect;
  APrim->Normal    = ImpNormal;
  APrim->Inverse   = ImpInverse;
  APrim->Bound     = ImpBound;
  APrim->InOut     = ImpInOut;
  APrim->Centre	   = ImpCentre;
  APrim->Random    = ImpRandom;
}

char *ImpName()
{
  return "IMPLICIT";
}
float pnoise3(float vec[3]);

Flt linearFunc(Vec *v)
{
  Flt a;

  return a;
}

void fillLinear(FILE *inputfile, char *, ImpFunc *thisFunc);
void fillPolar(FILE *inputfile, char *, ImpFunc *thisFunc);
void fillNoise(FILE *inputfile, char *, ImpFunc *thisFunc);
void fillTube(FILE *inputfile, char *, ImpFunc *thisFunc);
void fillTori(FILE *inputfile, char *, ImpFunc *thisFunc);
void fillCombi(FILE *inputfile, char *, ImpFunc *thisFunc);
void fillSummation(FILE *inputfile, char *, ImpFunc *thisFunc);
void fillPlanar(FILE *inputfile, char *, ImpFunc *thisFunc);

void *readComponent(FILE *inputfile, void (*fillInComponent)(FILE *, char *, ImpFunc *))
{
  char var[1024];
  ImpFunc *thisFunc = (ImpFunc *)malloc(sizeof(ImpFunc));
  Vec translate;
  Flt xRotate, yRotate, zRotate;
  Flt shrink, xScale, yScale, zScale;
  Matrix MDummy;
  char doMatrix;

  doMatrix = 'n';
  xScale = yScale = zScale = 1.0;
  xRotate = yRotate = zRotate = 0.0;
  SetVector(0.0, 0.0, 0.0, &translate);

  if (thisFunc != NULL)
  {
    thisFunc->transform = NULL;

    /* setup initial values for amplitude and power */
    thisFunc->amplitude = 1.0;
    thisFunc->power     = 1.0;

    /* Fill in subtype information and instantiate subtype storage */
    (fillInComponent)(NULL, NULL, thisFunc);

    /* Now, pick out the information stream from the file */

    do
    {
      fscanf(inputfile, " %s ", var);
      strupr(var);

      if (strcmp(var, "TRANSLATE") == ZERO)
      {
        ReadVec(inputfile, &(translate));
        doMatrix = 'y';
      }
      else if (strcmp(var, "ROTATE-X") == ZERO)
      {
        fscanf(inputfile, " " FltFmt " ", &(xRotate));
        doMatrix = 'y';
      }
      else if (strcmp(var, "ROTATE-Y") == ZERO)
      {
        fscanf(inputfile, " " FltFmt " ", &(yRotate));
        doMatrix = 'y';
      }
      else if (strcmp(var, "ROTATE-Z") == ZERO)
      {
        fscanf(inputfile, " " FltFmt " ", &(zRotate));
        doMatrix = 'y';
      }
      else if (strcmp(var, "SHRINK-X") == ZERO)
      {
        fscanf(inputfile, " " FltFmt " ", &(shrink));
        xScale /= shrink;
        doMatrix = 'y';
      }
      else if (strcmp(var, "SHRINK-Y") == ZERO)
      {
        fscanf(inputfile, " " FltFmt " ", &(shrink));
        yScale /= shrink;
        doMatrix = 'y';
      }
      else if (strcmp(var, "SHRINK-Z") == ZERO)
      {
        fscanf(inputfile, " " FltFmt " ", &(shrink));
        zScale /= shrink;
        doMatrix = 'y';
      }
      else if (strcmp(var, "POWER") == ZERO)
      {
        fscanf(inputfile, " " FltFmt " ", &(thisFunc->power));
      }
      else if (strcmp(var, "AMPLITUDE") == ZERO)
      {
        fscanf(inputfile, " " FltFmt " ", &(thisFunc->amplitude));
      }
      else
      {
        (fillInComponent)(inputfile, var, thisFunc);
      }

    } while (strcmp(var, ";") != ZERO);

    /* Generate Matrix information if required */
    if (doMatrix == 'y')
    {
      if ((thisFunc->transform = (Matrix *)malloc(sizeof(Matrix))) != NULL)
      {
        MIdentity(thisFunc->transform);
        MRotate(xRotate * PI/180.0, yRotate * PI/180.0, zRotate * PI/180.0, thisFunc->transform, &MDummy);
        MScale(xScale, yScale, zScale, thisFunc->transform, thisFunc->transform, &MDummy, &MDummy);
        MTranslate(&translate, thisFunc->transform, thisFunc->transform, &MDummy, &MDummy);
      }
      else
      {
        printf("Unable to allocate matrix information\n");
        exit(123);
      }
    }
  }
  return thisFunc;
}

void fillLinear(FILE *inputfile, char *var, ImpFunc *thisFunc)
{
  ifLinear *aLinear;

  printf("Inside fillLinear\n");

  if (inputfile == NULL)
  {
    thisFunc->funcType = 1;
    aLinear = (ifLinear *) malloc(sizeof(ifLinear));
    if (aLinear != NULL)
    {
      thisFunc->dataPtr = aLinear;
    }
    else
    {
      printf("Unable to allocate Linear, aborting\n");
      exit(20);
    }
  }
  else
  {
    aLinear = (ifLinear *)thisFunc->dataPtr;
    printf("VAR: %s\n", var);
  
    if (strcmp(var, "ORIGIN") == ZERO)
    {
      ReadVec(inputfile, &(aLinear->Origin));
    }
    else if (strcmp(var, "DIRECTION") == ZERO)
    {
      ReadVec(inputfile, &(aLinear->Direction));
    }
  }
}

void fillPlanar(FILE *inputfile, char *var, ImpFunc *thisFunc)
{
  ifPlanar *aPlanar;

  printf("Inside fillPlanar\n");

  if (inputfile == NULL)
  {
    thisFunc->funcType = 7;
    aPlanar = (ifPlanar *) malloc(sizeof(ifPlanar));
    if (aPlanar != NULL)
    {
      thisFunc->dataPtr = aPlanar;
      aPlanar->height = 1.0;
      aPlanar->width = 1.0;
    }
    else
    {
      printf("Unable to allocate Planar, aborting\n");
      exit(20);
    }
  }
  else
  {
    aPlanar = (ifPlanar *)thisFunc->dataPtr;
  
    if (strcmp(var, "ORIGIN") == ZERO)
    {
      ReadVec(inputfile, &(aPlanar->Origin));
    }
    else if (strcmp(var, "DIRECTION") == ZERO)
    {
      ReadVec(inputfile, &(aPlanar->Direction));
      VecUnit(&(aPlanar->Direction), &(aPlanar->Direction));
    }
    else if (strcmp(var, "HEIGHT") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(aPlanar->height));
    }
    else if (strcmp(var, "WIDTH") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(aPlanar->width));
    }
  }
}

void fillPolar(FILE *inputfile, char *var, ImpFunc *thisFunc)
{
  ifPolar *aPolar;

  if (inputfile == NULL)
  {
    thisFunc->funcType = 2;
    aPolar = (ifPolar *) malloc(sizeof(ifPolar));
    if (aPolar != NULL)
    {
      thisFunc->dataPtr = aPolar;
    }
    else
    {
      printf("Unable to allocate Polar, aborting\n");
      exit(20);
    }
  }
  else
  {
    aPolar = (ifPolar *)thisFunc->dataPtr;

    if (strcmp(var, "CENTRE") == ZERO || strcmp(var, "CENTER") == ZERO)
    {
      ReadVec(inputfile, &(aPolar->Centre));
    }
    if (strcmp(var, "HEIGHT") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(aPolar->Height));
    }
    if (strcmp(var, "ZRADIUS") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(aPolar->ZPoint));
    }
  }
}

void fillNoise(FILE *inputfile, char *var, ImpFunc *thisFunc)
{
  ifNoise *aNoise;

  if (inputfile == NULL)
  {
    thisFunc->funcType = 3;
    aNoise = (ifNoise *) malloc(sizeof(ifNoise));
    if (aNoise != NULL)
    {
      thisFunc->dataPtr = aNoise;
      aNoise->origin = 1.0;
      aNoise->amp    = 1.0;
      aNoise->freq   = 1.0;
      aNoise->terms  = 1;
    }
    else
    {
      printf("Unable to allocate Noise, aborting\n");
      exit(20);
    }
  }
  else
  {
    aNoise = (ifNoise *)thisFunc->dataPtr;

    if (strcmp(var, "FREQ") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(aNoise->freq));
    }
    else if (strcmp(var, "AMP") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(aNoise->amp));
    }
    else if (strcmp(var, "TERMS") == ZERO)
    {
      fscanf(inputfile, " %d ", &(aNoise->terms));
    }
    else if (strcmp(var, "ORIGIN") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(aNoise->origin));
    }
  }
}

void fillTube(FILE *inputfile, char *var, ImpFunc *thisFunc)
{
  ifTube *aTube;
  Vec to, t1;

  if (inputfile == NULL)
  {
    thisFunc->funcType = 5;
    aTube = (ifTube *) malloc(sizeof(ifTube));
    if (aTube != NULL)
    {
      thisFunc->dataPtr = aTube;
      aTube->Pow = 1.0;
    }
    else
    {
      printf("Unable to allocate Tube, aborting\n");
      exit(20);
    }
  }
  else
  {
    aTube = (ifTube *)thisFunc->dataPtr;

    if (strcmp(var, "FROM") == ZERO)
    {
      ReadVec(inputfile, &(aTube->From));
    }
    if (strcmp(var, "TO") == ZERO)
    {
      ReadVec(inputfile, &(aTube->To));
    }
    if (strcmp(var, "POWER") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(aTube->Pow));
    }
    if (strcmp(var, "HEIGHT") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(aTube->Height));
    }
    if (strcmp(var, "ZRADIUS") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(aTube->ZPoint));
    }
    if (strcmp(var, ";") == ZERO)
    {
      /* Tail end Processing */
      VecSub(&(aTube->To), &(aTube->From), &t1);
      VecUnit(&t1, &(aTube->Direction));
      aTube->Length = VecLen(&t1);
    }
  }
}

void fillTorus(FILE *inputfile, char *var, ImpFunc *thisFunc)
{
  ifTorus *aTorus;
  Vec to, t1;

  if (inputfile == NULL)
  {
    thisFunc->funcType = 8;
    aTorus = (ifTorus *) malloc(sizeof(ifTorus));
    if (aTorus != NULL)
    {
      thisFunc->dataPtr = aTorus;
    }
    else
    {
      printf("Unable to allocate Torus, aborting\n");
      exit(20);
    }
  }
  else
  {
    aTorus = (ifTorus *)thisFunc->dataPtr;

    if (strcmp(var, "CENTRE") == ZERO)
    {
      ReadVec(inputfile, &(aTorus->Centre));
    }
    if (strcmp(var, "AXIS") == ZERO)
    {
      ReadVec(inputfile, &(aTorus->Axis));
    }
    if (strcmp(var, "DIAMETER") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(aTorus->Diameter));
    }
    if (strcmp(var, "SIZE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(aTorus->Size));
    }
    if (strcmp(var, "MAGNITUDE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(aTorus->Magnitude));
    }
    if (strcmp(var, ";") == ZERO)
    {
      /* Tail end Processing */
    }
  }
}

void fillCombi(FILE *inputfile, char *var, ImpFunc *thisFunc)
{
  ifCombi *aCombi;

  if (inputfile == NULL)
  {
    thisFunc->funcType = 4;
    aCombi = (ifCombi *) malloc(sizeof(ifCombi));
    if (aCombi != NULL)
    {
      thisFunc->dataPtr = aCombi;
      aCombi->funcList = CreateList();
    }
    else
    {
      printf("Unable to allocate Combi, aborting\n");
      exit(20);
    }
  }
  else
  {
    aCombi = (ifCombi *)thisFunc->dataPtr;

    if (strcmp(var, "LINEAR") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillLinear), aCombi->funcList);
    }
    else if (strcmp(var, "POLAR") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillPolar), aCombi->funcList);
    }
    else if (strcmp(var, "TUBE") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillTube), aCombi->funcList);
    }
    else if (strcmp(var, "TORUS") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillTorus), aCombi->funcList);
    }
    else if (strcmp(var, "PLANAR") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillPlanar), aCombi->funcList);
    }
    else if (strcmp(var, "NOISE") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillNoise), aCombi->funcList);
    }
    else if (strcmp(var, "COMBI") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillCombi), aCombi->funcList);
    }
    else if (strcmp(var, "SUM") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillSummation), aCombi->funcList);
    }
  }
}

void fillSummation(FILE *inputfile, char *var, ImpFunc *thisFunc)
{
  ifCombi *aCombi;

  if (inputfile == NULL)
  {
    thisFunc->funcType = 6;
    aCombi = (ifCombi *) malloc(sizeof(ifCombi));
    if (aCombi != NULL)
    {
      thisFunc->dataPtr = aCombi;
      aCombi->funcList = CreateList();
    }
    else
    {
      printf("Unable to allocate Combi, aborting\n");
      exit(20);
    }
  }
  else
  {
    aCombi = (ifCombi *)thisFunc->dataPtr;

    if (strcmp(var, "LINEAR") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillLinear), aCombi->funcList);
    }
    else if (strcmp(var, "POLAR") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillPolar), aCombi->funcList);
    }
    else if (strcmp(var, "TUBE") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillTube), aCombi->funcList);
    }
    else if (strcmp(var, "TORUS") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillTorus), aCombi->funcList);
    }
    else if (strcmp(var, "PLANAR") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillPlanar), aCombi->funcList);
    }
    else if (strcmp(var, "NOISE") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillNoise), aCombi->funcList);
    }
    else if (strcmp(var, "COMBI") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillCombi), aCombi->funcList);
    }
    else if (strcmp(var, "SUM") == ZERO)
    {
      AddToList((void *)readComponent(inputfile, fillSummation), aCombi->funcList);
    }
  }
}

Imp *ImpRead(FILE *inputfile)
{
  Imp *Implicit;
  char var[20];
  Vec Sub1;
  Implicit = (Imp *)AllocHeap(sizeof(Imp));
  Implicit->eps = 0.01;
  Implicit->xPoint = 0.00;
  Implicit->funcList = CreateList();
  Implicit->Bound = NULL;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "EPSILON") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(Implicit->eps));
    }
    else if (strcmp(var, "CROSSPOINT") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(Implicit->xPoint));
    }
    else if (strcmp(var, "LINEAR") == ZERO)
    {
      AddToList(readComponent(inputfile, fillLinear), Implicit->funcList);
    }
    else if (strcmp(var, "POLAR") == ZERO)
    {
      AddToList(readComponent(inputfile, fillPolar), Implicit->funcList);
    }
    else if (strcmp(var, "TUBE") == ZERO)
    {
      AddToList(readComponent(inputfile, fillTube), Implicit->funcList);
    }
    else if (strcmp(var, "TORUS") == ZERO)
    {
      AddToList(readComponent(inputfile, fillTorus), Implicit->funcList);
    }
    else if (strcmp(var, "NOISE") == ZERO)
    {
      AddToList(readComponent(inputfile, fillNoise), Implicit->funcList);
    }
    else if (strcmp(var, "PLANAR") == ZERO)
    {
      AddToList(readComponent(inputfile, fillPlanar), Implicit->funcList);
    }
    else if (strcmp(var, "COMBI") == ZERO)
    {
      AddToList(readComponent(inputfile, fillCombi), Implicit->funcList);
    }
    else if (strcmp(var, "SUM") == ZERO)
    {
      AddToList(readComponent(inputfile, fillSummation), Implicit->funcList);
    }
    else if (strcmp(var, "BOUND") == ZERO)
    {
      Implicit->Bound = BoxRead(inputfile);
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Implicit\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);

  /* Build the static (to this object) bounding box */
  Implicit->BoundObj = AllocHeap(sizeof(Object));
  Implicit->BoundObj->kdata = AllocHeap(sizeof(Box));
  ImpBound(Implicit, Implicit->BoundObj->kdata);

  if (DEBUG == ON)
  {
    ImpPrint(Implicit);
  }
  return(Implicit);
}

void ImpPrint(Imp *Implicit)
{
  printf("Implicit with eps=" FltFmt "\n", Implicit->eps);
}

Flt aFunc(Vec *B, ImpFunc *IF)
{
  Flt Ans; /* Full Space */
  ItemEntry *List;
  Vec v, A, C, D;
  Flt r, x, y, z, a, b, mult;
  Flt f;
  float vN[3];
  int i;

  ifLinear *aLinear;
  ifPlanar *aPlanar;
  ifPolar  *aPolar;
  ifNoise  *aNoise;
  ifTube   *aTube;
  ifTorus  *aTorus;

  if (IF->transform != NULL)
  {
    MProjectVector(B, IF->transform, &A);
  }
  else
  {
    A = *B;
  }

  switch(IF->funcType)
  {
    case 1:
      /* Linear */
      aLinear = IF->dataPtr;

      VecSub(&A, &(aLinear->Origin), &v);
      Ans = VecDot(&(aLinear->Direction), &v);
      break;

    case 2:
      /* Polar */
      aPolar = IF->dataPtr;
      VecSub(&A, &(aPolar->Centre), &v);
      r = VecLen(&v);
      if (r < aPolar->ZPoint)
      {
        Ans = (cos(PI * r / aPolar->ZPoint) + 1.0) * aPolar->Height / 2.0;
      }
      else Ans = 0.0;

      break;

    case 3:
      /* Noise */
      aNoise = IF->dataPtr;
      x = A.x; y = A.y; z = A.z;
      x *= aNoise->freq;
      y *= aNoise->freq;
      z *= aNoise->freq;
      a = aNoise->amp;
      mult = 0.0;
      for (i = 0; i < aNoise->terms; i++)
      {
        vN[0] = x; vN[1] = y; vN[2] = z;
        mult += a * pnoise3(vN);
        a /= 2.0;
        x *= 2.0;
        y *= 2.0;
        z *= 2.0;
      }
      Ans = (mult + aNoise->origin);
      break;
  
    case 4:
      /* Combination */
      Ans = combiFunc(&A, (ifCombi *)IF->dataPtr); 
      break;
    
    case 5:
      /* Tube Calculation */
      aTube = IF->dataPtr;

      VecSub(&A, &(aTube->From), &v);
      a = VecDot(&(aTube->Direction), &v);

      if (a < 0.0)
      {
        VecSub(&A, &(aTube->From), &v);
        r = VecLen(&v);
      }
      else if (a > aTube->Length)
      {
        VecSub(&A, &(aTube->To), &v);
        r = VecLen(&v);
      }
      else
      {
        VecAdds(a, &(aTube->Direction), &(aTube->From), &v);
        VecSub(&A, &v, &v);
        r = VecLen(&v);
      }

      if (r < aTube->ZPoint)
      {
        Ans = (cos(PI * r / aTube->ZPoint) + 1.0) / 2.0;
      }
      else Ans = 0.0;

      if (aTube->Pow != 1.0) Ans = pow(Ans, aTube->Pow);
      
      Ans *= aTube->Height;

      break;
    
    case 6:
      /* Summation */
      Ans = sumFunc(&A, (ifCombi *)IF->dataPtr); 
      break;

    case 7:
      /* Planar */
      aPlanar = IF->dataPtr;

      VecSub(&A, &(aPlanar->Origin), &v);
      Ans = VecDot(&(aPlanar->Direction), &v) / aPlanar->width;
      if (Ans < -0.5)
      {
        Ans = 1.0;
      }
      else if (Ans > 0.5)
      {
        Ans = 0.0;
      }
      else
      {
        Ans *= PI;

        Ans = (-0.5 * sin(Ans)) + 0.5;
      }
      Ans *= aPlanar->height;
      break;

    case 8:
      /* Torus */
      aTorus = IF->dataPtr;

      /* work out projection distance of A on Axis. */
      VecSub(&A, &(aTorus->Centre), &D);
      f = VecDot(&D, &(aTorus->Axis));/* / VecLen(&D); */

      /* work out projection of Q onto plane of Torus */
      VecScalar(f, &(aTorus->Axis), &D);
      VecSub(&A, &D, &C);
      
      /* Work out scaling Factor to place this point on rim of torus */
      VecSub(&C, &(aTorus->Centre), &C);
      f = aTorus->Size / VecLen(&C);

      /* Use scale factor to place &A onto rim. */
      VecAdds(f, &C, &(aTorus->Centre), &v);

      /* v is now on the rim, find distance from v to B!, whew almost done. */
      VecSub(&v, &A, &C);
      f = VecLen(&C);

      /* we are now there, so max the root of f zero (for the surface of the torus) */
      /* f ranges from 0 to inf... 
       * we need to make it range from positive 1 to 0... giving us 0.5 at
       * diameter */
      f *= PI / (2.0 * aTorus->Diameter); /* f is now PI at diameter */
      if (f > PI)
      {
	      Ans = 0.0;
      }
      else
      {
	      Ans = (cos(f) + 1.0) / 2.0;
      }
      break;
    
  }

  /* now apply power and amplitude. */
  Ans = pow(Ans, IF->power);
  Ans *= IF->amplitude;
  return Ans;
}

Flt combiFunc(Vec *A, ifCombi *theCombi)
{
  Flt Ans = 1.0; /* Full Space */
  ItemEntry *List;
  ImpFunc *IF;

  for (List = theCombi->funcList->Next; List->Data != NULL; List = List->Next)
  {
    IF = (ImpFunc *)List->Data;

    Ans *= aFunc(A, IF);
  }

  return Ans;
}

Flt sumFunc(Vec *A, ifCombi *theCombi)
{
  Flt Ans = 0.0; /* Full Space */
  ItemEntry *List;
  ImpFunc *IF;

  for (List = theCombi->funcList->Next; List->Data != NULL; List = List->Next)
  {
    IF = (ImpFunc *)List->Data;

    Ans += aFunc(A, IF);
  }

  return Ans;
}

Flt Func(Vec *A, Imp *Implicit)
{
  Flt Ans = 0.0; /* Full Space */
  ItemEntry *List;
  ImpFunc *IF;

  for (List = Implicit->funcList->Next; List->Data != NULL; List = List->Next)
  {
    IF = (ImpFunc *)List->Data;
    Ans += aFunc(A, IF);
  }

  return Ans - Implicit->xPoint;
}

ItemEntry *ImpIntersect(ray *Ray, Object *AnObj)
{
  Imp *Implicit;
  ItemEntry *Ans, *Ans1=NULL;
  IntRec *AnInt;
  Flt entryT, exitT, t, t1, dX, dY, dZ, dL, v;
  Flt tmin, tmax;
  Vec A, B;
  Flt Dens1, Dens2;
 
  Implicit = (Imp *)AnObj->kdata;

  /* Find if it hits the bounding volume */
  Ans1 = BoxIntersect(Ray, Implicit->BoundObj);
  if (Ans1->Next->Data != NULL)
  {
    /* Ok, the bounding box was hit, so perform the walk from the
       entry to exit points */
    Ans = CreateList();

    entryT = ((IntRec *)(Ans1->Next->Data))->Dist;
    exitT = ((IntRec *)(Ans1->Prev->Data))->Dist;
 
    /* Free the boxint list */
    FreeIntList(Ans1);
    Ans1 = Ans;
 
    Dens1 = -100000000.0;
    for (t = entryT; t<exitT; t+=Implicit->eps)
    {
      VecAdds(t, &(Ray->Direction), &(Ray->Origin), &A);
      Dens2 = Func(&A, Implicit);

      if ((Dens1 * Dens2) <= 0.0)
      {
        /* Binary chop to narrow the gap */
        tmax = t;
        tmin = t - Implicit->eps;
        t1 = (tmax + tmin)/2.0;
        VecAdds(t1, &(Ray->Direction), &(Ray->Origin), &A);
        v = Func(&A, Implicit);
        while(fabs(tmax-tmin) >= (EFFECTIVE_ZERO * EFFECTIVE_ZERO))
        { 
          if (v*Dens1 >= 0.0) tmin = t1; else tmax = t1;
          
          t1 = (tmax + tmin)/2.0;
          VecAdds(t1, &(Ray->Direction), &(Ray->Origin), &A);
          v = Func(&A, Implicit);
        }
        /* Intersection somewhere in the last "eps" */
        /* Build an Intersection and insert it into the list */
        AnInt = AllocInt();
        Ans1 = LinkInToList(AnInt, Ans1, AnInt->Parent);

        AnInt->ModelHit = A;
        AnInt->Dirn	= (Dens2 > 0.0) ? ENTRY : EXIT;
        AnInt->Dist	= t1;
        AnInt->HitObj   = AnObj;
        AnInt->WorldHit = AnInt->ModelHit;
 
        /* Construct the Normal */
        AnInt->DoneNorm = -1;
        B = A; B.x -= Implicit->eps;
        dX = Func(&B, Implicit) - v;
        B = A; B.y -= Implicit->eps;
        dY = Func(&B, Implicit) - v;
        B = A; B.z -= Implicit->eps;
        dZ = Func(&B, Implicit) - v;
        dL = sqrt(dX * dX + dY * dY + dZ * dZ);
        AnInt->Normal.x = dX/dL;
        AnInt->Normal.y = dY/dL;
        AnInt->Normal.z = dZ/dL;
      }
      
      Dens1 = Dens2;
    }
  }
  else
  {
    Ans = Ans1;
  }
  return(Ans);
}

Vec *ImpNormal(IntRec *A, Imp *Implicit)
{
  return (&(A->Normal));
}

Vec *ImpInverse(IntRec *B, Imp *Implicit, Flt *u, Flt *v, Vec *Ans)
{
  *v = 0.0;
  *u = 0.0;
  return(Ans);
}

Box *ImpBound(Imp *Implicit, Box *Bound)
{
  Box *aBox;
  if (Implicit->Bound != NULL)
  {
    aBox = Implicit->Bound;
    Bound->A.x = aBox->A.x;
    Bound->A.y = aBox->A.y;
    Bound->A.z = aBox->A.z;
    Bound->B.x = aBox->B.x;
    Bound->B.y = aBox->B.y;
    Bound->B.z = aBox->B.z;
  }
  else
  {
    Bound->A.x = -1.5;
    Bound->A.y = -1.5;
    Bound->A.z = -1.5;
    Bound->B.x = 1.5;
    Bound->B.y = 2.5;
    Bound->B.z = 2.0;
  }
  return(Bound);
}

int ImpInOut(Imp *Implicit, Vec *A)
{
  Flt v;
  v = Func(A, Implicit);
  if (v >= 1.0)
  {
    return -1; /* inside sphere */
  }
  else
  {
    return 0; /* outside sphere */
  }
}

Vec *ImpCentre(Imp *Implicit, Vec *Cen)
{
  Cen->x = 0.0;
  Cen->y = 0.0;
  Cen->z = 0.0;
  return(Cen);
}

Vec *ImpRandom(Imp *Implicit, Vec *Random)
{
    Random->x = 0.0;
    Random->y = 0.0;
    Random->z = 0.0;
    return (Random);
}
