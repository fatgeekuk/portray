
camera
  look-at (0.0 0.1 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (2.0 0.2 -3)
  depth 0.022
  x-size 0.024
  y-size 0.032
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 768
  ysize 1024
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;

object
   sphere
      center (0 1 0)
      radius 1
   ;
;

object 
   implicit
      epsilon 0.01
      bound
         A (-1000 10 -1000)
         B (1000 -1 1000)
      ;
      linear
         origin (0 0 0)
         direction (0 1 0)
      ;
   ;
;
