
camera
  look-at (0.0 0.1 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (2.0 0.2 -3)
  depth 0.022
  x-size 0.032
  y-size 0.024
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 1024
  ysize 768
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;

object 
   implicit
      bound
         a (-1000 -1 -1000)
         b (1000 2 1000)
      ;
      epsilon 0.01
      crosspoint 0.6
      linear
         origin (0 0 0)
         direction (0 -1 0)
      ;
      noise
        freq 1
        amp 0.3
        terms 3
      ;
   ;
;
