
camera
  look-at (0.0 0.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (2.0 0.2 -4)
  depth 0.022
  x-size 0.032
  y-size 0.024
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 1024
  ysize 768
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;

object
   plane
      position (0 -1 0)
      normal (0 1 0)
      x-axis (1 0 0)
      y-axis (0 0 1)
   ;
;

object 
   implicit
      epsilon 0.01
      crosspoint 0.6
      bound
        a (-2 -2 -2)
        b ( 2  2  2)
      ;
      polar
        shrink-x 4.0
        height 2.0
        centre (0 0 0)
        zradius 1.7
      ;
      polar
        shrink-y 4.0
        height 2.0
        centre (0 0 0)
        zradius 1.7
      ;
      polar
        shrink-z 4.0
        height 2.0
        centre (0 0 0)
        zradius 1.7
      ;
      polar
        shrink-x 4.0
        shrink-y 4.0
        height -2.0
        centre (0 0 0)
        zradius 1.7
      ;
      polar
        shrink-y 4.0
        shrink-z 4.0
        height -2.0
        centre (0 0 0)
        zradius 1.7
      ;
      polar
        shrink-z 4.0
        shrink-x 4.0
        height -2.0
        centre (0 0 0)
        zradius 1.7
      ;
   ;
   material
      spec-colour (1 1 1)
      diff-colour (0 0 1)
      diffuse-reflection 0.6
      specular-reflection 0.4
   ;
;
