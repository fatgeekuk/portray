
camera
  look-at (0.0 0.1 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (2.0 0.2 -3)
  depth 0.022
  x-size 0.024
  y-size 0.032
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 768
  ysize 1024
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;

object
   plane
      position (0 -1 0)
      normal (0 1 0)
      x-axis (1 0 0)
      y-axis (0 0 1)
   ;
;

object 
   implicit
      epsilon 0.01
      bound a (-1000 -3 -1000) b (1000 3 1000) ;
      combi
         planar origin (0 3 0) direction (0 1 0) height 1.0 width 1.0 ;
         planar origin (0 1 0) direction (0 -1 0) height 1.0 width 1.0 ;
         noise freq 0.5 terms 2 amp -1.0 ;
      ;
   ;
   material
      spec-colour (1 1 1)
      diff-colour (0 0 1)
      diffuse-reflection 0.6
      specular-reflection 0.4
   ;
;
