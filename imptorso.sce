
camera
  look-at (0.0 0.1 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (2.0 2.2 -12)
  depth 0.022
  x-size 0.032
  y-size 0.024
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 1024
  ysize 768
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;

object 
   implicit
      bound
         a (-3 -7 -3)
         b (3 8 3)
      ;
      epsilon 0.01
      crosspoint 0.3
      tube from (0 4.3 0) to (0 6 0) zradius 0.9 height 1.0 ;
      tube from (-1.5 4.3 0) to (1.5 4.3 0) zradius 0.8 height 1.0 ;
      tube from (-1 4 0) to (-1 3 -1.4) zradius 1.0 height 1.0 ;
      tube from (1 4 0) to (1 3 -1.4) zradius 1.0 height 1.0 ;
      polar centre (-1 3 -1.4) zradius 1.0 height 0.2 ;
      polar centre (1 3 -1.4) zradius 1.0 height 0.2 ;
      tube shrink-z 0.8 from (-0.5 0 0.2) to (-1 4 0) zradius 2.0 height 1.0 ;
      tube shrink-z 0.8 from (0.5 0 0.2) to (1 4 0) zradius 2.0 height 1.0 ;
      polar centre (-1.3 0 1.0) zradius 2.0 height 1.0 ;
      polar centre (1.3 0 1.0) zradius 2.0 height 1.0 ;
      polar translate (0 1.6 -1.0) shrink-y 0.5 shrink-z 4.0 centre (0 0 0) zradius 1.0 height 1.0 ;
      polar centre (0 1.3 -1.1) zradius 0.1 height -1.0 ;
      tube from (-1.2 0 0) to (-1.2 -5 0) zradius 1.0 height 1.0 ;
      tube from (1.2 0 0) to (1.2 -5 0) zradius 1.0 height 1.0 ;
      polar centre (0 0 0) zradius 2.0 height 1.0 shrink-x 2.0 shrink-z 1.5 translate (-1.2 -2.3 0) ;
      polar centre (0 0 0) zradius 2.0 height 1.0 shrink-x 2.0 shrink-z 1.5 translate (1.2 -2.3 0) ;
   ;
;
