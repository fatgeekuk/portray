
camera
  look-at (0.0 0.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (0.0 3.2 -30)
  depth 0.080
  y-size 0.024
  x-size 0.032
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 640
  ysize 480
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;
object 
   implicit
      epsilon 0.005
      crosspoint 0.5
      bound
         A (-12 3 -12)
         B (12 -3 12)
      ;
      combi
         planar origin (0 2 0) direction (0 1 -0.7) width 0.4 height 1.0 ;
         planar rotate-y 72 origin (0 2 0) direction (0 1 -0.7) width 0.4 height 1.0 ;
         planar rotate-y 144 origin (0 2 0) direction (0 1 -0.7) width 0.4 height 1.0 ;
         planar rotate-y 216 origin (0 2 0) direction (0 1 -0.7) width 0.4 height 1.0 ;
         planar rotate-y 288 origin (0 2 0) direction (0 1 -0.7) width 0.4 height 1.0 ;
         planar rotate-y 36 origin (0 -2 0) direction (0 -1 -0.7) width 0.4 height 1.0 ;
         planar rotate-y 108 origin (0 -2 0) direction (0 -1 -0.7) width 0.4 height 1.0 ;
         planar rotate-y 180 origin (0 -2 0) direction (0 -1 -0.7) width 0.4 height 1.0 ;
         planar rotate-y 252 origin (0 -2 0) direction (0 -1 -0.7) width 0.4 height 1.0 ;
         planar rotate-y 324 origin (0 -2 0) direction (0 -1 -0.7) width 0.4 height 1.0 ;
      ;
   ;
;
