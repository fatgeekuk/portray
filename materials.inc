include colours.inc
define
   material named red-plastic
      colour red
      spec-colour white
      specular-reflection 0.3
      diffuse-reflection 0.7
      specular-power 3
   ;
   material named green-plastic
      colour green
      spec-colour white
      specular-reflection 0.3
      diffuse-reflection 0.7
      specular-power 3
   ;
   material named blue-plastic
      colour blue
      spec-colour white
      specular-reflection 0.3
      diffuse-reflection 0.7
      specular-power 3
   ;
;
