/* Matrix.c */

#include "vectors.h"
#include "rend_0.h"

Matrix *MIdentity(Matrix *AM)
{
  int i, j;
  for (i = 0; i<4; i++)
  {
    for (j = 0; j<i; j++)
    {
      (*AM)[i][j]=0.0;
      (*AM)[j][i]=0.0;
    }
    (*AM)[i][j] = 1.0;
  }
  return(AM);
}

Matrix *MTranspose(Matrix *AM, Matrix *BM)
{
  Matrix tM;
  int i, j;
  for (i=0; i<4; i++)
    for(j=0; j<4; j++)
      tM[j][i] = (*AM)[i][j];

  for (i=0; i<4; i++)
    for(j=0; j<4; j++)
      (*BM)[i][j] = tM[i][j];

  return(BM);
}


Matrix *MProduct(Matrix *AM, Matrix *BM, Matrix *CM)
{
  int i, j, k;
  Matrix tM;
   for (i = 0 ; i < 4 ; i++)
     for (j = 0 ; j < 4 ; j++)
     {
       tM[i][j] = 0.0;
       for (k = 0 ; k < 4 ; k++)
	   tM[i][j] += (*AM)[i][k] * (*BM)[k][j];
     }

   for (i = 0 ; i < 4 ; i++)
      for (j = 0 ; j < 4 ; j++)
	 (*CM)[i][j] = tM[i][j];

  return (CM);
}

Vec *MProjectVector(Vec *AVec, Matrix *AM, Vec *Answer)
{
  Vec tV, aV;
  aV.x = AVec->x + (*AM)[3][0];
  aV.y = AVec->y + (*AM)[3][1];
  aV.z = AVec->z + (*AM)[3][2];

  tV.x = aV.x * (*AM)[0][0] +
	 aV.y * (*AM)[1][0] +
	 aV.z * (*AM)[2][0];

  tV.y = aV.x * (*AM)[0][1] +
	 aV.y * (*AM)[1][1] +
	 aV.z * (*AM)[2][1];

  tV.z = aV.x * (*AM)[0][2] +
	 aV.y * (*AM)[1][2] +
	 aV.z * (*AM)[2][2];
  *Answer = tV;
  return (Answer);
}

Vec *MProjectNormal(Vec *AVec, Matrix *AM, Vec *Answer)
{
  Vec tV;
  tV.x = AVec->x * (*AM)[0][0] +
	 AVec->y * (*AM)[1][0] +
	 AVec->z * (*AM)[2][0];


  tV.y = AVec->x * (*AM)[0][1] +
	 AVec->y * (*AM)[1][1] +
	 AVec->z * (*AM)[2][1];

  tV.z = AVec->x * (*AM)[0][2] +
	 AVec->y * (*AM)[1][2] +
	 AVec->z * (*AM)[2][2];

  *Answer = tV;
  return (Answer);
}

Vec *MInvertVector(Vec *AVec, Matrix *AM, Vec *Answer)
{
  Vec tV, aV;

  tV.x = AVec->x * (*AM)[0][0] +
	 AVec->y * (*AM)[1][0] +
	 AVec->z * (*AM)[2][0];

  tV.y = AVec->x * (*AM)[0][1] +
	 AVec->y * (*AM)[1][1] +
	 AVec->z * (*AM)[2][1];

  tV.z = AVec->x * (*AM)[0][2] +
	 AVec->y * (*AM)[1][2] +
	 AVec->z * (*AM)[2][2];

  aV.x = tV.x + (*AM)[3][0];
  aV.y = tV.y + (*AM)[3][1];
  aV.z = tV.z + (*AM)[3][2];

  *Answer = aV;
  return (Answer);
}

Vec *MInvertNormal(Vec *AVec, Matrix *AM, Vec *Answer)
{
  Vec tV;
  tV.x = AVec->x * (*AM)[0][0] +
	 AVec->y * (*AM)[0][1] +
	 AVec->z * (*AM)[0][2];

  tV.y = AVec->x * (*AM)[1][0] +
	 AVec->y * (*AM)[1][1] +
	 AVec->z * (*AM)[1][2];

  tV.z = AVec->x * (*AM)[2][0] +
	 AVec->y * (*AM)[2][1] +
	 AVec->z * (*AM)[2][2];
  *Answer = tV;
  return (Answer);
}

Matrix *MCopy(Matrix *AM, Matrix *BM)
{
  int i, j;
  for (i = 0; i < 4; i++)
      for (j=0; j < 4; j++)
	  (*BM)[i][j] = (*AM)[i][j];

  return (BM);
}

Matrix *MScale(Flt XScale, Flt YScale, Flt ZScale, Matrix *AM, Matrix *BM, Matrix *CM, Matrix *DM)
{
  Matrix tM;
  MIdentity(&tM);
  tM[0][0] = 1.0/XScale;
  tM[1][1] = 1.0/YScale;
  tM[2][2] = 1.0/ZScale;
  MProduct(&tM, AM, BM);

  MIdentity(&tM);
  tM[0][0] = XScale;
  tM[1][1] = YScale;
  tM[2][2] = ZScale;
  MProduct(CM, &tM, DM);
  return(BM);
}

Matrix *MTranslate(Vec *AVec, Matrix *AM, Matrix *BM, Matrix *CM, Matrix *DM)
{
  MCopy(AM, BM);
  MCopy(CM, DM);
  (*BM)[3][0] = -AVec->x;
  (*BM)[3][1] = -AVec->y;
  (*BM)[3][2] = -AVec->z;
  (*DM)[3][0] = AVec->x;
  (*DM)[3][1] = AVec->y;
  (*DM)[3][2] = AVec->z;
  return(BM);
}

Matrix *MAxis(Vec *XAxis, Vec *YAxis, Vec *Origin, Matrix *AM, Matrix *BM)
{
  Vec ZAxis;

  MIdentity(AM);

  /* Setup Rotation Section of Matrix */
  (*AM)[0][0] = XAxis->x;
  (*AM)[1][0] = XAxis->y;
  (*AM)[2][0] = XAxis->z;
  (*AM)[0][1] = YAxis->x;
  (*AM)[1][1] = YAxis->y;
  (*AM)[2][1] = YAxis->z;

  /* Generate z vector from x times y */
  VecUnit(VecProduct(XAxis, YAxis, &ZAxis), &ZAxis);
  (*AM)[0][2] = ZAxis.x;
  (*AM)[1][2] = ZAxis.y;
  (*AM)[2][2] = ZAxis.z;

  MTranspose(AM, BM);


  /* Setup Translation sections of Matrices */
  (*AM)[3][0] = -(Origin->x);
  (*AM)[3][1] = -(Origin->y);
  (*AM)[3][2] = -(Origin->z);

  (*BM)[3][0] = Origin->x;
  (*BM)[3][1] = Origin->y;
  (*BM)[3][2] = Origin->z;

  /* Setup Null sections of Matrices */
  (*AM)[0][3] = 0.0;
  (*AM)[1][3] = 0.0;
  (*AM)[2][3] = 0.0;
  (*AM)[3][3] = 1.0;
  (*BM)[0][3] = 0.0;
  (*BM)[1][3] = 0.0;
  (*BM)[2][3] = 0.0;
  (*BM)[3][3] = 1.0;
  return(AM);


   /* Now both the Model Matrix and its inverse are set up */
}

Matrix *MRotate(Flt a, Flt b, Flt c, Matrix *AM, Matrix *BM)
{
  /* Note a = Rotation About X,
	  b = Rotation About Y,
	  c = Rotation About Z.
  */
  Matrix tM;
  Flt cosx, cosy, cosz, sinx, siny, sinz;

  MIdentity (AM);
  cosx = cos (a);
  sinx = sin (a);
  cosy = cos (b);
  siny = sin (b);
  cosz = cos (c);
  sinz = sin (c);

  (*AM)[1][1] = cosx;
  (*AM)[2][2] = cosx;
  (*AM)[1][2] = sinx;
  (*AM)[2][1] = 0.0 - sinx;

  MTranspose(AM, BM);

  MIdentity (&tM);
  tM[0][0] = cosy;
  tM[2][2] = cosy;
  tM[0][2] = 0.0 - siny;
  tM[2][0] = siny;
  MProduct(AM, &tM, AM);
  MTranspose(&tM, &tM);
  MProduct(&tM, BM, BM);

  MIdentity (&tM);
  tM[0][0] = cosz;
  tM[1][1] = cosz;
  tM[0][1] = sinz;
  tM[1][0] = 0.0 - sinz;
  MProduct(AM, &tM, AM);
  MTranspose(&tM, &tM);
  MProduct(&tM, BM, BM);

  return(AM);
}


void MPrint(Matrix *AM)
{
  int i, j;
  printf("\n");
  for (i=0; i<4; i++)
  {
    printf("| ");
    for (j=0; j<4; j++)
	printf(" %6.3lf ", (*AM)[i][j]);
    printf(" |\n");
  }
}
