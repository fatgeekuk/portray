/* MATRIX.H */


typedef Flt Matrix [4][4];
Matrix *MIdentity(Matrix *AM);
Matrix *MTranspose(Matrix *AM, Matrix *BM);
Matrix *MProduct(Matrix *AM, Matrix *BM, Matrix *CM);
void MPrint(Matrix *AM);
Vec *MProjectVector(Vec *AVec, Matrix *AM, Vec *Answer);
Vec *MProjectNormal(Vec *AVec, Matrix *AM, Vec *Answer);
Vec *MInvertVector(Vec *AVec, Matrix *AM, Vec *Answer);
Vec *MInvertNormal(Vec *AVec, Matrix *AM, Vec *Answer);
Matrix *MRotate(Flt a, Flt b, Flt c, Matrix *AM, Matrix *BM);
Matrix *MTranslate(Vec *A, Matrix *AM, Matrix *BM, Matrix *CM, Matrix *DM);
Matrix *MAxis(Vec *XAxis, Vec *YAxis,Vec *Origin, Matrix *AM, Matrix *BM);
Matrix *MScale(Flt XScale, Flt YScale, Flt ZScale, Matrix *AM, Matrix *BM, Matrix *CM, Matrix *DM);
