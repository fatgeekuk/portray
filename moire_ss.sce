include colours.inc

camera
  look-at (3.6 1.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (3.6 1.0 -10)
  depth 0.04
  x-size 0.048
  y-size 0.024
  filter standard
  filter super-sample x-samples 5 y-samples 5 ;
;

image
  xsize 1536
  ysize 768
;

light
  colour (0.6 0.6 0.6)
  position (-20.0 50 -50)
  shape sphere
     center (-20 50 -50)
     radius 10
  ;
  samples 500
  min-samples 50
;

light
  colour (0.4 0.4 0.4)
  position (3.6 1.0 -10)
;


object
  box
    a (-1000 -1000 -1)
    b (1000 1000 1)
  ;
  material
    colour (0.8 0.8 1.0)
  ;
;

object
  box
    a (0 0 -1.5)
    b (3.47 2.44 1.5)
  ;
  material
    image
      file MoireSS.tga
      x-size 3.47
      y-size 2.44
      flip
    ;
  ;
;

object
  cylinder
  	from (0 -0.5 -1)
  	to   (3.47 -0.5 -1)
  	radius 0.1
  ;
;

object
  cylinder
    from (0 2.94 -1)
    to   (3.47 2.94 -1)
    radius 0.1
  ;
;

object
  cylinder
    from (-0.5 0 -1)
    to   (-0.5 2.44 -1)
    radius 0.1
  ;
;

object
  cylinder
    from (3.97 0.0 -1)
    to   (3.97 2.44 -1)
    radius 0.1
  ;
;


object
  csg
    intersect
    operand1
	  torus
	    radius 0.5
	    norm-radius 0.1
	    plane-radius 0.1
	  ;
	  translate (0 0 -1)
	  x-rotate 90
	;
	operand2
	  box
	    a (0 0 0)
	    b (-1 -1 -1.2)
	  ;
	;
  ;
;

object
  csg
    intersect
    operand1
	  torus
	    radius 0.5
	    norm-radius 0.1
	    plane-radius 0.1
	  ;
	  translate (3.47 0 -1)
	  x-rotate 90
	;
	operand2
	  box
	    a (3.47 0 0)
	    b (4.1 -1 -1.2)
	  ;
	;
  ;
;

object
  csg
    intersect
    operand1
	  torus
	    radius 0.5
	    norm-radius 0.1
	    plane-radius 0.1
	  ;
	  translate (0 2.44 -1)
	  x-rotate 90
	;
	operand2
	  box
	    a (0 2.44 0)
	    b (-1 3.1 -1.2)
	  ;
	;
  ;
;

object
  csg
    intersect
    operand1
	  torus
	    radius 0.5
	    norm-radius 0.1
	    plane-radius 0.1
	  ;
	  translate (3.47 2.44 -1)
	  x-rotate 90
	;
	operand2
	  box
	    a (3.47 2.44 0)
	    b (4.1  3.1 -1.2)
	  ;
	;
  ;
;

object
  box
    a (-1000 -0.5 1)
    b (1000 -10000 -1.1)
  ;
;

object
  box
    a (-1000 2.94 1)
    b (1000 10000 -1.1)
  ;
;

object
  box
    a (-0.5 -1000 1)
    b (-1000 1000 -1.1 )
  ;
;

object
  box
    a (3.97 -1000 1)
    b (1000 1000 -1.1)
  ;
;

object
  csg
    subtract
    operand1
      box
        a (0 0 1)
        b (-1 -1 -1.1)
      ;
    ;
    operand2
      cylinder
        from (0 0 1)
        to   (0 0 -2)
        radius 0.5
      ;
    ;
  ;
;
object
  csg
    subtract
    operand1
      box
        a (0 2.44 1)
        b (-1 3 -1.1)
      ;
    ;
    operand2
      cylinder
        from (0 2.44 1)
        to   (0 2.44 -2)
        radius 0.5
      ;
    ;
  ;
;
object
  csg
    subtract
    operand1
      box
        a (3.47 0 1)
        b (5 -1 -1.1)
      ;
    ;
    operand2
      cylinder
        from (3.47 0 1)
        to   (3.47 0 -2)
        radius 0.5
      ;
    ;
  ;
;
object
  csg
    subtract
    operand1
      box
        a (3.47 2.44 1)
        b (5 3 -1.1)
      ;
    ;
    operand2
      cylinder
        from (3.47 2.44 1)
        to   (3.47 2.44 -2)
        radius 0.5
      ;
    ;
  ;
;
