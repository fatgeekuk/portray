
//       stack o' balls

include colours.lib

camera
    look-from (-2 4 -4)
    Look-at (0 0 0)
    drop-line (0 -1 0)
    depth 0.64
    y-size 0.24
    x-size 0.32
    filter adaptive quick sub-thresh 0.02 ;
//    filter standard
;

image
   xsize 640
   ysize 480
;

light
    position (-10 6 0)
    color  (1 1 1)
    shape sphere
      centre (-10 6 0)
      radius 1
    ;
    samples 1
    min-samples 1
;


object
   disk
      radius 10.0
      position (0 0 0)
      normal (0 1 0)
      x-axis (1 0 0)
      y-axis (0 0 1)
   ;
;

object
   csg
      subtract
      operand1
         cylinder
            from (0 -10 0)
            to   (0 0.2 0)
            radius 1.00
         ;
         material
            specular-reflection 0.95
            diffuse-reflection 0.05
            colour (1 1 1)
            named shiny
         ;
      ;
      operand2
         cylinder
            from (0 -9 0)
            to   (0 1 0)
            radius 0.95
         ;
         material called shiny ;
      ;
   ;
;



