#include <math.h>
#include "vectors.h"
#define hermite(p0, p1, r0, r1, t) (p0*((2.0*t-3.0)*t*t+1.0) + p1*(-2.0*t+3.0)*t*t + r0*((t-2.0)*t+1.0)*t + r1*(t-1.0)*t*t)
#define rand3a(x,y,z) frand( 67*(x) + 59*(y) + 71*(z))
#define rand3b(x,y,z) frand( 73*(x) + 79*(y) + 83*(z))
#define rand3c(x,y,z) frand( 89*(x) + 97*(y) +101*(z))
#define rand3d(x,y,z) frand(103*(x) +107*(y) +109*(z))

long xlim[3][2];
double xarg[3];

double frand(long s)
{
  s = s << 13 ^ s;
  return(1.0 - ((s*(s*s*15731+789221)+1376312589)&0x7fffffff) / 1073741824.0);
}

void interpolate(double f[4], int i, int n)
{
  double f0[4], f1[4];
  int a, b, c;
  a = i&1;
  b = (i>>1)&1;
  c = i>>2;
  if (n==0)
  {
    f[0] = rand3a(xlim[0][a], xlim[1][b], xlim[2][c]);
    f[1] = rand3b(xlim[0][a], xlim[1][b], xlim[2][c]);
    f[2] = rand3c(xlim[0][a], xlim[1][b], xlim[2][c]);
    f[3] = rand3d(xlim[0][a], xlim[1][b], xlim[2][c]);
  }
  else
  {
    n--;
    interpolate(f0,i,n);
    interpolate(f1, i | 1 << n, n);
    f[0] = (1.0-xarg[n])*f0[0] + xarg[n]*f1[0];
    f[1] = (1.0-xarg[n])*f0[1] + xarg[n]*f1[1];
    f[2] = (1.0-xarg[n])*f0[2] + xarg[n]*f1[2];
    f[3] = hermite(f0[3], f1[3], f0[n], f1[n], xarg[n]);
  }
}

Vec *noise3(Flt x, Flt y, Flt z, Vec *Res)
{
  extern double floor();
  static double f[4];
  xlim[0][0] = floor(x); xlim[0][1] = xlim[0][0] + 1;
  xlim[1][0] = floor(y); xlim[1][1] = xlim[1][0] + 1;
  xlim[2][0] = floor(z); xlim[2][1] = xlim[2][0] + 1;
  xarg[0] = x - xlim [0][0];
  xarg[1] = y - xlim [1][0];
  xarg[2] = z - xlim [2][0];
  interpolate(f, 0, 3);
  Res->x = f[0];
  Res->y = f[1];
  Res->z = f[2];
  return(Res);
}

/* noise function over R3 - implemented by a pseudorandom tricubic spline */

#define B 256
static p[B + B + 2];
static Flt g[B + B + 2][3];

static start=1;

#define setup(qq,b0, b1, r0, r1) \
	t = qq + 10000.; \
        b0 = ((int)t) & (B-1); \
        b1 = (b0+1) & (B-1); \
        r0 = t - (int)t; \
        r1 = r0 - 1.0;

static initnoise();

Flt noise3new(Flt x, Flt y, Flt z)
{
  int bx0, bx1, by0, by1, bz0, bz1, b00, b10, b01, b11;
  float rx0, rx1, ry0, ry1, rz0, rz1, sx, sy, sz, a, b, c, d, t, u, v;
  register i, j;

  if (start)
  {
    start = 0;
    initnoise();
  }
  setup(x, bx0, bx1, rx0, rx1);
  setup(y, by0, by1, ry0, ry1);
  setup(z, bz0, bz1, rz0, rz1);

  i = p[bx0];
  j = p[bx1];

  b00 = p[i+by0];
  b10 = p[j+by0];
  b01 = p[i+by1];
  b11 = p[j+by1];

  /* printf("00=%d 01=%d 10=%d 11=%d\n", b00, b01, b10, b11);
  */
  #define at(qq, rx, ry, rz) ( rx * g[qq][0] + ry * g[qq][1] + rz * g[qq][2] )
  #define s_curve(t) ( t * t * (3.0 - 2.0 * t))
  #define lerp(t, a, b) (a + t * (b - a) )
 
  sx = s_curve(rx0);
  sy = s_curve(ry0);
  sz = s_curve(rz0);

  u = at (b00 + bz0, rx0, ry0, rz0);
  v = at (b10 + bz0, rx1, ry0, rz0);
  a = lerp(sx, u, v);

  u = at (b01 + bz0, rx0, ry1, rz0);
  v = at (b11 + bz0, rx1, ry1, rz0);
  b = lerp (sx, u, v);

  c = lerp(sy, a, b);

  u = at (b00 + bz1, rx0, ry0, rz1);
  v = at (b10 + bz1, rx1, ry0, rz1);
  a = lerp(sx, u, v);

  u = at (b01 + bz1, rx0, ry1, rz1);
  v = at (b11 + bz1, rx1, ry1, rz1);
  b = lerp (sx, u, v);

  d = lerp(sy, a, b);
  return 1.5 * lerp(sz, c, d);

}

static initnoise()
{
  long random();
  int i, j, k;
  float v[3], s;

  /* Create an array of random gradient vectors uniformly on the unit sphere */
  srandom(1);
  for (i = 0; i < B; i++)
  {
    do
    {
      for (j=0;   j<3; j++)
        v[j] = (float)((random() % (B + B)) - B) / B;

      s = v[0]*v[0] + v[1]*v[1] + v[2]*v[2];
      printf("Try Again(%f)\n", s);
    }
    while (s > 1.0);

    s = sqrt(s);
    for (j=0; j<3; j++)
    {
      g[i][j] = v[j] / s;
      printf("G(%d %d) = %f\n", i, j, g[i][j]);
    }
    
  }
  for (i=0; i < B; i++)
    p[i] = i;
  
  for (i=B; i > 0; i -= 2)
  {
    k = p[i];
    p[i] = p[j = random() % B];
    p[j] = k;
  }

  /* Extend g and p arrays to allow for faster indexing. */
  for (i=0; i<B+2; i++)
  {
    p[B+i] = p[i];
    for (j=0; j < 3; j++)
      g[B + i][j] = g[i][j];
  }
}
