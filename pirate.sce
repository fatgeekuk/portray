Light
   Position ( -200 100 -100 )
   Colour ( 1 1 1 )
   shape sphere
      center (-200 100 -100)
      radius 30
   ;
   samples 100
   min-samples 10
;
camera
   look-at ( 0 1.5 0 )
   drop-line (0 -1 0)
   origin ( 0 3 -10 )
   depth 0.8
   x-size 0.64
   y-size 0.48
   filter standard
;

background ( 0 0 1 )

image
   xsize  640
   ysize  480
;

object
   sphere
      center (0 1 0)
      radius 1.0
   ;
;

object
   plane
      position (0 0 0)
      normal (0 1 0)
      x-axis (1 0 0)
      y-axis (0 0 1)
   ;
   material
      turb
         power 9
         terms 9
         size 20
         material-a
            colour (1 1 1)
         ;
         material-b
            chequers
               material-a
                  colour (1 1 1)
               ;
               material-b
                  colour (0 0 0)
               ;
            ;
         ;
      ;

   ;
;
