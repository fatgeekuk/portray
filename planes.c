
/*
  *************************************************************************
 **                                                                       **
 **                           Plane Section                               **
 **                                                                       **
  *************************************************************************
*/

char   *PlaName();
Pla    *PlaRead();
void    PlaPrint();
ItemEntry *PlaIntersect(ray *Ray, Object *AnObj);
Vec    *PlaNormal();
Vec    *PlaInverse();
Box    *PlaBound();
int     PlaInOut();
Vec    *PlaCentre();

void InitPlane(Prim *APrim)
{
  APrim->Name      = PlaName;
  APrim->Read	   = (void *)PlaRead;
  APrim->Print     = PlaPrint;
  APrim->Intersect = PlaIntersect;
  APrim->Normal    = PlaNormal;
  APrim->Inverse   = PlaInverse;
  APrim->Bound     = PlaBound;
  APrim->InOut     = PlaInOut;
  APrim->Centre    = PlaCentre;
}

char *PlaName()
{
  return "PLANE";
}

Pla *PlaRead(FILE *inputfile)
{
  Pla *Plane;
  Vec Sub1;
  char var[20];
  Plane = (Pla *)AllocHeap(sizeof(Pla));
  SetVector(0.0, 0.0, 0.0, &(Plane->Position));
  SetVector(0.0, 1.0, 0.0, &(Plane->Normal));
  SetVector(1.0, 0.0, 0.0, &(Plane->XAxis));
  SetVector(0.0, 0.0, 1.0, &(Plane->YAxis));
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "POSITION") == ZERO)
    {
      ReadVec(inputfile, &(Plane->Position));
    }
    else if (strcmp(var, "NORMAL") == ZERO)
    {
      ReadVec(inputfile, &(Plane->Normal));
    }
    else if (strcmp(var, "X-AXIS") == ZERO)
    {
      VecUnit(ReadVec(inputfile, &Sub1), &(Plane->XAxis));
    }
    else if (strcmp(var, "Y-AXIS") == ZERO)
    {
       VecUnit(ReadVec(inputfile, &Sub1), &(Plane->YAxis));
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Plane\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);
  VecUnit(&(Plane->Normal),&(Plane->Normal));
  if (DEBUG == ON)
  {
    PlaPrint(Plane);
  }
  return(Plane);
}

void PlaPrint(Pla *Plane)
{
  printf("Plane with position at (" FltFmt ", " FltFmt ", " FltFmt ") and normal of (" FltFmt ", " FltFmt ", " FltFmt ")\n",
   Plane->Position.x,
   Plane->Position.y,
   Plane->Position.z,
   Plane->Normal.x,
   Plane->Normal.y,
   Plane->Normal.z);
}

ItemEntry *PlaIntersect(ray *Ray, Object *AnObj)
{
  ItemEntry *Ans=CreateList();
  IntRec *AnInt=NULL;
  Pla *Plane;
  Flt t1;
  Vec Normal, Sub1;

  Plane = (Pla *)AnObj->kdata;
  Normal = Plane->Normal;
  t1 = -VecDot(&(Plane->Normal), &(Ray->Direction));
  if (fabs(t1) > EFFECTIVE_ZERO)
  {
    AnInt = AllocInt();

    if (t1 < EFFECTIVE_ZERO)
    {
      VecNegate(&Normal, &Normal);
      AnInt->Dirn = EXIT;
    }
    else
    {
      AnInt->Dirn = ENTRY;
    }
    VecSub(&(Ray->Origin), &(Plane->Position),&Sub1);
    AnInt->Dist    = VecDot(&(Plane->Normal), &Sub1) / t1;
    VecAdds(AnInt->Dist, &(Ray->Direction), &(Ray->Origin), &(AnInt->ModelHit));
    AnInt->HitObj   = AnObj;
    AnInt->WorldHit = AnInt->ModelHit;
    LinkInToList(AnInt, Ans, AnInt->Parent);
  }
  return(Ans);
}

Vec *PlaNormal(IntRec *A, Pla *Plane)
{
  A->Normal = Plane->Normal;
  return (&(A->Normal));
}

Vec *PlaInverse(IntRec *B, Pla *Plane, Flt *u, Flt *v, Vec *Ans)
{
  Vec Sub1;
  Vec *A;

  A = &(B->ModelHit);
  VecSub(A, &(Plane->Position),&Sub1);
  *u = VecDot(&(Plane->YAxis), &Sub1);
  *v = VecDot(&(Plane->XAxis), &Sub1);
  return(A);
}

Box *PlaBound(Pla *Plane, Box *Bound)
{
  Bound->A.x = -999.99;
  Bound->A.y = -999.99;
  Bound->A.z = -999.99;
  Bound->B.x =  999.99;
  Bound->B.y =  999.99;
  Bound->B.z =  999.99;
  return(Bound);
}

int PlaInOut(Pla *Plane, Vec *A)
{
  Vec Sub1;
  VecSub(A, &(Plane->Position),&Sub1);
  if (VecDot(&(Plane->Normal), &Sub1) > 0.0)
  {
    return 0;
  }
  else
  {
    return -1;
  }
}

Vec *PlaCentre(Pla *Plane, Vec *Cen)
{
  Cen->x = Plane->Position.x;
  Cen->y = Plane->Position.y;
  Cen->z = Plane->Position.z;
  return(Cen);
}
