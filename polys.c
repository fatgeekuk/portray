/*
  *************************************************************************
 **                                                                       **
 **			      Polygon Section				  **
 **                                                                       **
  *************************************************************************
*/

char *PolName();
Pol *PolRead();
void PolPrint();
ItemEntry *PolIntersect(ray *Ray, Object *AnObj);
Vec *PolNormal();
Vec *PolInverse();
Box *PolBound();
int PolInOut();
Vec *PolCentre();
Contour *ReadContour(FILE *inputfile);
LineSeg *ReadLineSeg(FILE *inputfile);


/* this is here for reference only, the real copy resides in PRIMS.H

typedef struct Vec2d
{
  Flt x, y;
} Vec2d;

typedef struct LineSeg
{
  struct *NextLineSeg;
  Vec AVec;
} LineSeg;

typedef struc Contour
{
  struct *NextContour;
  LineSeg *HeadSeg;
} Contour;

typedef struct Pol
{
  Vec Position, Normal;
  Vec XAxis, YAxis;
  Contour *HeadCont;
} Pla;

*/

void InitPoly(Prim *APrim)
{
  APrim->Name	   = PolName;
  APrim->Read	   = (void *)PolRead;
  APrim->Print	   = PolPrint;
  APrim->Intersect = PolIntersect;
  APrim->Normal    = PolNormal;
  APrim->Inverse   = PolInverse;
  APrim->Bound	   = PolBound;
  APrim->InOut	   = PolInOut;
  APrim->Centre    = PolCentre;
}

char *PolName()
{
  return "POLYGON";
}

Pol *PolRead(FILE *inputfile)
{
  Pol *Poly;
  Vec Sub1;
  Flt x, y, z;
  char var[20];
  Contour *ACont;
  LineSeg *ALineSeg;

  Poly = (Pol *)AllocHeap(sizeof(Pol));
  Poly->HeadCont = NULL;
  Poly->BoundA.x = 999999999.999;
  Poly->BoundA.y = 999999999.999;
  Poly->BoundA.z = 999999999.999;
  Poly->BoundB.x = -999999999.999;
  Poly->BoundB.y = -999999999.999;
  Poly->BoundB.z = -999999999.999;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "POSITION") == ZERO)
    {
      ReadVec(inputfile, &(Poly->Position));
    }
    else if (strcmp(var, "NORMAL") == ZERO)
    {
      ReadVec(inputfile, &(Poly->Normal));
    }
    else if (strcmp(var, "X-AXIS") == ZERO)
    {
      VecUnit(ReadVec(inputfile, &Sub1), &(Poly->XAxis));
    }
    else if (strcmp(var, "Y-AXIS") == ZERO)
    {
       VecUnit(ReadVec(inputfile, &Sub1), &(Poly->YAxis));
    }
    else if (strcmp(var, "CONTOUR") == ZERO)
    {
      ACont = ReadContour(inputfile);
      ACont->NextCont = Poly->HeadCont;
      Poly->HeadCont = ACont;
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Polygon\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);
  VecUnit(&(Poly->Normal),&(Poly->Normal));

  for (ACont = Poly->HeadCont; ACont != NULL; ACont = ACont->NextCont)
  {
    for (ALineSeg = ACont->HeadSeg; ALineSeg != NULL; ALineSeg = ALineSeg->NextLineSeg)
    {
      x = Poly->Position.x + ALineSeg->i * Poly->XAxis.x + ALineSeg->j * Poly->YAxis.x;
      y = Poly->Position.y + ALineSeg->i * Poly->XAxis.y + ALineSeg->j * Poly->YAxis.y;
      z = Poly->Position.z + ALineSeg->i * Poly->XAxis.z + ALineSeg->j * Poly->YAxis.z;
      if (x < Poly->BoundA.x) Poly->BoundA.x = x;
      if (y < Poly->BoundA.y) Poly->BoundA.y = y;
      if (z < Poly->BoundA.z) Poly->BoundA.z = z;
      if (x > Poly->BoundB.x) Poly->BoundB.x = x;
      if (y > Poly->BoundB.y) Poly->BoundB.y = y;
      if (z > Poly->BoundB.z) Poly->BoundB.z = z;
    }
  }

  if (DEBUG == ON)
  {
    PolPrint(Poly);
  }
  return(Poly);
}

void PolPrint(Pol *Poly)
{
  printf("Polygon with position at (" FltFmt ", " FltFmt ", " FltFmt ") and normal of (" FltFmt ", " FltFmt ", " FltFmt ")\n",
   Poly->Position.x,
   Poly->Position.y,
   Poly->Position.z,
   Poly->Normal.x,
   Poly->Normal.y,
   Poly->Normal.z);
}

ItemEntry *PolIntersect(ray *Ray, Object *AnObj)
{
  ItemEntry *Ans=CreateList();
  Pol *Poly;
  IntRec *AnInt=NULL;
  Flt t1, answer, u, v, x;
  Flt x1, y1, x2, y2;
  Vec Normal, Sub1, Sub2;
  int Wind;
  enum IntDirn Dirn;
  Contour *ACont;
  LineSeg *ALine, *HeadLine;

  Poly = (Pol *)AnObj->kdata;
  Normal = Poly->Normal;
  t1 = -VecDot(&(Poly->Normal), &(Ray->Direction));
  if (fabs(t1) > EFFECTIVE_ZERO)
  {
    if (t1 < EFFECTIVE_ZERO)
    {
      VecNegate(&Normal, &Normal);
      Dirn = EXIT;
    }
    else
    {
      Dirn = ENTRY;
    }
    VecSub(&(Ray->Origin), &(Poly->Position),&Sub1);
    answer = VecDot(&(Poly->Normal), &Sub1) / t1;
    if (answer <= 1000000.0)
    {
      VecAdds(answer, &(Ray->Direction), &(Ray->Origin), &Sub1);
      PolInverse(&Sub1, Poly, &v, &u, &Sub2);
      Wind = 0;
      /* Ok Winding Number is initialised and u,v is generated */
      /* so now go on and do the winding */

      for (ACont = Poly->HeadCont; ACont != NULL; ACont = ACont->NextCont)
      {
        for (ALine = ACont->HeadSeg, HeadLine=ACont->HeadSeg; ALine != NULL; ALine = ALine->NextLineSeg)
        {
          y1 = ALine->j - v;
          x1 = ALine->i - u;
          if (ALine->NextLineSeg == NULL)
          {
            y2 = HeadLine->j - v;
            x2 = HeadLine->i - u;
          }
          else
          {
            y2 = ALine->NextLineSeg->j - v;
            x2 = ALine->NextLineSeg->i - u;
          }

          if (y1 * y2 <= 0.0)
          {
            x = x1 + ((x2 - x1) * fabs(y1/(y2-y1)));
            if (x >= 0.0)
            {
              Wind++;
            }
          }
        }
      }
      if ((Wind & 1) == 1)
      {
        AnInt = AllocInt();
        VecAdds(answer, &(Ray->Direction), &(Ray->Origin), &(AnInt->ModelHit));
        AnInt->Dist = answer;
        AnInt->Dirn = Dirn;
        AnInt->HitObj   = AnObj;
        AnInt->WorldHit = AnInt->ModelHit;
        LinkInToList(AnInt, Ans, AnInt->Parent);
      }
    }
  }
  return (Ans);
}

Vec *PolNormal(IntRec *A, Pol *Poly)
{
  A->Normal = Poly->Normal;
  return (&(A->Normal));
}

Vec *PolInverse(IntRec *B, Pol *Poly, Flt *u, Flt *v, Vec *Ans)
{
  Vec *A;
  Vec Sub1;

  A = &(B->ModelHit);

  VecSub(A, &(Poly->Position),&Sub1);
  *u = VecDot(&(Poly->YAxis), &Sub1);
  *v = VecDot(&(Poly->XAxis), &Sub1);
  return(A);
}

Box *PolBound(Pol *Poly, Box *Bound)
{
  Bound->A.x = Poly->BoundA.x;
  Bound->A.y = Poly->BoundA.y;
  Bound->A.z = Poly->BoundA.z;
  Bound->B.x = Poly->BoundB.x;
  Bound->B.y = Poly->BoundB.y;
  Bound->B.z = Poly->BoundB.z;
  return(Bound);
}

int PolInOut(Pol *Poly, Vec *A)
{
  Vec Sub1;
  VecSub(A, &(Poly->Position),&Sub1);
  if (VecDot(&(Poly->Normal), &Sub1) > 0.0)
  {
    return 0;
  }
  else
  {
    return -1;
  }
}

Vec *PolCentre(Pol *Poly, Vec *Cen)
{
  *Cen = Poly->Position;
  return(Cen);
}

Contour *ReadContour(FILE *inputfile)
{
  LineSeg *TailSeg;
  Contour *ThisCont;
  char var[20];
  ThisCont = AllocHeap(sizeof(Contour));
  ThisCont->HeadSeg = NULL;
  ThisCont->NextCont = NULL;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "FROM") == ZERO)
    {
      ThisCont->HeadSeg = ReadLineSeg(inputfile);
      TailSeg = ThisCont->HeadSeg;
    }
    else if (strcmp(var, "TO") == ZERO)
    {
      if (ThisCont->HeadSeg == NULL)
      {
	printf("ERROR -- Attempting TO with no prior FROM in Poly-Contour\n");
	exit(1);
      }
      else
      {
	TailSeg->NextLineSeg = ReadLineSeg(inputfile);
	TailSeg = TailSeg->NextLineSeg;
      }
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Poly-Contour\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);

  if (TailSeg == ThisCont->HeadSeg)
  {
    printf("ERROR -- Must use at least a FROM - TO pair to form a Contour\n");
    exit(1);
  }
  return(ThisCont);


}

LineSeg *ReadLineSeg(FILE *inputfile)
{
  LineSeg *ThisSeg;
  ThisSeg = AllocHeap(sizeof(LineSeg));
  ThisSeg->NextLineSeg = NULL;
  fscanf(inputfile, " ( " FltFmt " " FltFmt " ) ", &(ThisSeg->i), &(ThisSeg->j));
  return(ThisSeg);
}
