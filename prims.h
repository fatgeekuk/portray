typedef struct Sph
{
  Vec Centre;
  Flt Radius, RadSq, CentDot;
  Vec Equator, Pole, VP;
} Sph;


/***************************************
*** Types used by Implicit Functions  ***
 ***************************************/

typedef struct ifLinear
{
  Vec Origin;
  Vec Direction;
} ifLinear;

typedef struct ifCombi
{
  ItemEntry *funcList;
} ifCombi;

typedef struct ifNoise
{
  Flt amp;
  Flt freq;
  int terms;
  Flt origin;
} ifNoise;

typedef struct ifTube
{
  Vec From, To;
  Vec Direction;
  Flt Length;
  Flt ZPoint, Height, Pow;
} ifTube;

typedef struct ifTorus
{
	Vec Centre, Axis;
	Flt Size, Diameter, Magnitude;
} ifTorus;

typedef struct ifPolar
{
  Vec Centre;
  Flt ZPoint, Height;
} ifPolar;

typedef struct ifPlanar
{
  Vec Origin;
  Vec Direction;
  Flt width, height;
} ifPlanar;

typedef struct ImpFunc
{
  int funcType;
  void *dataPtr;
  Matrix *transform;
  Flt power, amplitude;
} ImpFunc;

typedef struct Imp
{
  Flt eps, xPoint;
  Object *BoundObj;
  ItemEntry *funcList;
  Box *Bound;
} Imp;

typedef struct Tor
{
  Flt MajRad, MinRada, MinRadb;
  Flt rho, a0, b0;
  Vec Equator;
  Matrix Model, Inverse;
} Tor;

typedef struct Pla
{
  Vec Position, Normal;
  Vec XAxis, YAxis;
} Pla;

typedef struct Dsc
{
  Vec Position, Normal;
  Vec XAxis, YAxis;
  Flt Radius;
} Dsc;

typedef struct Vec2d
{
  Flt x, y;
} Vec2d;

typedef struct LineSeg
{
  struct LineSeg *NextLineSeg;
  Flt i, j;
} LineSeg;

typedef struct Contour
{
  struct Contour *NextCont;
  LineSeg *HeadSeg;
} Contour;

typedef struct Pol
{
  Vec Position, Normal;
  Vec XAxis, YAxis;
  Vec BoundA, BoundB;
  Contour *HeadCont;
} Pol;

typedef struct Tri
{
  Vec A, B, C;
  Vec uAB, uBC, uCA;
  Flt lAB, lBC, lCA;
  Vec nA, nB, nC;
  Vec vAB;
  Vec Normal;
  Vec Centre;
} Tri;

typedef struct Cyl
{
  Vec Start;
  Flt Length, Radius;
  Flt m, c, sint, cost;
  Flt Rad1Sq, Rad2Sq;
  Matrix Model;
  Matrix Inverse;
} Cyl;

typedef struct TriVec
{
  int type;
  struct TriVec *NextTriV;
  Vec ThisOne;
} TriVec;

typedef struct TriSVec
{
  int type;
  struct TriSVec *NextTriV;
  Vec ThisOne;
  Vec PNormal;
} TriSVec;


typedef struct RawTri
{
  struct RawTri *MapTri;
  Flt lAB, lBC, lCA;
  TriVec *A;
  TriVec *B;
  TriVec *C;
  TriVec *N;
  TriVec *uAB, *uBC, *uCA;
} RawTri;

typedef struct Raw
{
  int Smooth;
  struct BSPNode *BSPRoot;
  Box Bounder;
  RawTri *LastHit;
} Raw;

enum CSGOp {UNION, SUBTRACT, COMMON, MERGE};

typedef struct CSG
{
  enum CSGOp ThisOp;
  Object *Op1;
  Object *Op2;
} CSG;

typedef struct Compound
{
  Object *BoundObj;
  ItemEntry *objList;
  Box *Bound;
  Matrix *Model;
  Matrix *Inverse;
} Compound;

