
#define FltStr "%lf"
#define Flt double

typedef struct Vec
{
  Flt x, y, z;
}

typedef struct Colour
{
  Flt red, green, blue;
}
