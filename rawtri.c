/*
  *************************************************************************
 **                                                                       **
 **			 Raw Triangle Section				  **
 **                                                                       **
  *************************************************************************
*/

char   *RTriName();
Raw    *RTriRead();
ItemEntry *RTriIntersect(ray *Ray, Object *AnObj);
Vec    *RTriNormal();
Vec    *RTriInverse();
Box    *RTriBound();
int	RTriInOut();
Vec    *RTriCentre();

void RTBSPBuild(BSPNode *BSPRoot, int Level, enum BSPAxis ThisAxis);
Flt RTIntersect(ray *ARay, BSPNode *BSPRoot, RawTri **LastHit);
void RTBBound(RawTri *ATri, Box *Bound);
void RTBSPGen(BSPNode *BSPRoot);
void RTBSPBound(BSPNode *BSPRoot, Box *ABox);


/**************************************************************************
    There follows some special purpose code particular to this Primative
 **************************************************************************/

TriVec *StoreVec(Vec *APt, Flt Weld, TriVec **TriList, int *Welded)
{
  TriVec *Search, *Found;
  Found = NULL;
  for (Search = *TriList; Search != NULL && !Found; Search = Search->NextTriV)
  {
    if ((Search->type == 1) &&
	fabs(Search->ThisOne.x - APt->x) < Weld &&
	fabs(Search->ThisOne.y - APt->y) < Weld &&
	fabs(Search->ThisOne.z - APt->z) < Weld)
       Found = Search;
  }
  if (Found == NULL)
  {
    Found = malloc(sizeof(TriVec));
    if (Found != NULL)
    {
      Found->ThisOne = *APt;
      Found->type = 1;
      Found->NextTriV = *TriList;
      *TriList = Found;
      *Welded = 0;
    }
    else
    {
      printf("ERROR - Unable to allocate new Triangle Point record, heap exhausted\n");
      exit(1);
    }
  }
  else
    *Welded = 1;
  return(Found);
}

TriSVec *StoreSVec(Vec *APt, Flt Weld, TriVec **TriList, int *Welded)
{
  TriSVec *Search, *Found;
  Found = NULL;
  for (Search = (TriSVec *)*TriList; Search != NULL && !Found; Search = Search->NextTriV)
  {
    if (Search->type == 0 &&
	fabs(Search->ThisOne.x - APt->x) < Weld &&
	fabs(Search->ThisOne.y - APt->y) < Weld &&
	fabs(Search->ThisOne.z - APt->z) < Weld)
	  Found = Search;
  }
  if (Found == NULL)
  {
    Found = malloc(sizeof(TriSVec));
    if (Found != NULL)
    {
      Found->ThisOne = *APt;
      Found->type = 0;
      SetVector(0.0, 0.0, 0.0, &(Found->PNormal));
      Found->NextTriV = (TriSVec *)*TriList;
      *TriList = (TriVec *)Found;
      *Welded = 0;
    }
    else
    {
      printf("ERROR - Unable to allocate new Trinagle Point record, heap exhausted\n");
      exit(1);
    }
  }
  else
    *Welded = 1;

  return(Found);
}


/**************************************************************************
    There follows The Normal Method Routines for the PortRAY implemenation
 **************************************************************************/



void InitRTriangle(Prim *APrim)
{
  APrim->Name	   = RTriName;
  APrim->Read	   = (void *)RTriRead;
  APrim->Print	   = NULL;
  APrim->Intersect = RTriIntersect;
  APrim->Normal    = RTriNormal;
  APrim->Inverse   = RTriInverse;
  APrim->Bound	   = RTriBound;
  APrim->InOut	   = RTriInOut;
  APrim->Centre    = RTriCentre;
}

char *RTriName()
{
  return "RAW";
}

Raw *RTriRead(FILE *inputfile)
{
  int scanned;
  Vec Origin;
  ItemEntry *AList;
  char var[20], filename[30], mapfilename[30];
  FILE *RawFile, *MapFile;
  RawTri *ARawTri;
  TriVec *VecList;
  Raw	 *ARawHdr;
  Flt dist, Weld, XRot, YRot, ZRot;
  int Smooth, Welded, NoOfWelds, Map, Flip;
  Matrix Model, Inverse;
  Vec A, B, C, Tempa, Tempb, Tempc;
  Vec D, uAB, uBC, uCA, Sub1;

  Smooth = 0;
  filename[0] = 0;
  NoOfWelds = 0;
  ARawHdr = AllocHeap(sizeof(Raw));
  ARawHdr->Smooth = 0;
  VecList = NULL;
  XRot = 0.0;
  YRot = 0.0;
  ZRot = 0.0;
  Weld = 0.00001;
  Map  = 0;
  Flip = 0;
  SetVector(0.0, 0.0, 0.0, &Origin);

  /* Process Description Info */
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "FILE") == ZERO)
    {
      fscanf(inputfile, " %s ", filename);
    }
    else if (strcmp(var, "FLIP") == ZERO)
    {
      Flip = 1;
    }
    else if (strcmp(var, "MAP") == ZERO)
    {
      fscanf(inputfile, " %s ", mapfilename);
      Map = 1;
    }
    else if (strcmp(var, "WELD") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Weld);
    }
    else if (strcmp(var, "X-ROTATION") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &XRot);
    }
    else if (strcmp(var, "Y-ROTATION") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &YRot);
    }
		else if (strcmp(var, "Z-ROTATION") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &ZRot);
    }
		else if (strcmp(var, "AT") == ZERO)
    {
      ReadVec(inputfile, &Origin);
    }
    else if (strcmp(var, "SMOOTH") == ZERO)
    {
      Smooth = 1;
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("Unknown Verb (%s) in Scene RAW Primative\n", var);
      exit(1);
		}
  } while (strcmp(var, ";") != ZERO);

  MRotate(XRot*PI/180.0, YRot*PI/180.0, ZRot*PI/180.0, &Model, &Inverse);
  ARawHdr->Smooth = Smooth;
  ARawHdr->BSPRoot = AllocHeap(sizeof(BSPNode));
  ARawHdr->BSPRoot->ObjectList = CreateList();
  ARawHdr->BSPRoot->NoOfObjs = 0;
  /* Load and Process RAW triangle Data */
   /* Load Triangle Data from RAW file */

   if ((RawFile = fopen(filename, "r")) == NULL)
   {
     printf("Unable to Open Raw File Successfully\n");
     exit(1);
   }
   else
   {
     if (Map != 0)
     {
       MapFile = fopen(mapfilename, "r");
       if (MapFile == NULL)
       {
	 printf("Error -- Unable to open map file\n");
	 exit(1);
       }
     }
     printf("Reading Raw File.");
     scanned = 0;
     while (!feof(RawFile) || scanned == EOF)
     {
       scanned = fscanf(RawFile, " " FltFmt " " FltFmt " " FltFmt " " FltFmt " " FltFmt " " FltFmt " " FltFmt " " FltFmt " " FltFmt " ",
				   &(A.x), &(A.y), &(A.z),
				   &(B.x), &(B.y), &(B.z),
				   &(C.x), &(C.y), &(C.z));

       if (scanned == 0)
       {
         printf("Error - Unable to scan input triangle\n");
         exit(1);
       }
       /* Rotate and Translate the model coordinates into World Coordinates */
       MInvertVector(&A, &Model, &A);
       MInvertVector(&B, &Model, &B);
       MInvertVector(&C, &Model, &C);
       VecSub(&A, &Origin, &A);
       VecSub(&B, &Origin, &B);
       VecSub(&C, &Origin, &C);
       printf("."); fflush(stdout);
       VecSub(&A, &B, &Tempa);
       VecSub(&A, &C, &Tempb);
       VecSub(&B, &C, &Tempc);
       if (VecLen(&Tempa) < Weld || VecLen(&Tempb) <Weld || VecLen(&Tempc) < Weld)
       {
	 printf("OUCH, Degenerate Triangle, Discarding");
       }
       else
       {
	 /* Allocate the the space for the triangle and place it in the Header
	    Triangle List */

	 ARawTri = malloc(sizeof(RawTri));
	 if (ARawTri != NULL)
	 {
	   AddToList(ARawTri, ARawHdr->BSPRoot->ObjectList);
	   ARawHdr->BSPRoot->NoOfObjs += 1;

	   /* Now Fill in the Verticies and store away for later use */
	   if (Smooth == 0)
	   {
	     ARawTri->A = StoreVec(&A, Weld, &(VecList), &Welded);
	     NoOfWelds += Welded;
	     ARawTri->B = StoreVec(&B, Weld, &(VecList), &Welded);
	     NoOfWelds += Welded;
	     ARawTri->C = StoreVec(&C, Weld, &(VecList), &Welded);
	     NoOfWelds += Welded;
	   }
	   else
	   {
	     ARawTri->A = (TriVec *)StoreSVec(&A, Weld, &(VecList), &Welded);
	     NoOfWelds += Welded;
	     ARawTri->B = (TriVec *)StoreSVec(&B, Weld, &(VecList), &Welded);
	     NoOfWelds += Welded;
	     ARawTri->C = (TriVec *)StoreSVec(&C, Weld, &(VecList), &Welded);
	     NoOfWelds += Welded;
	   }
	 }
	 else
	 {
	   printf("ERROR - Unable to allocate new Raw Triangle, heap exhausted\n");
	   exit(1);
	 }
	 if (Map != 0)
	 {

       scanned = fscanf(MapFile, " " FltFmt " " FltFmt " " FltFmt " " FltFmt " " FltFmt " " FltFmt " " FltFmt " " FltFmt " " FltFmt " ",
				      &(A.x), &(A.y), &(A.z),
				      &(B.x), &(B.y), &(B.z),
				      &(C.x), &(C.y), &(C.z));
	   ARawTri->MapTri = malloc(sizeof(RawTri));
	   ARawTri = ARawTri->MapTri;
	   ARawTri->A = StoreVec(&A, Weld, &VecList, &Welded);
	   ARawTri->B = StoreVec(&B, Weld, &VecList, &Welded);
	   ARawTri->C = StoreVec(&C, Weld, &VecList, &Welded);
	 }
	 else
	 {
	   ARawTri->MapTri = NULL;
	 }
       }
     }
     fclose(RawFile);
     if (Map != 0) fclose(MapFile);

   }

     /* Build Half Space info and Normals from Triangle Vertices */
     /* Also Build the Bounding Box */

     printf("\nBuilding Normals.");
     for (AList = ARawHdr->BSPRoot->ObjectList->Next; AList->Data != NULL; AList = AList->Next)
     {
       ARawTri = AList->Data;
       A = ARawTri->A->ThisOne;
       B = ARawTri->B->ThisOne;
       C = ARawTri->C->ThisOne;
       printf(".");
       fflush(stdout);
       VecSub(&B, &A, &Sub1);
       VecUnit(&Sub1, &uAB);
       VecSub(&C, &B, &Sub1);
       VecUnit(&Sub1, &uBC);
       VecSub(&A, &C, &Sub1);
       VecUnit(&Sub1, &uCA);

       /* Calculate Triangle->uAB */
       /* find Distance Along AB that is at right-angles to C */
       VecSub(&C, &A, &Sub1);
       dist = VecDot(&uAB, &Sub1);

       /* Generate That Point */
       VecAdds(dist,&uAB, &A, &D);

       /* Generate the unit vector Along the Perpendicular DC */
       VecSub(&C, &D, &Sub1);
       ARawTri->lAB = VecLen(&Sub1);
       VecScalar(1.0 / ARawTri->lAB, &Sub1, &uAB);
       ARawTri->uAB = StoreVec(&uAB, Weld, &(VecList), &Welded);
       NoOfWelds += Welded;


       /* Calculate Triangle->uBC */
       VecSub(&A, &B, &Sub1);
       dist = VecDot(&uBC, &Sub1);
       VecAdds(dist,&uBC, &B, &D);
       VecSub(&A, &D, &Sub1);
       ARawTri->lBC = VecLen(&Sub1);
       VecScalar(1.0 / ARawTri->lBC, &Sub1, &uBC);
       ARawTri->uBC = StoreVec(&uBC, Weld, &(VecList), &Welded);
       NoOfWelds += Welded;

       /* Calculate Triangle->uCA */
       VecSub(&B, &C, &Sub1);
       dist = VecDot(&uCA, &Sub1);
       VecAdds(dist,&uCA, &C, &D);
       VecSub(&B, &D, &Sub1);
       ARawTri->lCA = VecLen(&Sub1);
       VecScalar(1.0/ARawTri->lCA, &Sub1, &uCA);
       ARawTri->uCA = StoreVec(&uCA, Weld, &(VecList), &Welded);
       NoOfWelds += Welded;

       /* Triangle->vAB = uAB; */

       /* Create surface Normal, and store it */

       VecUnit(VecProduct(&uAB, &uCA, &Sub1), &Sub1);
       if (Flip == 1)
       {
         Sub1.x = -Sub1.x;
         Sub1.y = -Sub1.y;
         Sub1.z = -Sub1.z;
       }
       ARawTri->N = StoreVec(&Sub1, Weld, &(VecList), &Welded);
       NoOfWelds += Welded;

       /* If smoothing Required, Add the Current Normal Into the Normal
	  For that point */

       if (Smooth == 1)
       {
          VecAdd(&Sub1,&(((TriSVec *)ARawTri->A)->PNormal),&(((TriSVec *)ARawTri->A)->PNormal));
          VecAdd(&Sub1,&(((TriSVec *)ARawTri->B)->PNormal),&(((TriSVec *)ARawTri->B)->PNormal));
          VecAdd(&Sub1,&(((TriSVec *)ARawTri->C)->PNormal),&(((TriSVec *)ARawTri->C)->PNormal));
       }
     }
     /* Now if it is smoothing, Go through all Point Normals and Normalize */
     if (Smooth == 1)
     {
       for (AList = ARawHdr->BSPRoot->ObjectList->Next; AList->Data != NULL; AList = AList->Next)
       {
          ARawTri = AList->Data;
          VecUnit(&(((TriSVec *)ARawTri->A)->PNormal),&(((TriSVec *)ARawTri->A)->PNormal));
          VecUnit(&(((TriSVec *)ARawTri->B)->PNormal),&(((TriSVec *)ARawTri->B)->PNormal));
          VecUnit(&(((TriSVec *)ARawTri->C)->PNormal),&(((TriSVec *)ARawTri->C)->PNormal));
       }
     }
  /* Now use the SPECIAL BSP Routines to build the BSP tree for this */
  RTBSPGen(ARawHdr->BSPRoot);

  printf("No Of Welds %d\n", NoOfWelds);
  return(ARawHdr);
}

ItemEntry *RTriIntersect(ray *Ray, Object *AnObj)
{
  ItemEntry *Ans = CreateList(), *Ans1;
  IntRec *AnInt;
  RawTri *LastHit;
  Raw *Triangle = (Raw *)AnObj->kdata;
  Flt Hit;
  Hit = RTIntersect(Ray, Triangle->BSPRoot, &LastHit);
  if (Hit > EFFECTIVE_ZERO)
  {
    AnInt = AllocInt();
    VecAdds(Hit, &(Ray->Direction), &(Ray->Origin), &(AnInt->ModelHit));
    AnInt->Dist = Hit;
    AnInt->Dirn = ENTRY;
    AnInt->HitObj = AnObj;
    AnInt->WorldHit = AnInt->ModelHit;
    AnInt->UtilPtr  = (void *)LastHit;

    Ans1 = LinkInToList(AnInt, Ans, AnInt->Parent);
    AnInt = AllocInt();
    VecAdds(Hit + 0.00001, &(Ray->Direction), &(Ray->Origin), &(AnInt->ModelHit));
    AnInt->Dist     = Hit + 0.00001;
    AnInt->Dirn     = EXIT;
    AnInt->HitObj   = AnObj;
    AnInt->WorldHit = AnInt->ModelHit;
    AnInt->UtilPtr  = (void *)LastHit;

    LinkInToList(AnInt, Ans1, AnInt->Parent);
  }

  return(Ans);
}

Vec *RTriNormal(IntRec *A, Raw *Triangle)
{
  Flt u, v, w;
  Vec Sub1;

  if (Triangle->Smooth == 1)
  {
    VecSub(&(A->ModelHit), &(((RawTri *)(A->UtilPtr))->A->ThisOne),&Sub1);
    u = VecDot(&(((RawTri *)(A->UtilPtr))->uAB->ThisOne), &Sub1) / ((RawTri *)(A->UtilPtr))->lAB;
    VecSub(&(A->ModelHit), &(((RawTri *)(A->UtilPtr))->B->ThisOne),&Sub1);
    v = VecDot(&(((RawTri *)(A->UtilPtr))->uBC->ThisOne), &Sub1) / ((RawTri *)(A->UtilPtr))->lBC;
    VecSub(&(A->ModelHit), &(((RawTri *)(A->UtilPtr))->C->ThisOne),&Sub1);
    w = VecDot(&(((RawTri *)(A->UtilPtr))->uCA->ThisOne), &Sub1) / ((RawTri *)(A->UtilPtr))->lCA;
    SetVector(0.0, 0.0, 0.0, &(A->Normal));
    VecAdds(v, &(((TriSVec *)((RawTri *)(A->UtilPtr))->A)->PNormal), &(A->Normal), &(A->Normal));
    VecAdds(w, &(((TriSVec *)((RawTri *)(A->UtilPtr))->B)->PNormal), &(A->Normal), &(A->Normal));
    VecAdds(u, &(((TriSVec *)((RawTri *)(A->UtilPtr))->C)->PNormal), &(A->Normal), &(A->Normal));
    VecUnit(&(A->Normal), &(A->Normal));
  }
  else
    A->Normal=((RawTri *)(A->UtilPtr))->N->ThisOne;

  return (&(A->Normal));
}

Vec *RTriInverse(IntRec *A, Raw *Triangle, Flt *u, Flt *v, Vec *Ans)
{
  Flt a, b, c;
  Vec AnAns;
  Vec Sub1;
  SetVector(0.0, 0.0, 0.0, &AnAns);
  if (((RawTri *)(A->UtilPtr))->MapTri !=  NULL)
  {
    VecSub(&(A->ModelHit), &(((RawTri *)(A->UtilPtr))->A->ThisOne),&Sub1);
    a = VecDot(&(((RawTri *)(A->UtilPtr))->uAB->ThisOne), &Sub1) / ((RawTri *)(A->UtilPtr))->lAB;
    VecSub(&(A->ModelHit), &(((RawTri *)(A->UtilPtr))->B->ThisOne),&Sub1);
    b = VecDot(&(((RawTri *)(A->UtilPtr))->uBC->ThisOne), &Sub1) / ((RawTri *)(A->UtilPtr))->lBC;
    VecSub(&(A->ModelHit), &(((RawTri *)(A->UtilPtr))->C->ThisOne),&Sub1);
    c = VecDot(&(((RawTri *)(A->UtilPtr))->uCA->ThisOne), &Sub1) / ((RawTri *)(A->UtilPtr))->lCA;
    VecAdds(b, &((((RawTri *)(A->UtilPtr))->MapTri)->A->ThisOne), &AnAns, &AnAns);
    VecAdds(c, &((((RawTri *)(A->UtilPtr))->MapTri)->B->ThisOne), &AnAns, &AnAns);
    VecAdds(a, &((((RawTri *)(A->UtilPtr))->MapTri)->C->ThisOne), &AnAns, &AnAns);
  }
  *u = AnAns.x;
  *v = AnAns.z;
  return(Ans);
}

Box *RTriBound(Raw *Triangle, Box *Bound)
{

  *Bound = Triangle->BSPRoot->Bounder;
  return(Bound);
}

int RTriInOut(Raw *Triangle, Vec *A)
{
    return 0;
}

Vec *RTriCentre(Raw *Triangle, Vec *Cen)
{
  Cen->x = 0.0;
  Cen->y = 0.0;
  Cen->z = 0.0;
  return(Cen);
}

#define BSPMaxDepth 20

void BSPBuild(BSPNode *BSPRoot, int Level, enum BSPAxis ThisAxis);
void BSPBound(BSPNode *BSPRoot, Box *ABox);
int BSPInside(Box *ABox, Box ABound);


void RTBSPGen(BSPNode *BSPRoot)
{
  enum BSPAxis Axis;
  Axis = XAxis;
  RTBSPBuild(BSPRoot, 0, Axis);

}

void RTBSPBuild(BSPNode *BSPRoot, int Level, enum BSPAxis ThisAxis)
{
  ItemEntry *AList, *NextItem, *ChildList0, *ChildList1;
  enum BSPAxis NextAxis;
  Box ABound, Child0, Child1;
  RawTri *ThisTri;
  int	 ChildCnt0=0, ChildCnt1=0;
  Flt MidPt;
  ChildList0 = NULL;
  ChildList1 = NULL;
  NextAxis = XAxis;
  printf("BSP Generate for Level %d\n", Level);
  printf("Original No Of Objects %d\n", BSPRoot->NoOfObjs);
  ChildList0 = CreateList();
  ChildList1 = CreateList();

  BSPRoot->Child0 = NULL;
  BSPRoot->Child1 = NULL;
  RTBSPBound(BSPRoot, &(BSPRoot->Bounder));

  if (Level < BSPMaxDepth && BSPRoot->NoOfObjs > 4)
  {
    /* Build Sub-Bounds */
    Child0 = BSPRoot->Bounder;
    Child1 = BSPRoot->Bounder;
    if (ThisAxis == XAxis)
    {
      MidPt = (BSPRoot->Bounder.A.x + BSPRoot->Bounder.B.x) / 2.0;
      Child0.B.x = MidPt;
      Child1.A.x = MidPt;
      NextAxis = YAxis;
    }
    else if (ThisAxis == YAxis)
    {
      MidPt = (BSPRoot->Bounder.A.y + BSPRoot->Bounder.B.y) / 2.0;
      Child0.B.y = MidPt;
      Child1.A.y = MidPt;
      NextAxis = ZAxis;
    }
    else if (ThisAxis == ZAxis)
    {
      MidPt = (BSPRoot->Bounder.A.z + BSPRoot->Bounder.B.z) / 2.0;
      Child0.B.z = MidPt;
      Child1.A.z = MidPt;
      NextAxis = XAxis;
    }

    /* Share out any objects that are Wholly within either Bounding Box */

    AList = BSPRoot->ObjectList->Next;
    while (AList->Data != NULL)
    {
      ThisTri = AList->Data;
      NextItem = AList->Next;
      RTBBound(ThisTri, &ABound);
      if (BSPInside(&ABound, Child0) != 0)
      {
	RemoveFromList(AList);
	AddToList(ThisTri, ChildList0);
	BSPRoot->NoOfObjs -= 1;
	ChildCnt0 += 1;
      }
      else if (BSPInside(&ABound, Child1) != 0)
      {
	RemoveFromList(AList);
	AddToList(ThisTri, ChildList1);
	BSPRoot->NoOfObjs -= 1;
	ChildCnt1 += 1;
      }
      AList = NextItem;
    }
    if (ChildCnt0 > 0)
    {
      printf("Assigning %d objects to Child0\n", ChildCnt0);
      BSPRoot->Child0 = AllocHeap(sizeof(BSPNode));
      BSPRoot->Child0->NoOfObjs	  = ChildCnt0;
      BSPRoot->Child0->Child0	  = NULL;
      BSPRoot->Child0->Child1	  = NULL;
      BSPRoot->Child0->ObjectList = ChildList0;
      RTBSPBuild(BSPRoot->Child0, Level + 1, NextAxis);
    }
    else
    {
      DestroyList(ChildList0);
    }
    if (ChildCnt1 > 0)
    {
      printf("Assigning %d objects to Child1\n", ChildCnt1);
      BSPRoot->Child1 = AllocHeap(sizeof(BSPNode));
      BSPRoot->Child1->NoOfObjs	  = ChildCnt1;
      BSPRoot->Child1->Child0	  = NULL;
      BSPRoot->Child1->Child1	  = NULL;
      BSPRoot->Child1->ObjectList = ChildList1;
      RTBSPBuild(BSPRoot->Child1, Level + 1, NextAxis);
    }
    else
    {
      DestroyList(ChildList1);
    }

  }
  printf("Final No Of Objects at this level %d\n", BSPRoot->NoOfObjs);
}

void RTBSPBound(BSPNode *BSPRoot, Box *ABox)
{
  ItemEntry *AnItem;
  Box bnder;
  RawTri *ATri;
  AnItem = BSPRoot->ObjectList->Next;
  ATri = AnItem->Data;
  RTBBound(ATri, ABox);
  for (AnItem = AnItem->Next; AnItem->Data != NULL; AnItem = AnItem->Next)
  {
    ATri = AnItem->Data;
    RTBBound(ATri, &bnder);
    CombineBound(ABox,&bnder, ABox);
  }
  if (BSPRoot->Child1 != NULL)
  {
    CombineBound(ABox,&(BSPRoot->Child1->Bounder),ABox);
  }

  if (BSPRoot->Child0 != NULL)
  {
    CombineBound(ABox,&(BSPRoot->Child0->Bounder),ABox);
  }
}

void RTBBound(RawTri *ATri, Box *Bound)
{
  Bound->A = ATri->A->ThisOne;
  if (ATri->B->ThisOne.x < Bound->A.x) Bound->A.x = ATri->B->ThisOne.x;
  if (ATri->B->ThisOne.y < Bound->A.y) Bound->A.y = ATri->B->ThisOne.y;
  if (ATri->B->ThisOne.z < Bound->A.z) Bound->A.z = ATri->B->ThisOne.z;
  if (ATri->C->ThisOne.x < Bound->A.x) Bound->A.x = ATri->C->ThisOne.x;
  if (ATri->C->ThisOne.y < Bound->A.y) Bound->A.y = ATri->C->ThisOne.y;
  if (ATri->C->ThisOne.z < Bound->A.z) Bound->A.z = ATri->C->ThisOne.z;

  Bound->B = ATri->A->ThisOne;
  if (ATri->B->ThisOne.x > Bound->B.x) Bound->B.x = ATri->B->ThisOne.x;
  if (ATri->B->ThisOne.y > Bound->B.y) Bound->B.y = ATri->B->ThisOne.y;
  if (ATri->B->ThisOne.z > Bound->B.z) Bound->B.z = ATri->B->ThisOne.z;
  if (ATri->C->ThisOne.x > Bound->B.x) Bound->B.x = ATri->C->ThisOne.x;
  if (ATri->C->ThisOne.y > Bound->B.y) Bound->B.y = ATri->C->ThisOne.y;
  if (ATri->C->ThisOne.z > Bound->B.z) Bound->B.z = ATri->C->ThisOne.z;

}

Flt RTIntersect(ray *ARay, BSPNode *BSPRoot, RawTri **LastHit)
{
  ItemEntry *AnItem;
  Vec Normal, Sub1, A;
  int BoundHit;
  RawTri *ATri, *ATri2;
  Flt Intersection, GoodInt, answer, t1, u, v, w, Dist;
  GoodInt = -1.0;
  BoundHit = BoxHit(ARay, &(BSPRoot->Bounder), &Dist);
  if (BoundHit)
  {
    for (AnItem = BSPRoot->ObjectList->Next; AnItem->Data != NULL; AnItem = AnItem->Next)
    {
      ATri = AnItem->Data;
      Normal = ATri->N->ThisOne;
      t1 = -VecDot(&Normal, &(ARay->Direction));
      if (t1 < EFFECTIVE_ZERO)
      {
         VecNegate(&Normal, &Normal);
         VecDot(&Normal, &(ARay->Direction));
         t1 = -t1;
      }

      VecSub(&(ARay->Origin), &(ATri->A->ThisOne), &Sub1);
      answer = VecDot(&Normal, &Sub1) / t1;

      if (answer < EFFECTIVE_ZERO || (answer > GoodInt && GoodInt > 0.0))
         answer = -1.0;
      else
      {
        VecAdds(answer, &(ARay->Direction), &(ARay->Origin), &A);
        VecSub(&A, &(ATri->A->ThisOne),&Sub1);
        u = VecDot(&(ATri->uAB->ThisOne), &Sub1);

        if (u < 0.0)
          answer = -1.0;
        else
        {
          VecSub(&A, &(ATri->B->ThisOne),&Sub1);
          v = VecDot(&(ATri->uBC->ThisOne), &Sub1);
          if (v < 0.0)
            answer = -1.0;
          else
          {
            VecSub(&A, &(ATri->C->ThisOne),&Sub1);
            w = VecDot(&(ATri->uCA->ThisOne), &Sub1);
            if (w < 0.0)
              answer = -1.0;
          }
        }
      }
      if (answer > 0.0 && (answer < GoodInt || GoodInt < 0.0))
      {
        GoodInt = answer;
        *LastHit = ATri;
      }
    }
    if (BSPRoot->Child0 != NULL)
    {
      Intersection = RTIntersect(ARay, BSPRoot->Child0, &ATri2);
      if (Intersection > 0.0 && (Intersection < GoodInt || GoodInt < 0.0))
      {
        GoodInt = Intersection;
        *LastHit = ATri2;
      }
    }
    if (BSPRoot->Child1 != NULL)
    {
      Intersection = RTIntersect(ARay, BSPRoot->Child1, &ATri2);
      if (Intersection > 0.0 && (Intersection < GoodInt || GoodInt < 0.0))
      {
        GoodInt = Intersection;
        *LastHit = ATri2;
      }
    }
  }
  return(GoodInt);
}
