/*
  *************************************************************************
 **                                                                       **
 **       Renderer                                                        **
 **           Structure definitions                                       **
 **                                                                       **
 **                                                                       **
  *************************************************************************
*/
/*
#include <malloc.h>

*/
#include <stdlib.h>
#include <math.h>
#include "matrix.h"

#define VERSION "v0.17"
#define NOOFLEXEMES 20
#define IN 1
#define OUT 0
#define OK 1
#define NOK 0
#define YES 1
#define NO 0
#define ON 1
#define OFF 0
#define TRUE 1
#define FALSE 0
#define EFFECTIVE_ZERO 0.0001
#define RECURSE_LEVEL 8
#define VALUE 0
#define PROC 1
#define ZERO 0
#define PI 3.14159253589

#ifdef __BORLANDC__
   #define _kbhit() kbhit()
   #define _getch() getch()
#else
   #define strupr(n) _strupr(n)
#endif
void strupr(char *astring);
typedef struct Comp
{
  Matrix Model;
  Matrix Inverse;
} Comp;

typedef struct ColDefn
{
  struct ColDefn *NextCol;
  colour Value;
  char *Name;
} ColDefn;

typedef struct ItemEntry
{
    struct ItemEntry *Prev;
    struct ItemEntry *Next;
    void *Data;
} ItemEntry;

typedef struct MatDef
{
  CFlt           kri;
  CFlt           kdiff;
  CFlt           kspec;
  CFlt           kspecpow;
  CFlt           ktran;
  CFlt           kref;
  CFlt           Attenuate;
  colour         SpecCol;
  colour         DiffCol;
  colour         TranCol;
  colour         AmbCol;
  char           Dust;
  char           World;
  Flt            GlossSpread;
  int            GlossSamples;
  Flt            DiffSpread;
  int            DiffSamples;
  struct MatDef *(*ShaderEval) ();
  void          *ShaderData;
  ItemEntry     *Modifiers;
} MatDef;

typedef struct Box
{
  Vec A, B;
  Vec Centre;
  Flt XArea, YArea, ZArea, Area;
} Box;

enum BSPAxis {XAxis, YAxis, ZAxis};
enum IntDirn {ENTRY=0, EXIT, NONE};

typedef struct IntRec
{
  ItemEntry     *Parent;
  enum IntDirn   Dirn;
  Flt            Dist;
  Flt            u, v;
  Vec            ModelHit;
  Vec		 WorldHit;
  Vec		 Normal;
  struct Object *HitObj;
  void          *UtilPtr;
  MatDef        *Material;
  Vec            MapCoord;
  long int       DoneMap : 1;
  long int       DoneMap2 : 1;
  long int       DoneNorm : 1;

} IntRec;


typedef struct
{
  int ResultTarget;
  void (*ShaderValue)(void *ShaderData, IntRec *AnInt, colour *result);
  void *ShaderData;
} ModRec;

typedef struct Prim
{
  struct Prim *NextPrim;
  char	    *(*Name)	  ();
  void	     (*Print)	  ();
  void	    *(*Read)	  ();
  ItemEntry *(*Intersect) (ray *Ray, struct Object *AnObj);
  Vec	    *(*Normal)	  ();
  Vec	    *(*Inverse)	  (IntRec *B, void *pdata, Flt *u, Flt *v, Vec *Ans);
  Box	    *(*Bound)	  ();
  int	     (*InOut)	  ();
  Vec	    *(*Centre)	  ();
  Vec	    *(*Random)	  ();
  Flt        (*Area)      ();
} Prim;

typedef struct
{
  colour aColour;
  Flt    u, v;
} IlluminEntry;

typedef struct IlluminQTree
{
  Flt cu, cv;
  int Hits;
  ItemEntry *Base;
  struct IlluminQTree *TL;
  struct IlluminQTree *TR;
  struct IlluminQTree *BL;
  struct IlluminQTree *BR;
} IlluminQTree;

typedef struct
{
  Flt Area;
  int Hits;
  IlluminQTree *Tree;
} IlluminParam;

typedef struct Object
{
  /* struct Object *NextObject; */
  ItemEntry *List;
  Prim      *Primative;
  void      *kdata;
  MatDef    *Material;
  Matrix    *Model;
  Matrix    *Inverse;
  ItemEntry *IlluminList;
 /*  char       ObjectType; */
  long int   ObjType : 2;
  long int   MapHere : 2;
} Object;

typedef struct BSPNode
{
  int NoOfObjs;
  Box Bounder;
  ItemEntry *ObjectList;
  struct BSPNode *Child0;
  struct BSPNode *Child1;
} BSPNode;

/* enum BSPAxis {XAxis, YAxis, ZAxis}; */

#include "prims.h"
#include "shads.h"


typedef struct Proc
{
  struct Proc *NextProc;
  char    *(*Name)  ();
  MatDef  *(*Eval)  ();
  void     (*Value) (void *Data, IntRec *AnInt, colour *Answer);
  void	  *(*Read)  ();
} Proc;

typedef struct NameList
{
  struct NameList *NextName;
  char *NameText;
  void *ValuePtr;
} NameList;

typedef struct Defin
{
  struct Defin *NextDefinition;
  BSPNode *BSPRoot;
  char Name[20];
} Defin;

typedef struct LightInfo
{
  Vec XAxis, YAxis;
  Flt DepthMap[101][101];
  Object *TheCyl;
} LightInfo;

typedef struct Light
{
  struct Light *NextLight;
  colour LColour;
  CFlt    FallOff;
  point  LPosition;
  Vec    Direction;
  Flt    Angle, HotSpot, Radius;
  Vec    PointAt;
  int	 Shadows;
  int	 Samples;
  int	 MinSamples;
  int    Spot;
  colour Occlusion;
  colour DiffLight;
  Prim	 *Primative;
  Object *LastShadow;
  void	 *kdata;

  LightInfo *TheInfo;
} Light;


typedef struct Filter
{
  struct Filter *NextFilter;
  char	 *(*Name)();
  colour *(*Exec)();
  void	 *(*Read)();
} Filter;

#include "filters.h"
#include "globals.h"

extern long VAddCnt, VLenCnt, VDotCnt, VSubCnt, VCombCnt, VAddsCnt, VScalCnt, VUnitCnt, VProdCnt, VNegCnt, CacheHit, CacheMiss, CacheTot, ItemCount;
extern long RaysPerPixel;

/* void strupr(); */

int ReadGlobals(char *command, FILE *inputfile);
