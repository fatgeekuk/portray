#include "vectors.h"
#include <stdio.h>
#include "rend_0.h"
#include <stdlib.h>
#include <string.h>
#include "rend_2.h"
#include "rend_3.h"
#include "rend_4.h"
#include "rend_6.h"
#include "rend_8.h"

int main(int argc, char *argv[])
{
  char scenefilename[40];
  char camerafilename[40];
  char outputfilename[60];
  char *s;
  DEBUG = OFF;
  printf("PortRAY " VERSION "\n");
  printf("  Copyright Sigma Solutions, 1993, 1994, 1995, 1997.\n");
  if (argc < 3 || argc > 4)
  {
    printf("ERROR -- Incorrect No of parameters\n");
    printf("       PORTRAY camerafile scenefile outputfile\n");
    printf("    or PORTRAY scenefile outputfile\n");
    exit(1);
  }
  else
  {
    VAddCnt=0;
    VLenCnt=0;
    VDotCnt=0;
    VSubCnt=0;
    VCombCnt=0;
    VAddsCnt=0;
    VScalCnt=0;
    VUnitCnt=0;
    VProdCnt=0;
    VNegCnt=0;
    CacheHit=0;
    CacheMiss=0;
    CacheTot=0;
    RaysPerPixel = 0;
    InitDataStorage((unsigned int)10000);
    SetColour(0.0, 0.0, 0.0, &Background);
    CreateDefMaterial(&DefaultMaterial);

    if (argc == 3)
    {
      strcpy(scenefilename, argv[1]);
    }
    else
    {
      strcpy(scenefilename, argv[2]);
      strcpy(camerafilename, argv[1]);
      ReadCameraFile(camerafilename);
    }
    strcpy(SceneFName, scenefilename);
    ReadSceneFile(scenefilename);
    BSPGen(BSPRoot);
    s = getenv("IMAGEDIR");
    if (s != NULL)
    {
      strcpy(outputfilename, s);
      strcat(outputfilename, "\\");
      if (argc==3)
      {
        strcat(outputfilename, argv[2]);
      }
      else
      {
        strcat(outputfilename, argv[3]);
      }
    }
    else
    {
      if (argc==3)
	strcpy(outputfilename, argv[2]);
      else
	strcpy(outputfilename, argv[3]);
    }
    RenderScene(outputfilename);
    printf("Image (%s) Completed\n", outputfilename);
  }
  return(0);
}
