
#define STEP 0.05

void FRenderScene(char *outputfilename)
{
  fpos_t LinePos, CurrPos;
  FILE *StatsFile;
  clock_t timer1, timer2;
  long RayCount=0, lx, ly;
  char drive[_MAX_DRIVE * 30];
  char dir[_MAX_DIR];
  char file[_MAX_FNAME];
  char ext[_MAX_EXT];
  IntRec *AnInt;
  int i, Counter;
  Light *ALight;
  Matrix AM, SpotMatrix, SpotInverse;
  Vec A, B, C, SpotPoint, SpotUp, SpotLeft, SpotFront, SpotOrig;
  ray SRay, LRay;
  ItemEntry *LightInts, *NextInt;
  int Count=1, LineCounter, vstepi;
  float Theta, Chi, u, v, w, vrad, step, vstep, vstepdeg;
  float MaxAngle, ThetaRad, SpotScale;
  colour LightColour;
  int LCount, Li;

  fprintf(stderr,"Entered FRender\n"); fflush(stdout);
  LineNumber=0;
  NoOfInts=0;
  HitInts=0;
  HitBounds=0;
  MissBounds=0;
  WasteHBounds=0;
  RaysCast=0;
  RCacheHits=0;
  RCacheMiss=0;
  NoOfDurns=0;
  MaxHashDepth=0;
  Durn1=0;
  Durn2=0;
  Durn3=0;
  Durn4=0;
  Durn5=0;
  LastLine = clock();

  StartTime = time(NULL);
  StartTicks = clock();

  /* Create Camera Matrix */
  MIdentity(&AM);
  VecUnit(&CameraXSide, &A);
  VecUnit(&CameraYSide, &B);
  VecSub(&CameraPinhole, &CameraOrigin, &C);
  VecUnit(&C, &C);
  MAxis(&A, &B, &C, &AM, &CameraMatrix);  


  for (ALight = Lights, Li=1; ALight != NULL; ALight = ALight->NextLight, Li++)
  {
    /* Cast Ray Toward Camera */
    CastRayToCamera(&(ALight->LPosition), &(ALight->LColour));
    if (ALight->Spot == 1)
    {
      MaxAngle = 180 * ALight->Angle / PI;
      VecNegate(&(ALight->Direction), &SpotPoint);
      SetVector(1.0, 1.0, 1.0, &SpotLeft)
      VecProduct(&SpotPoint, &SpotLeft, &SpotUp);
      if (VecLen(&SpotUp) > 0.001)
      {
        VecProduct(&SpotPoint, &SpotUp, &SpotLeft);

      }
      else
      {
        SetVector(-1.0, 1.0, 1.0, &SpotLeft);
        VecProduct(&SpotPoint, &SpotLeft, &SpotUp);
        VecProduct(&SpotPoint, &SpotUp, &SpotLeft);
      }
      VecUnit(&SpotUp, &SpotUp);
      VecUnit(&SpotLeft, &SpotLeft);
      SetVector(0.0, 0.0, 0.0, &SpotOrig);
      MAxis(&SpotPoint, &SpotLeft, &SpotOrig, &SpotMatrix, &SpotInverse); 
    }
    else
       MaxAngle = 180.0;

    /* Ok, we are going to cast lots of rays from the lightsources */
    /* Generate the Sphere for Light Directions */
    LRay.Origin = ALight->LPosition;
    Counter = 0;
    LineCounter = 0;
    for (Theta = 0.0; Theta <= MaxAngle; Theta += STEP)
    {
      ThetaRad = (Theta*PI)/180.0;
      u = cos(ThetaRad);
      step = (2.0 * PI)/((1.0/STEP)*360.0);
      vrad = sin((Theta*PI)/180.0);
      vstep = (vrad * 2.0 * PI)/step;
      if (vstep == 0) vstep = 1;
      vstepi = vstep;
      vstepdeg = 360.0 / vstepi;
      LineCounter += Counter;
      Counter = 0;
      fprintf(stderr, "{%lf}", Theta);
      for (Chi = 0.0; Chi < 360.0; Chi += vstepdeg)
      {
        Counter++;
        v = sin((Chi*PI)/180.0) * vrad;
        w = cos((Chi*PI)/180.0) * vrad;
        /* if (Count++ == 1000)
        {
          fprintf(stderr,"[%d]", i); fflush(stdout);
          Count = 1;
        } */
        /* Create a random ray to shoot */
        /* RandomNormal(&(LRay.Direction)); */
        LRay.Direction.x = u;
        LRay.Direction.y = v;
        LRay.Direction.z = w;
        if (ALight->Spot)
        {
          MInvertVector(&(LRay.Direction), &SpotInverse, &(LRay.Direction));
          if (ThetaRad < ALight->HotSpot)
            SpotScale = 1.0;
          else if (ThetaRad > ALight->Angle)
            SpotScale = 0.0;
          else
            SpotScale = 1.0 - pow((ThetaRad - ALight->HotSpot) / (ALight->Angle - ALight->HotSpot), 2.0);
          
          ColourShade(SpotScale, &(ALight->LColour), &LightColour);
          FTrace(&LRay, 0, 1.0, &LightColour);                                  
        }
        else
          FTrace(&LRay,0, 1.0, &(ALight->LColour));
      }
    }
  }
  fclose(OutputFile);
  fprintf(stderr,"Completed.\n");
}

