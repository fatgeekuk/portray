/*
  *************************************************************************
 **                                                                       **
 **       Renderer                                                        **
 **           Camera File read module                                     **
 **                                                                       **
 **                                                                       **
  *************************************************************************
*/

#include <stdio.h>
#include <string.h>
#include "vectors.h"
#include "rend_0.h"
#include "rend_3.h"
#include "rend_4.h"

void ReadCameraInfo(FILE *inputfile);
void ReadScreenInfo(FILE *inputfile);

int REFLECTIONS=YES;
int SHADOWS=YES;

enum COMMCODE
  {
    INVALIDCOMMAND=1,
    LIGHT,
    CAMERA,
    IMAGE,
    REMARK,
    COLOUR,
    BLANK
  };

char commands[][10]
={
   "*",
   "LIGHT",
   "CAMERA",
   "IMAGE",
   "~",
   "COLOUR",
   "",
   "-*"
 };

Vec CameraPinhole={0.0, 0.0, -1.0};
Vec CameraOrigin={0.0, 0.0, 0.0};
Vec CameraXSide={1.0, 0.0, 0.0};
Vec CameraYSide={0.0, 1.0, 0.0};
Vec BGUp={0.0, 1.0, 0.0};
unsigned int ImageXSize=320;
unsigned int ImageYSize=240;
char Stopable;
colour Background;
colour BackHorizon;
int BackFade=0;
int CalcDiffuse=0;

void ReadCameraFile(char *inputfilename)
{

  FILE *inputfile;
  Light *Alight;
  char command[20];
  enum COMMCODE commandcode;
  int i;
  printf("Attempting to open Camera file : %s ", inputfilename);
  if ((inputfile = fopen(inputfilename, "r")) == ZERO)
  {
    printf("Unable to Open Successfully\n");
    exit(1);
  }
  else
  {
    printf("Opened OK, Reading\n");
    while (!feof(inputfile))
    {
      fscanf(inputfile, "%s", command);
      strupr(command);
      commandcode = INVALIDCOMMAND;
      for (i=0; strcmp(commands[i], "-*") != ZERO; i++)
      {
        if (strcmp(commands[i], command) == ZERO) commandcode = i+1;
      };
      switch(commandcode)
      {
        case(INVALIDCOMMAND) :
        {
          printf("Invalid Command (%s)\n", command);
          break;
        }
        case(LIGHT) :
        {
          Alight = (Light *)ReadLightInfo(inputfile);
          (Alight->NextLight) = Lights;
          Lights = Alight;
          break;
        }
        case(CAMERA) :
        {
          ReadCameraInfo(inputfile);
          break;
        }
        case(IMAGE) :
        {
          ReadScreenInfo(inputfile);
          break;
        }
        case(COLOUR) :
        {
          ReadColourInfo(inputfile);
          break;
        }
        case(REMARK) :
        {
          int ch;
          do
          {
            ch = fgetc(inputfile);
          } while (ch != '~' && !feof(inputfile));
          break;
        }
      }
    }
    printf("completed\n");
    fclose(inputfile);
  }
}

void ReadCameraInfo(FILE *inputfile)
{
  Vec DropLine, LookAt, LineOfSight;
  UseFilt *ThisUFilt;
  Flt XSize=0.02, YSize=0.02, Depth=0.03;

  Vec Sub1, Sub2;
  char varname[20], FiltName[20];
  Filter *AFilt;
  do
  {
    fscanf(inputfile, " %s ",varname);
    strupr(varname);

    if (strcmp("LOOK-AT", varname) == ZERO)
    {
      ReadVec(inputfile, &LookAt);
      if (DEBUG == ON)
      {
        printf("Look At ");
        PrintVec(&LookAt);
        printf("\n");
      }
      else
      {
        printf(".");
      }
    } else if (strcmp("DROP-LINE", varname) == ZERO)
    {
      ReadVec(inputfile, &DropLine);
      if (DEBUG == ON)
      {
        printf("Drop-Line is ");
        PrintVec(&DropLine);
        printf("\n");
      }
      else
      {
        printf(".");
      }
    } else if (strcmp("ORIGIN"   , varname) == ZERO ||
               strcmp("LOOK-FROM", varname) == ZERO)
    {
      ReadVec(inputfile, &CameraOrigin);
      if (DEBUG == ON)
      {
        printf("Look-From : ");
        PrintVec(&CameraOrigin);
        printf("\n");
      }
      else
      {
        printf(".");
      }
    } else if (strcmp("X-SIZE", varname) == ZERO ||
               strcmp("WIDTH", varname)  == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &XSize);
      if (DEBUG == ON)
      {
        printf("X Size is " FltFmt "\n", XSize);
      }
      else
      {
        printf(".");
      }
    }
    else if (strcmp("Y-SIZE", varname) == ZERO ||
             strcmp("HEIGHT", varname) == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &YSize);
      if (DEBUG == ON)
      {
        printf("Y Size is " FltFmt "\n", YSize);
      }
      else
      {
        printf(".");
      }
    }
    else if (strcmp("DEPTH", varname) == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Depth);
      if (DEBUG == ON)
      {
        printf("Depth is " FltFmt "\n", Depth);
      }
      else
      {
        printf(".");
      }
    }
    else if (strcmp("FILTER", varname) == ZERO)
    {
      fscanf(inputfile, " %s ", FiltName);
      strupr(FiltName);
      for (AFilt = Filters; AFilt != NULL; AFilt = AFilt->NextFilter)
      {
        if (strcmp(FiltName, (AFilt->Name)()) == ZERO)
        {
          ThisUFilt = AllocUseFilt();
          ThisUFilt->ThisFilter = AFilt;
          ThisUFilt->fdata = (AFilt->Read)(inputfile);
        }

      }
    }
    else if (strcmp(";", varname) != ZERO)
    {
      if (ReadGlobals(varname, inputfile) != ZERO)
      {
        printf("Error, Inrecognised keyword (%s) in Camera Section\n", varname);
        exit(1);
      }
    }
  } while (strcmp(";", varname) != ZERO);
  VecSub(&LookAt, &CameraOrigin, &LineOfSight);
  VecUnit(&LineOfSight, &LineOfSight);
  VecScalar(Depth, &LineOfSight, &Sub1);
  VecAdd(&CameraOrigin, &Sub1, &CameraPinhole);
  VecScalar(XSize, VecUnit(VecProduct(&DropLine, &LineOfSight, &Sub1),&Sub2), &CameraXSide);
  VecScalar(YSize, VecUnit(VecProduct(&LineOfSight, &CameraXSide, &Sub1),&Sub2),&CameraYSide);
}

void ReadScreenInfo(FILE *inputfile)
{
  char varname[20];
  Stopable = 'Y';

  do
  {
    fscanf(inputfile, " %s ",varname);
    strupr(varname);

    if (strcmp("NONSTOP", varname) == ZERO)
    {
      Stopable = 'N';
    }
    else if (strcmp("XSIZE", varname) == ZERO ||
             strcmp("WIDTH", varname) == ZERO)
    {
      fscanf(inputfile, " %d ", &ImageXSize);
      if (DEBUG == ON)
      {
        printf("X-Size is %d\n", ImageXSize);
      }
      else
      {
        printf(".");
      }
    }
    else if (strcmp("YSIZE", varname) == ZERO ||
             strcmp("HEIGHT", varname) == ZERO)
    {
      fscanf(inputfile, " %d ", &ImageYSize);
      if (DEBUG == ON)
      {
        printf("Y-Size is at %d\n", ImageYSize);
      }
      else
      {
        printf(".");
      }
    }
    else if (strcmp("CALC-DIFFUSE", varname) == ZERO)
    {
      CalcDiffuse = YES;
      if (DEBUG == ON)
      {
        printf("Calc Diffuse\n");
      }
      else
      {
        printf(".");
      }
    }
    else if (strcmp("NO-SHADOWS", varname) == ZERO)
    {
      SHADOWS = NO;
      if (DEBUG == ON)
      {
        printf("No Shadows\n");
      }
      else
      {
        printf(".");
      }
    }
    else if (strcmp("INTERSECT-BUFFER", varname) == ZERO)
    {
      fscanf(inputfile, " %d ", &IntBuffSize);
    }
    else if (strcmp("REFLECTION-DEPTH", varname) == ZERO)
    {
      fscanf(inputfile, " %d ", &RecurseDepth);
    }
    else if (strcmp("NO-REFLECTIONS", varname) == ZERO)
    {
      REFLECTIONS = NO;
      if (DEBUG == ON)
      {
         printf("No Reflections\n");
      }
      else
      {
         printf(".");
      }
    }
    else if (strcmp(";", varname) != ZERO)
    {

      if (ReadGlobals(varname, inputfile) != ZERO)
      {
	printf("ERROR - Unrecognised Keyword (%s) in Image Section\n", varname);
	exit(1);
      }
    }
  } while (strcmp(";", varname) != ZERO);
}
