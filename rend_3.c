/*
  *************************************************************************
 **                                                                       **
 **       Renderer                                                        **
 **           Scene File read module                                      **
 **                                                                       **
 **                                                                       **
  *************************************************************************
*/

#include "vectors.h"
#include "rend_0.h"
#include <stdio.h>
#include <string.h>
#include "rend_4.h"
#include "boxs.h"
#include "rend_8.h"
#include "rend_6.h"
#include "rend_2.h"

Light *ReadLightInfo();
void ReadColourInfo();
Object *ReadObjectInfo();
void ReadDefinitionInfo();
Comp *ReadComplexInfo();
Box *CombineBound();
MatDef *ReadMaterial(FILE *inputfile);
Defin *ReadCompDefInfo(FILE *inputfile);

ItemEntry *ReadModifiers(FILE *inputfile, int Types, char *TypeNames[]);

colour Ambiant;
MatDef *DefaultMaterial;
void CreateDefMaterial(MatDef **Answer);

void ReadSceneFile(char *inputfilename)
{
  Object *AnObj;
  Light *ALight;
  FILE *inputfile;
  char command[20];
  char incfile[30];
  printf("Attempting to open Scene file : %s ", inputfilename);
  if ((inputfile = fopen(inputfilename, "r")) == NULL)
  {
    printf("Unable to Open Successfully\n");
    exit(1);
  }
  else
  {
    printf("Opened OK, Reading...\n");
    while (!feof(inputfile))
    {
      fscanf(inputfile, "%s", command);
      strupr(command);
      if (strcmp(command, "LIGHT")==ZERO)
      {
        ALight = ReadLightInfo(inputfile);
        (ALight->NextLight) = Lights;
        Lights = ALight;
      }
      else if (strcmp(command, "BACKGROUND")==ZERO)
      {
        ReadCol(inputfile, &Background);
      }
      else if (strcmp(command, "BACK-UP")==ZERO)
      {
        ReadVec(inputfile, &BGUp);
        BackFade = 1;
      }
      else if (strcmp(command, "BACK-HORIZON")==ZERO)
      {
        ReadCol(inputfile, &BackHorizon);
        BackFade = 1;
      }
      else if (strcmp(command, "AMBIANT")==ZERO)
      {
        ReadCol(inputfile, &Ambiant);
      }
      else if (strcmp(command, "INCLUDE")==ZERO)
      {
        fscanf(inputfile, " %s ", incfile);
        ReadSceneFile(incfile);
      }
      else if (strcmp(command, "OBJECT")==ZERO)
      {
        AnObj = ReadObjectInfo(inputfile);
        AnObj->List = AddToList(AnObj, BSPRoot->ObjectList);
        /*
        (AnObj->NextObject) = BSPRoot->ObjectList;
        BSPRoot->ObjectList = AnObj;
        */
        BSPRoot->NoOfObjs += 1;
      }
      else if (strcmp(command, "MATERIAL")==ZERO)
      {
        DefaultMaterial = ReadMaterial(inputfile);
      }
      else if (strcmp(command, "DEFINE")==ZERO)
      {
        ReadDefinitionInfo(inputfile);
      }
      else if (strcmp(command, "AIR")==ZERO)
      {
        AirMat = ReadMaterial(inputfile);
      }
      else if (strcmp(command, "CAMERA")==ZERO)
      {
        ReadCameraInfo(inputfile);
      }
      else if (strcmp(command, "IMAGE")==ZERO)
      {
        ReadScreenInfo(inputfile);
      }
      else if (strcmp(command, "COLOUR")==ZERO || strcmp(command, "COLOR")==ZERO)
      {
        ReadColourInfo(inputfile);
      }
      else
      {
	if (ReadGlobals(command, inputfile) != ZERO)
	{
	  printf("ERROR -- Unknown verb (%s) in Scene File\n", command);
	  exit(1);
	}
      }
    }
    fclose(inputfile);

    if (AirMat == NULL)
    {
      CreateDefMaterial(&AirMat);
      AirMat->ktran = 1.0;
      AirMat->kspec = 0.0;
      AirMat->kdiff = 0.0;
      SetColour(1.0, 1.0, 1.0, &(AirMat->TranCol));
    }

    printf("Completed, OK.\n");
  }
}

void ReadColourInfo(FILE *inputfile)
{
  char Buff[42];
  ColDefn *ACol;
  fscanf(inputfile, " %40s ", Buff);
  printf("Reading colour definition for %s\n", Buff);
  if (SearchCols(Buff) == NULL)
  {
    if ((ACol = malloc(sizeof(ColDefn))) == NULL)
    {
      printf("Error -- Unable to allocate Colour Entry\n");
      exit(1);
    }
    if ((ACol->Name = malloc(strlen(Buff)+1))==NULL)
    {
      printf("Error -- Unable to allocate String Space for Colour Name\n");
      exit(1);
    }
    strcpy(ACol->Name, Buff);
    ReadCol(inputfile, &(ACol->Value));
    ACol->NextCol = Colours;
    Colours = ACol;
  }
  else
  {
    printf("Warning -- Attempted Redefinition of %s Colour\n", Buff);
  }

}

Light *ReadLightInfo(FILE *inputfile)
{
  char command[20], PrimName[20];
  Prim *PrimI;
  Vec PointAt, Sub1;
  Light *ALight = (Light *)AllocLight();
  Flt Radius, HotRad, Rad1;
  ALight->Primative  = NULL;
  ALight->Samples    = 1;
  ALight->MinSamples = 1;
  ALight->LastShadow = NULL;
  ALight->FallOff    = 0.0;
  ALight->Shadows    = 1;
  ALight->Spot       = 0;
  ALight->Angle      = 30.0;
  ALight->HotSpot    = 25.0;
  /* Read in light info into allocated space */
  do
  {
    fscanf(inputfile, " %s ", command);
    strupr(command);
    if (strcmp(command, "COLOUR") == ZERO || strcmp(command, "COLOR") == ZERO)
    {
      ReadCol(inputfile, &(ALight->LColour));
    }
    else if (strcmp(command, "POSITION") == ZERO)
    {
      ReadVec(inputfile, &(ALight->LPosition));
    }
    else if (strcmp(command, "POINT-AT") == ZERO)
    {
      ReadVec(inputfile, &PointAt);
      ALight->Spot = 1;
    }
    else if (strcmp(command, "RADIUS") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Radius);
    }
    else if (strcmp(command, "HOT-SPOT") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &HotRad);
    }
    else if (strcmp(command, "FALL-OFF") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(ALight->FallOff));
    }
    else if (strcmp(command, "NO-SHADOWS") == ZERO)
    {
      ALight->Shadows = 0;
    }
    else if (strcmp(command, "SAMPLES") == ZERO)
    {
      if (ALight->Primative == NULL)
      {
         printf("Error - Attempting to set samples for a Light with no Defined Shape\n");
         exit(2);
      }
      else
         fscanf(inputfile, " %d ", &(ALight->Samples));
    }
    else if (strcmp(command, "MIN-SAMPLES") == ZERO)
    {
      if (ALight->Primative == NULL)
      {
         printf("Error - Attempting to set samples for a Light with no Defined Shape\n");
         exit(2);
      }
      else
         fscanf(inputfile, " %d ", &(ALight->MinSamples));
    }
    else if (strcmp(command, "SHAPE")==ZERO)
    {
      fscanf(inputfile, " %s ", PrimName);
      strupr(PrimName);
      for (PrimI=Primatives; PrimI != NULL; PrimI = PrimI->NextPrim)
      {
        if (strcmp(PrimName, (PrimI->Name)()) == ZERO)
        {
          if (PrimI->Random == NULL)
          {
            printf("Error - Attempted use of Geometry that does not Support RANDOM\n");
            exit(1);
          }
          else
          {
            ALight->Primative = PrimI;
            ALight->kdata   = (PrimI->Read)(inputfile);
          }
        }
      }
    }
    else if (strcmp(command, ";") == ZERO)
    {
    }
    else
    {
      if (ReadGlobals(command, inputfile) != ZERO)
      {
         printf("ERROR -- Unknown verb (%s) in Lightsource Definition\n",command);
         exit(1);
      }
    }
  } while (strcmp(command,";") != ZERO && !feof(inputfile));
  if (ALight->MinSamples > ALight->Samples) ALight->MinSamples = ALight->Samples;
  if (ALight->Spot == 1)
  {
     VecSub(&(ALight->LPosition), &PointAt, &Sub1);
     Rad1 = VecLen(&Sub1);
     Rad1 *= Rad1;
     Rad1 += Radius * Radius;
     ALight->Angle = asin(Radius / sqrt(Rad1));
     ALight->Radius = Radius;

     Rad1 += (HotRad * HotRad - Radius * Radius);
     ALight->HotSpot = asin(HotRad / sqrt(Rad1));

     VecUnit(&Sub1, &(ALight->Direction));
     ALight->PointAt = PointAt;
  }
  return(ALight);
}

Object *ReadObjectInfo(FILE *inputfile)
{
  int WordOK;
  char command[30];
  char PrimName[20], DoMats, Inherit;
  Prim *PrimI;
  Defin *DefiI;
  Flt Scale, ScaleX, ScaleY, ScaleZ, XTheta, YTheta, ZTheta;
  Vec Origin;
  Object *AnObject = AllocObject();
  AnObject->Material = NULL;
  AnObject->ObjType = -1; /* This Object is primative */
  AnObject->Model = NULL;
  AnObject->Inverse = NULL;
  DoMats = 'N';
  ScaleX = 1.0;
  ScaleY = 1.0;
  ScaleZ = 1.0;
  XTheta = 0.0;
  YTheta = 0.0;
  ZTheta = 0.0;
  SetVector(0.0, 0.0, 0.0, &Origin);
  Inherit = 'N';

  do
  {
    fscanf(inputfile, " %s ", command);
    strupr(command);

    if (strcmp(command, "MATERIAL") == ZERO)
    {
      AnObject->Material = ReadMaterial(inputfile);
    }
    else if (strcmp(command, "PRIMATIVE") == ZERO)
    {
      fscanf(inputfile, " %s ", PrimName);
      strupr(PrimName);
      for (PrimI=Primatives; PrimI != NULL; PrimI = PrimI->NextPrim)
      {
        if (strcmp(PrimName, (PrimI->Name)()) == ZERO)
        {
          AnObject->Primative = PrimI;
          AnObject->kdata     = (PrimI->Read)(inputfile);
        }
      }
      AnObject->ObjType = -1; /* This Object is primative */
    }
    else if (strcmp(command, "SHRINK-X") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Scale);
      ScaleX = 1.0 / Scale;
      DoMats = 'Y';
    }
    else if (strcmp(command, "SHRINK") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Scale);
      ScaleX = 1.0 / Scale;
      ScaleY = 1.0 / Scale;
      ScaleZ = 1.0 / Scale;
      DoMats = 'Y';
    }
    else if (strcmp(command, "INHERIT") == ZERO)
    {
      Inherit = 'Y';
    }
    else if (strcmp(command, "SHRINK-Y") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Scale);
      ScaleY = 1.0 / Scale;
      DoMats = 'Y';
    }
    else if (strcmp(command, "SHRINK-Z") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Scale);
      ScaleZ = 1.0 / Scale;
      DoMats = 'Y';
    }
    else if (strcmp(command, "STRETCH-X") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Scale);
      ScaleX = Scale;
      DoMats = 'Y';
    }
    else if (strcmp(command, "STRETCH-Y") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Scale);
      ScaleY = Scale;
      DoMats = 'Y';
    }
    else if (strcmp(command, "STRETCH-Z") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Scale);
      ScaleZ = Scale;
      DoMats = 'Y';
    }
    else if (strcmp(command, "STRETCH") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Scale);
      ScaleX = Scale;
      ScaleY = Scale;
      ScaleZ = Scale;
      DoMats = 'Y';
    }
    else if (strcmp(command, "TRANSLATE") == ZERO)
    {
      DoMats = 'Y';
      ReadVec(inputfile, &Origin);
    }
    else if (strcmp(command, "X-ROTATE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &XTheta);
      DoMats = 'Y';
      if (DEBUG == ON)
      {
    printf(" XTheta : " FltFmt "\n", XTheta);
      }
    }
    else if (strcmp(command, "Y-ROTATE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &YTheta);
      DoMats = 'Y';
      if (DEBUG == ON)
      {
    printf(" YTheta : " FltFmt "\n", YTheta);
      }
    }
    else if (strcmp(command, "Z-ROTATE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &ZTheta);
      DoMats = 'Y';
      if (DEBUG == ON)
      {
    printf(" ZTheta : " FltFmt "\n", ZTheta);
      }
    }
    else if (strcmp(command, "COMPLEX") == ZERO)
    {
      fscanf(inputfile, " %s ", PrimName);
      strupr(PrimName);
      AnObject->Primative = NULL;
      for (DefiI=Definitions; DefiI != NULL; DefiI = DefiI->NextDefinition)
      {
	if (strcmp(PrimName, DefiI->Name) == ZERO)
	{
	  AnObject->Primative = (Prim *)DefiI;
	}
      }
      if (AnObject->Primative == NULL)
      {
        printf("Error -- Complex Object (%s), not found\n", PrimName);
        exit(1);
      }
      else
      {
        AnObject->ObjType = 0; /* This Object complex */
        AnObject->kdata = ReadComplexInfo(inputfile);
      }
    }
    else if (strcmp(command, ";") != ZERO)
    {
      WordOK = 0;
      for (PrimI=Primatives; PrimI != NULL; PrimI = PrimI->NextPrim)
      {
        if (strcmp(command, (PrimI->Name)()) == ZERO)
        {
          AnObject->Primative = PrimI;
          AnObject->kdata     = (PrimI->Read)(inputfile);
          AnObject->ObjType = -1; /* This Object is primative */
          WordOK = 1;
        }
      }
      for (DefiI=Definitions; DefiI != NULL; DefiI = DefiI->NextDefinition)
      {
        if (strcmp(command, DefiI->Name) == ZERO)
        {
          AnObject->Primative = (Prim *)DefiI;
          AnObject->ObjType = 0; /* This Object complex */
          AnObject->kdata = ReadComplexInfo(inputfile);
          WordOK = 1;
        }
      }

      if (WordOK != 1)
      {
	if (ReadGlobals(command, inputfile) != ZERO)
	{
	  printf("ERROR -- Unknown verb (%s) in Object Definition\n",command);
	  exit(1);
	 }
      }
    }
  } while (strcmp(command,";") != ZERO && !feof(inputfile));
  if (AnObject->Material == NULL && Inherit == 'N')
  {
    AnObject->Material = DefaultMaterial;
  }

  if (DoMats == 'Y')
  {
    AnObject->Model   = AllocHeap(sizeof(Matrix));
    AnObject->Inverse = AllocHeap(sizeof(Matrix));

    MRotate(XTheta * PI/180.0, YTheta * PI/180.0, ZTheta * PI/180.0, AnObject->Model, AnObject->Inverse);
    MScale(ScaleX, ScaleY, ScaleZ, AnObject->Model, AnObject->Model, AnObject->Inverse, AnObject->Inverse);
    MTranslate(&Origin, AnObject->Model, AnObject->Model, AnObject->Inverse, AnObject->Inverse);
  }
  return(AnObject);
}

void CreateDefMaterial(MatDef **Answer)
{
  *Answer = (MatDef *)AllocHeap(sizeof(MatDef));
  (*Answer)->kref         = 1.0;
  (*Answer)->kri          = 1.0;
  (*Answer)->kdiff        = 1.0;
  (*Answer)->kspec        = 0.0;
  (*Answer)->ktran        = 0.0;
  (*Answer)->Dust         = 0;
  (*Answer)->World        = 0;
  (*Answer)->GlossSamples = 0;
  (*Answer)->GlossSpread  = 0.0;
  (*Answer)->DiffSamples  = 0;
  (*Answer)->DiffSpread   = 0.0;
  (*Answer)->Attenuate    = 0.0;
  (*Answer)->kspecpow     = 30.0;
  (*Answer)->ShaderEval   = NULL;
  (*Answer)->Modifiers    = NULL;
  SetColour(0.8, 0.8, 0.8, &((*Answer)->DiffCol));
  SetColour(0.0, 0.0, 0.0, &((*Answer)->SpecCol));
  SetColour(0.0, 0.0, 0.0, &((*Answer)->TranCol));
  SetColour(0.0, 0.0, 0.0, &((*Answer)->AmbCol));

}

MatDef *ReadMaterial(FILE *inputfile)
{
  char command[30], ProcName[30];
  Proc *ProcI;
  colour AColour;
  NameList *AName;
  MatDef *Answer;
  int AnswerType, AOK, cNumber;
  ModRec *aMod;

  static char *MatTypes[] = {"COLOUR", "DIFFUSE", "SPECULAR"};
  Flt Number;
  AnswerType = 0;
  Answer=NULL;
  /* Now fill in defaults */
  do
  {
    fscanf(inputfile, " %s ", command);
    strupr(command);
    if (strcmp(command, "NAMED") == ZERO)
    {
      if (AnswerType == 0)
      {
         CreateDefMaterial(&Answer);
         AnswerType = 1;
      }
      if (AnswerType == 2)
      {
        printf("Error, Attempt to rename a named material\n");
      }
      fscanf(inputfile, " %s ", ProcName);
      strupr(ProcName);
      AName = AllocHeap(sizeof(NameList));
      AName->NextName = ListOfNames;
      AName->NameText = AllocHeap(strlen(ProcName) + 2);
      strcpy(AName->NameText, ProcName);
      AName->ValuePtr = Answer;
      ListOfNames = AName;
    }
    else if (strcmp(command, "CALLED") == ZERO)
    {
      if (AnswerType == 1)
      {
        printf("Overlaying a Defined Material with a Named one wastes space\n");
        exit (1);
      }
      fscanf(inputfile, " %s ", ProcName);
      strupr(ProcName);
      for (AName = ListOfNames; AName != NULL; AName = AName->NextName)
      {
        if (strcmp(ProcName, AName->NameText) == ZERO)
        {
          Answer = AName->ValuePtr;
        }
      }
      if (Answer == NULL)
      {
        printf("Error - Unrecognised/Undefined Material (%s)\n", ProcName);
        exit(1);
      }
    }
    else if (strcmp(command, "COLOUR") == ZERO || strcmp(command, "COLOR") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;
      ReadCol(inputfile, &AColour);
      Answer->SpecCol = AColour;
      Answer->TranCol = AColour;
      Answer->DiffCol = AColour;
    }
    else if (strcmp(command, "DUST") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;
      fscanf(inputfile, " " CFltFmt " ", &(Answer->Attenuate));
      Answer->Dust = 1;
    }
    else if (strcmp(command, "SPEC-COLOUR") == ZERO || strcmp(command, "SPEC-COLOR") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;
      ReadCol(inputfile, &(Answer->SpecCol));
    }
    else if (strcmp(command, "DIFF-COLOUR") == ZERO || strcmp(command, "DIFF-COLOR") == ZERO)
    {
      if (AnswerType == 0)
	 CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;
      ReadCol(inputfile, &(Answer->DiffCol));
    }
    else if (strcmp(command, "TRAN-COLOUR") == ZERO || strcmp(command, "TRAN-COLOR") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;
      ReadCol(inputfile, &(Answer->TranCol));
    }
    else if (strcmp(command, "AMB-COLOUR") == ZERO || strcmp(command, "AMB-COLOR") == ZERO)
    {
      if (AnswerType == 0)
	 CreateDefMaterial(&Answer);
      else
      {
	if (AnswerType != 1)
	{
	  printf("Error - Attempting to set characteristics for a Named Material\n");
	  exit(1);
	}
      }
      AnswerType = 1;
      ReadCol(inputfile, &(Answer->AmbCol));
    }
    else if (strcmp(command, "SHADER") == ZERO)
    {
      if (AnswerType == 0)
         CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;
      fscanf(inputfile, " %s ", ProcName);
      strupr(ProcName);
      for (ProcI=Procedures; ProcI != NULL; ProcI = ProcI->NextProc)
      {
        if (strcmp(ProcName, (ProcI->Name)()) == ZERO)
        {
          Answer->ShaderEval = ProcI->Eval;
          Answer->ShaderData = (ProcI->Read)(inputfile);
        }
      }
    }
    else if (strcmp(command, "DIFFUSE-REFLECTION") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;
      fscanf(inputfile, " " FltFmt " ", &Number);
      Answer->kdiff = Number;
    }
    else if (strcmp(command, "SPECULAR-REFLECTION") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;
      fscanf(inputfile, " " FltFmt " ", &Number);
      Answer->kspec = Number;
    }
    else if (strcmp(command, "REFLECTION") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;
      fscanf(inputfile, " " FltFmt " ", &Number);
      Answer->kref = Number;
    }
    else if (strcmp(command, "SPECULAR-POWER") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;
      fscanf(inputfile, " " FltFmt " ", &Number);
      Answer->kspecpow = Number;
    }
    else if (strcmp(command, "REFRACTIVE-INDEX") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;

      fscanf(inputfile, " " FltFmt " ", &Number);
      Answer->kri = Number;
    }
    else if (strcmp(command, "GLOSS") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;

      fscanf(inputfile, " " FltFmt " ", &Number);
      Answer->GlossSpread = Number;
      printf("Gloss=" FltFmt "\n", Answer->GlossSpread);
      if (Answer->GlossSamples == 0)
        Answer->GlossSamples = 1;
    }
    else if (strcmp(command, "GLOSS-SAMPLES") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;

      fscanf(inputfile, " %d ", &cNumber);
      Answer->GlossSamples = cNumber;
      printf("Gloss Samples = %d\n", cNumber);
    }
    else if (strcmp(command, "DIFF-SAMPLES") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;

      fscanf(inputfile, " %d ", &cNumber);
      Answer->DiffSamples = cNumber;
      printf("Diffuse Samples = %d\n", cNumber);
    }
    else if (strcmp(command, "TRANSPARANCY") == ZERO || strcmp(command, "TRANSPARENCY") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;
      fscanf(inputfile, " " FltFmt " ", &Number);
      Answer->ktran = Number;
    }
    else if (strcmp(command, "WORLD") == ZERO || strcmp(command, "WORLD") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;
      Answer->World = 1;
    }
    else if (strcmp(command, ";") == ZERO)
    {
      /* Do nothing */
    }
    else if (strcmp(command, "MODIFIER") == ZERO)
    {
      if (AnswerType == 0)
        CreateDefMaterial(&Answer);
      else
      {
        if (AnswerType != 1)
        {
          printf("Error - Attempting to set characteristics for a Named Material\n");
          exit(1);
        }
      }
      AnswerType = 1;
      Answer->Modifiers = ReadModifiers(inputfile, 3, MatTypes);
    }
    else
    {
      AOK = 0;
      for (ProcI=Procedures; ProcI != NULL; ProcI = ProcI->NextProc)
      {
        if (strcmp(command, (ProcI->Name)()) == ZERO)
        {
          if (AnswerType == 0)
             CreateDefMaterial(&Answer);
          else
          {
            if (AnswerType != 1)
            {
              printf("Error - Attempting to set characteristics for a Named Material\n");
              exit(1);
            }
          }
          AnswerType = 1;

          Answer->ShaderEval = ProcI->Eval;
          Answer->ShaderData = (ProcI->Read)(inputfile);
          AOK = -1;
        }
      }

      for (AName = ListOfNames; AName != NULL; AName = AName->NextName)
      {
        if (strcmp(command, AName->NameText) == ZERO)
        {
          if (AnswerType == 1)
          {
            printf("Overlaying a Defined Material with a Named one wastes space\n");
            exit (1);
          }

          Answer = AName->ValuePtr;
          AOK = -1;
        }
      }

      if (AOK == 0)
      {

        if (ReadGlobals(command, inputfile) != ZERO)
        {
          printf("ERROR - UNKNOWN VERB (%s) in Material Definition\n", command);
          exit(1);
        }
      }

    }
  } while (strcmp(command, ";") != ZERO && !feof(inputfile));
  if (Answer == NULL) CreateDefMaterial(&Answer);
  return Answer;
}

ItemEntry *ReadModifiers(FILE *inputfile, int Types, char *TypeNames[])
{
  ItemEntry *Answer = CreateList();
  char command[1024];
  ModRec *aMod;
  Proc *ProcI;
  int i, fin=0;
 
  static char *IntMods[]={"U", "V", "UV", "DU", "DV", "DUV", "NORMAL"};
  static int  NumIntMods = 7;

  while (!fin)
  {
    fscanf(inputfile, " %s ", command);
    strupr(command);
    if (strcmp(command, ";") == 0)
    {
      fin = 1;
    }
    else
    {
      if ((aMod = malloc(sizeof(ModRec))) == NULL)
      {
        printf("Error - Unable to allocate space for the Material Modifier\n");
        exit(1);
      }
      else
      {
        AddToTailList(aMod, Answer);

        for (i=0; i<NumIntMods; i++)
        {
          if (strcmp(command, IntMods[i]) == 0)
          {
            aMod->ResultTarget = -1 - i;
          }
        }

        for (i=0; i<Types; i++)
        {
          printf("%d:", i); fflush(stdout);
          printf("%s:", TypeNames[i]); fflush(stdout);
          if (strcmp(command, TypeNames[i]) == 0)
          {
            aMod->ResultTarget = i;
          }
        }
        fscanf(inputfile, " %s ", command);
        strupr(command);
        
        for (ProcI=Procedures; ProcI != NULL; ProcI = ProcI->NextProc)
        {
          if (strcmp(command, (ProcI->Name)()) == ZERO)
          {
            aMod->ShaderValue = ProcI->Value;
            aMod->ShaderData = (ProcI->Read)(inputfile);
          }
        }
      }
    }
  }

  return Answer;
}

Comp *ReadComplexInfo(FILE *inputfile)
{
  Vec Origin;
  Flt XTheta, YTheta, ZTheta, XScale, YScale, ZScale;
  Comp *answer;
  char command[30];
  answer = (Comp *)AllocHeap(sizeof(Comp));
  MIdentity(&(answer->Model));
  XTheta = 0.0;
  YTheta = 0.0;
  ZTheta = 0.0;
  XScale = 1.0;
  YScale = 1.0;
  ZScale = 1.0;
  SetVector(0.0, 0.0, 0.0, &Origin);
  do
  {
	fscanf(inputfile, " %s ", command);
	strupr(command);
	if (strcmp(command, "AT") == ZERO)
	{
	  ReadVec(inputfile, &Origin);
	  if (DEBUG == ON)
	  {
		printf(" AT :");
		PrintVec(&Origin);
		printf("\n");
	  }
	}
	else if (strcmp(command, "X-SCALE") == ZERO)
	{
      fscanf(inputfile, " " FltFmt " ", &XScale);
	  if (DEBUG == ON)
	  {
        printf(" XScale : " FltFmt "\n", XScale);
	  }
	}
	else if (strcmp(command, "Y-SCALE") == ZERO)
	{
      fscanf(inputfile, " " FltFmt " ", &YScale);
	  if (DEBUG == ON)
	  {
        printf(" YScale : " FltFmt "\n", YScale);
	  }
	}
	else if (strcmp(command, "Z-SCALE") == ZERO)
	{
      fscanf(inputfile, " " FltFmt " ", &ZScale);
	  if (DEBUG == ON)
	  {
        printf(" ZScale : " FltFmt "\n", ZScale);
	  }
	}
	else if (strcmp(command, "X-ROTATE") == ZERO)
	{
      fscanf(inputfile, " " FltFmt " ", &XTheta);
	  if (DEBUG == ON)
	  {
        printf(" XTheta : " FltFmt "\n", XTheta);
	  }
	}
	else if (strcmp(command, "Y-ROTATE") == ZERO)
	{
      fscanf(inputfile, " " FltFmt " ", &YTheta);
	  if (DEBUG == ON)
	  {
        printf(" YTheta : " FltFmt "\n", YTheta);
	  }
	}
	else if (strcmp(command, "Z-ROTATE") == ZERO)
	{
      fscanf(inputfile, " " FltFmt " ", &ZTheta);
	  if (DEBUG == ON)
	  {
        printf(" ZTheta : " FltFmt "\n", ZTheta);
	  }
	}
	else if (strcmp(command, ";") != ZERO)
	{
	  if (ReadGlobals(command, inputfile) != ZERO)
	  {
	    printf("ERROR -- Unknown verb (%s) in COMPLEX Definition\n",command);
	    exit(1);
	  }
	}
  } while (strcmp(command,";") != ZERO && !feof(inputfile));
  /* Ok Combine Origin and Theta Info into the transformation matrix
     for this complex */

  MRotate(XTheta * PI/180.0, YTheta * PI/180.0, ZTheta * PI/180.0, &(answer->Model), &(answer->Inverse));
  MScale(XScale, YScale, ZScale, &(answer->Model), &(answer->Model), &(answer->Inverse), &(answer->Inverse));
  MTranslate(&Origin, &(answer->Model), &(answer->Model), &(answer->Inverse), &(answer->Inverse));
  return(answer);
}

void ReadDefinitionInfo(FILE *inputfile)
{
  char command[30];
  do
  {
    fscanf(inputfile, " %s ", command);
    strupr(command);
    if (strcmp(command, "MATERIAL") == ZERO)
    {
      ReadMaterial(inputfile);
    }
    else if (strcmp(command, "OBJECT") == ZERO)
    {

      ReadCompDefInfo(inputfile);
    }
    else if (strcmp(command, "~")==ZERO)
    {
      do
      {
	fscanf(inputfile, " %s ", command);
      } while (strcmp(command, "~") && !feof(inputfile));
    }
    else if (strcmp(command, ";") != ZERO)
    {
      if (ReadGlobals(command, inputfile) != ZERO)
      {
	printf("ERROR -- Unknown verb (%s) in DEFINE Definition\n",command);
	exit(1);
      }
    }
  } while (strcmp(command,";") != ZERO && !feof(inputfile));
}

Defin *ReadCompDefInfo(FILE *inputfile)
{
  char command[30];
  Object *AnObj;
  Defin *ADefinition = (Defin *)AllocDefinition();

  strcpy(ADefinition->Name, "");
  ADefinition->BSPRoot = AllocHeap(sizeof(BSPNode));
  ADefinition->BSPRoot->NoOfObjs   = 0;
  ADefinition->BSPRoot->Child0	   = NULL;
  ADefinition->BSPRoot->Child1	   = NULL;
  ADefinition->BSPRoot->ObjectList = CreateList();

  do
  {
    fscanf(inputfile, " %s ", command);
    strupr(command);
    if (strcmp(command, "NAME") == ZERO)
    {
      fscanf(inputfile, " %s ", ADefinition->Name);
      strupr(ADefinition->Name);
      if (DEBUG == ON)
      {
	printf("Defining Name : %s\n", ADefinition->Name);
      }
    }
    else  if (strcmp(command, "OBJECT") == ZERO)
    {
      AnObj = ReadObjectInfo(inputfile);
      AnObj->List = AddToList(AnObj, ADefinition->BSPRoot->ObjectList);
      ADefinition->BSPRoot->NoOfObjs += 1;
    }
    else if (strcmp(command, "~")==ZERO)
    {
      do
      {
	fscanf(inputfile, " %s ", command);
      } while (strcmp(command, "~") && !feof(inputfile));
    }
    else if (strcmp(command, ";") != ZERO)
    {

      if (ReadGlobals(command, inputfile) != ZERO)
      {
	printf("ERROR -- Unknown verb (%s) in DEFINE Definition\n",command);
	exit(1);
      }
    }
  } while (strcmp(command,";") != ZERO && !feof(inputfile));
  BSPGen(ADefinition->BSPRoot);
  printf("Bounder = ");
  BoxPrint(&(ADefinition->BSPRoot->Bounder));
  return(ADefinition);
}

Box *CombineBound(Box *A, Box *B, Box *C)
{
  Box Ans;
  if (A->A.x <= B->A.x)
    Ans.A.x = A->A.x;
  else
    Ans.A.x = B->A.x;

  if (A->A.y <= B->A.y)
    Ans.A.y = A->A.y;
  else
    Ans.A.y = B->A.y;

  if (A->A.z <= B->A.z)
    Ans.A.z = A->A.z;
  else
    Ans.A.z = B->A.z;

  if (A->B.x <= B->B.x)
    Ans.B.x = B->B.x;
  else
    Ans.B.x = A->B.x;

  if (A->B.y <= B->B.y)
    Ans.B.y = B->B.y;
  else
    Ans.B.y = A->B.y;

  if (A->B.z <= B->B.z)
    Ans.B.z = B->B.z;
  else
    Ans.B.z = A->B.z;

  *C = Ans;
  return(C);
}

int ReadGlobals(char *command, FILE *inputfile)
{
  int ch;
  if (strcmp(command, "~")==ZERO)
  {
    do
    {
      ch = fgetc(inputfile);
    } while ((ch != '~') && !feof(inputfile));
  }
  else if (strcmp(command, "//")==ZERO)
  {
    do
    {
      ch = fgetc(inputfile);
    } while ((ch != '\n') && !feof(inputfile));
  }
  else
  {
    return(1);
  }
  return(0);
}

void strupr(char *str)
{ 
  char *astr=str;
   while (*astr != 0) {*astr = toupper(*astr); astr++; };
}
