Light *ReadLightInfo(FILE *inputfile);
void ReadSceneFile(char *inputfilename);
Defin *ReadDefinitionInfo(FILE *inputfile);
Comp *ReadComplexInfo(FILE *inputfile);
Object *ReadObjectInfo(FILE *inputfile);
Box *CombineBound(Box *A, Box *B, Box *C);
MatDef *ReadMaterial(FILE *inputfile);
void CreateDefMaterial(MatDef **Answer);
void ReadColourInfo(FILE *inputfile);
