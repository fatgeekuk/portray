/*
  *************************************************************************
 **                                                                       **
 **       Renderer                                                        **
 **           Data Storage module                                         **
 **            ( Iight sources, objects, etc )                            **
 **                                                                       **
  *************************************************************************
*/

#include <stdio.h>
#include "vectors.h"
#include "rend_0.h"
#include <string.h>
#include "rend_5.h"
#include "rend_7.h"

int DEBUG;

Light     *Lights;
BSPNode   *BSPRoot;
Prim      *Primatives;
Proc      *Procedures;
Defin     *Definitions;
Filter    *Filters;
UseFilt   *UsedFilters;
char      *Heap;
NameList  *ListOfNames;
int        IntBuffSize;
ItemEntry *IntFreeBuff;
MatDef    *AirMat;
ItemEntry *ImList;
ColDefn   *Colours;
ItemEntry *HeadList;
ItemEntry *LFields;

Object *AllocObject();
Light *AllocLight();
void *AllocHeap(int NoOfElements);
void FreeInt(IntRec *ThisInt);
IntRec *AllocInt();
void FreeIntList(ItemEntry *ThisInt);

ItemEntry *CreateList();
ItemEntry *AddToList(void *DataPtr, ItemEntry *AList);
void *RemoveFromList(ItemEntry *AnItem);
ItemEntry *LinkInToList(void *DataPtr, ItemEntry *AList, ItemEntry *A);
void *UnlinkFromList(ItemEntry *AnItem);
void DestroyList(ItemEntry *AnItem);

unsigned int BytesUsed;
unsigned int HeapSize;
unsigned int RecurseDepth;

void InitDataStorage(unsigned int SizeOfHeap)
{
  Heap        = (char *)malloc(SizeOfHeap);
  Lights      = NULL;
  Primatives  = NULL;
  Procedures  = NULL;
  Definitions = NULL;
  Filters     = NULL;
  UsedFilters = NULL;
  ListOfNames = NULL;
  IntFreeBuff = NULL;
  AirMat      = NULL;
  Colours     = NULL;
  HeadList    = NULL;
  IntBuffSize = 200;
  RecurseDepth = RECURSE_LEVEL;
  if (Heap == NULL)
  {
    printf("ERROR -- Unable to Allocate Memory for Data Storage\n");
    printf("     Size of %d bytes\n", SizeOfHeap);
    exit(1);
  }
  BytesUsed = 0;
  HeapSize  = SizeOfHeap;
  BSPRoot = AllocHeap(sizeof(BSPNode));
  BSPRoot->ObjectList = CreateList();
  BSPRoot->Child0     = NULL;
  BSPRoot->Child1     = NULL;
  BSPRoot->NoOfObjs   = 0;
  ImList = CreateList();
  InitPrimatives();
  InitTextures();
  InitFilters();
}

void *AllocHeap(int Size)
{
  void *answer =  Heap;
/*   printf("Attempting Allocation of %d Bytes...",Size); */
  if (BytesUsed + Size < HeapSize)
  {
    Heap         += Size;
    BytesUsed	 += Size;
/*    printf("OK (%X)\n",answer); */
  }
  else
  {
    printf("Message -- Heap Exhausted, attempting extra allocation\n");
    Heap = malloc((unsigned int)10000);
    BytesUsed = 0;
    HeapSize = (unsigned int)10000;
    if (Heap == NULL)
    {
      printf("Unable to allocate more space, terminating Render\n");
      exit(2);
    }
    else
    {
      answer = Heap;
      Heap += Size;
      BytesUsed += Size;
      /* printf("OK (%X)\n", answer); */
    }
  }
  return(answer);
}

Object *AllocObject()
{
  Object *anObject;
  anObject = AllocHeap(sizeof(Object));
  anObject->IlluminList = NULL;
  return (anObject);
}

Light *AllocLight()
{
  return (AllocHeap(sizeof(Light)));
}

Prim *AllocPrimative()
{
  Prim *Ans;
  Ans = AllocHeap(sizeof(Prim));
  Ans->NextPrim = Primatives;
  Primatives = Ans;
  Ans->Random = NULL;
  return (Ans);
}

Proc *AllocProcedure()
{
  Proc *Ans;
  Ans = AllocHeap(sizeof(Proc));
  Ans->NextProc = Procedures;
  Procedures = Ans;
  return (Ans);
}

Defin *AllocDefinition()
{
  Defin *Ans;
  Ans = AllocHeap(sizeof(Defin));
  Ans->NextDefinition = Definitions;
  Definitions = Ans;
  return (Ans);
}

Filter *AllocFilter()
{
  Filter *Ans;
  Ans = AllocHeap(sizeof(Filter));
  Ans->NextFilter = Filters;
  Filters = Ans;
  return (Ans);
}


UseFilt *AllocUseFilt()
{
  UseFilt *Ans;
  Ans = AllocHeap(sizeof(UseFilt));
  Ans->NextUsed = UsedFilters;
  UsedFilters = Ans;
  return (Ans);
}

IntRec *AllocInt()
{
  IntRec *Ans=NULL;
  if (IntFreeBuff->Next->Data == NULL)
  {
    Ans = malloc(sizeof(IntRec));
    Ans->Parent = AddToList(Ans, IntFreeBuff);
    Ans=UnlinkFromList(IntFreeBuff->Next);
  }
  else
  {
    Ans          = UnlinkFromList(IntFreeBuff->Next);
    Ans->Dist    = 0.0;
    Ans->Dirn    = NONE;
    Ans->HitObj  = NULL;
    Ans->DoneMap = 0;
    Ans->DoneMap2 = 0;
    Ans->DoneNorm = 0;
  }
  return(Ans);
}

void FreeInt(IntRec *ThisInt)
{
  LinkInToList(ThisInt, IntFreeBuff, ThisInt->Parent);
}

void FreeIntList(ItemEntry *ThisList)
{
  IntRec *AnInt;
  if (ThisList != NULL)
  {
    while (ThisList->Next->Data != NULL)
    {
      AnInt = UnlinkFromList(ThisList->Next);
      FreeInt(AnInt);
    }
    DestroyList(ThisList);
  }
}

ItemEntry *CreateList()
{
  ItemEntry *Answer;
  if (HeadList != NULL)
  {
    Answer = HeadList;
    HeadList = HeadList->Next;
  }
  else
  {
    Answer = malloc(sizeof(ItemEntry));
    if (Answer == NULL)
    {
      printf("Error -- Heap Exhausted\n");
      exit(1);
    }
  }
  Answer->Next = Answer;
  Answer->Prev = Answer;
  Answer->Data = NULL;
  ItemCount++;
  return (Answer);
}

ItemEntry *AddToTailList(void *DataPtr, ItemEntry *AList)
{
  ItemEntry *A;
  A = malloc(sizeof(ItemEntry));
  if (A==NULL)
  {
    printf("Error -- Unable to allocate ItemEntry\n");
    exit(10);
  }
  A->Prev = AList->Prev;
  A->Next = AList;
  AList->Prev->Next = A;
  AList->Prev = A;
  A->Data = (void *)DataPtr;
  ItemCount++;
  return(A);
}

ItemEntry *AddToList(void *DataPtr, ItemEntry *AList)
{
  ItemEntry *A;
  A = malloc(sizeof(ItemEntry));
  if (A==NULL)
  {
    printf("Error -- Unable to allocate ItemEntry\n");
    exit(10);
  }
  A->Next = AList->Next;
  A->Prev = AList;
  AList->Next->Prev = A;
  AList->Next = A;
  A->Data = (void *)DataPtr;
  ItemCount++;
  return(A);
}

ItemEntry *LinkInToList(void *DataPtr, ItemEntry *AList, ItemEntry *A)
{
  if (A==NULL)
  {
    printf("Error -- Unable to allocate ItemEntry\n");
    exit(10);
  }
  A->Next = AList->Next;
  A->Prev = AList;
  AList->Next->Prev = A;
  AList->Next = A;
  A->Data = (void *)DataPtr;
  ItemCount++;
  return(A);
}


void *RemoveFromList(ItemEntry *AnItem)
{
  void *Ans=NULL;
  if (AnItem->Data == NULL)
  {
    printf("Error -- Attempted to unlink a head record\n");
    exit(5);
  }
  else
  {
    AnItem->Prev->Next = AnItem->Next;
    AnItem->Next->Prev = AnItem->Prev;
    Ans = AnItem->Data;
    free(AnItem);
    ItemCount--;
  }
  return(Ans);
}

void *UnlinkFromList(ItemEntry *AnItem)
{
  void *Ans=NULL;
  if (AnItem->Data == NULL)
  {
    printf("Error -- Attempted to unlink a head record\n");
    exit(5);
  }
  else
  {
    AnItem->Prev->Next = AnItem->Next;
    AnItem->Next->Prev = AnItem->Prev;
    Ans = AnItem->Data;
    ItemCount--;
  }
  return(Ans);
}

void DestroyList(ItemEntry *AnItem)
{
  if (AnItem->Data != NULL || AnItem->Next != AnItem || AnItem->Prev != AnItem)
  {
    printf("Error -- Attempt to Destroy an unempty List\n");
    exit(6);
  }
  else
  {
    AnItem->Next = HeadList;
    HeadList = AnItem;
    ItemCount--;
  }
}

