void InitDataStorage(int SizeOfHeap);
Object *AllocObject();
Light *AllocLight();
Prim *AllocPrimative();
Proc *AllocProcedure();
Defin *AllocDefinition();
void *AllocHeap(int Size);
UseFilt *AllocUseFilt();
Filter *AllocFilter();
void FreeInt(IntRec *ThisInt);
IntRec *AllocInt();
void FreeIntList(ItemEntry *ThisItem);

ItemEntry *CreateList();
ItemEntry *AddToList(void *DataPtr, ItemEntry *AList);
ItemEntry *AddToTailList(void *DataPtr, ItemEntry *AList);
void *RemoveFromList(ItemEntry *AnItem);
ItemEntry *LinkInToList(void *DataPtr, ItemEntry *AList, ItemEntry *A);
void *UnlinkFromList(ItemEntry *AnItem);
void DestroyList(ItemEntry *AnItem);
