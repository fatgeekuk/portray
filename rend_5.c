/*
  *************************************************************************
 **                                                                       **
 **       Renderer                                                        **
 **           Primative Definition Module                                 **
 **            ( Spheres, Planes, Cylanders, etc )                        **
 **                                                                       **
  *************************************************************************
*/

#include <stdio.h>
#include <string.h>
#include "vectors.h"
#include "rend_0.h"
#include "rend_3.h"
#include "rend_4.h"
#include "rend_6.h"
#define PI 3.141592653589
#include "spheres.c"
#include "planes.c"
#include "triangle.c"
#include "boxs.c"
#include "cylinder.c"
#include "rawtri.c"
#include "polys.c"
#include "bool.c"
#include "tori.c"
#include "discs.c"
#include "implicit.c"
#include "compound.c"
/* #include "noise.h" */
