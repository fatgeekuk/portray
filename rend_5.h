
void ResolveMaterial(MatDef *Source, IntRec *AnInt, MatDef *Dest);
void InitPrimatives();
void InitTextures();

char	  *BrickName();
MatDef	  *BrickShader();
BrickVars *BrickRead();

char	  *CheqName();
MatDef	  *CheqShader();
CheqVars  *CheqRead();

char	  *NoiseName();
MatDef	  *NoiseShader();
NoiseVars *NoiseRead();

char	  *BumpyName();
MatDef	  *BumpyShader();
BumpyVars *BumpyRead();

char	  *FadesName();
colour	  *FadesShader();
FadeVars  *FadesRead();

void InitChequers();
void InitBrick();
void InitGranite();
void InitNoise();
void InitMarble();
void InitBumpy();
void InitFades();
