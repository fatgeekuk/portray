#include <time.h>
colour *Trace(CFlt Weight, ray *PrimaryRay, int Iter, BSPNode *ObjectList, colour *Answer);
void RenderScene(char *outputfilename);
ItemEntry *IntMerge(ItemEntry *Ans1, ItemEntry *Ans2, enum CSGOp ThisOp);

ItemEntry *IntersectObject(ray *ARay, Object *AnObj, int Mode, Flt MinDist);
LightInfo *GenLightMap(Light *ALight);

extern char SceneFName[60];
extern char ImageFName[60];
extern int  LineNumber;
extern int  NoOfInts;
extern int  NoOfBounds;
extern int  RaysCast;
extern int  RCacheHits;
extern int  RCacheMiss;
extern int  MaxHashDepth;
extern time_t StartTime;
extern double Durn1;
extern double Durn2;
extern double Durn3;
extern double Durn4;
extern double Durn5;
extern int  NoOfDurns;
