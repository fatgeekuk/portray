/*
  ******************************************
 **                                        **   
 **       Renderer                         **
 **           Scene Rendering Module       **
 **                                        **
  ******************************************
*/

/* #include <conio.h> */
#include "vectors.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/* #include <ncurses/curses.h> */
#include "rend_0.h"
#include "rend_3.h"
#include "rend_4.h"
#include "rend_5.h"
#include "rend_6.h"
#include "rend_8.h"
#include <time.h>
/* #include <dos.h> */
#include <string.h>
#include "bmp.h"
#include "tga.h"
#include "boxs.h"
#define _MAX_DRIVE 5
#define _MAX_DIR 100
#define _MAX_FNAME 50
#define _MAX_EXT 50
#ifdef __BORLANDC__
	 #include <dir.h>
	#define _MAX_DRIVE MAXDRIVE
	#define _MAX_DIR   MAXDIR
	#define _MAX_FNAME MAXFILE
	#define _MAX_EXT   MAXEXT
#endif

/* Definitions of Global variables used in Status Page */
void PrintStats(FILE *OutFile);
char SceneFName[60];
char ImageFName[60];
int  LineNumber=0;
int  NoOfInts=0;
int  HitInts=0;
int  HitBounds;
int  MissBounds;
int  WasteHBounds;
int  RaysCast=0;
int  RCacheHits=0;
int  RCacheMiss=0;
int  MaxHashDepth;
time_t StartTime;
clock_t StartTicks;
clock_t LastLine;
double Durn1;
double Durn2;
double Durn3;
double Durn4;
double Durn5;
int  NoOfDurns=0;
unsigned long StartY;






ItemEntry *CylIntersect();
colour *Trace(CFlt Weight, ray *PrimaryRay, int Iter, BSPNode *BSPRoot, colour *Answer);
colour *Shade(CFlt Weight, IntRec *AnInt, Vec *RayDir, int Iter, BSPNode *BSPRoot, colour *answer);
colour *kDiff(colour *surfcol, colour *answer);
colour *kSpec(CFlt Weight, IntRec *AnInt, Vec *RayDir, int Iter, BSPNode *BSPRoot, colour *answer, MatDef *Material);
colour *kTran(CFlt Weight, IntRec *AnInt, Vec *RayDir, int Iter, BSPNode *BSPRoot, colour *answer, MatDef *Material);
Vec *TransmissionDirection(Vec *Insident, Vec *Normal, Flt OldRI, Flt NewRI, int *AOK, Vec *answer);
MatDef *SurfaceColour(IntRec *AnInt, MatDef *MatAns);
ItemEntry *Intersect(ray *ARay, BSPNode *BSPRoot, int Mode, Flt MinDist);
void SetupIntFreeList();
ItemEntry *IntersectObject(ray *ARay, Object *AnObj, int Mode, Flt MinDist);
ItemEntry *IntMerge(ItemEntry *Ans1, ItemEntry *Ans2, enum CSGOp ThisOp);
ItemEntry *IntMerge2(ItemEntry *Ans1, ItemEntry *Ans2);

colour *LightDust(Flt MaxDist, Light *ALight, ray *ARay, MatDef *AirMat, colour *Ans);

typedef struct LineSegment
{
   Flt x1, y1, x2, y2;
} LineSegment;

typedef struct BoundSquare
{
   Flt xmin, ymin, xmax, ymax;
} BoundSquare;


int ClipCode(BoundSquare *BS, Flt x, Flt y);


Flt AmbiantRI;
char INOUT;

void RenderScene(char *outputfilename)
{
  fpos_t LinePos, CurrPos;
  FILE *OutputFile, *StatsFile;
  char *PixelBuffer;
  unsigned char ABuff[2];
  unsigned int x, y, SampleCount, OldXSize, OldYSize;
  Vec XBase, YBase, XStep, YStep, Sub1;
  colour PixColour;
  clock_t timer1, timer2;
  long RayCount=0, lx, ly;
  char drive[_MAX_DRIVE * 30];
  char dir[_MAX_DIR];
  char file[_MAX_FNAME];
  char ext[_MAX_EXT];
  struct TGAHdr TGAFileHdr;
  IntRec *AnInt;
  int i, ch;
  Light *ALight;


  LineNumber=0;
  NoOfInts=0;
  HitInts=0;
  HitBounds=0;
  MissBounds=0;
  WasteHBounds=0;
  RaysCast=0;
  RCacheHits=0;
  RCacheMiss=0;
  NoOfDurns=0;
  MaxHashDepth=0;
  Durn1=0;
  Durn2=0;
  Durn3=0;
  Durn4=0;
  Durn5=0;
  LastLine = clock();

  StartTime = time(NULL);
  StartTicks = clock();
  for (i = 0; i<20; i++)
  {
    ItemEntry *AnItem = malloc(sizeof(ItemEntry));
    AnItem->Next = HeadList;
    HeadList = AnItem;
  }
  IntFreeBuff = CreateList();
  for (i = 0; i < IntBuffSize; i++)
  {
    if ((AnInt = malloc(sizeof(IntRec))) == NULL)
    {
      printf("Error -- Unable to allocate Intersection Buffer\n");
      exit(1);

    }
    AnInt->Parent = AddToList(AnInt, IntFreeBuff);

  }
  SetupIntFreeList();

  timer1 = clock();
  if((PixelBuffer = malloc(3*ImageXSize+4)) == NULL)
  {
    printf("Error -- Unable to allocate Pixel Buffer\n");
    exit(1);
  }
  for (ALight = Lights; ALight != NULL; ALight = ALight->NextLight)
  {
    if (ALight->Spot == 1)
    {
      printf("Generating Shadow Map\n");
      ALight->TheInfo = GenLightMap(ALight);
    }
  }
  printf("Start Render   ");
  fflush(stdout);

  strcpy(ImageFName, outputfilename);
  /* First Check to see if this file already exists, and if it does, attempt
     to read the header to see if you can continue
  */

  if ((OutputFile = fopen(outputfilename, "r+b")) != NULL && 0)
  {
    /* Ok, File exists, attempt to read header */
    fread(&TGAFileHdr, sizeof(char), sizeof(TGAFileHdr), OutputFile);
    printf("Aok!");
    if (TGAFileHdr.IDLen      != 2  ||
	TGAFileHdr.ColMapInfo != 0  ||
	TGAFileHdr.ImageType  != 2  ||
	TGAFileHdr.CMOriginLo != 0  ||
	TGAFileHdr.CMOriginHi != 0  ||
	TGAFileHdr.CMLengthLo != 0  ||
	TGAFileHdr.CMLengthHi != 0  ||
	TGAFileHdr.CMEntrySize!= 0  ||
	TGAFileHdr.XOriginLo  != 0  ||
	TGAFileHdr.XOriginHi  != 0  ||
	TGAFileHdr.YOriginLo  != 0  ||
	TGAFileHdr.YOriginHi  != 0  ||
	TGAFileHdr.Depth      != 24 ||
	TGAFileHdr.TGAInfo    != 0)
    {
      printf("Error -- This TGA file does not agree with format used by PortRAY\n");
      exit(1);
    }
    OldXSize = (unsigned int)TGAFileHdr.WidthLo	+ ((unsigned int)TGAFileHdr.WidthHi	* 256);
    OldYSize = (unsigned int)TGAFileHdr.HeightLo + ((unsigned int)TGAFileHdr.HeightHi * 256);
    if (ImageXSize != OldXSize ||
	ImageYSize != OldYSize)
    {
      printf("Error -- The size of the TGA file does not agree with image description\n");
      printf("         The Old Size was Width:%d Height:%d\n", OldXSize, OldYSize);
      exit(1);
    }
    /* Get the last Y coordinate written to and reopen file for write at the
       correct location
    */
    if (fgetpos(OutputFile, &LinePos) != ZERO)
    {
      printf("Error -- Unable to get File Position for Later use\n");
      exit(1);
    }
    fread(&ABuff, sizeof(char), 2, OutputFile);
    StartY = (unsigned int)ABuff[0] + (((unsigned int)ABuff[1]) * 256) + 1;
    printf("Starting a Line %d\n", StartY);
    CurrPos = LinePos + 2l + (3l * (long)ImageXSize * (long)StartY);
    fclose(OutputFile);
    if ((OutputFile = fopen(outputfilename, "a+b")) == NULL)
    {
      printf("Error -- Unable to re-open file for appending data\n");
      exit(1);
    }
    if (fsetpos(OutputFile, &CurrPos) != ZERO)
    {
      printf("Error -- Unable to reposition file pointer for append\n");
      exit(1);
    }
  }
  else
  {
    if ((OutputFile = fopen(outputfilename, "wb")) == NULL)
    {
      printf("ERROR -- Cannot Open OutputFile (%s)\n", outputfilename);
      exit(1);
    }
    else
    {
      TGAFileHdr.IDLen	  = 2;
      TGAFileHdr.ColMapInfo = 0;
      TGAFileHdr.ImageType  = 2;
      TGAFileHdr.CMOriginLo = 0;
      TGAFileHdr.CMOriginHi = 0;
      TGAFileHdr.CMLengthLo = 0;
      TGAFileHdr.CMLengthHi = 0;
      TGAFileHdr.CMEntrySize= 0;
      TGAFileHdr.XOriginLo  = 0;
      TGAFileHdr.XOriginHi  = 0;
      TGAFileHdr.YOriginLo  = 0;
      TGAFileHdr.YOriginHi  = 0;
      TGAFileHdr.WidthLo    = ImageXSize & 0xFF;
      TGAFileHdr.WidthHi    = ImageXSize / 256;
      TGAFileHdr.HeightLo   = ImageYSize & 0xFF;
      TGAFileHdr.HeightHi   = ImageYSize / 256;
      TGAFileHdr.Depth	  = 24;
      TGAFileHdr.TGAInfo	  = 0;
      fwrite(&TGAFileHdr, sizeof(char), sizeof(TGAFileHdr), OutputFile);
      if (fgetpos(OutputFile, &LinePos) != ZERO)
      {
        printf("Error -- Unable to get File Position for Later use\n");
        exit(1);
      }
      StartY = 0;
      ABuff[0] = 0;
      ABuff[1] = 0;
      fwrite(ABuff, sizeof(char), 2, OutputFile);
      CurrPos = LinePos + 2;
    }
  }
  SampleCount = 0;
  VecComb(.5, &CameraXSide, 0.5, &CameraYSide, &Sub1);
  VecSub(&CameraOrigin, &Sub1, &YBase);
  VecScalar(1.0/(Flt)ImageXSize, &CameraXSide, &XStep);
  VecScalar(1.0/(Flt)ImageYSize, &CameraYSide, &YStep);
  VecAdds((Flt)StartY, &YStep, &YBase, &YBase);
  printf("Rendering\n  up to (%d).", ImageYSize);
  for (y=StartY, ly = ((long)StartY) << 8; y<ImageYSize; y++, ly += 256)
  {
    XBase = YBase;
    for (x=0, lx = 0; x<ImageXSize; x++, lx += 256)
    {
      AmbiantRI = 1.0;
      ((UsedFilters->ThisFilter)->Exec)(UsedFilters->NextUsed,
					&CameraPinhole,
					&XBase,
					&XStep,
					&YStep,
					UsedFilters->fdata,
					lx,
					ly,
					256,
					&PixColour);
      PixelBuffer[SampleCount++] = (char)(255.0 * PixColour.blue);
      PixelBuffer[SampleCount++] = (char)(255.0 * PixColour.green);
      PixelBuffer[SampleCount++] = (char)(255.0 * PixColour.red);
      VecAdd(&XBase, &XStep, &XBase);
      RayCount++;
    }
    while ((SampleCount % 3) != 0)
      PixelBuffer[SampleCount++] = 0;

    LineNumber = y;
    printf("."); fflush(stdout);
     PrintStats(stdout);
    fwrite(PixelBuffer, sizeof(char), SampleCount, OutputFile);
    /* fgetpos(OutputFile, &CurrPos);
    fsetpos(OutputFile, &LinePos);
    ABuff[0] = y & 0xFF;
    ABuff[1] = (y & 0xFF00) >> 8;
    fwrite(ABuff, sizeof(char), 2, OutputFile);
    fsetpos(OutputFile, &CurrPos);
    
    if (Stopable == 'Y' && (ch = getch()) == ERR)
    {
      fflush(OutputFile);
      fclose(OutputFile);
      printf("Render Suspended at line %d\n", y);
      ch = getch();
      printf("CacheTot %ld, CacheHit %ld, CacheMiss %ld\n", CacheTot, CacheHit, CacheMiss);

      exit(1);
    }
    */
    SampleCount = 0;
    VecAdd(&YBase, &YStep, &YBase);
  }
  if (SampleCount > 0)
    fwrite(PixelBuffer, sizeof(char), SampleCount, OutputFile);
  fclose(OutputFile);
  if ((StatsFile = fopen("Stats.TXT", "w"))==NULL)
  {
    printf("Error - Unable to open stats file for output\n");
  }
  else
  {
    PrintStats(StatsFile);
    fclose(StatsFile);
  }

  timer2 = clock();
  printf("Completed.\n");
}

void PrintStats(FILE *OutFile)
{
  time_t CurrTime, TimeToEnd;
  clock_t CurrTicks;
  Flt AveLineTime;
  char Buff1[30], Buff2[30], Buff3[30], *atime, *btime;

  CurrTime  = time(NULL);
  strcpy(Buff1, ctime(&CurrTime));
  strcpy(Buff2, ctime(&StartTime));

  AveLineTime = ((Flt)(CurrTime - StartTime))/((Flt)(1+LineNumber));
   TimeToEnd = CurrTime + (ImageYSize - (1+LineNumber)) * (((float)(CurrTime - StartTime)) / ((float)(1+LineNumber)));

  /* TimeToEnd = CurrTime + AveLineTime * ( ImageYSize - (1 + LineNumber));
*/

  
  fprintf(OutFile, "\n\n\n\n\n\n\n\n\nScene Name : %s\n", SceneFName);
  fprintf(OutFile, "Image Name : %s Size %dx%d\n\n", ImageFName, ImageXSize, ImageYSize);
  fprintf(OutFile, "  Line No. : %d\n\n", LineNumber);
  fprintf(OutFile, "Cache Hits : %10ld       Bounds Hits : %10ld   Rays/Pixel : %6.3f\n"
                   "      Miss : %10ld       Wasted Hits : %10ld   Hash Depth : %4d\n"
                   "     Total : %10ld              Miss : %10ld\n"
                   "percentage : %10.2lf\n\n"
                   " Int Tests : %10ld        Start Time : %s"
                   "      Hits : %10ld      Current Time : %s"
                   "      Miss : %10ld  Estimated Finish : %s"
                   " Hit perc. : %10.2lf      Seconds/Line : %6.2lf\n\n"
                   " Render Duration in Seconds %8.2lf\n",
          RCacheHits, HitBounds, ((Flt)RaysCast) / ((Flt)(ImageXSize * ((LineNumber+1)-StartY))),
          RCacheMiss, WasteHBounds, MaxHashDepth,
          RCacheHits + RCacheMiss, MissBounds,
          100.0 * ((Flt)RCacheHits) / ((Flt)(RCacheHits + RCacheMiss)), 
          NoOfInts, Buff2, HitInts, Buff1, NoOfInts-HitInts, ctime(&TimeToEnd), 
          100.0 * ((Flt)HitInts)/(0.0001 + (Flt)NoOfInts),
          AveLineTime, ((Flt)(CurrTime-StartTime)));

  LastLine = CurrTicks;
}

colour *Trace(CFlt Weight, ray *PrimaryRay, int Iter, BSPNode *BSPRoot, colour *Answer)
{
  ItemEntry *IntList, *AnItem;
  IntRec *CurrInt, *IntScan;
  Flt LastDist, Dist, MaxDist;
  MatDef AMaterial;
  colour ShadeValue, Subc1;
  Light *ALight;
  int Done;
  Done = 0;
 
 
  IntList = Intersect(PrimaryRay, BSPRoot, 0, 99999999.99);
  LastDist = 0.0;
  CurrInt = NULL;
  if (BackFade == 0)
     *Answer = Background;
  else
  {
    FadeColours(fabs(VecDot(&(PrimaryRay->Direction), &BGUp)), &Background, &BackHorizon, Answer); 
  }

  SetColour(1.0, 1.0, 1.0, &ShadeValue);

  MaxDist = 99999999.99;
  for (AnItem = IntList->Next; AnItem->Data != NULL && !Done; AnItem = AnItem->Next)
  {
    IntScan = AnItem->Data;
    Dist = IntScan->Dist - LastDist;
    if (IntScan->Dist > EFFECTIVE_ZERO)
    {

      MaxDist = IntScan->Dist;

      if (CurrInt != NULL)
      {
         SurfaceColour(CurrInt, &AMaterial);
      }
      else
      {

         AMaterial = *AirMat;

      }

      if (Dist <= 0.0)
      {
         printf("Warning -- Minus/Zero Flt used in POW\n");
      }

      Subc1.red   = pow(AMaterial.ktran * AMaterial.TranCol.red,   Dist);
      Subc1.green = pow(AMaterial.ktran * AMaterial.TranCol.green, Dist);
      Subc1.blue  = pow(AMaterial.ktran * AMaterial.TranCol.blue,  Dist);

      ColourFilter(&ShadeValue, &Subc1, &ShadeValue);
      Shade(Weight, IntScan, &(PrimaryRay->Direction), Iter, BSPRoot, Answer);
      ColourFilter(Answer, &ShadeValue, Answer);


      Done = -1;
    }

    if (IntScan->Dirn == ENTRY)
      CurrInt = IntScan;
    else
      CurrInt = NULL;

    LastDist = IntScan->Dist;

  }
  /* If required apply Dust Shader */
  if (AMaterial.Dust == 1)
  {

    for (ALight = Lights; ALight != NULL; ALight = ALight->NextLight)
    {

       if (ALight->Spot == 1)
       {
         colour Dusty;
         ColourAddS(Answer, AMaterial.Attenuate, LightDust(MaxDist, ALight, PrimaryRay, &AMaterial, &Dusty), Answer);
       }
    }
  }

  if (Answer->red   > 1.0) Answer->red   = 1.0;
  if (Answer->green > 1.0) Answer->green = 1.0;
  if (Answer->blue  > 1.0) Answer->blue  = 1.0;

  FreeIntList(IntList);

  return Answer;
}

colour *LightDust(Flt MaxDist, Light *ALight, ray *ARay, MatDef *AirMat, colour *Ans)
{
  Vec A, B, C, D, Dir1, Dir2, Sub1;
  Flt t1, t2, SegLen=0.0, ox1, oy1;
  BoundSquare BS;
  LineSegment OutLine;
  ItemEntry *IntList;
  SetColour(0.0, 0.0, 0.0, Ans);

  A = ARay->Origin;

  IntList = CylIntersect(ARay, ALight->TheInfo->TheCyl);
  if (IntList->Next->Data != NULL)
  {
    if (((IntRec *)(IntList->Next->Data))->Dist < MaxDist)
    {
      if (((IntRec *)(IntList->Prev->Data))->Dist > EFFECTIVE_ZERO)
      {
        IntRec *AnInt = IntList->Next->Data;
        BoundSquare ASq;
        int i, j, u, v, oi=-1, oj=-1, Done=0;
        Flt x, y, Pos, ind1, ind2, Dist;
        Vec aPt;

        if (AnInt->Dist < EFFECTIVE_ZERO)
        {
          A = ARay->Origin;
        }
        else
        {
          A = AnInt->WorldHit;
        }
        if (((IntRec *)(IntList->Prev->Data))->Dist > MaxDist)
        {
          VecAdds(MaxDist, &(ARay->Direction), &(ARay->Origin), &B);
        }
        else
          B = ((IntRec *)(IntList->Prev->Data))->WorldHit;

        VecSub(&A, &(ALight->LPosition), &Dir1);
        VecUnit(&Dir1, &Dir1);
        VecSub(&B, &(ALight->LPosition), &Dir2);
        VecUnit(&Dir2, &Dir2);

        t1 = -VecDot(&(ALight->Direction), &Dir1);
        if (fabs(t1) < EFFECTIVE_ZERO)
        {
          printf("ERROR - Bummer A!!!\n");
          exit(22);
        }
        VecSub(&(ALight->LPosition), &(ALight->PointAt),&Sub1);
        t1 = VecDot(&(ALight->Direction), &Sub1) / t1;

        t2 = -VecDot(&(ALight->Direction), &Dir2);
        if (fabs(t2) < EFFECTIVE_ZERO)
        {
          printf("ERROR - Bummer B!!!\n");
          exit(22);
        }
        VecSub(&(ALight->LPosition), &(ALight->PointAt),&Sub1);
        t2 = VecDot(&(ALight->Direction), &Sub1) / t2;

        VecAdds(t1, &Dir1, &(ALight->LPosition), &C);
        VecAdds(t2, &Dir2, &(ALight->LPosition), &D);

        VecSub(&C, &(ALight->PointAt),&Sub1);
        OutLine.x1 = VecDot(&(ALight->TheInfo->XAxis), &Sub1);
        OutLine.y1 = VecDot(&(ALight->TheInfo->YAxis), &Sub1);

        VecSub(&D, &(ALight->PointAt),&Sub1);
        OutLine.x2 = VecDot(&(ALight->TheInfo->XAxis), &Sub1);
        OutLine.y2 = VecDot(&(ALight->TheInfo->YAxis), &Sub1);

        BS.xmin = -(ALight->Radius);
        BS.ymin = -(ALight->Radius);
        BS.xmax = (ALight->Radius);
        BS.ymax = (ALight->Radius);

        x = OutLine.x1;
        y = OutLine.y1;
        /* Do Scan Conversion to Light Dist Buff */
        i = (int) ((50.0 * x/ALight->Radius)+50.0);
        j = (int) ((50.0 * y/ALight->Radius)+50.0);
        u = (int) ((50.0 * OutLine.x2/ALight->Radius)+50.0);
        v = (int) ((50.0 * OutLine.y2/ALight->Radius)+50.0);

        ind1 = (OutLine.x2 - OutLine.x1);
        ind2 = (OutLine.y2 - OutLine.y1);
        i = (int) ((50.0 * x/ALight->Radius)+50.0);
        j = (int) ((50.0 * y/ALight->Radius)+50.0);
        ox1 = OutLine.x1;
        oy1 = OutLine.y1;
        while (!Done)
        {
          LineSegment InsLine;

          x = OutLine.x1;
          y = OutLine.y1;

          if (i == oi && j == oj)
          {
            i = u;
            j = v;
            printf("Bail!|");
          }
          oi = i;
          oj = j;
          ASq.xmin = ((Flt)(i-50) / 50.0) * ALight->Radius;
          ASq.ymin = ((Flt)(j-50) / 50.0) * ALight->Radius;
          ASq.xmax = ((Flt)(i-49) / 50.0) * ALight->Radius;
          ASq.ymax = ((Flt)(j-49) / 50.0) * ALight->Radius;

          if (fabs(ind1) > EFFECTIVE_ZERO)
            Pos = ((x - ox1)/ind1);
          else
          {
            Pos = ((y - oy1)/ind2);
          }

          aPt.x = A.x + (B.x - A.x) * Pos;
          aPt.y = A.y + (B.y - A.y) * Pos;
          aPt.z = A.z + (B.z - A.z) * Pos;
          VecSub(&(ALight->LPosition), &aPt, &Sub1);
          Dist = VecLen(&Sub1);
          VecUnit(&Sub1, &Sub1);

          ClipLine(&OutLine, &ASq, &InsLine);
          if (Dist < ALight->TheInfo->DepthMap[i][j])
          {
            Flt Angle, SpotScale;

            Angle = acos(VecDot(&(ALight->Direction), &Sub1));
            if (Angle < ALight->HotSpot)
              SpotScale = 1.0;
            else
              if (Angle > ALight->Angle)
                SpotScale = 0.0;
              else
                SpotScale = 1.0 - pow((Angle - ALight->HotSpot) / (ALight->Angle - ALight->HotSpot), 2.0);

            if (ALight->FallOff > 0.0)
            {
              SpotScale = pow(SpotScale, ALight->FallOff * Dist);
            }
            SegLen += SpotScale * sqrt(pow((InsLine.x1 - InsLine.x2), 2.0) + pow((InsLine.y1 - InsLine.y2), 2.0));
          }
          OutLine.x1 = InsLine.x2;
          OutLine.y1 = InsLine.y2;
          if (i == u && j == v) Done = 1;
          else
          {

            if (InsLine.x2 == ASq.xmin)
            {
              i--;
              if (i < 0) { printf("EBail1!|"); exit(23); }
            }
            if (InsLine.x2 == ASq.xmax)
            {
              i++;
              if (i > 100) { printf("EBail2!|"); exit(23); }
            }
            if (InsLine.y2 == ASq.ymin)
            {
              j--;
              if (j < 0) { printf("EBail3!|"); exit(23); }
            }
            if (InsLine.y2 == ASq.ymax)
            {
              j++;
              if (j > 100) { printf("EBail4!|"); exit(23); }
            }
          }
        }
      }
    }
  }
  FreeIntList(IntList);

  ColourShade(SegLen/ALight->Radius, &(ALight->LColour), Ans);
  return(Ans);
}

int ClipLine(LineSegment *ALine, BoundSquare *BS, LineSegment *Ans)
{
  int SCode, ECode, Inside, Outside, Swaps=0;

  *Ans = *ALine;
  SCode = ClipCode(BS, Ans->x1, Ans->y1);
  ECode = ClipCode(BS, Ans->x2, Ans->y2);

  Inside  = (SCode | ECode) == 0;
  Outside = (SCode & ECode) != 0;
  while (!Outside && !Inside)
  {
    if (SCode == 0)
    {
      int t;
      Flt ft;

      Swaps++;

      t = SCode;
      SCode = ECode;
      ECode = t;

      ft     = Ans->x1;
      Ans->x1 = Ans->x2;
      Ans->x2 = ft;

      ft     = Ans->y1;
      Ans->y1 = Ans->y2;
      Ans->y2 = ft;

    }
    if ((SCode & 1) != 0)
    {
      Ans->y1 += (Ans->y2 - Ans->y1)*(BS->xmin - Ans->x1)/(Ans->x2-Ans->x1);
      Ans->x1 = BS->xmin;
    }
    else if ((SCode & 2) != 0)
    {
      Ans->x1 += (Ans->x2 - Ans->x1)*(BS->ymin - Ans->y1)/(Ans->y2-Ans->y1);
      Ans->y1 = BS->ymin;
    }
    else if ((SCode & 4) != 0)
    {
      Ans->y1 += (Ans->y2 - Ans->y1)*(BS->xmax - Ans->x1)/(Ans->x2-Ans->x1);
      Ans->x1 = BS->xmax;
    }
    else if ((SCode & 8) != 0)
    {
      Ans->x1 += (Ans->x2 - Ans->x1)*(BS->ymax - Ans->y1)/(Ans->y2-Ans->y1);
      Ans->y1 = BS->ymax;
    }
    SCode = ClipCode(BS, Ans->x1, Ans->y1);

    Inside  = (SCode | ECode) == 0;
    Outside = (SCode & ECode) != 0;


  }
  if ((Swaps & 1) == 1)
  {
    int t;
    Flt ft;

    t = SCode;
    SCode = ECode;
    ECode = t;

    ft     = Ans->x1;
    Ans->x1 = Ans->x2;
    Ans->x2 = ft;

    ft     = Ans->y1;
    Ans->y1 = Ans->y2;
    Ans->y2 = ft;

  }
  return (Outside);
}

int ClipCode(BoundSquare *BS, Flt x, Flt y)
{
  int Ans=0;
  if (x < BS->xmin) Ans |= 1;
  if (x > BS->xmax) Ans |= 4;
  if (y < BS->ymin) Ans |= 2;
  if (y > BS->ymax) Ans |= 8;
  return(Ans);
}



ItemEntry *Intersect(ray *ARay, BSPNode *BSPRoot, int Mode, Flt MinDist)
{
  enum CSGOp OpNone=MERGE;
  IntRec *BoundInt;
  Object *AnObj;
  ItemEntry *AnItem, *BoundList, *IntList, *IntScan, *Ans=CreateList();
  int BoundHit;
  Flt Dist;

  BoundHit = BoxHit(ARay, &(BSPRoot->Bounder), &Dist);
  if (BoundHit != 0)
  {
      if (Dist < MinDist || Mode == 1)
    {
      HitBounds++;
      for (AnItem = BSPRoot->ObjectList->Next; AnItem->Data != NULL; AnItem = AnItem->Next)
      {
        AnObj = AnItem->Data;
        IntList = IntersectObject(ARay, AnObj, Mode, MinDist);
        if (IntList->Next->Data != NULL)
        {
          IntScan = Ans;
          Ans = IntMerge2(IntList, IntScan);
        }
        else DestroyList(IntList);
      }

      for (IntScan=Ans->Next; IntScan->Data != NULL && ((IntRec *)(IntScan->Data))->Dist < EFFECTIVE_ZERO; IntScan=IntScan->Next);
      if (IntScan->Data != NULL)
      {
        if (((IntRec *)(IntScan->Data))->Dist < MinDist) MinDist = ((IntRec *)(IntScan->Data))->Dist;
      }

      if (BSPRoot->Child0 != NULL)
      {
        IntList = Intersect(ARay, BSPRoot->Child0, Mode, MinDist);
        if (IntList->Next->Data != NULL)
        {
          IntScan = Ans;
          Ans = IntMerge2(IntList, IntScan);
        }
        else DestroyList(IntList);
        for (IntScan=Ans->Next; IntScan->Data != NULL && ((IntRec *)(IntScan->Data))->Dist < EFFECTIVE_ZERO; IntScan=IntScan->Next);
        if (IntScan->Data != NULL)
        {
          if (((IntRec *)(IntScan->Data))->Dist < MinDist)
          {
            MinDist = ((IntRec *)(IntScan->Data))->Dist;
          }
        }
      }

      if (BSPRoot->Child1 != NULL)
      {
        IntList = Intersect(ARay, BSPRoot->Child1, Mode, MinDist);
        if (IntList->Next->Data != NULL)
        {
          IntScan = Ans;
          Ans = IntMerge2(IntList, IntScan);
        }
        else DestroyList(IntList);
        for (IntScan=Ans->Next; IntScan->Data != NULL && ((IntRec *)(IntScan->Data))->Dist < EFFECTIVE_ZERO; IntScan=IntScan->Next);
        if (IntScan->Data != NULL)
        {
          if (((IntRec *)(IntScan->Data))->Dist < MinDist)
          {
            BSPNode *t;
            t = BSPRoot->Child0;
            BSPRoot->Child0 = BSPRoot->Child1;
            BSPRoot->Child1 = t;
            MinDist = ((IntRec *)(IntScan->Data))->Dist;
          }
        }
      }

      if (Ans->Next->Data == NULL) WasteHBounds++;
    } else MissBounds++;
  } else MissBounds++;

  return(Ans);
}

ItemEntry *IntersectCSG(ray *ARay, BSPNode *BSPRoot, int Mode, Flt MinDist)
{
  enum CSGOp OpNone=MERGE;
  Object *AnObj;
  int BoundHit;
  Flt Dist;
  ItemEntry *AnItem, *BoundList, *IntList, *IntScan, *Ans=CreateList();

  BoundHit = BoxHit(ARay, &(BSPRoot->Bounder), &Dist);
  if (BoundHit != 0)
  {
    if (Dist < MinDist || Mode == 1)
    {
      HitBounds++;
      for (AnItem = BSPRoot->ObjectList->Next; AnItem->Data != NULL; AnItem = AnItem->Next)
      {
        AnObj = AnItem->Data;
        IntList = IntersectObject(ARay, AnObj, Mode, MinDist);
        if (IntList->Next->Data != NULL)
        {
          IntScan = Ans;
          Ans = IntMerge2(IntList, IntScan);
        }
        else DestroyList(IntList);
      }

      for (IntScan=Ans->Next; IntScan->Data != NULL && ((IntRec *)(IntScan->Data))->Dist < EFFECTIVE_ZERO; IntScan=IntScan->Next);
      if (IntScan->Data != NULL)
      {
        if (((IntRec *)(IntScan->Data))->Dist < MinDist) MinDist = ((IntRec *)(IntScan->Data))->Dist;
      }

      if (BSPRoot->Child0 != NULL)
      {
        IntList = Intersect(ARay, BSPRoot->Child0, Mode, MinDist);

        if (IntList->Next->Data != NULL)
        {
          IntScan = Ans;
          Ans = IntMerge2(IntList, IntScan);
        }
        else DestroyList(IntList);

        for (IntScan=Ans->Next; IntScan->Data != NULL && ((IntRec *)(IntScan->Data))->Dist < EFFECTIVE_ZERO; IntScan=IntScan->Next);
        if (IntScan->Data != NULL)
        {
          if (((IntRec *)(IntScan->Data))->Dist < MinDist)
          {
            MinDist = ((IntRec *)(IntScan->Data))->Dist;
          }
        }
      }

      if (BSPRoot->Child1 != NULL)
      {
        IntList = Intersect(ARay, BSPRoot->Child1, Mode, MinDist);
        if (IntList->Next->Data != NULL)
        {
          IntScan = Ans;
          Ans = IntMerge2(IntList, IntScan);
        }
        else DestroyList(IntList);
        for (IntScan=Ans->Next; IntScan->Data != NULL && ((IntRec *)(IntScan->Data))->Dist < EFFECTIVE_ZERO; IntScan=IntScan->Next);
        if (IntScan->Data != NULL)
        {
          if (((IntRec *)(IntScan->Data))->Dist < MinDist)
          {
            BSPNode *t;
            t = BSPRoot->Child0;
            BSPRoot->Child0 = BSPRoot->Child1;
            BSPRoot->Child1 = t;
            MinDist = ((IntRec *)(IntScan->Data))->Dist;
          }
        }
      }

      if (Ans->Next->Data == NULL) WasteHBounds++;
    } else MissBounds++;
  } else MissBounds++;

  return(Ans);
}

ItemEntry *IntersectObject(ray *ARay, Object *AnObj, int Mode, Flt MinDist)
{
  ItemEntry *IntScan, *Ans, *BoundList;
  IntRec *AnInt;
  Comp *CompInfo;
  ray BRay, CRay;
  int BoundHit;
  Flt Dist;

  NoOfInts++;

  if (AnObj->Model != NULL)
  {
    MProjectVector(&(ARay->Origin)   , AnObj->Model, &(BRay.Origin));
    MProjectNormal(&(ARay->Direction), AnObj->Model, &(BRay.Direction));
  }
  else
    BRay = *ARay;

  if (AnObj->ObjType != 0)
  {
    Ans = ((AnObj->Primative)->Intersect)(&BRay, AnObj);

    for (IntScan = Ans->Next; IntScan->Data != NULL; IntScan = IntScan->Next)
    {
      AnInt = IntScan->Data;
      if (AnInt->DoneNorm == 0)
      {
        ((AnInt->HitObj->Primative)->Normal)(AnInt, AnInt->HitObj->kdata);
        AnInt->DoneNorm = -1;
/*         if (VecDot(&(ARay->Direction), &(AnInt->Normal)) > 0.0)
        {
          VecNegate(&(AnInt->Normal),&(AnInt->Normal));
        } */
      }
      AnInt->Material = AnInt->HitObj->Material;

    }

  }
  else
  {
    CompInfo = (Comp *)(AnObj->kdata);
    MProjectVector(&(BRay.Origin)   , &(CompInfo->Model), &(CRay.Origin));
    MProjectNormal(&(BRay.Direction), &(CompInfo->Model), &(CRay.Direction));
    BoundHit = BoxHit(&CRay, &(((Defin*)AnObj->Primative)->BSPRoot->Bounder), &Dist);
    if (BoundHit != 0)
    {
      if (Dist < MinDist || Mode == 1)
      {
        HitBounds++;
        Ans = Intersect(&CRay, ((Defin*)AnObj->Primative)->BSPRoot, Mode, MinDist);
        for (IntScan = Ans->Next; IntScan->Data != NULL; IntScan = IntScan->Next)
        {
          AnInt = IntScan->Data;
          MInvertVector(&(AnInt->WorldHit), &(CompInfo->Inverse), &(AnInt->WorldHit));
          MInvertNormal(&(AnInt->Normal), &(CompInfo->Model), &(AnInt->Normal));
          VecUnit(&(AnInt->Normal), &(AnInt->Normal));
          if (AnInt->Material == NULL) AnInt->Material = AnObj->Material;
        }
      }
      else
      {
        Ans = CreateList();
        MissBounds++;
      }
    }
    else
    {
      Ans = CreateList();
      MissBounds++;
    }
  }
  if (AnObj->MapHere != 0)
  {
    for (IntScan = Ans->Next; IntScan->Data != NULL; IntScan = IntScan->Next)
    {
      if (AnInt->DoneMap2 == 0)
      {
        AnInt->MapCoord = AnInt->WorldHit;
        AnInt->DoneMap2 = 1;
      }
    }
  }
  if (AnObj->Model != NULL)
  {
    for (IntScan = Ans->Next; IntScan->Data != NULL; IntScan = IntScan->Next)
    {
      AnInt = IntScan->Data;
      MInvertVector(&(AnInt->WorldHit), AnObj->Inverse, &(AnInt->WorldHit));
      MInvertNormal(&(AnInt->Normal), AnObj->Model, &(AnInt->Normal));
      VecUnit(&(AnInt->Normal), &(AnInt->Normal));
    }
  }

  if (Ans->Next->Data != NULL) HitInts++;

  return (Ans);
}

MatDef *SurfaceColour(IntRec *AnInt, MatDef *MatAns)
{
  Object *AnObject;
  AnObject = AnInt->HitObj;
  if (AnInt->DoneMap2 == 0)
  {
    AnInt->DoneMap2 = 1;
    AnInt->MapCoord = AnInt->WorldHit;
  }
  ResolveMaterial(AnInt->Material, AnInt, MatAns);

  return(MatAns);
}

void CalcLighting(IntRec *AnInt, MatDef *HitMat, BSPNode *BSPRoot)
{
  int i, ShadSame;
  Light *ALight;
  ItemEntry *AnItem, *ShadeIntList;
  IntRec *IntScan, *CurrInt, *LastInt;
  Flt DistToLight, Dist, LastDist;
  CFlt Shunt, SampleWeight, FallOff;
  Vec DirToLight, LightPos, *Normal, *Hit;
  ray ShadowRay;
  colour ShadeValue, ShadeInt, LastShVal;
  colour DiffLight, Subc1;
  Flt Subf1, SpotScale, Angle;
  MatDef AMaterial;
  Normal = &(AnInt->Normal);
  Hit    = &(AnInt->WorldHit);


  /*   SetColour(1.0, 1.0, 1.0, &ShadeValue); */
  for (ALight = Lights;  ALight != NULL; ALight = ALight->NextLight)
  {
    SampleWeight = 1.0 / ALight->Samples;
    SetColour(0.0, 0.0, 0.0, &ShadeInt);
    SetColour(0.0, 0.0, 0.0, &DiffLight);
    ShadSame = -1;
    for (i=0; i<ALight->Samples; i++)
    {
      if (ALight->Primative == NULL)
      {
        VecSub(&(ALight->LPosition), Hit, &DirToLight);
      }
      else
      {
        (ALight->Primative->Random)(ALight->kdata, &LightPos);
        VecSub(&LightPos, Hit, &DirToLight);
      }
      DistToLight = VecLen(&DirToLight);
      VecUnit(&DirToLight, &DirToLight);
      FallOff = pow(DistToLight, ALight->FallOff);

      if (ALight->Spot == 1)
      {
         Angle = acos(VecDot(&(ALight->Direction), &DirToLight));
         if (Angle < ALight->HotSpot)
           SpotScale = 1.0;
         else
           if (Angle > ALight->Angle)
             SpotScale = 0.0;
           else
             SpotScale = 1.0 - pow((Angle - ALight->HotSpot) / (ALight->Angle - ALight->HotSpot), 2.0);
      }
      else
         SpotScale = 1.0;


      ShadowRay.Origin = *Hit;
      ShadowRay.Direction = DirToLight;
      LastDist = VecDot(&(ShadowRay.Direction), Normal);
      if (LastDist < 0.0 && HitMat->ktran == 0.0)
      {
        SetColour(0.0, 0.0, 0.0, &ShadeValue);
      }
      else
      {

         if (SHADOWS==YES && ALight->Shadows == 1)
           ShadeIntList = Intersect(&ShadowRay, BSPRoot, 1, DistToLight);
         else
           ShadeIntList = CreateList();

         CurrInt = NULL;
         LastDist = 0.0;

         for (SetColour(1.0, 1.0, 1.0, &ShadeValue), AnItem = ShadeIntList->Next; AnItem->Data != NULL; AnItem = AnItem->Next)
         {
           IntScan = AnItem->Data;
           if (IntScan != NULL)
           {
             if (IntScan->Dist > EFFECTIVE_ZERO && IntScan->Dist < DistToLight)
             {
               Dist = IntScan->Dist - LastDist;
               if (Dist <= 0.0) Dist = EFFECTIVE_ZERO;

               if (CurrInt != NULL)
               {
                  SurfaceColour(CurrInt, &AMaterial);
               }
               else
               {
                  AMaterial = *AirMat;
               }

               Subc1.red   = pow(AMaterial.ktran * AMaterial.TranCol.red,   Dist);
               Subc1.green = pow(AMaterial.ktran * AMaterial.TranCol.green, Dist);
               Subc1.blue  = pow(AMaterial.ktran * AMaterial.TranCol.blue,  Dist);
               ColourFilter(&ShadeValue, &Subc1, &ShadeValue);
             }
             LastDist = IntScan->Dist;
           }

           if (IntScan->Dirn == ENTRY)
             CurrInt = IntScan;
           else
             CurrInt = NULL;

           LastInt = IntScan;

         }
         if (i == 0)
         {
           LastShVal = ShadeValue;
         }
         else if (i < ALight->MinSamples)
         {
           if (fabs(LastShVal.red   - ShadeValue.red) +
               fabs(LastShVal.green - ShadeValue.green) +
               fabs(LastShVal.blue  - ShadeValue.blue) > 0.001)
           {
             ShadSame = 0;
           }
         }
         FreeIntList(ShadeIntList);
      }

      ColourAddS(&ShadeInt, SpotScale * SampleWeight / FallOff, &ShadeValue, &ShadeInt);
      ColourAddS(&DiffLight, (SpotScale * SampleWeight / FallOff) * fabs(VecDot(&(ShadowRay.Direction), Normal)), &ShadeValue, &DiffLight);
      if (i == ALight->MinSamples-1 && ShadSame == -1)
      {
        Shunt = ((Flt)(ALight->Samples)) / ((Flt)(ALight->MinSamples));
        ShadeInt.red    *= Shunt;
        ShadeInt.green  *= Shunt;
        ShadeInt.blue   *= Shunt;
        DiffLight.red   *= Shunt;
        DiffLight.green *= Shunt;
        DiffLight.blue  *= Shunt;
        i = ALight->Samples;
      }
    }
    ALight->Occlusion = ShadeInt;
    ALight->DiffLight = DiffLight;
  }
}



colour *Shade(CFlt Weight, IntRec *AnInt, Vec *RayDir, int Iter, BSPNode *BSPRoot, colour *answer)
{
  colour Subc1;
  MatDef Material;
  SurfaceColour(AnInt, &Material);
  ColourFilter(&(Material.AmbCol), &Ambiant, answer);
  CalcLighting(AnInt, &Material, BSPRoot);
  if (Material.kdiff != 0.0)
  {
    ColourAddS(answer,
	       Material.kdiff,
	       kDiff(&(Material.DiffCol), &Subc1),
	       answer);
  }
 
  if (Material.kspec != 0.0)
  {
    ColourAddS(answer,
	       Material.kspec,
	       kSpec(Weight * Material.kspec, AnInt, RayDir, Iter, BSPRoot, &Subc1, &Material),
	       answer);
  }
 
  if (Material.ktran != 0.0)
  {
    ColourAddS(answer,
	       Material.ktran,
	       kTran(Weight * Material.ktran, AnInt, RayDir, Iter, BSPRoot, &Subc1, &Material),
	       answer);
  }
  return(answer);
}

colour *kDiff(colour *surfcol, colour *answer)
{
  colour Subc1, Subc2;
  Light *ALight;
  /* colour LightIntensity; */
  SetColour(0.0, 0.0, 0.0, answer);
  SetColour(0.0, 0.0, 0.0, &Subc1);
  SetColour(0.0, 0.0, 0.0, &Subc2);

  for (ALight = Lights;  ALight != NULL; ALight = ALight->NextLight)
  {
    ColourFilter(&(ALight->DiffLight), ColourFilter(surfcol, &(ALight->LColour), &Subc1), &Subc2);
    ColourAddS(answer,
	       1.0,
	       &Subc2,
	       answer);
  }

  return(answer);
}

colour *kSpec(CFlt Weight, IntRec *AnInt, Vec *RayDir, int Iter, BSPNode *BSPRoot, colour *answer, MatDef *Material)
{
  colour Subc1, Subc2, Subc3;
  Light *ALight;
  Flt DistToLight, Subf1;
  CFlt Lambert;
  colour LightIntensity;
  Vec DirToLight, ReflectDir, Sub1;
  ray ReflectRay;
  int i;
  ray R2;
  Flt u, v, theta, phi, r;
  Flt x, y, z, x1, y1, z1;
  SetColour(0.0, 0.0, 0.0, answer);
  SetColour(0.0, 0.0, 0.0, &Subc1);
  SetColour(0.0, 0.0, 0.0, &Subc2);
  SetColour(0.0, 0.0, 0.0, &Subc3);
  Subf1 = VecDot(&(AnInt->Normal), RayDir);
  VecAdds( -2.0 * Subf1, &(AnInt->Normal), RayDir, &ReflectDir);
  ReflectRay.Direction = ReflectDir;
  ReflectRay.Origin    = AnInt->WorldHit;
  VecNegate(RayDir, RayDir);
  for (ALight=Lights; ALight != NULL; ALight = ALight->NextLight)
  {
    VecSub(&(ALight->LPosition), &(AnInt->WorldHit), &DirToLight);

    DistToLight = VecLen(&DirToLight);

    VecUnit(&DirToLight, &DirToLight);
    VecAdd(RayDir, &DirToLight, &Sub1);
    VecUnit(&Sub1, &Sub1);
    Lambert = VecDot(&Sub1, &(AnInt->Normal));

    if (Lambert < 0.0) Lambert = 0.0;
    ColourShade(Lambert,
                &(ALight->Occlusion),
                &LightIntensity);

    LightIntensity.red	 = pow(LightIntensity.red,   Material->kspecpow);
    LightIntensity.green = pow(LightIntensity.green, Material->kspecpow);
    LightIntensity.blue  = pow(LightIntensity.blue,  Material->kspecpow);

    ColourAddS(answer,
	       1.0,
	       ColourFilter(&LightIntensity,
			    ColourFilter(&(ALight->LColour),
					 &(Material->SpecCol),
					 &Subc1),
			    &Subc3),
	       answer);
  }

  if (REFLECTIONS == YES && Material->kref > 0.0 && Weight > 0.005)
  {
    if (Material->GlossSamples > 0)
    {
      R2 = ReflectRay;
      for (i=0; i < Material->GlossSamples; i++)
      {
        u =  rand();
        u /= RAND_MAX;
        v =  rand();
        v /= RAND_MAX;
        theta = acos(1.0-2.0*u);
        phi   = 2.0 * PI * v;
        x = 0.0;
        y = 1.0;
        z = 0.0;
        x1 = x * cos(theta) - y * sin(theta);
        y1 = x * sin(theta) + y * cos(theta);
        z1 = z;
        x  = x1 * cos(phi) - z1 * sin(phi);
        y  = y1;
        z  = x1 * sin(phi) + z1 * cos(phi);
 /*        r = rand() * 1.0;
        r /= RAND_MAX;
        r = 1.0-cos((PI/2)*r); */
        r = Material->GlossSpread;
        if (r == 0.0) r = 0.1;
        R2.Direction.x = ReflectRay.Direction.x + r * x;
        R2.Direction.y = ReflectRay.Direction.y + r * y;
        R2.Direction.z = ReflectRay.Direction.z + r * z;
        if (Iter < RecurseDepth) ColourAddS(answer,
                         Material->kref * (1.0/Material->GlossSamples),
                         ColourFilter(&(Material->SpecCol),
                                  Trace(Weight * Material->kref * (1.0/Material->GlossSamples),
                                    &R2,
                                    Iter+1,
                                    BSPRoot,
                                    &Subc1
                                   ),
                                  &Subc2
                                 ),
                         answer
                        );
      }
    }
    else
    {
      if (Iter < RecurseDepth) ColourAddS(answer,
                       Material->kref,
                       ColourFilter(&(Material->SpecCol),
                                Trace(Weight * Material->kref,
                                  &ReflectRay,
                                  Iter+1,
                                  BSPRoot,
                                  &Subc1
                                 ),
                                &Subc2
                               ),
                       answer
                      );
    }
  }
  VecNegate(RayDir, RayDir);

  return(answer);
}

colour *kTran(CFlt Weight, IntRec *AnInt, Vec *RayDir, int Iter, BSPNode *BSPRoot, colour *answer, MatDef *AMat)
{
  CFlt NewRI, OldRI;
  ray RefractRay;
  int AOK;
  Vec TranDir;
  colour Subc1;

  SetColour(0.0, 0.0, 0.0, answer);
  SetColour(0.0, 0.0, 0.0, &Subc1);

  if (AnInt->Dirn == ENTRY)
  {
    NewRI = AMat->kri;
    OldRI = 1.0;
  }
  else
  {
    NewRI = 1.0;
    OldRI = AMat->kri;
  }

  TransmissionDirection(RayDir, &(AnInt->Normal), OldRI, NewRI, &AOK, &TranDir);

  if (AOK == YES)
  {
    AmbiantRI = NewRI;
    RefractRay.Origin	 = AnInt->WorldHit;
    RefractRay.Direction = TranDir;
    if (Iter < RecurseDepth && Weight > 0.05) Trace(Weight, &RefractRay, Iter+1, BSPRoot, answer);
  }
  else
  {
	if (AMat->kspec != 0.0)
	{
	ColourAddS(answer,
		 AMat->kspec,
		 kSpec(Weight * AMat->kspec, AnInt, RayDir, Iter+1, BSPRoot, &Subc1, AMat),
		 answer);
	}
  }

  return(answer);
}

Vec *TransmissionDirection(Vec *Insident, Vec *Normal, Flt OldRI, Flt NewRI, int *AOK, Vec *answer)
{
  Vec ANorm;
  Flt eta, c1, cs2;
  ANorm = *Normal;
  eta = OldRI / NewRI;
  c1 = -VecDot(Insident, &ANorm);
  if (c1 < 0.0)
  {
     VecNegate(&ANorm, &ANorm);
     c1 = -VecDot(Insident,&ANorm);
  }
  cs2 = 1.0 - eta * eta * (1.0 - c1*c1);

  if (cs2 < EFFECTIVE_ZERO)
     *AOK = NO;
  else
  {
    *AOK = YES;
    VecComb(eta, Insident, eta*c1-sqrt(cs2), &ANorm, answer);
  }
  return(answer);
}

void SetupIntFreeList()
{
  ItemEntry *AnItem;
  IntRec *AnInt;
  for (AnItem = IntFreeBuff->Next; AnItem->Data != NULL; AnItem = AnItem->Next)
  {
    AnInt = AnItem->Data;
    AnInt->Dirn	  = NONE;
    AnInt->Dist	  = 9999999999.999;
    AnInt->HitObj = NULL;
  }

}

ItemEntry *IntMerge(ItemEntry *Ans1, ItemEntry *Ans2, enum CSGOp ThisOp)
{
  ItemEntry *Ans=CreateList(), *AnsTail;
  IntRec *A, *B;
  int Leva1, Leva2, Lev1, Lev2, Lev, OldLev;
  int Pick;

  Lev1 = 0;
  Lev2 = 0;

  AnsTail = Ans;

  if (Ans1->Next->Data != NULL)
  {
    if (((IntRec *)(Ans1->Next->Data))->Dirn == EXIT)
    {
      Lev1 = 1;
    }
    else
    {
      Lev1 = 0;
    }
  }

  if (Ans2->Next->Data != NULL)
  {
    if (((IntRec *)(Ans2->Next->Data))->Dirn == EXIT)
    {
      Lev2 = 1;
    }
    else
    {
      Lev2 = 0;
    }
  }

  if (ThisOp == SUBTRACT)
     Lev = Lev1 - Lev2;
  else
     Lev = Lev1 + Lev2;

  while (!(Ans1->Next->Data == NULL && Ans2->Next->Data == NULL))
  {
    OldLev = Lev;
    if (Ans1->Next->Data != NULL)
    {
      if (Ans2->Next->Data == NULL)
      {
        Pick = 1;
      }
      else
      {
        if (((IntRec *)(Ans1->Next->Data))->Dist < ((IntRec *)(Ans2->Next->Data))->Dist)
        {
          Pick = 1;
        }
        else
        {
          Pick = 2;
        }
      }
    }
    else
    {
      Pick = 2;
    }

    if (Pick == 1)
    {
      if (((IntRec *)(Ans1->Next->Data))->Dirn == ENTRY)
      {
        Lev1++;
      }
      else
      {
        Lev1--;
      }

    }
    else
    {
      if (((IntRec *)(Ans2->Next->Data))->Dirn == ENTRY)
      {
        Lev2++;
      }
      else
      {
        Lev2--;
      }
    }

    if (Lev1 > 0)
      Leva1 = 1;
    else
      Leva1 = 0;

    if (Lev2 > 0)
      Leva2 = 1;
    else
      Leva2 = 0;


    if (ThisOp == UNION)
    {
      /* Handle Union */
      Lev = Leva1 + Leva2;
      if ((Lev > 0 && OldLev == 0) || (Lev == 0 && OldLev > 0))
      {
        if (Pick == 1)
        {
          A = UnlinkFromList(Ans1->Next);
          AnsTail = LinkInToList(A, AnsTail, A->Parent);
        }
        else
        {
          A = UnlinkFromList(Ans2->Next);
          AnsTail = LinkInToList(A, AnsTail, A->Parent);
        }
      }
      else
      {
        if (Pick == 1)
        {
          B = UnlinkFromList(Ans1->Next);
          FreeInt(B);
        }
        else
        {
          B = UnlinkFromList(Ans2->Next);
          FreeInt(B);
        }
      }
    }
    else if (ThisOp == COMMON)
    {
      /* Handle Intersection */
      printf("");fflush(stdout);
      Lev = Leva1 + Leva2;
      if ((Lev < 2 && OldLev == 2) || (Lev == 2 && OldLev < 2))
      {
        if (Pick == 1)
        {

          A = UnlinkFromList(Ans1->Next);
          AnsTail = LinkInToList(A, AnsTail, A->Parent);
        }
        else
        {
          A = UnlinkFromList(Ans2->Next);
          AnsTail = LinkInToList(A, AnsTail, A->Parent);
        }
      }
      else
      {
        if (Pick == 1)
        {
          B = UnlinkFromList(Ans1->Next);
          FreeInt(B);

        }
        else
        {
          B = UnlinkFromList(Ans2->Next);
          FreeInt(B);
        }
      }
      printf("");fflush(stdout);
    }
    else if (ThisOp == SUBTRACT)
    {
      /* Handle Subtraction */
      Lev = Leva1 - Leva2;
      if ((Lev < 1 && OldLev == 1) || (Lev == 1 && OldLev < 1))
      {
        if (Pick == 1)
        {

          A = UnlinkFromList(Ans1->Next);
          AnsTail = LinkInToList(A, AnsTail, A->Parent);

        }
        else
        {
          A = UnlinkFromList(Ans2->Next);
          AnsTail = LinkInToList(A, AnsTail, A->Parent);
          if (A->Dirn == EXIT)
            A->Dirn = ENTRY;
          else
            A->Dirn = EXIT;

          VecNegate(&(A->Normal), &(A->Normal));
        }
      }
      else
      {
        if (Pick == 1)
        {
          B = UnlinkFromList(Ans1->Next);
          FreeInt(B);
        }
        else
        {
          B = UnlinkFromList(Ans2->Next);
          FreeInt(B);
        }
      }

    }
    else
    {
      /* Handle None */
      Lev = Leva1 + Leva2;
/*	if (Lev != OldLev)
      {
        if (Pick == 1)
        {
          A = UnlinkFromList(Ans1->Next);
          AnsTail = LinkInToList(A, AnsTail, A->Parent);
        }
        else
        {
          A = UnlinkFromList(Ans2->Next);
          AnsTail = LinkInToList(A, AnsTail, A->Parent);
        }
      }
      else
      {
        if (Pick == 1)
        {
          B = UnlinkFromList(Ans1->Next);
          FreeInt(B);
        }
        else
        {
          B = UnlinkFromList(Ans2->Next);
          FreeInt(B);
        }
      }
    */
	if (Pick == 1)
	{
      A = UnlinkFromList(Ans1->Next);
      AnsTail = LinkInToList(A, AnsTail, A->Parent);
	}
	else
	{
      A = UnlinkFromList(Ans2->Next);
      AnsTail = LinkInToList(A, AnsTail, A->Parent);
	}

    }
  }
  DestroyList(Ans1);
  DestroyList(Ans2);

  return(Ans);
}

ItemEntry *IntMerge2(ItemEntry *Ans1, ItemEntry *Ans2)
{
  ItemEntry *Ans=CreateList(), *AnsTail;
  IntRec *A;

  AnsTail = Ans;

  while (!(Ans1->Next->Data == NULL || Ans2->Next->Data == NULL))
  {
    if (((IntRec *)(Ans1->Next->Data))->Dist < ((IntRec *)(Ans2->Next->Data))->Dist)
	{
      A = UnlinkFromList(Ans1->Next);
      AnsTail = LinkInToList(A, AnsTail, A->Parent);
	}
	else
	{
      A = UnlinkFromList(Ans2->Next);
      AnsTail = LinkInToList(A, AnsTail, A->Parent);
	}
  }
  while (Ans1->Next->Data != NULL)
  {
    A = UnlinkFromList(Ans1->Next);
    AnsTail = LinkInToList(A, AnsTail, A->Parent);
  }
  while (Ans2->Next->Data != NULL)
  {
    A = UnlinkFromList(Ans2->Next);
    AnsTail = LinkInToList(A, AnsTail, A->Parent);
  }
  DestroyList(Ans1);
  DestroyList(Ans2);

  return(Ans);
}

LightInfo *GenLightMap(Light *ALight)
{
  Vec A, XStep, YStep, XStop;
  int i, j;
  LightInfo *Ans;
  ray ARay;
  ItemEntry *IntList, *AnItem;
  IntRec *AnInt;

  if ((Ans = malloc(sizeof(LightInfo))) == NULL)
  {
    printf("Error -- Unable to allocate storage for Light Map\n");
    exit(10);
  }
  SetVector(1.0, 0.0, 0.0, &A);
  if (VecDot(&A, &(ALight->Direction)) > 0.99)
  {
    SetVector(0.0, 1.0, 0.0, &A);
  }
  VecProduct(&A, &(ALight->Direction), &(Ans->XAxis));
  VecProduct(&(Ans->XAxis), &(ALight->Direction), &(Ans->YAxis));
  VecUnit(&(Ans->XAxis),&(Ans->XAxis));
  VecUnit(&(Ans->YAxis),&(Ans->YAxis));

  VecAdds(-ALight->Radius, &(Ans->XAxis), &(ALight->PointAt), &A);
  VecAdds(-ALight->Radius, &(Ans->YAxis), &A, &A);

  VecScalar(ALight->Radius / 50.0, &(Ans->XAxis), &XStep);
  VecScalar(ALight->Radius / 50.0, &(Ans->YAxis), &YStep);

  for (j = 0; j < 100; j++)
  {
    XStop = A;
    for (i = 0; i < 100; i++)
    {
      /* Create Ray */
      ARay.Origin = ALight->LPosition;
      VecSub(&A, &(ALight->LPosition), &(ARay.Direction));
      VecUnit(&(ARay.Direction), &(ARay.Direction));

      IntList = Intersect(&ARay, BSPRoot, 0, 9999999999.9999);
      Ans->DepthMap[i][j] = 99999999999.9;
      for (AnItem = IntList->Next; AnItem->Data != NULL; AnItem=AnItem->Next)
      {
        AnInt = AnItem->Data;
        if (AnInt->Dist > EFFECTIVE_ZERO && AnInt->Dist < Ans->DepthMap[i][j])
        {
          Ans->DepthMap[i][j] = AnInt->Dist;
        }
      }


      FreeIntList(IntList);
      VecAdd(&A, &XStep, &A);
    }
    VecAdd(&XStop, &YStep, &A);
  }
  {
     Cyl *ACyl;
     Vec Sub1, XAxis, YAxis;
     if ((Ans->TheCyl = malloc(sizeof(Object))) == NULL)
     {
        printf("Error - Unable to allocate Light Object\n");
        exit(1);
     }
     if ((ACyl = malloc(sizeof(Cyl))) == NULL)
     {
       printf("Error - Unable to allocate Cone\n");
       exit(1);
     }
     Ans->TheCyl->kdata = ACyl;

     /* Initialise the Light Cone */

     VecSub(&(ALight->LPosition), &(ALight->PointAt), &Sub1);
     ACyl->Length = VecLen(&Sub1);
     VecSub(&(ALight->PointAt), &(ALight->LPosition), &Sub1);
     VecUnit(&Sub1, &YAxis);
     ACyl->Length = VecLen(&Sub1);
     ACyl->c = 0.0;
     ACyl->m = (ALight->Radius) / ACyl->Length;
     ACyl->sint = -sin(atan(ACyl->m));
     ACyl->cost = cos(atan(ACyl->m));
     ACyl->Rad1Sq = 0.0;
     ACyl->Rad2Sq = pow(ALight->Radius, 2.0);
     ACyl->Radius = ALight->Radius;

     SetVector(0.0, 0.0, 1.0, &Sub1);
     if (fabs(VecDot(&YAxis, &Sub1)) > 0.8)
     {
       SetVector(1.0, 0.0, 0.0, &Sub1);
     }

     MIdentity(&(ACyl->Model));
     VecUnit(VecProduct(&YAxis, &Sub1, &XAxis), &XAxis);
     MAxis(&XAxis, &YAxis, &(ALight->LPosition), &(ACyl->Model), &(ACyl->Inverse));


  }
  return(Ans);
}
