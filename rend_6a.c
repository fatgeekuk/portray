/*
  *************************************************************************
 **                                                                       **
 **       Renderer                                                        **
 **           Scene Rendering Module                                      **
 **                                                                       **
  *************************************************************************
*/

#include <conio.h>
#include "vectors.h"
#include <stdio.h>
#include <stdlib.h>
#include "rend_0.h"
#include <time.h>
#include <dos.h>
#include <string.h>
#include <graph.h>
#include "bmp.h"
#include "boxs.h"


colour *Trace();
colour *Shade();
colour *kDiff();
colour *kSpec();
colour *kTran();
Vec *TransmissionDirection();
colour *SurfaceColour();
void Intersect();

Flt AmbiantRI;
char INOUT;

void RenderScene(char *outputfilename)
{
  FILE *OutputFile;
  char *PixelBuffer;
  int x, y, SampleCount, PixCount=0;
  Vec XBase, YBase, XStep, YStep, Sub1;
  colour PixColour;
  time_t timer1, timer2;
  long RayCount=0;
  char drive[_MAX_DRIVE];
  char dir[_MAX_DIR];
  char file[_MAX_FNAME];
  char ext[_MAX_EXT];
  struct BMPFileHeader BMPFileHD;
  struct BMPInfoHeader BMPInfoHD;

  timer1 = time(NULL);
  if((PixelBuffer = malloc(3*ImageXSize+4)) == NULL)
  {
    printf("Error -- Unable to allocate Pixel Buffer\n");
    exit(1);
  }

  printf("Start Render   ");

  _splitpath(outputfilename,drive,dir,file,ext); /* split the string to separate elems */
  strcpy(ext,".BMP");
  _makepath(outputfilename,drive,dir,file,ext);	 /* merge everything into one string */

  if ((OutputFile = fopen(outputfilename, "wb")) == NULL)
  {
    printf("ERROR -- Cannot Open OutputFile (%s)\n", outputfilename);
    exit(1);
  }
  else
  {
    /* Prepare the BMP File Header */
    BMPFileHD.Ident0	='B';
    BMPFileHD.Ident1	='M';
    BMPFileHD.filesize	=sizeof(BMPFileHD)+sizeof(BMPInfoHD)+4*((long)ImageXSize * (long)ImageYSize);
    BMPFileHD.Fill1	=0;
    BMPFileHD.Fill2	=0;
    BMPFileHD.PixStart	=sizeof(BMPFileHD)+sizeof(BMPInfoHD);
    /* Prepare the BMP Info Header */
    BMPInfoHD.InfoLen		= sizeof(BMPInfoHD);
    BMPInfoHD.Width		= (long)ImageXSize;
    BMPInfoHD.height		= (long)ImageYSize;
    BMPInfoHD.Setto1		=1;
    BMPInfoHD.BitsPerPix	=24;
    BMPInfoHD.CompScheme	=0;
    BMPInfoHD.BitsLen		=0;
    BMPInfoHD.HPixperMetre	=0;
    BMPInfoHD.VPixperMetre	=0;
    BMPInfoHD.ColsInImage	=0;
    BMPInfoHD.ImpColsImage	=0;
    fwrite(&BMPFileHD, sizeof(char), sizeof(BMPFileHD), OutputFile);
    fwrite(&BMPInfoHD, sizeof(char), sizeof(BMPInfoHD), OutputFile);
    SampleCount = 0;
    VecSub(&CameraOrigin, VecComb(.5, &CameraXSide, 0.5, &CameraYSide, &Sub1), &YBase);
    VecScalar(1.0/(Flt)ImageXSize, &CameraXSide, &XStep);
    VecScalar(1.0/(Flt)ImageYSize, &CameraYSide, &YStep);
    printf("Rendering\n  up to (%d).", ImageYSize);
    for (y=0; y<ImageYSize; y++)
    {
      XBase = YBase;
      for (x=0; x<ImageXSize; x++)
      {
	AmbiantRI = 1.0;
	((UsedFilters->ThisFilter)->Exec)(UsedFilters->NextUsed,
					  &CameraPinhole,
					  &XBase,
					  &XStep,
					  &YStep,
					  UsedFilters->fdata,
					  &PixColour);

	PixelBuffer[SampleCount++] = (char)(255.0 * PixColour.red);
	PixelBuffer[SampleCount++] = (char)(255.0 * PixColour.green);
	PixelBuffer[SampleCount++] = (char)(255.0 * PixColour.blue);
	VecAdd(&XBase, &XStep, &XBase);
	RayCount++;
	if ((RayCount % 100) == 0)
	{
	  printf(".");

	}
      }
      while ((SampleCount % 3) != 0)
	 PixelBuffer[SampleCount++] = 0;

      fwrite(PixelBuffer, sizeof(char), SampleCount, OutputFile);
      SampleCount = 0;
      VecAdd(&YBase, &YStep, &YBase);
    }
    if (SampleCount > 0)
      fwrite(PixelBuffer, sizeof(char), SampleCount, OutputFile);
    fclose(OutputFile);
    timer2 = time(NULL);
    printf("Completed, Run for %ld Seconds.\n", timer2-timer1);
  }
}

colour *Trace(ray *PrimaryRay, int Iter, Object *ObjectList, colour *Answer)
{
  Object *Obj;
  Flt Intersection, GoodInt;
  Vec Hit;
  GoodInt = 9999.9;
  Intersect(PrimaryRay, &Hit, &Obj, &Intersection, ObjectList);
  if (Obj != NULL)
  {
    Shade(&Hit, Obj, &(PrimaryRay->Direction), Iter, ObjectList, Answer);
    if (Answer->red   > 1.0) Answer->red   = 1.0;
    if (Answer->green > 1.0) Answer->green = 1.0;
    if (Answer->blue  > 1.0) Answer->blue  = 1.0;
  }
  else
  {
    SetColour(0.0, 0.0, 0.0, Answer);
  }
  return Answer;
}

void Intersect(ray *ARay, Vec *Hit, Object **Obj, Flt *Dist, Object *ObjectList)
{
  Object *AnObj, *AnoObj;
  Flt Intersection, GoodInt;
  ray BRay,CRay;
  Comp *CompInfo;
  GoodInt = 9999.9;
  *Obj = NULL;
  for (AnObj = ObjectList; AnObj != NULL; AnObj = AnObj->NextObject)
  {
    if (AnObj->ObjectType =='P')
    {
      ((AnObj->Primative)->Intersect)(ARay, AnObj->kdata, &Intersection);
      if (Intersection > EFFECTIVE_ZERO && Intersection < GoodInt)
      {
	*Obj = AnObj;
	GoodInt = Intersection;
	CRay = *ARay;
      }
    }
    else
    {
      CompInfo = (Comp *)(AnObj->kdata);
      BRay = *ARay;
      VecSub(&(BRay.Origin), &(CompInfo->Offset), &(BRay.Origin));
      BoxIntersect(&BRay, &(((Defin*)AnObj->Primative)->Bounder), Dist);
      if (*Dist > EFFECTIVE_ZERO && *Dist < GoodInt)
      {
	Intersect(&BRay, Hit, &AnoObj, Dist, ((Defin*)AnObj->Primative)->Obj);
	if (*Dist > EFFECTIVE_ZERO && *Dist < GoodInt)
	{
	  GoodInt = *Dist;
	  *Obj = AnoObj;
	  CRay = BRay;
	}
      }
    }
  }
  if (*Obj != NULL)
  {
    VecComb(1.0, &(CRay.Origin), GoodInt, &(CRay.Direction), Hit);
    *Dist = GoodInt;
  }
  else
  {
    *Dist = -1.0;
  }
}

colour *SurfaceColour(Object *AnObject, Vec *Hit, colour *answer, Vec *Normal)
{
  Flt u, v;
  Vec Sub1, Centre;
  colour *Acol;
  if (AnObject->ColourType == VALUE)
  {
    Acol = &(AnObject->kColour.Value);

    SetColour(Acol->red, Acol->green, Acol->blue, answer);
  }
  else
  {
    Sub1 = *Hit;
    /* ((AnObject->Primative)->Centre) (AnObject->kdata, &Centre);
    VecSub(Hit,&Centre, &Sub1); */
    ((AnObject->Primative)->Inverse) (Hit, AnObject->kdata, &u, &v);

    (AnObject->kColour.Procedure.Eval)(AnObject->kColour.Procedure.Vars,
				       u,
				       v,
				       answer,
				       &Sub1,
				       Normal
				       );

  }
  return(answer);
}

Flt ShadowInt(ray *ARay, Flt *Dist, Object *ObjectList, Light *ThisLight)
{
  Flt Ans;
  Object *AnObj, *AnoObj;
  Flt Intersection;
  ray BRay;
  Comp *CompInfo;
  Ans = 1.0;
  if (ThisLight->LastShadow != NULL)
  {
    (((ThisLight->LastShadow)->Primative)->Intersect)(ARay, (ThisLight->LastShadow)->kdata, &Intersection);
    if (Intersection > EFFECTIVE_ZERO)
    {
      Ans *= (ThisLight->LastShadow)->ktran;
    }
  }
  AnObj = ObjectList;
  while ((Ans > EFFECTIVE_ZERO) && (AnObj != NULL))
  {
    if (AnObj->ObjectType == 'P')
    {
      if (AnObj != ThisLight->LastShadow)
      {
	((AnObj->Primative)->Intersect)(ARay, AnObj->kdata, &Intersection);
	if (Intersection > EFFECTIVE_ZERO)
	{
	  Ans *= AnObj->ktran;
	  ThisLight->LastShadow = AnObj;
	}
      }
    }
    else
    {
      /*
	 Warning This code WILL NOT WORK, it is attempting to Cast AnObj to
	 Comp *, This is invalid and needs more work, it also needs the
	 intersecting object returned as a Pass By Reference Formal Parameter


      BRay = *ARay;
      CompInfo = (Comp *)(AnObj->kdata);
      VecSub(&(BRay.Origin), &(CompInfo->Offset), &(BRay.Origin));
      BoxIntersect(&BRay, &(((Defin*)AnObj->Primative)->Bounder), &Intersection);
      if (Intersection > EFFECTIVE_ZERO)
      {
	ShadowInt(&BRay, &Intersection, ((Defin*)AnObj->Primative)->Obj, ThisLight);
	if (Intersection > EFFECTIVE_ZERO)
	{
	  Ans *= AnoObj->ktran;
	}
      }
      */
    }
    AnObj = AnObj->NextObject;
  }
  return(Ans);
}

void CalcLighting(Vec *Hit, Vec *Normal. Vec *RayDir, Object *ObjectList)
{
  int i;
  Light *ALight;
  Flt DistToShadow, ShadeInt, SampleWeight;
  Vec DirToLight, LightPos;
  ray ShadowRay;
  Flt ShadeValue;
  Flt DiffLight;

  for (ALight = Lights;  ALight != NULL; ALight = ALight->NextLight)
  {
    SampleWeight = 1.0 / ALight->Samples;
    ShadeInt = 0.0;
    DiffLight = 0.0;
    for (i=0; i<ALight->Samples; i++)
    {
      if (ALight->Primative == NULL)
      {
	 VecSub(&(ALight->LPosition), Hit, &DirToLight);
      }
      else
      {
	(ALight->Primative->Random)(ALight->kdata, &LightPos);
	VecSub(&LightPos, Hit, &DirToLight);
      }
      VecUnit(&DirToLight, &DirToLight);
      if (SHADOWS==YES)
      {
	ShadowRay.Origin = *Hit;
	ShadowRay.Direction = DirToLight;
	ShadeValue = ShadowInt(&ShadowRay, &DistToShadow, ObjectList, ALight);
	ShadeInt += (SampleWeight * ShadeValue);
	DiffLight += SampleWeight * VecDot(&(ShadowRay.Direction), Normal);
      }
      else
      {
	ShadeInt += SampleWeight;
	DiffLight += SampleWeight * VecDot(&(ShadowRay.Direction), Normal);
      }
    }
    ALight->Occlusion = ShadeInt;
    ALight->DiffLight = DiffLight;
  }
}



colour *Shade(Vec *Hit, Object *AnObject, Vec *RayDir, int Iter, Object *ObjectList, colour *answer)
{
  colour Col, Subc1;
  Vec Normal;
  ((AnObject->Primative)->Normal) (Hit, AnObject->kdata, &Normal);
  SurfaceColour(AnObject, Hit, &Col, &Normal);
  ColourFilter(&Col, &Ambiant, answer);


  if (VecDot(&Normal, RayDir) > EFFECTIVE_ZERO)
  {
    VecNegate(&Normal, &Normal);
    INOUT = OUT;
  }
  else
    INOUT = IN;

  CalcLighting(Hit, &Normal, RayDir, ObjectList);
  if (AnObject->kdiff != 0.0)
  {
    ColourAddS(answer,
	       AnObject->kdiff,
	       kDiff(Hit, &Normal, &Col, &Subc1),
	       answer);
  }

  if (AnObject->kspec != 0.0)
  {
    ColourAddS(answer,
	       AnObject->kspec,
	       kSpec(Hit, &Normal, RayDir, AnObject, Iter, &Col, ObjectList, &Subc1),
	       answer);

  }

  if (AnObject->ktran != 0.0)
  {
    ColourAddS(answer,
	       AnObject->ktran,
	       kTran(Hit, &Normal, RayDir, AnObject, Iter, ObjectList, &Subc1),
	       answer);
  }
  return(answer);
}

colour *kDiff(Vec *Hit, Vec *Normal, colour *surfcol, colour *answer)
{
  Light *ALight;
  Flt LightIntensity;
  Vec DirToLight;
  colour Subc1;
  SetColour(0.0, 0.0, 0.0, answer);

  for (ALight = Lights;  ALight != NULL; ALight = ALight->NextLight)
  {
    VecSub(&(ALight->LPosition), Hit, &DirToLight);
    VecUnit(&DirToLight, &DirToLight);
    if (ALight->Occlusion > EFFECTIVE_ZERO)
    {
      LightIntensity = ALight->Occlusion * ALight->DiffLight;
      if (LightIntensity > 0.0)
      {
	ColourAddS(answer,
		   LightIntensity,
		   ColourFilter(surfcol,
				&(ALight->LColour),
				&Subc1),
		   answer);
      }
    }
  }

  return(answer);
}

colour *kSpec(Vec *Hit, Vec *Normal, Vec *RayDir, Object *AnObject, int Iter, colour *surfcol, Object *ObjectList, colour *answer)
{
  colour Subc1;
  Light *ALight;
  Flt DistToLight, LightIntensity;
  Vec DirToLight, ReflectDir, Sub1;
  ray ReflectRay;
  SetColour(0.0, 0.0, 0.0, answer);
  VecAdds( -2.0 * VecDot(Normal, RayDir), Normal, RayDir, &ReflectDir);
  ReflectRay.Direction = ReflectDir;
  ReflectRay.Origin    = *Hit;
  VecNegate(RayDir, RayDir);
  for (ALight=Lights; ALight != NULL; ALight = ALight->NextLight)
  {
    VecSub(&(ALight->LPosition), Hit, &DirToLight);
    DistToLight = VecLen(&DirToLight);
    VecUnit(&DirToLight, &DirToLight);
    LightIntensity = ALight->Occlusion * VecDot(VecUnit(VecAdd(RayDir, &DirToLight, &Sub1), &Sub1), Normal);
    if (LightIntensity > 0.0)
    {
      LightIntensity = pow(LightIntensity, AnObject->kspecpow);
      ColourAddS(answer,
		 LightIntensity,
		 ColourFilter(&(ALight->LColour),
			      surfcol,
			      &Subc1),
		 answer);
    }
  }
  if (REFLECTIONS == YES)
  {
    if (Iter < RECURSE_LEVEL) ColourAddS(answer,
					 1.0,
					 ColourFilter(surfcol,
						      Trace(&ReflectRay,
							    Iter+1,
							    ObjectList,
							    &Subc1
							   )
						     ),
					 answer
					);
  }
  return(answer);
}

colour *kTran(Vec *Hit, Vec *Normal, Vec *RayDir, Object *AnObject, int Iter, Object *ObjectList, colour *answer)
{
  Flt NewRI, OldRI = AmbiantRI;
  ray RefractRay;
  int AOK;
  Vec TranDir;
  SetColour(0.0, 0.0, 0.0, answer);
  if (INOUT == IN)
     NewRI = AnObject->kri;
  else
     NewRI = 1.0;

  TransmissionDirection(RayDir, Normal, AmbiantRI, NewRI, &AOK, &TranDir);
  if (AOK == YES)
  {
    AmbiantRI = NewRI;
    RefractRay.Origin    = *Hit;
    RefractRay.Direction = TranDir;
    if (Iter < RECURSE_LEVEL) Trace(&RefractRay, Iter+1, ObjectList, answer);
  }
  AmbiantRI = OldRI;
  return(answer);
}

Vec *TransmissionDirection(Vec *Insident, Vec *Normal, Flt OldRI, Flt NewRI, int *AOK, Vec *answer)
{
  Flt eta, c1, cs2;
  eta = OldRI / NewRI;
  c1 = -VecDot(Insident, Normal);
  cs2 = 1.0 - eta * eta * (1.0 - c1*c1);
  if (cs2 < EFFECTIVE_ZERO)
     *AOK = NO;
  else
  {
    *AOK = YES;
    VecComb(eta, Insident, eta*c1-sqrt(cs2), Normal, answer);
  }
  return(answer);
}
