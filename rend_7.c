/*
  *************************************************************************
 **                                                                       **
 **       Renderer                                                        **
 **       Filter Definition Module                                        **
 **                                                                       **
 **                                                                       **
  *************************************************************************
*/

#include <stdio.h>
#include <string.h>
#include "vectors.h"
#include "rend_0.h"
#include "rend_4.h"
#include "rend_6.h"

void InitStdFilt(Filter *AFilter);
void InitSuperFilt(Filter *AFilter);
void InitDiskFilt(Filter *AFilter);
void InitLensFilt(Filter *AFilter);
void InitAdapFilt(Filter *AFilter);

char *StdFiltName();
colour *StdFiltExec();
void *StdFiltRead();

void InitFilters()
{
  InitStdFilt(AllocFilter());
  InitSuperFilt(AllocFilter());
  InitDiskFilt(AllocFilter());
  InitLensFilt(AllocFilter());
  InitAdapFilt(AllocFilter());
}

void InitStdFilt(Filter *AFilter)
{
  AFilter->Name = StdFiltName;
  AFilter->Exec = StdFiltExec;
  AFilter->Read = StdFiltRead;
}

char *StdFiltName()
{
  return "STANDARD";
}

colour *StdFiltExec(
		    UseFilt *NextFilter,
		    Vec *PinHole,
		    Vec *FilmPos,
		    Vec *FilmX,
		    Vec *FilmY,
		    void *fdata,
		    long x,
		    long y,
		    int subdiv,
		    colour *Answer)
{
  Vec TDir;
  ray PrimaryRay;
  PrimaryRay.Origin = *PinHole;
  VecSub(PinHole, FilmPos, &TDir);
  VecUnit(&TDir, &(PrimaryRay.Direction));
  RaysCast++;
  RCacheMiss++;
  Trace(1.0, &PrimaryRay, 1, BSPRoot, Answer);
  return(Answer);
}

void *StdFiltRead(FILE *inputfile)
{
  return (NULL);
}

char *AdapFiltName();
colour *AdapFiltExec();
void *AdapFiltRead();

void InitAdapFilt(Filter *AFilter)
{
  AFilter->Name = AdapFiltName;
  AFilter->Exec = AdapFiltExec;
  AFilter->Read = AdapFiltRead;
}

char *AdapFiltName()
{
  return "ADAPTIVE";
}

colour *ShootRay(AdapFiltVars *fdata, Vec *PinHole, Vec *FilmPos, long x, long y, colour *Answer, int *traced)
{
  Vec TDir;
  AdapCacheItem *Cache, *CacheAns;
  ray PrimaryRay;
  int found, NextFree;
  Flt MinUsed;
  unsigned char HashIndex;
  ItemEntry *AnItem;
  unsigned int HashDepth;


  HashIndex = (x & 0x000000FF) ^
	     ((x >> 8) & 0x000000FF) ^
	     ((x >>16) & 0x000000FF) ^
	     ((x >>24) & 0x000000FF) ^
	      (y & 0x000000FF)	     ^
	     ((y >> 8) & 0x000000FF) ^
	     ((y >>16) & 0x000000FF) ^
	     ((y >>24) & 0x000000FF);

  found = 0;
  NextFree = 0;
  MinUsed = 0;
  CacheTot++;
  CacheAns	= NULL;
  HashDepth = 0;
  for (AnItem = fdata->HashTable[HashIndex]->Next; (AnItem->Data != NULL) && found == 0; AnItem = AnItem->Next)
  {

    HashDepth++;
    Cache = AnItem->Data;
    if (Cache != NULL)
    {
      if (Cache->FPx == x)
      {
        if (Cache->FPy == y)
        {
          /* It has been found in the cache, so dont trace, just return
             the precalculated result and increment the used flag in the
             cache, and abort search */

          *Answer = Cache->Answer;
          found = 1;
          *traced = 0;
          CacheHit++;
          UnlinkFromList(Cache->Used);
          Cache->Used = LinkInToList(Cache, fdata->UsedList, Cache->Used);
          CacheAns = Cache;
          RCacheHits++;
        }
      }
    }
  }
  if (HashDepth > MaxHashDepth) MaxHashDepth = HashDepth;
  if (found == 0)
  {
    RCacheMiss++;
    RaysCast++;
    CacheMiss++;
    PrimaryRay.Origin = *PinHole;
    VecSub(PinHole, FilmPos, &TDir);
    VecUnit(&TDir, &(PrimaryRay.Direction));
    Trace(1.0, &PrimaryRay, 1, BSPRoot, Answer);
    *traced = 1;
    /* You have had to trace the ray, SO find a blank space in the
       cache and record it for later, if there is no blank space, find
       the one used the least */


    CacheAns = fdata->UsedList->Prev->Data;
    UnlinkFromList(CacheAns->Used);

    if (CacheAns->Hash != NULL)
    {
      UnlinkFromList(CacheAns->Hash);
    }

    CacheAns->Used = LinkInToList(CacheAns, fdata->UsedList, CacheAns->Used);

    CacheAns->Hash = LinkInToList(CacheAns, fdata->HashTable[HashIndex], CacheAns->Hash);

    CacheAns->Answer = *Answer;
    CacheAns->FPx    = x;
    CacheAns->FPy    = y;
    CacheAns->Area   = 1.0;
  }

  return(Answer);
}

colour *AdapFiltDo(
		    UseFilt *NextFilter,
		    Vec *PinHole,
		    Vec *FilmPos,
		    Vec *FilmX,
		    Vec *FilmY,
		    AdapFiltVars *fdata,
		    Flt SubThresh,
		    int Level,
		    long x,
		    long y,
		    int subdiv,
		    colour *Answer,
		    long *NoOfRays)
{
  int traced, refined, subsubdiv;
  colour Ans1, Ans2, Ans3, Ans4;
  Vec AFilmPos, AFilmX, AFilmY;
  Flt NewThresh;
  AFilmPos = *FilmPos;
  ShootRay(fdata, PinHole, &AFilmPos, x, y, &Ans1, &traced);
  *NoOfRays += traced;

  VecAdd(&AFilmPos, FilmX, &AFilmPos);
  ShootRay(fdata, PinHole, &AFilmPos, x + subdiv, y, &Ans2, &traced);
  *NoOfRays += traced;

  VecAdd(&AFilmPos, FilmY, &AFilmPos);
  ShootRay(fdata, PinHole, &AFilmPos, x + subdiv, y + subdiv, &Ans3, &traced);
  *NoOfRays += traced;

  VecSub(&AFilmPos, FilmX, &AFilmPos);
  ShootRay(fdata, PinHole, &AFilmPos, x, y + subdiv, &Ans4, &traced);
  *NoOfRays += traced;

  Answer->red	= (Ans1.red   + Ans2.red   + Ans3.red	+ Ans4.red  ) / 4.0;
  Answer->green = (Ans1.green + Ans2.green + Ans3.green + Ans4.green) / 4.0;
  Answer->blue	= (Ans1.blue  + Ans2.blue  + Ans3.blue	+ Ans4.blue ) / 4.0;

  refined = 0;

  if (Level < fdata->MaxDepth)
  {
    VecScalar(.5, FilmX, &AFilmX);
    VecScalar(.5, FilmY, &AFilmY);
    subsubdiv = subdiv/2;
    NewThresh = fdata->Relax * SubThresh;

    if (fabs(Ans1.red	- Answer->red  ) > SubThresh ||
	fabs(Ans1.green - Answer->green) > SubThresh ||
	fabs(Ans1.blue	- Answer->blue ) > SubThresh)
    {
      /* Subdivide */
      AFilmPos = *FilmPos;
      AdapFiltDo(NextFilter, PinHole, &AFilmPos, &AFilmX, &AFilmY, fdata, NewThresh, Level + 1, x, y, subsubdiv, &Ans1, NoOfRays);
      refined = 1;

    }

    if (fabs(Ans2.red	- Answer->red  ) > SubThresh ||
	fabs(Ans2.green - Answer->green) > SubThresh ||
	fabs(Ans2.blue	- Answer->blue ) > SubThresh)
    {
      /* Subdivide */
      AFilmPos = *FilmPos;
      VecAdd(&AFilmPos, &AFilmX, &AFilmPos);
      AdapFiltDo(NextFilter, PinHole, &AFilmPos, &AFilmX, &AFilmY, fdata, NewThresh, Level + 1, x + subsubdiv, y, subsubdiv,	&Ans2, NoOfRays);
      refined = 1;
    }

    if (fabs(Ans3.red	- Answer->red  ) > SubThresh ||
	fabs(Ans3.green - Answer->green) > SubThresh ||
	fabs(Ans3.blue	- Answer->blue ) > SubThresh)
    {
      /* Subdivide */
      AFilmPos = *FilmPos;
      VecAdd(&AFilmPos, &AFilmX, &AFilmPos);
      VecAdd(&AFilmPos, &AFilmY, &AFilmPos);
      AdapFiltDo(NextFilter, PinHole, &AFilmPos, &AFilmX, &AFilmY, fdata, NewThresh, Level + 1, x + subsubdiv, y + subsubdiv, subsubdiv, &Ans3, NoOfRays);
      refined = 1;
    }

    if (fabs(Ans4.red	- Answer->red  ) > SubThresh ||
	fabs(Ans4.green - Answer->green) > SubThresh ||
	fabs(Ans4.blue	- Answer->blue ) > SubThresh)
    {
      /* Subdivide */
      AFilmPos = *FilmPos;
      VecAdd(&AFilmPos, &AFilmY, &AFilmPos);
      AdapFiltDo(NextFilter, PinHole, &AFilmPos, &AFilmX, &AFilmY, fdata, NewThresh, Level + 1, x, y + subsubdiv, subsubdiv, &Ans4, NoOfRays);
      refined = 1;
    }
  }
  Answer->red	= (Ans1.red   + Ans2.red   + Ans3.red	+ Ans4.red  ) / 4.0;
  Answer->green = (Ans1.green + Ans2.green + Ans3.green + Ans4.green) / 4.0;
  Answer->blue	= (Ans1.blue  + Ans2.blue  + Ans3.blue	+ Ans4.blue ) / 4.0;
  return(Answer);
}


colour *AdapFiltExec(
		    UseFilt *NextFilter,
		    Vec *PinHole,
		    Vec *FilmPos,
		    Vec *FilmX,
		    Vec *FilmY,
		    AdapFiltVars *fdata,
		    long x,
		    long y,
		    int subdiv,
		    colour *Answer)
{
  static long int TotRays=0;
  Vec XStep, YStep, AFilmPos;
  long xpt, ypt, stepper;
  colour Ans1, Ans2, Ans3, Ans4;
  int traced, PixCheck;
  Flt Diff;
  VecAdd(FilmX, FilmX, &XStep);
  VecAdd(FilmY, FilmY, &YStep);
  RaysPerPixel = 0;
  traced = 0;

  if (fdata->SubSam == 1)
  {
    stepper = subdiv;
    xpt = x / subdiv;
    ypt = y / subdiv;
    PixCheck = 0;

    AFilmPos = *FilmPos;
    if ((xpt & 1) == 1)
    {
      VecSub(&AFilmPos, FilmX, &AFilmPos);
      xpt--;
      PixCheck = 1;
    }
    if ((ypt & 1) == 1)
    {
      VecSub(&AFilmPos, FilmY, &AFilmPos);
      ypt--;
      PixCheck += 2;
    }
    xpt *= subdiv;
    ypt *= subdiv;

    ShootRay(fdata, PinHole, &AFilmPos, xpt, ypt, &Ans1, &traced);
    TotRays += traced;

    VecAdd(&AFilmPos, &XStep, &AFilmPos);
    ShootRay(fdata, PinHole, &AFilmPos, xpt + stepper + stepper, ypt, &Ans2, &traced);
    TotRays += traced;

    VecAdd(&AFilmPos, &YStep, &AFilmPos);
    ShootRay(fdata, PinHole, &AFilmPos, xpt + stepper + stepper, ypt + stepper + stepper, &Ans3, &traced);
    TotRays += traced;

    VecSub(&AFilmPos, &XStep, &AFilmPos);
    ShootRay(fdata, PinHole, &AFilmPos, xpt, ypt + stepper + stepper, &Ans4, &traced);
    TotRays += traced;

    Answer->red	= (Ans1.red   + Ans2.red   + Ans3.red	+ Ans4.red  ) / 4.0;
    Answer->green = (Ans1.green + Ans2.green + Ans3.green + Ans4.green) / 4.0;
    Answer->blue	= (Ans1.blue  + Ans2.blue  + Ans3.blue	+ Ans4.blue ) / 4.0;

    Diff = 999.9;
    if (PixCheck < 0 || PixCheck > 3)
    {
      printf("Tilt!|");
      exit(4);
    }
    switch (PixCheck)
    {
      case 0 :
	       Diff = fabs(Ans1.red   - Answer->red) +
		      fabs(Ans1.green - Answer->green) +
		      fabs(Ans1.blue  - Answer->blue);
	       break;
    case 1 :
	       Diff = fabs(Ans2.red   - Answer->red) +
		      fabs(Ans2.green - Answer->green) +
		      fabs(Ans2.blue  - Answer->blue);
	       break;
    case 2 :
	       Diff = fabs(Ans4.red   - Answer->red) +
		      fabs(Ans4.green - Answer->green) +
		      fabs(Ans4.blue  - Answer->blue);
	       break;
    case 3 :
	       Diff = fabs(Ans3.red   - Answer->red) +
		      fabs(Ans3.green - Answer->green) +
		      fabs(Ans3.blue  - Answer->blue);
	       break;
    }
    if (Diff > fdata->SubThresh)
    {
      AdapFiltDo(NextFilter, PinHole, FilmPos, FilmX, FilmY, fdata, fdata->Threshold, 0, x, y, subdiv, Answer, &RaysPerPixel);
      TotRays += RaysPerPixel;
    }
    else
    {
    }
  }
  else
  {
    AdapFiltDo(NextFilter, PinHole, FilmPos, FilmX, FilmY, fdata, fdata->Threshold, 0, x, y, subdiv, Answer, &RaysPerPixel);
    TotRays += RaysPerPixel;
  }
/*  printf("[%ld]", TotRays); */
  return(Answer);
}

void *AdapFiltRead(FILE *inputfile)
{
  AdapFiltVars *MyVars=AllocHeap(sizeof(AdapFiltVars));
  char varname[20];
  int i;
  AdapCacheItem *AnItem;

  MyVars->MaxDepth  = 3;
  MyVars->CacheSize = 1024;
  MyVars->Threshold = 0.15;
  MyVars->Relax     = 1.2;
  MyVars->SubThresh = 0.02;
  MyVars->SubSam    = 0;
  do
  {
    fscanf(inputfile, " %s ",varname);
    strupr(varname);

    if (strcmp("MAX-DEPTH", varname) == ZERO)
    {
      fscanf(inputfile, " %d ", &(MyVars->MaxDepth));
    } else if (strcmp("CACHE-SIZE", varname) == ZERO)
    {
      fscanf(inputfile, " %d ", &(MyVars->CacheSize));
    } else if (strcmp("THRESHOLD", varname) == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(MyVars->Threshold));
    } else if (strcmp("RELAX", varname) == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(MyVars->Relax));
    } else if (strcmp("SUB-THRESH", varname) == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(MyVars->SubThresh));
    } else if (strcmp("QUICK", varname) == ZERO)
    {
	MyVars->SubSam = 1;
    } else if (strcmp(";", varname) != ZERO)
    {
      printf("Error - Unknown Verb in Adaptive Filter\n");
      exit(1);
    }


  } while (strcmp(";", varname) != ZERO);
  AnItem = NULL;
  MyVars->UsedList = CreateList();
  for (i=0; i<MyVars->CacheSize; i++)
  {
    if ((AnItem = malloc(sizeof(AdapCacheItem))) == NULL)
    {
      printf("Error - Unable to allocate Supersampling Cache Buffer\n");
      exit(1);

    }
    AnItem->Used = AddToList(AnItem, MyVars->UsedList);
    AnItem->Hash = malloc(sizeof(ItemEntry));
    AnItem->Hash->Data = AnItem;
    AnItem->Hash->Next = AnItem->Hash;
    AnItem->Hash->Prev = AnItem->Hash;
    AnItem->Area = 0.0;
    AnItem->FPx = -2000000000;
    AnItem->FPy = -2000000000;
  }
  for (i = 0; i < 256; i++)
  {
    MyVars->HashTable[i] = CreateList();
  }
  return (MyVars);
}

char *SuperFiltName();
colour *SuperFiltExec();
void *SuperFiltRead();


void InitSuperFilt(Filter *AFilter)
{
  AFilter->Name = SuperFiltName;
  AFilter->Exec = SuperFiltExec;
  AFilter->Read = SuperFiltRead;
}

char *SuperFiltName()
{
  return "SUPER-SAMPLE";
}

colour *SuperFiltExec(
		      UseFilt *NextFilter,
		      Vec *PinHole,
		      Vec *FilmPos,
		      Vec *FilmX,
		      Vec *FilmY,
		      SuperFiltVars *fdata,
		      long x,
		      long y,
		      int subdiv,
		      colour *Answer)
{
  int u, v, us, vs;
  Flt SuperWeight;
  Vec NewFilmX, NewFilmY, NewFilmPos, NewFilmPos1;
  colour ThisAnswer;
  SetColour(0.0, 0.0, 0.0, Answer);
  SuperWeight = 1.0 / (fdata->XSamples * fdata->YSamples);
  NewFilmPos = *FilmPos;
  VecScalar(1.0/fdata->XSamples, FilmX, &NewFilmX);
  VecScalar(1.0/fdata->YSamples, FilmY, &NewFilmY);
  us = subdiv / fdata->XSamples;
  vs = subdiv / fdata->YSamples;
  for (v=0; v<fdata->YSamples; v++)
  {
    NewFilmPos1 = NewFilmPos;
    for (u=0; u<fdata->XSamples; u++)
    {
      ((NextFilter->ThisFilter)->Exec)(NextFilter->NextUsed,
				       PinHole,
				       &NewFilmPos1,
				       &NewFilmX,
				       &NewFilmY,
				       NextFilter->fdata,
				       x + u * us,
				       y + v * vs,
                       (int) (subdiv / sqrt(fdata->XSamples * fdata->YSamples)),
				       &ThisAnswer);
      Answer->red   += ThisAnswer.red	* SuperWeight;
      Answer->green += ThisAnswer.green * SuperWeight;
      Answer->blue  += ThisAnswer.blue	* SuperWeight;
      VecAdd(&NewFilmPos1, &NewFilmX, &NewFilmPos1);
    }
    VecAdd(&NewFilmPos, &NewFilmY, &NewFilmPos);
  }
  return(Answer);
}

void *SuperFiltRead(FILE *inputfile)
{
  SuperFiltVars *MyVars=AllocHeap(sizeof(SuperFiltVars));
  char varname[20];
  MyVars->XSamples = 2;
  MyVars->YSamples = 2;
  do
  {
    fscanf(inputfile, " %s ",varname);
    strupr(varname);

    if (strcmp("X-SAMPLES", varname) == ZERO)
    {
      fscanf(inputfile, " %d ", &(MyVars->XSamples));
    }
    else if (strcmp("Y-SAMPLES", varname) == ZERO)
    {
      fscanf(inputfile, " %d ", &(MyVars->YSamples));
    }
  } while (strcmp(";", varname) != ZERO);

  return (MyVars);
}

char *DiskFiltName();
colour *DiskFiltExec();
void *DiskFiltRead();


void InitDiskFilt(Filter *AFilter)
{
  AFilter->Name = DiskFiltName;
  AFilter->Exec = DiskFiltExec;
  AFilter->Read = DiskFiltRead;
}

char *DiskFiltName()
{
  return "DISK";
}

colour *DiskFiltExec(
		      UseFilt *NextFilter,
		      Vec *PinHole,
		      Vec *FilmPos,
		      Vec *FilmX,
		      Vec *FilmY,
		      DiskFiltVars *fdata,
		      long x,
		      long y,
		      int subdiv,
		      colour *Answer)
{
  int i;
  Flt u, v, u1, v1;
  Flt DiskWeight, ThisWeight, CumeWeight;
  Vec NewFilmX, NewFilmY, NewFilmPos;
  colour ThisAnswer;
  SetColour(0.0, 0.0, 0.0, Answer);
  DiskWeight = 1.0 / fdata->NoOfSamples;
  NewFilmPos = *FilmPos;
  CumeWeight = sqrt(fdata->NoOfSamples);
  VecScalar(CumeWeight, FilmX, &NewFilmX);
  VecScalar(CumeWeight, FilmY, &NewFilmY);

  CumeWeight = 0.0;

  for (i=0; i<fdata->NoOfSamples; i++)
  {
    u =  rand();
    u /= RAND_MAX;
    v =  rand();
    v /= RAND_MAX;
    u *= 2 * PI;
    v =  fdata->Radius * sqrt(v);
    u1 =  v * sin(u);
    v1 =  v * cos(u);
    VecComb(u1, FilmX, v1, FilmY, &NewFilmPos);
    ThisWeight = 1-VecLen(&NewFilmPos) / fdata->Radius;
    CumeWeight += ThisWeight;
    VecAdd(FilmPos,&NewFilmPos, &NewFilmPos);


      ((NextFilter->ThisFilter)->Exec)(NextFilter->NextUsed,
				       PinHole,
				       &NewFilmPos,
				       &NewFilmX,
				       &NewFilmY,
				       NextFilter->fdata,
				       x + (long)((Flt)(subdiv) * u1),
				       y + (long)((Flt)(subdiv) * v1),
				       subdiv / sqrt(fdata->NoOfSamples),
				       &ThisAnswer);

      Answer->red   += ThisAnswer.red	* DiskWeight * ThisWeight;
      Answer->green += ThisAnswer.green * DiskWeight * ThisWeight;
      Answer->blue  += ThisAnswer.blue	* DiskWeight * ThisWeight;
  }
  CumeWeight /= fdata->NoOfSamples;
  Answer->red	/= CumeWeight;
  Answer->green /= CumeWeight;
  Answer->blue	/= CumeWeight;
  return(Answer);
}

void *DiskFiltRead(FILE *inputfile)
{
  DiskFiltVars *MyVars=AllocHeap(sizeof(DiskFiltVars));
  char varname[20];
  MyVars->Radius      = 0.707;
  MyVars->NoOfSamples = 4;
  do
  {
    fscanf(inputfile, " %s ",varname);
    strupr(varname);

    if (strcmp("RADIUS", varname) == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(MyVars->Radius));
    }
    else if (strcmp("SAMPLES", varname) == ZERO)
    {
      fscanf(inputfile, " %d ", &(MyVars->NoOfSamples));
    }
  } while (strcmp(";", varname) != ZERO);
  if (DEBUG == ON)
  {
    printf(" Disk Sampler Radius " FltFmt ", %d Samples\n", MyVars->Radius, MyVars->NoOfSamples);
  }
  return (MyVars);
}

char *LensFiltName();
colour *LensFiltExec();
void *LensFiltRead();


void InitLensFilt(Filter *AFilter)
{
  AFilter->Name = LensFiltName;
  AFilter->Exec = LensFiltExec;
  AFilter->Read = LensFiltRead;
}

char *LensFiltName()
{
  return "LENS";
}

colour *LensFiltExec(
		      UseFilt *NextFilter,
		      Vec *PinHole,
		      Vec *FilmPos,
		      Vec *FilmX,
		      Vec *FilmY,
		      LensFiltVars *fdata,
		      long x,
		      long y,
		      int subdiv,
		      colour *Answer)
{
  int i;
  Flt u, v, u1, v1;
  Flt LensWeight, FilmToLens;
  Vec NewFilmPos, LensPos, NewPinHole, Sub1;
  Vec XDir, YDir;
  colour ThisAnswer;
  SetColour(0.0, 0.0, 0.0, Answer);
  LensWeight = 1.0 / fdata->NoOfSamples;
  VecUnit(FilmX, &XDir);
  VecUnit(FilmY, &YDir);
  VecScalar(fdata->Radius, &XDir, &XDir);
  VecScalar(fdata->Radius, &YDir, &YDir);
  for (i=0; i<fdata->NoOfSamples; i++)
  {
    u =  rand();
    u /= RAND_MAX;
    v =  rand();
    v /= RAND_MAX;
    u *= 2 * PI;
    v =  sqrt(v);
    u1 =  v * sin(u);
    v1 =  v * cos(u);
    VecComb(u1, &XDir, v1, &YDir, &LensPos);
    VecAdd(PinHole,&LensPos, &NewPinHole);
    VecSub(FilmPos, PinHole, &Sub1);
    FilmToLens = VecLen(&Sub1);
    VecAdds((FilmToLens+(fdata->FocalLength)) / fdata->FocalLength, &LensPos, FilmPos, &NewFilmPos);


      ((NextFilter->ThisFilter)->Exec)(NextFilter->NextUsed,
				       &NewPinHole,
				       &NewFilmPos,
				       FilmX,
				       FilmY,
				       NextFilter->fdata,
				       (long)(x + subdiv * u1),
				       (long)(y + subdiv * v1),
				       (int)(subdiv / sqrt(fdata->NoOfSamples)),
				       &ThisAnswer);

      Answer->red   += ThisAnswer.red	* LensWeight;
      Answer->green += ThisAnswer.green * LensWeight;
      Answer->blue  += ThisAnswer.blue	* LensWeight;
  }
  return(Answer);
}

void *LensFiltRead(FILE *inputfile)
{
  LensFiltVars *MyVars=AllocHeap(sizeof(LensFiltVars));
  char varname[20];
  /* These Default Values are chosen to Give a relatively Large Depth of
     field to fit most scene files */
  MyVars->Radius      = 0.1;
  MyVars->NoOfSamples = 4;
  MyVars->FocalLength = 1000.0;

  do
  {
    fscanf(inputfile, " %s ",varname);
    strupr(varname);

    if (strcmp("RADIUS", varname) == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(MyVars->Radius));
    } else if (strcmp("SAMPLES", varname) == ZERO)
    {
      fscanf(inputfile, " %d ", &(MyVars->NoOfSamples));
    } else if (strcmp("FOCAL-LENGTH", varname) == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(MyVars->FocalLength));
    }
  } while (strcmp(";", varname) != ZERO);
  if (DEBUG == ON)
  {
    printf(" Lens Sampler Radius " FltFmt ", %d Samples, " FltFmt " Focal Length\n", MyVars->Radius, MyVars->NoOfSamples, MyVars->FocalLength);
  }
  return (MyVars);
}
