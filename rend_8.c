/*
  *************************************************************************
 **                                                                       **
 **       Renderer                                                        **
 **       BSP Generation Module                                           **
 **                                                                       **
  *************************************************************************
*/

#include <stdio.h>
#include "vectors.h"
#include "rend_0.h"
#include <string.h>
#include "rend_5.h"
#include "rend_7.h"
#include "rend_4.h"
#include "rend_3.h"

#define BSPMaxDepth 20

#include "rend_8.h"


void BSPGen(BSPNode *BSPRoot)
{
  enum BSPAxis Axis;
  Axis = XAxis;
  BSPBuild(BSPRoot, 0, Axis);

}

void BSPBuild(BSPNode *BSPRoot, int Level, enum BSPAxis ThisAxis)
{
  enum BSPAxis NextAxis;
  enum BSPAxis StartAxis;
  Box ABound, Child0, Child1;
  Object *ThisObj;
  /*
  Object *ChildList0, *ChildList1;
  */

  ItemEntry *AnItem, *ChildList0, *ChildList1, *NextItem;

  int	 ChildCnt0=0, ChildCnt1=0, fin;
  Flt MidPt;
  ChildList0 = CreateList();
  ChildList1 = CreateList();
  NextAxis = XAxis;
/*  printf("BSP Generate for Level %d\n", Level);
  printf("Original No Of Objects %d\n", BSPRoot->NoOfObjs); */
  BSPRoot->Child0 = NULL;
  BSPRoot->Child1 = NULL;
  BSPBound(BSPRoot, &(BSPRoot->Bounder));

  if (Level < BSPMaxDepth && BSPRoot->NoOfObjs > 4)
  {
    fin = 0;
    StartAxis = ThisAxis;
    while (!fin)
    {
       /* Build Sub-Bounds */
       Child0 = BSPRoot->Bounder;
       Child1 = BSPRoot->Bounder;

       if (ThisAxis == XAxis)
       {
         MidPt = (BSPRoot->Bounder.A.x + BSPRoot->Bounder.B.x) / 2.0;
         Child0.B.x = MidPt;
         Child1.A.x = MidPt;
         NextAxis = YAxis;
       }
       else if (ThisAxis == YAxis)
       {
         MidPt = (BSPRoot->Bounder.A.y + BSPRoot->Bounder.B.y) / 2.0;
         Child0.B.y = MidPt;
         Child1.A.y = MidPt;
         NextAxis = ZAxis;
       }
       else if (ThisAxis == ZAxis)
       {
         MidPt = (BSPRoot->Bounder.A.z + BSPRoot->Bounder.B.z) / 2.0;
         Child0.B.z = MidPt;
         Child1.A.z = MidPt;
         NextAxis = XAxis;
       }

       NextItem = BSPRoot->ObjectList->Next;
       /* Share out any objects that are Wholly within either Bounding Box */
       for (AnItem = NextItem; NextItem->Data != NULL; AnItem = NextItem)
       {
         ThisObj = AnItem->Data;
         NextItem = AnItem->Next;
         BoundObject(ThisObj, &ABound);

         if (BSPInside(&ABound, Child0) != 0)
         {
           RemoveFromList(AnItem);
           ThisObj->List = AddToList(ThisObj, ChildList0);
           BSPRoot->NoOfObjs -= 1;
           ChildCnt0 += 1;
         }
         else if (BSPInside(&ABound, Child1) != 0)
         {
           RemoveFromList(AnItem);
           ThisObj->List = AddToList(ThisObj, ChildList1);
           BSPRoot->NoOfObjs -= 1;
           ChildCnt1 += 1;
         }
       }
       if (ChildCnt0 > 0)
       {
/*          printf("Assigning %d objects to Child0\n", ChildCnt0); */
         BSPRoot->Child0 = AllocHeap(sizeof(BSPNode));
         BSPRoot->Child0->NoOfObjs    = ChildCnt0;
         BSPRoot->Child0->Child0      = NULL;
         BSPRoot->Child0->Child1      = NULL;
         BSPRoot->Child0->ObjectList = ChildList0;
         BSPBuild(BSPRoot->Child0, Level + 1, NextAxis);
         fin = -1;
       }

       if (ChildCnt1 > 0)
       {
/*         printf("Assigning %d objects to Child1\n", ChildCnt1); */
         BSPRoot->Child1 = AllocHeap(sizeof(BSPNode));
         BSPRoot->Child1->NoOfObjs    = ChildCnt1;
         BSPRoot->Child1->Child0      = NULL;
         BSPRoot->Child1->Child1      = NULL;
         BSPRoot->Child1->ObjectList = ChildList1;
         BSPBuild(BSPRoot->Child1, Level + 1, NextAxis);
         fin = -1;
       }
       else
       {
         if (ChildCnt0 == 0)
         {
           ThisAxis = NextAxis;
           if (ThisAxis == StartAxis) fin = -1;
         }
       }
     }
  }
  if (ChildCnt0 == 0) DestroyList(ChildList0);
  if (ChildCnt1 == 0) DestroyList(ChildList1);
/*   printf("Final No Of Objects at this level %d\n", BSPRoot->NoOfObjs); */
}

int BSPInside(Box *ABox, Box ABound)
{
  if (ABox->A.x < ABound.A.x) return 0;
  if (ABox->A.y < ABound.A.y) return 0;
  if (ABox->A.z < ABound.A.z) return 0;
  if (ABox->B.x > ABound.B.x) return 0;
  if (ABox->B.y > ABound.B.y) return 0;
  if (ABox->B.z > ABound.B.z) return 0;
  return(-1);
}

int BSPPInside(Vec *APt, Box ABound)
{
  if (APt->x < ABound.A.x) return 0;
  if (APt->y < ABound.A.y) return 0;
  if (APt->z < ABound.A.z) return 0;
  if (APt->x > ABound.B.x) return 0;
  if (APt->y > ABound.B.y) return 0;
  if (APt->z > ABound.B.z) return 0;
  return(-1);
}

void BoundObject(Object *AnObj, Box *ABox)
{
  if (AnObj->ObjType != 0)
     ((AnObj->Primative)->Bound)(AnObj->kdata, ABox);
  else
  {
     *ABox = ((Defin *)(AnObj->Primative))->BSPRoot->Bounder;
     MapBound(ABox, &(((Comp *)(AnObj->kdata))->Inverse), ABox);
  }
  if (AnObj->Model != NULL)
  {
    MapBound(ABox, AnObj->Inverse, ABox);
  }
}

void BSPBound(BSPNode *BSPRoot, Box *ABox)
{
  Box bnder;
  ItemEntry *AnItem;
  Object *AnObject = BSPRoot->ObjectList->Next->Data;

  AnItem = BSPRoot->ObjectList->Next;

  BoundObject(AnObject, ABox);

  while (AnItem->Data != NULL)
  {
    AnObject = AnItem->Data;
    BoundObject(AnObject, &bnder);
    CombineBound(ABox,&bnder, ABox);
    AnItem = AnItem->Next;
  }
  if (BSPRoot->Child1 != NULL)
  {
    CombineBound(ABox,&(BSPRoot->Child1->Bounder),ABox);
  }

  if (BSPRoot->Child0 != NULL)
  {
    CombineBound(ABox,&(BSPRoot->Child0->Bounder),ABox);
  }
  BoxPrint(ABox);
}


Box *MapBound(Box *ABox, Matrix *Model, Box *BBox)
{
  Vec B1, B2, B3;

  B2.x = 999999999.0;
  B2.y = B2.x;
  B2.z = B2.y;
  B3.x = -999999999.0;
  B3.y = B3.x;
  B3.z = B3.y;

  /* I KNOW, this IS brute force, and it IS clumsy, but to force this into
     a LOOP would have slowed it down, taken a LOT longer to code, Made it
     LESS easy to see what is going on, and MORE difficult to maintain
     So if you want to recode as a loop, feel free, and good riddence,
     I have a LOT better things to do that polishing a simple bit of code
     like this - I have the JOY of writing the manual for this sucker
     Best Regards PM '93 */
  /* But NOT any more JOY, I have found someone willing and a LOT more
     able to write the manual than me, so I can concentrate on the code */


  B1.x = ABox->A.x;
  B1.y = ABox->A.y;
  B1.z = ABox->A.z;

  MInvertVector(&B1, Model, &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  B1.x = ABox->B.x;
  B1.y = ABox->A.y;
  B1.z = ABox->A.z;

  MInvertVector(&B1, Model, &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  B1.x = ABox->A.x;
  B1.y = ABox->B.y;
  B1.z = ABox->A.z;

  MInvertVector(&B1, Model, &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  B1.x = ABox->B.x;
  B1.y = ABox->B.y;
  B1.z = ABox->A.z;

  MInvertVector(&B1, Model, &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  B1.x = ABox->A.x;
  B1.y = ABox->A.y;
  B1.z = ABox->B.z;

  MInvertVector(&B1, Model, &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  B1.x = ABox->B.x;
  B1.y = ABox->A.y;
  B1.z = ABox->B.z;

  MInvertVector(&B1, Model, &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  B1.x = ABox->A.x;
  B1.y = ABox->B.y;
  B1.z = ABox->B.z;

  MInvertVector(&B1, Model, &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;

  B1.x = ABox->B.x;
  B1.y = ABox->B.y;
  B1.z = ABox->B.z;

  MInvertVector(&B1, Model, &B1);

  if (B1.x < B2.x) B2.x = B1.x;
  if (B1.y < B2.y) B2.y = B1.y;
  if (B1.z < B2.z) B2.z = B1.z;
  if (B1.x > B3.x) B3.x = B1.x;
  if (B1.y > B3.y) B3.y = B1.y;
  if (B1.z > B3.z) B3.z = B1.z;


  /* Now pack the answer back into the output bounding box and get going */
  BBox->A.x = B2.x;
  BBox->A.y = B2.y;
  BBox->A.z = B2.z;
  BBox->B.x = B3.x;
  BBox->B.y = B3.y;
  BBox->B.z = B3.z;
  return(BBox);

}
