void BSPGen(BSPNode *BSPRoot);
void BSPBuild(BSPNode *BSPRoot, int Level, enum BSPAxis ThisAxis);
void BSPBound(BSPNode *BSPRoot, Box *ABox);
int BSPInside(Box *ABox, Box ABound);
int BSPPInside(Vec *APt, Box ABound);
Box *MapBound(Box *ABox, Matrix *Model, Box *BBox);
void BoundObject(Object *AnObj, Box *ABox);
