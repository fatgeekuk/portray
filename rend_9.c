/*
  *************************************************************************
 **                                                                       **
 **       Renderer                                                        **
 **           Primative Definition Module                                 **
 **            ( Spheres, Planes, Cylanders, etc )                        **
 **                                                                       **
  *************************************************************************
*/

#include <stdio.h>
#include <string.h>
#include "vectors.h"
#include "rend_0.h"
#include "rend_3.h"
#include "rend_4.h"
#include "rend_6.h"
#include "noise.h"
#include "tga.h"

CFlt SSin(CFlt Value);
/*  Primative Type Definitions */

void	   InitPrimatives();

void	   InitBrick();
char	   *BrickName();
MatDef	   *BrickShader();
BrickVars  *BrickRead();

void	   InitTile();
char	   *TileName();
MatDef	   *TileShader();
TileVars   *TileRead();

void	   InitChequers();
char	   *CheqName();
MatDef	   *CheqShader();
void 	    CheqValue();
CheqVars   *CheqRead();

void	   InitNoise();
char	   *NoiseName();
MatDef	   *NoiseShader();
NoiseVars  *NoiseRead();

void	   InitBumpy();
char	   *BumpyName();
MatDef	   *BumpyShader();
BumpyVars  *BumpyRead();

void	   InitFades();
char	   *FadesName();
MatDef	   *FadesShader();
FadeVars   *FadesRead();

void	   InitTurb3D();
char	   *Turb3DName();
MatDef	   *Turb3DShader();
void	    Turb3DValue();
Turb3DVars *Turb3DRead();

void       InitTurb();
char       *TurbName();
MatDef     *TurbShader();
TurbVars   *TurbRead();

void	   InitImage();
char	   *ImageName();
MatDef	   *ImageShader();
void 	    ImageValue();
ImageVars  *ImageRead();

void InitSphere(Prim *);
void InitPlane(Prim *);
void InitTriangle(Prim *);
void InitBox(Prim *);
void InitCylinder(Prim *);
void InitRTriangle(Prim *);
void InitPoly(Prim *);
void InitCSG(Prim *);
void InitDisk(Prim *);
void InitComp(Prim *);

void ResolveMap(IntRec *AnInt);

void InitPrimatives()
{
  InitSphere(AllocPrimative());
  InitPlane(AllocPrimative());
  InitTriangle(AllocPrimative());
  InitBox(AllocPrimative());
  InitCylinder(AllocPrimative());
  InitRTriangle(AllocPrimative());
  InitPoly(AllocPrimative());
  InitCSG(AllocPrimative());
  InitDisk(AllocPrimative());
  InitTorus(AllocPrimative());
  InitImplicit(AllocPrimative());
  InitComp(AllocPrimative());
}

void InitTextures()
{
  InitTile(AllocProcedure());
  InitChequers(AllocProcedure());
  InitBrick(AllocProcedure());
  InitNoise(AllocProcedure());
  InitBumpy(AllocProcedure());
  InitFades(AllocProcedure());
  InitTurb3D(AllocProcedure());
  InitTurb(AllocProcedure());
  InitImage(AllocProcedure());
}
void ApplyModToInt(colour *aColour, IntRec *AnInt, int Target)
{
  if (Target == -1)
  {
    /* Replace U */
    if (AnInt->DoneMap == 0) ResolveMap(AnInt);
    AnInt->u = (aColour->red + aColour->blue + aColour->green) / 3.0;
  }
  else if (Target == -2)
  {
    /* Replace V */
    if (AnInt->DoneMap == 0) ResolveMap(AnInt);
    AnInt->v = (aColour->red + aColour->blue + aColour->green) / 3.0;
  }
  else if (Target == -3)
  {
    /* Replace Both U and V */
    if (AnInt->DoneMap == 0) ResolveMap(AnInt);
    AnInt->u = aColour->red;
    AnInt->v = aColour->green;
  } 
  else if (Target == -4)
  {
    /* Add to U */
    if (AnInt->DoneMap == 0) ResolveMap(AnInt);
    AnInt->u += aColour->red;
  } 
  else if (Target == -5)
  {
    /* Add to V */
    if (AnInt->DoneMap == 0) ResolveMap(AnInt);
    AnInt->v += aColour->green;
  } 
  else if (Target == -6)
  {
    /* Add to Both U and V */
    if (AnInt->DoneMap == 0) ResolveMap(AnInt);
    AnInt->u += aColour->red;
    AnInt->v += aColour->green;
  } 
  else if (Target == -7)
  {
    /* Perturb Normal */
    AnInt->Normal.x += ((2.0 * aColour->red)   - 1.0);
    AnInt->Normal.y += ((2.0 * aColour->green) - 1.0);
    AnInt->Normal.z += ((2.0 * aColour->blue)  - 1.0);
    VecUnit(&(AnInt->Normal), &(AnInt->Normal));
  } 
}

void ResolveMap(IntRec *AnInt)
{
  Object *AnObject = AnInt->HitObj;
  Vec Sub1;
  
  if (AnObject->Primative->Inverse != NULL)
  {
    ((AnObject->Primative)->Inverse) (AnInt,
                      AnObject->kdata,
                      &(AnInt->u),
                      &(AnInt->v),
                      &Sub1);
    AnInt->DoneMap = 1;
  }
}

void ResolveMaterial(MatDef *Source, IntRec *AnInt, MatDef *Dest)
{
  ItemEntry *AnItem;
  colour    aColour;
  int       Target;

  if (Source == NULL)
  {
    Source = DefaultMaterial;
  }
  *Dest = *Source;
  if (Source->Modifiers)
  {
    for (AnItem = Source->Modifiers->Next; AnItem->Data != NULL; AnItem = AnItem->Next)
    {
      (((ModRec *)(AnItem->Data))->ShaderValue)(((ModRec *)(AnItem->Data))->ShaderData, AnInt, &aColour);

      Target = ((ModRec*)(AnItem->Data))->ResultTarget;
      
      if (Target < 0)
      {
        ApplyModToInt(&aColour, AnInt, Target);
      }
      else if (Target == 0)
      {
        Dest->DiffCol = aColour;
/*        Dest->SpecCol = aColour; */
      }
      else if (Target == 1)
      {
        Dest->kdiff = (aColour.red + aColour.blue + aColour.green) / 3.0;
      }
      else if (Target == 2)
      {
        Dest->kspec = (aColour.red + aColour.blue + aColour.green) / 3.0;
      }
    }
  }
  if (Source->ShaderEval != NULL)
  {
    (Source->ShaderEval)(Source->ShaderData, AnInt, Dest);
  }
}

#define CopyInt(AInt, BInt) { *(BInt) = *(AInt); }

/*
  *************************************************************************
 **                                                                       **
 **                           Procedural Colours                          **
 **                                                                       **
  *************************************************************************
*/

void InitTile(Proc *AProc)
{
  AProc->Name = TileName;
  AProc->Eval = TileShader;
  AProc->Read = TileRead;
}

char *TileName()
{
  return "TILES";
}

MatDef *TileShader(TileVars *parms, IntRec *AnInt, MatDef *MatAns)
{
  Flt u, v;
  int a, b;
  Vec Sub1;


  if (AnInt->DoneMap == 0) ResolveMap(AnInt);
  u = AnInt->u;
  v = AnInt->v;
  u += 1000.0;
  v += 1000.0;
  a = u / parms->Size;
  b = v / parms->Size;
  if ((a % parms->TileWidth) < parms->GroutWidth || (b % parms->TileWidth) < parms->GroutWidth)
     ResolveMaterial(parms->Grout, AnInt, MatAns);
  else
     ResolveMaterial(parms->Face, AnInt, MatAns);
  return(MatAns);
}

TileVars *TileRead(FILE *inputfile)
{
  char var[20];
  TileVars *answer;
  answer = (TileVars *)AllocHeap(sizeof(TileVars));
  answer->Face	     = NULL;
  answer->Grout      = NULL;
  answer->TileWidth  = 8;
  answer->GroutWidth = 1;
  answer->Size	     = 1.0;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "FACE") == ZERO)
    {
      answer->Face = ReadMaterial(inputfile);
    }
    else if (strcmp(var, "TILE-WIDTH") == ZERO)
    {
      fscanf(inputfile, " %d ", &(answer->TileWidth));
    }
    else if (strcmp(var, "GROUT-WIDTH") == ZERO)
    {
      fscanf(inputfile, " %d ", &(answer->GroutWidth));
    }
    else if (strcmp(var, "GROUT") == ZERO)
    {
      answer->Grout = ReadMaterial(inputfile);
    }
    else if (strcmp(var, "SIZE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(answer->Size));
      if (answer->Size < EFFECTIVE_ZERO)
      {
	printf("ERROR -- Size of Grout Colour cannot be Zero\n");
	exit(1);
      }
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Chequers\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);
  return(answer);
}

void InitChequers(Proc *AProc)
{
  AProc->Name  = CheqName;
  AProc->Eval  = CheqShader;
  AProc->Value = CheqValue;
  AProc->Read  = CheqRead;
}

char *CheqName()
{
  return "CHEQUERS";
}

void CheqValue(CheqVars *parms, IntRec *AnInt, colour *Answer)
{
  int a, b;
  Flt u, v;
  MatDef *MatAns;

  if (AnInt->DoneMap == 0) ResolveMap(AnInt);

  u = AnInt->u;
  v = AnInt->v;
  u = u / parms->Size;
  v = v / parms->Size;
  u -= floor(u);
  v -= floor(v); 
  a = 2.0 * u;
  b = 2.0 * v;
  if (a != b)
  {
    if (parms->A != NULL)
       ResolveMaterial(parms->A, AnInt, MatAns);
  }
  else
  {
    if (parms->B != NULL)
       ResolveMaterial(parms->B, AnInt, MatAns);
  }
}

MatDef *CheqShader(CheqVars *parms, IntRec *AnInt, MatDef *MatAns)
{
  int a, b;
  Flt u, v;

  if (AnInt->DoneMap == 0) ResolveMap(AnInt);

  u = AnInt->u;
  v = AnInt->v;
  u = u / parms->Size;
  v = v / parms->Size;
  u -= floor(u);
  v -= floor(v); 
  a = 2.0 * u;
  b = 2.0 * v;
  if (a != b)
  {
    if (parms->A != NULL)
       ResolveMaterial(parms->A, AnInt, MatAns);
  }
  else
  {
    if (parms->B != NULL)
       ResolveMaterial(parms->B, AnInt, MatAns);
  }
  return(MatAns);
}

CheqVars *CheqRead(FILE *inputfile)
{
  char var[20];
  CheqVars *answer;
  answer = (CheqVars *)AllocHeap(sizeof(CheqVars));
  answer->Size = 1.0;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "MATERIAL-A") == ZERO)
    {
      answer->A = ReadMaterial(inputfile);
    }
    else if (strcmp(var, "MATERIAL-B") == ZERO)
    {
      answer->B = ReadMaterial(inputfile);
    }
    else if (strcmp(var, "SIZE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(answer->Size));
      if (answer->Size < EFFECTIVE_ZERO)
      {
	printf("ERROR -- Size of Chequer Colour cannot be Zero\n");
	exit(1);
      }
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Chequers\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);
  return(answer);
}

void InitBrick(Proc *AProc)
{
  AProc->Name = BrickName;
  AProc->Eval = BrickShader;
  AProc->Read = BrickRead;
}

BrickVars *BrickRead(FILE *inputfile)
{
  char var[20];
  BrickVars *answer;
  answer = (BrickVars *)AllocHeap(sizeof(BrickVars));
  answer->A = NULL;
  answer->B = NULL;
  answer->Size = 1.0;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "MORTAR") == ZERO)
    {
      answer->A = ReadMaterial(inputfile);
    }
    else if (strcmp(var, "BRICK") == ZERO)
    {
      answer->B = ReadMaterial(inputfile);
    }
    else if (strcmp(var, "SIZE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(answer->Size));
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Chequers\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);
  return(answer);
}

char *BrickName()
{
  return "BRICKS";
}

MatDef *BrickShader(BrickVars *parms, IntRec *AnInt, MatDef *MatAns)
{
  int a, b;
  Flt u, v;

  if (AnInt->DoneMap == 0) ResolveMap(AnInt);

  u = AnInt->u;
  v = AnInt->v;
  a = 12.0 * u/parms->Size;
  b = 12.0 * v/parms->Size;
  while (a<0) a += 12;
  while (b<0) b += 12;
  while (a>11) a -= 12;
  while (b>11) b -= 12;
  if (b <= 0)
  {
     if (parms->A != NULL)
    ResolveMaterial(parms->A, AnInt, MatAns);
  }
  else if (b <= 5)
  {
    if (a>0)
    {
       if (parms->B != NULL)
      ResolveMaterial(parms->B, AnInt, MatAns);
    }
    else
    {
       if (parms->A != NULL)
      ResolveMaterial(parms->A, AnInt, MatAns);
    }
  }
  else if (b <= 6)
  {
    if (parms->A != NULL)
       ResolveMaterial(parms->A, AnInt, MatAns);
  }
  else
  {
    if (a <= 5 || a >= 7)
    {
       if (parms->B != NULL)
      ResolveMaterial(parms->B, AnInt, MatAns);
    }
    else
    {
       if (parms->A != NULL)
      ResolveMaterial(parms->A, AnInt, MatAns);
    }
  }
  return(MatAns);
}

void InitFades(Proc *AProc)
{
  AProc->Name = FadesName;
  AProc->Eval = FadesShader;
  AProc->Read = FadesRead;
}

char *FadesName()
{
  return "FADES";
}


void FadeMaterials(Flt Depth, MatDef *AMat, MatDef *BMat, Vec *Norm1, Vec *Norm2, MatDef *CMat, Vec *RetNorm)
{
  CFlt D1, D2;
  D1 = Depth;
  D2 = 1.0 - Depth;
  if (AMat->kri != BMat->kri)
  {
    CMat->kri    = AMat->kri      * D2 + BMat->kri      * D1;
  }
  else
  {
    CMat->kri = AMat->kri;
  }

  if (AMat->kdiff != BMat->kdiff)
  {
    CMat->kdiff  = AMat->kdiff    * D2 + BMat->kdiff    * D1;
  }
  else
  {
    CMat->kdiff = AMat->kdiff;
  }

  if (AMat->kspec != BMat->kspec)
  {
    CMat->kspec  = AMat->kspec    * D2 + BMat->kspec    * D1;
  }
  else
  {
    CMat->kspec = AMat->kspec;
  }


  if (AMat->kspecpow != BMat->kspecpow)
  {
    CMat->kspecpow  = AMat->kspecpow  * D2 + BMat->kspecpow  * D1;
  }
  else
  {
    CMat->kspecpow = AMat->kspecpow;
  }

  if (AMat->ktran != BMat->ktran)
  {
    CMat->ktran = AMat->ktran   * D2 + BMat->ktran   * D1;
  }
  else
  {
    CMat->ktran = AMat->ktran;
  }

  if (AMat->kref != BMat->kref)
  {
    CMat->kref  = AMat->kref    * D2 + BMat->kref    * D1;
  }
  else
  {
    CMat->kref = AMat->kref;
  }

  if (AMat->GlossSpread != BMat->GlossSpread)
  {
    CMat->GlossSpread = AMat->GlossSpread * D2 + BMat->GlossSpread * D1;
  }
  else
  {
    CMat->GlossSpread = AMat->GlossSpread;
  }

  if (AMat->GlossSamples > BMat->GlossSamples)
  {
    CMat->GlossSamples = AMat->GlossSamples;
  }
  else
  {
    CMat->GlossSamples = BMat->GlossSamples;
  }

  FadeColours(D1, &(AMat->DiffCol), &(BMat->DiffCol), &(CMat->DiffCol));
  FadeColours(D1, &(AMat->SpecCol), &(BMat->SpecCol), &(CMat->SpecCol));
  FadeColours(D1, &(AMat->TranCol), &(BMat->TranCol), &(CMat->TranCol));
  FadeColours(D1, &(AMat->AmbCol),  &(BMat->AmbCol),  &(CMat->AmbCol));
  VecComb((Flt)D2, Norm1, (Flt)D1, Norm2, RetNorm);
}


MatDef *FadesShader(FadeVars *parms, IntRec *AnInt, MatDef *MatAns)
{
  Vec *Normal;
  Vec *Posn;
  CFlt Depth;
  Vec Sub1;
  FadeRecord *ARec;
  IntRec Rec1, Rec2;
  MatDef A, B;
  Normal = &(AnInt->Normal);
  Posn   = &(AnInt->MapCoord);
  VecSub(Posn, &(parms->Origin), &Sub1);

  Depth = VecDot(&(parms->Axis), &Sub1);

  Depth = ((Depth/parms->TotalDepth) - floor(Depth/parms->TotalDepth)) * parms->TotalDepth;
/*   Depth = fmod(Depth + 1000.0 * parms->TotalDepth, parms->TotalDepth); */

  ARec = parms->Records;

  while (Depth > ARec->NextRecord->Depth && ARec->NextRecord != NULL)
  {
    Depth -= ARec->NextRecord->Depth;
    ARec = ARec->NextRecord;
  }
  /* ARec Now Points to the TOP record for the Range Required */
  Depth /= ARec->NextRecord->Depth;
  if (ARec->Material == NULL)
  {
    FadeColours(Depth, &(ARec->Colour), &(ARec->NextRecord->Colour), &(MatAns->DiffCol));
  }
  else
  {
    CopyInt(AnInt, &Rec1);
    CopyInt(AnInt, &Rec2);

    ResolveMaterial(ARec->Material, &Rec1, &A);
    ResolveMaterial(ARec->NextRecord->Material, &Rec2, &B);
    FadeMaterials(Depth, &A, &B, &(Rec1.Normal), &(Rec2.Normal), MatAns, Normal);
  }
  return(MatAns);
}

FadeVars *FadesRead(FILE *inputfile)
{
  char var[20];
  CFlt Depth;
  colour ACol;
  FadeVars *answer;
  FadeRecord *FRec;
  MatDef *AMaterial;
  answer = (FadeVars *)AllocHeap(sizeof(FadeVars));
  SetVector(0.0, 1.0, 0.0, &(answer->Axis));
  SetVector(0.0, 0.0, 0.0, &(answer->Origin));
  answer->TotalDepth = 0;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "AXIS") == ZERO)
    {
      ReadVec(inputfile, &(answer->Axis));
    }
    else if (strcmp(var, "MATERIALS") == ZERO)
    {
      do
      {
        fscanf(inputfile, " " CFltFmt " ", &Depth);
        AMaterial = ReadMaterial(inputfile);
        FRec = AllocHeap(sizeof(FadeRecord));
        FRec->Material = AMaterial;
        FRec->Depth = Depth;
        FRec->NextRecord = answer->Records;
        answer->Records = FRec;
        answer->TotalDepth += Depth;
      } while (Depth != 0.0 && !feof(inputfile));
    }
    else if (strcmp(var, "COLOURS") == ZERO || strcmp(var, "COLORS") == ZERO)
    {
      do
      {
        fscanf(inputfile, " " CFltFmt " ", &Depth);
        ReadCol(inputfile, &ACol);
        FRec = AllocHeap(sizeof(FadeRecord));
        FRec->Colour = ACol;
        FRec->Depth  = Depth;
        FRec->Material = NULL;
        FRec->NextRecord = answer->Records;
        answer->Records  = FRec;
        answer->TotalDepth += Depth;
      } while (Depth != 0.0 && !feof(inputfile));
      if (Depth > 0.0 && feof(inputfile))
      {
        printf("Error - Reached end of file while reading Fader Records\n");
        exit(1);
      }
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Noise\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);

  return(answer);
}

void InitTurb3D(Proc *AProc)
{
  AProc->Name  = Turb3DName;
  AProc->Eval  = Turb3DShader;
  AProc->Value = Turb3DValue;
  AProc->Read  = Turb3DRead;
}

char *Turb3DName()
{
  return "TURB3D";
}

void Turb3DValue(Turb3DVars *parms, IntRec *AnInt, colour *Answer)
{
  Vec ASub, BSub;
  Flt Size, Depth;
  int Terms;
  IntRec Int2;
  Vec Posn;
  Object *AnObject = AnInt->HitObj;
  if (AnInt->DoneMap == 0) ResolveMap(AnInt);
  Posn = AnInt->MapCoord;
  ASub = Posn;
  Size	= parms->Size;
  Depth = parms->Depth;

  Posn.x = 0.0;
  Posn.y = 0.0;
  Posn.z = 0.0;
  for (Terms = parms->Terms; Terms > 0; Terms--)
  {
    noise3(ASub.x / Size, ASub.y / Size, ASub.z / Size, &BSub);
    VecAdds(Depth, &BSub, &Posn, &Posn);
    Depth /= 2.0;
    Size /= 2.0;
  }
  Answer->red   = (0.5 * Posn.x)+0.5;
  Answer->green = (0.5 * Posn.y)+0.5;
  Answer->blue  = (0.5 * Posn.z)+0.5;
}

MatDef *Turb3DShader(Turb3DVars *parms, IntRec *AnInt, MatDef *MatAns)
{
  IntRec Int2;
  colour aCol;
  Vec    aVec;

  Turb3DValue(parms, AnInt, &aCol);

  CopyInt(AnInt, &Int2);

  aVec.x = (2.0 * aCol.red)   - 1.0;
  aVec.y = (2.0 * aCol.green) - 1.0;
  aVec.z = (2.0 * aCol.blue)  - 1.0;

  Int2.u += aVec.x;
  Int2.v += aVec.y;

  VecAdd(&aVec, &(Int2.MapCoord), &(Int2.MapCoord));
  ResolveMaterial(parms->Material, &Int2, MatAns);
  return(MatAns);
  
}

Turb3DVars *Turb3DRead(FILE *inputfile)
{
  char var[20];
  Turb3DVars *answer;
  int Modifier;
  answer = (Turb3DVars *)AllocHeap(sizeof(Turb3DVars));
  answer->Terms = 5;
  answer->Depth = 1.0;
  answer->Size	= 1.0;
  answer->Material = NULL;
  Modifier = 0;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "MODIFIER") == ZERO)
    {
      Modifier = 1;
    }
    else if (strcmp(var, "TERMS") == ZERO)
    {
      fscanf(inputfile, " %d ", &(answer->Terms));
    }
    else if (strcmp(var, "DEPTH") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(answer->Depth));
    }
    else if (strcmp(var, "SIZE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(answer->Size));
    }
    else if (strcmp(var, "MATERIAL") == ZERO)
    {
      answer->Material = ReadMaterial(inputfile);
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Turb3d\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);

  if (answer->Material == NULL && !Modifier)
  {
    printf("Error - Turbulance has Nothing to work on\n");
    exit(1);
  }
  return(answer);
}


void InitTurb(Proc *AProc)
{
  AProc->Name = TurbName;
  AProc->Eval = TurbShader;
  AProc->Read = TurbRead;
}

char *TurbName()
{
  return "TURB";
}

MatDef *TurbShader(TurbVars *parms, IntRec *AnInt, MatDef *MatAns)
{
  Vec ASub, BSub, CSub;
  Flt XSize, YSize, ZSize, Depth, MaxDepth = 0.0;
  CFlt Slider;
  int Terms, i;
  MatDef A, B;
  IntRec Rec1, Rec2;
  ASub = AnInt->MapCoord;
  XSize  = parms->XSize;
  YSize  = parms->YSize;
  ZSize  = parms->ZSize;
  Depth = 0.5;
  SetVector(0.0, 0.0, 0.0, &CSub);
  for (Terms = parms->Terms; Terms > 0; Terms--)
  {
    noise3(ASub.x / XSize, ASub.y / YSize, ASub.z / ZSize, &BSub);
    VecAdds(Depth, &BSub, &CSub, &CSub);
    MaxDepth += Depth;
    Depth /= 2.0;
    XSize /= 2.0;
    YSize /= 2.0;
    ZSize /= 2.0;
  }
  Slider = 0.5 + (0.5 * (CSub.x / MaxDepth));

  for (i=0; i<parms->Power; i++) Slider = SSin(Slider);

  CopyInt(AnInt, &Rec1);
  CopyInt(AnInt, &Rec2);

  ResolveMaterial(parms->AMaterial, &Rec1, &A);
  ResolveMaterial(parms->BMaterial, &Rec2, &B);
  FadeMaterials(Slider, &A, &B, &(Rec1.Normal), &(Rec2.Normal), MatAns, &(AnInt->Normal));
  return (MatAns);
}

CFlt SSin(CFlt Value)
{
   CFlt Ans;
   Ans = 0.5 + 0.5 * sin((Value-0.5) * PI);
   return(Ans);
}

TurbVars *TurbRead(FILE *inputfile)
{
  char var[20];
  TurbVars *answer;
  Flt Size=1.0, XSize=1.0, YSize=1.0, ZSize = 1.0;
  answer = (TurbVars *)AllocHeap(sizeof(TurbVars));
  answer->Terms = 5;
  answer->XSize  = 1.0;
  answer->YSize  = 1.0;
  answer->ZSize  = 1.0;
  answer->Power = 3;
  answer->AMaterial = NULL;
  answer->BMaterial = NULL;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "TERMS") == ZERO)
    {
      fscanf(inputfile, " %d ", &(answer->Terms));
    }
    else if (strcmp(var, "POWER") == ZERO)
    {
      fscanf(inputfile, " %d ", &(answer->Power));
    }
    else if (strcmp(var, "SIZE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Size);
    }
    else if (strcmp(var, "X-SIZE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &XSize);
    }
    else if (strcmp(var, "Y-SIZE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &YSize);
    }
    else if (strcmp(var, "Z-SIZE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &ZSize);
    }
    else if (strcmp(var, "MATERIAL-A") == ZERO)
    {
      answer->AMaterial = ReadMaterial(inputfile);
    }
    else if (strcmp(var, "MATERIAL-B") == ZERO)
    {
      answer->BMaterial = ReadMaterial(inputfile);
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Turb\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);
  answer->XSize = Size * XSize;
  answer->YSize = Size * YSize;
  answer->ZSize = Size * ZSize;

  return(answer);
}


void InitImage(Proc *AProc)
{
  AProc->Name  = ImageName;
  AProc->Eval  = ImageShader;
  AProc->Value = ImageValue;
  AProc->Read  = ImageRead;
  printf("XX=%x\n", ImageRead);
}

char *ImageName()
{
  return "IMAGE";
}

MatDef *ImageShader(ImageVars *parms, IntRec *AnInt, MatDef *MatAns)
{
  ImageValue(parms, AnInt, &(MatAns->DiffCol));

  return (MatAns);
}

void ImageValue(ImageVars *parms, IntRec *AnInt, colour *Answer)
{
  int i, j, HashIndex, hit, readcnt, xsize;
  unsigned long ui, vi, uo, vo, ItemSize = (parms->Flags >> 1);
  ItemEntry *AnItem;
  ICacheEntry *AnIC;
  long FilePos;
  Flt ua, va;
  char Buff[32];
  Flt u, v;
  Vec Sub1;

  Object *AnObject = AnInt->HitObj;

  if (AnInt->DoneMap == 0)
  {
    if (AnObject->Primative->Inverse != NULL)
    {
      ((AnObject->Primative)->Inverse) (AnInt,
                        AnObject->kdata,
                        &(AnInt->u),
                        &(AnInt->v),
                        &Sub1);
      AnInt->DoneMap = 1;
    }
  }
  if (parms->Flip)
  {
    u = AnInt->u - parms->xpos;
    v = AnInt->v - parms->ypos;
  }
  else
  {
    v = AnInt->u - parms->xpos;
    u = AnInt->v - parms->ypos;
  }
  if (parms->LRUList == NULL)
  {
    /* This image has never been used before, so open up the cache table
       and LRU lists, prior to the first cache miss */

    parms->LRUList = CreateList();
    for (i = 0; i < 128; i++)
    {
      parms->HashTable[i] = CreateList();
    }

  }

  ua = (u / parms->xscale) - floor(u / parms->xscale);
  va = (v / parms->yscale) - floor(v / parms->yscale);

  ui = ua * parms->xsize;
  vi = va * parms->ysize;

  if (ui >= parms->LineLength) ui = parms->LineLength - 1;
  if (vi >= parms->LineHeight) vi = parms->LineHeight - 1;

  if ((parms->Info & 0x20) != 0)
  {
    vi = parms->LineHeight - (vi + 1);
  }

  uo = ui & 0x07;
  vo = vi & 0x07;

  ui = ui & 0xFFFFFFF8;
  vi = vi & 0xFFFFFFF8;

  HashIndex = (ui & 0x0000007F)       ^
	     ((ui >> 8) & 0x0000007F) ^
	     ((ui >>16) & 0x0000007F) ^
	     ((ui >>24) & 0x0000007F) ^
	      (vi & 0x0000007F)	      ^
	     ((vi >> 8) & 0x0000007F) ^
	     ((vi >>16) & 0x0000007F) ^
	     ((vi >>24) & 0x0000007F);

  hit = 0;
  for (AnItem = parms->HashTable[HashIndex]->Next; AnItem->Data != NULL && hit == 0; AnItem = AnItem->Next)
  {
    AnIC = AnItem->Data;
    if (ui == AnIC->xpos && vi == AnIC->ypos)
    {
      hit = 1;
      RemoveFromList(AnIC->LRUPtr);
    }
  }
  if (hit == 0)
  {
    /* Cache Miss Situation, Panic Chip Engaging, Panic Chip Engaged...
       uuueeerrrhhhuuurrrggghhh 	*/
    if (parms->MaxCache > parms->CurrCache)
    {
      if ((AnIC = malloc(sizeof(ICacheEntry)))==NULL)
      {
        printf("Error -- Unable to allocate cache entry\n");
        exit(1);
      }
      parms->CurrCache++;
      if ((parms->Flags & 0x01) == 1)
      {
        AnIC->ImageInfo = malloc(sizeof(ImageNPalEntry));
      }
      else
      {
        AnIC->ImageInfo = malloc(sizeof(ImagePalEntry));
      }

    }
    else
    {
      AnIC = RemoveFromList(parms->LRUList->Prev);
      RemoveFromList(AnIC->HashPtr);
    }

    AnIC->xpos = ui;
    AnIC->ypos = vi;



    /* Now, fill in the cache data block */
    for (i = 0; i<8; i++)
    {
      if ((vi + i) < parms->LineHeight)
      {
	if ((parms->Flags & 0x01) == 1)
	{
	  FilePos = parms->HomePos + (ItemSize * (long)ui) + (ItemSize * (long)parms->LineLength) * (long)(vi + i);
	  if (ui == (parms->LineLength & 0xFFFFFFF8))
	  {
	    xsize = ItemSize * (parms->LineLength & 0x00000007);
	  }
	  else
	  {
	    xsize = 8 * ItemSize;
	  }
	}
	else
	{
	  FilePos = parms->HomePos + ((long)ui) + ((long)parms->LineLength) * (long)(vi + i);
	  if (ui == (parms->LineLength & 0xFFFFFFF8))
	  {
	    xsize = (parms->LineLength & 0x00000007);
	  }
	  else
	  {
	    xsize = 8;
	  }
	}
	fseek(parms->ImageFile, FilePos, SEEK_SET);

	readcnt = fread(&Buff, sizeof(char), xsize, parms->ImageFile);
	if (readcnt != xsize)
	{
	  printf("Error Reading File(%d, %d)[%ld, %ld], %ld\n", readcnt, xsize, ui, vi, ItemSize);
	  exit(1);
	}
	if ((parms->Flags & 0x01) == 1)
	{
	  for (j = 0; j < 8; j++)
	  {
	    if (ItemSize == 3 || ItemSize == 4)
	    {
	      ((ImageNPalEntry *)(AnIC->ImageInfo))->Blue[j][i]	 = Buff[j*ItemSize];
	      ((ImageNPalEntry *)(AnIC->ImageInfo))->Green[j][i] = Buff[j*ItemSize+1];
	      ((ImageNPalEntry *)(AnIC->ImageInfo))->Red[j][i]	 = Buff[j*ItemSize+2];
	    }
	    else
	    {
	      ((ImageNPalEntry *)(AnIC->ImageInfo))->Blue[j][i]	 =  (Buff[ItemSize * j] & 0x1F) << 3;
	      ((ImageNPalEntry *)(AnIC->ImageInfo))->Green[j][i] = ((Buff[1 + ItemSize * j] & 0x03) << 6) + ((Buff[ItemSize * j] & 0xE0) >> 2);
	      ((ImageNPalEntry *)(AnIC->ImageInfo))->Red[j][i]	 =  (Buff[1 + ItemSize * j] & 0x7C) << 1;;
	    }

	  }
	}
	else
	{
	  for (j = 0; j < 8; j++)
	  {
	    ((ImagePalEntry *)(AnIC->ImageInfo))->Index[j][i] = Buff[j];
	  }
	}
      }

    }


    AnIC->HashPtr = AddToList(AnIC, parms->HashTable[HashIndex]);
  }
  if ((parms->Flags & 0x01) == 1)
  {
    Answer->red	  = ((Flt)(((ImageNPalEntry *)(AnIC->ImageInfo))->Red[uo][vo]))	  / 256.0;
    Answer->green = ((Flt)(((ImageNPalEntry *)(AnIC->ImageInfo))->Green[uo][vo])) / 256.0;
    Answer->blue  = ((Flt)(((ImageNPalEntry *)(AnIC->ImageInfo))->Blue[uo][vo]))  / 256.0;
  }
  else
  {
    int Index = ((ImagePalEntry *)(AnIC->ImageInfo))->Index[uo][vo];

    Answer->red	  = parms->ThePal->Colours[Index].IRed	 / 256.0;
    Answer->green = parms->ThePal->Colours[Index].IGreen / 256.0;
    Answer->blue  = parms->ThePal->Colours[Index].IBlue  / 256.0;
  }
  AnIC->LRUPtr = AddToList(AnIC, parms->LRUList);
}

ImageVars *ImageRead(FILE *inputfile)
{
  unsigned char var[20], *name, FileName[120], Buff[5];
  ImageVars *Answer;
  ItemEntry *AnItem;
  ImageList *AnImage;
  int FlipIt, i, MaxCache=128, ImageXSize, ImageYSize, CMStart, CMLength, CMSize;
  Flt XSize, YSize, XPos, YPos;
  struct TGAHdr TGAFileHdr;

  printf("EEE"); fflush(stdout);
  strcpy(FileName,"");
  Answer = NULL;
  XSize  = 1.00;
  YSize  = 1.00;
  XPos   = 0.0;
  YPos   = 0.0;
  FlipIt = 0;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "FILE") == ZERO)
    {
      fscanf(inputfile, "%s", FileName);
    }
    else if (strcmp(var, "MAXCACHE") == ZERO)
    {
      fscanf(inputfile, "%d", &MaxCache);
    }
    else if (strcmp(var, "FLIP") == ZERO)
    {
      FlipIt = 1;
    }
    else if (strcmp(var, "X-POS") == ZERO)
    {
      fscanf(inputfile, "" FltFmt "", &XPos);
    }
    else if (strcmp(var, "Y-POS") == ZERO)
    {
      fscanf(inputfile, "" FltFmt "", &YPos);
    }
    else if (strcmp(var, "X-SIZE") == ZERO)
    {
      fscanf(inputfile, "" FltFmt "", &XSize);
    }
    else if (strcmp(var, "Y-SIZE") == ZERO)
    {
      fscanf(inputfile, "" FltFmt "", &YSize);
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Image\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);

  if (strlen(FileName) > 0)
  {
    for (AnItem = ImList->Next; AnItem->Data != NULL; AnItem = AnItem->Next)
    {
      AnImage = AnItem->Data;
      if (strcmp(FileName, AnImage->FileName) == ZERO)
      {
        Answer = (ImageVars *)AllocHeap(sizeof(ImageVars));

        name = malloc(strlen(AnImage->FileName)+1);
        strcpy(name, AnImage->FileName);

        Answer->LRUList    = AnImage->VarPtr->LRUList;
        Answer->CurrCache  = 0;
        Answer->ImageFile  = AnImage->VarPtr->ImageFile;
        Answer->HomePos    = AnImage->VarPtr->HomePos;
        Answer->LineLength = AnImage->VarPtr->LineLength;
        Answer->LineHeight = AnImage->VarPtr->LineHeight;
        Answer->MaxCache   = AnImage->VarPtr->MaxCache;
        Answer->Info       = AnImage->VarPtr->Info;
        Answer->Flags      = AnImage->VarPtr->Flags;
        Answer->ThePal     = AnImage->VarPtr->ThePal;
        ImageXSize = AnImage->VarPtr->LineLength;
        ImageYSize = AnImage->VarPtr->LineHeight;


        for (i = 0; i < 128; i++)
            Answer->HashTable[i] = AnImage->VarPtr->HashTable[i];

      }
    }
    if (Answer == NULL)
    {
      Answer = (ImageVars *)AllocHeap(sizeof(ImageVars));
      name = malloc(strlen(FileName)+1);
      strcpy(name, FileName);

      AnImage = malloc(sizeof(ImageList));

      AnImage->VarPtr = Answer;
      AnImage->FileName = name;

      AddToList(AnImage, ImList);

      Answer->ImageFile = NULL;

      for (i = 0; i<128; i++)
	  Answer->HashTable[i] = NULL;

      Answer->LRUList = NULL;

      Answer->CurrCache = 0;

      if ((Answer->ImageFile = fopen(FileName, "rb"))==NULL)
      {
        printf("Error -- Unable to open image file (%s)\n", FileName);
        exit(7);
      }
      else
      {
      fread(&TGAFileHdr, sizeof(char), sizeof(TGAFileHdr), Answer->ImageFile);
      if (TGAFileHdr.XOriginLo    != 0    ||
          TGAFileHdr.XOriginHi    != 0    ||
          TGAFileHdr.YOriginLo    != 0    ||
          TGAFileHdr.YOriginHi    != 0)
      {
        printf("Error -- This TGA file does not agree with format used by PortRAY\n");
        exit(1);
      }
      ImageXSize = (unsigned int)TGAFileHdr.WidthLo   + ((unsigned int)TGAFileHdr.WidthHi * 256);
      ImageYSize = (unsigned int)TGAFileHdr.HeightLo + ((unsigned int)TGAFileHdr.HeightHi * 256);
      if (fgetpos(Answer->ImageFile, &(Answer->HomePos)) != ZERO)
      {
        printf("Error -- Unable to get File Position for Later use\n");
        exit(1);
      }
      printf("%d\n", TGAFileHdr.IDLen);
      Answer->HomePos = Answer->HomePos + TGAFileHdr.IDLen;
      Answer->LineLength = ImageXSize;
      Answer->LineHeight = ImageYSize;
      Answer->MaxCache   = MaxCache;
      Answer->Info       = TGAFileHdr.TGAInfo;
      Answer->Flags      = 0;
      if (TGAFileHdr.ImageType == 2)
      {
        /* Non Paletted Image */
        Answer->Flags |= 0x01;
        Answer->Flags |= (TGAFileHdr.Depth / 8) << 1;

        if (TGAFileHdr.ColMapInfo != 0)
        {
          printf("Error Reading (%s), Image type 2 should NOT have a colour map\n", FileName);
          exit(1);
        }

      }
      else if (TGAFileHdr.ImageType == 1)
      {
        /* Paletted image */
        Answer->Flags &= 0xFE;
        if (TGAFileHdr.ColMapInfo != 1)
        {
          printf("Error Reading (%s), Image type 1 should have a colour map\n", FileName);
          exit(1);
        }
        Answer->ThePal = malloc(sizeof(Palette));
        CMStart  = (unsigned int)TGAFileHdr.CMOriginLo     + ((unsigned int)TGAFileHdr.CMOriginHi * 256);
        CMLength = (unsigned int)TGAFileHdr.CMLengthLo + ((unsigned int)TGAFileHdr.CMLengthHi * 256);
        CMSize   = (unsigned int)TGAFileHdr.CMEntrySize / 8;
        fsetpos(Answer->ImageFile, &(Answer->HomePos));
        printf ("CSize %d CMLength %d CMStart %d\n", CMSize, CMLength, CMStart);

        for (i = CMStart; i < CMLength; i++)
        {
          fread(Buff, CMSize, 1, Answer->ImageFile);
          if (CMSize == 3 || CMSize == 4)
          {
            Answer->ThePal->Colours[i].IBlue  = Buff[0];
            Answer->ThePal->Colours[i].IGreen = Buff[1];
            Answer->ThePal->Colours[i].IRed   = Buff[2];
            printf("%d : %d %d %d\n", i, 
                    Answer->ThePal->Colours[i].IRed, 
                    Answer->ThePal->Colours[i].IGreen, 
                    Answer->ThePal->Colours[i].IBlue); 
          }
          else if (CMSize == 2)
          {
            Answer->ThePal->Colours[i].IBlue  =  (Buff[0] & 0x1F) << 3;
            Answer->ThePal->Colours[i].IGreen = ((Buff[1] & 0x03) << 6) + ((Buff[0] & 0xE0) >> 2);
            Answer->ThePal->Colours[i].IRed   =  (Buff[1] & 0x7C) << 1;

          }
        }
        Answer->HomePos += CMLength * CMSize;
      }

      else
      {
        printf("Error -- Unsupported Image Type (%s)\n", FileName);
      }

      }
    }
  }
  else
  {
    printf("Error -- No image file specified for Image Map\n");
    exit(8);
  }
  Answer->xsize    = (Flt)ImageXSize;
  Answer->xscale   = (Flt)XSize;
  Answer->ysize    = (Flt)ImageYSize;
  Answer->yscale   = (Flt)YSize;
  Answer->Flip     = FlipIt;
  Answer->xpos	   = XPos;
  Answer->ypos     = YPos;

  /* Now, lift out the x-size and y-size of the image in pixels from the
     file, to help in image size calcs later */


  return(Answer);
}

void InitNoise(Proc *AProc)
{
  AProc->Name = NoiseName;
  AProc->Eval = NoiseShader;
  AProc->Read = NoiseRead;
}

char *NoiseName()
{
  return "NOISE";
}

MatDef *NoiseShader(NoiseVars *parms, IntRec *AnInt, MatDef *MatAns)
{
  Vec ASub;
  Vec *Posn;
  Posn = &(AnInt->MapCoord);
  noise3(Posn->x / (parms->Size), Posn->y / (parms->Size), Posn->z / (parms->Size), &ASub);
  if (parms->Material != NULL)
     ResolveMaterial(parms->Material, AnInt, MatAns);

  MatAns->DiffCol.red	= parms->Dominant.red	+ (parms->Varience * ASub.x);
  MatAns->DiffCol.green = parms->Dominant.green + (parms->Varience * ASub.y);
  MatAns->DiffCol.blue	= parms->Dominant.blue	+ (parms->Varience * ASub.z);
  return(MatAns);
}

NoiseVars *NoiseRead(FILE *inputfile)
{
  char var[20];
  NoiseVars *answer;
  answer = (NoiseVars *)AllocHeap(sizeof(NoiseVars));
  answer->Size = 1.0;
  answer->Material = NULL;
  SetColour(0.5, 0.5, 0.5, &(answer->Dominant));
  answer->Varience = 0.5;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "SIZE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(answer->Size));
      if (answer->Size < EFFECTIVE_ZERO)
      {
	printf("ERROR -- Size of Noise Colour cannot be Zero\n");
	exit(1);
      }
    }
    else if (strcmp(var, "MATERIAL") == ZERO)
    {
      answer->Material = ReadMaterial(inputfile);
    }
    else if (strcmp(var, "VARIENCE") == ZERO)
    {
      fscanf(inputfile, " " CFltFmt " ", &(answer->Varience));
    }
    else if (strcmp(var, "COLOUR") == ZERO || strcmp(var, "COLOR") == ZERO)
    {
      ReadCol(inputfile, &(answer->Dominant));
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Noise\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);
  return(answer);
}

void InitBumpy(Proc *AProc)
{
  AProc->Name = BumpyName;
  AProc->Eval = BumpyShader;
  AProc->Read = BumpyRead;
}

char *BumpyName()
{
  return "BUMPY";
}

MatDef *BumpyShader(BumpyVars *parms, IntRec *AnInt, MatDef *MatAns)
{
  Flt XSize, YSize, ZSize, Depth;
  Flt AVecDot;
  Vec ASub, BSub, CSub;
  Vec *Posn;
  Vec *Normal;
  int Terms;

  Posn = &(AnInt->MapCoord);
  Normal = &(AnInt->Normal);
  ASub = *Posn;
  XSize	= 1.0 / parms->XSize;
  YSize	= 1.0 / parms->YSize;
  ZSize	= 1.0 / parms->ZSize;
  Depth = parms->Depth;
  SetVector(0.0, 0.0, 0.0, &CSub);
  for (Terms = parms->Terms; Terms > 0; Terms--)
  {
    noise3(ASub.x * XSize, ASub.y * YSize, ASub.z * ZSize, &BSub);
    VecAdds(Depth, &BSub, &CSub, &CSub);
    Depth /= 2.0;
    XSize *= 2.0;
    YSize *= 2.0;
    ZSize *= 2.0;
  }

  VecUnit(&CSub, &BSub);
  AVecDot =  acos(VecDot(&BSub, Normal));
  if (AVecDot < parms->Saturation)
  {
    VecNegate(&CSub, &CSub);
    VecAdds(parms->Depth, &CSub, Normal, Normal);
    VecUnit(Normal, Normal);
  }

  if (parms->Material != NULL)
     ResolveMaterial(parms->Material, AnInt, MatAns);

  return(MatAns);
}

BumpyVars *BumpyRead(FILE *inputfile)
{
  Flt Size, XSize, YSize, ZSize;
  char var[20];
  BumpyVars *answer;
  answer = (BumpyVars *)AllocHeap(sizeof(BumpyVars));
  Size = 1.0;
  XSize = 1.0;
  YSize = 1.0;
  ZSize = 1.0;
  answer->Terms      = 1;
  answer->Saturation = 8.0;
  answer->Depth      = 0.4;
  answer->Material   = NULL;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "SIZE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &Size);
      if (Size < EFFECTIVE_ZERO)
      {
	printf("ERROR -- Size of Bumpy Shader cannot be Zero\n");
	exit(1);
      }
    }
    else if (strcmp(var, "TERMS") == ZERO)
    {
      fscanf(inputfile, " %d ", &(answer->Terms));
    }
    else if (strcmp(var, "XSIZE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &XSize);
      if (XSize < EFFECTIVE_ZERO)
      {
	printf("ERROR -- Size of Bumpy Shader cannot be Zero\n");
	exit(1);
      }
    }
    else if (strcmp(var, "MATERIAL") == ZERO)
    {
      answer->Material = ReadMaterial(inputfile);
    }
    else if (strcmp(var, "YSIZE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &YSize);
      if (YSize < EFFECTIVE_ZERO)
      {
	printf("ERROR -- Size of Bumpy Shader cannot be Zero\n");
	exit(1);
      }
    }
    else if (strcmp(var, "ZSIZE") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &ZSize);
      if (ZSize < EFFECTIVE_ZERO)
      {
	printf("ERROR -- Size of Bumpy Shader cannot be Zero\n");
	exit(1);
      }
    }
    else if (strcmp(var, "SATURATION") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(answer->Saturation));
      if (answer->Saturation < EFFECTIVE_ZERO)
      {
	printf("ERROR -- Saturation of Bumpy Shader cannot be Zero\n");
	exit(1);
      }
    }
    else if (strcmp(var, "DEPTH") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(answer->Depth));
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Bumpy\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);
  answer->XSize = XSize * Size;
  answer->YSize = YSize * Size;
  answer->ZSize = ZSize * Size;
  return(answer);
}


/*
void strupr(char *astring)
{
  char *chp;
  chp = astring;
  while (*chp != ZERO)
  {
    if (*chp >= 'a' && *chp <= 'z') *chp -= ('a'-'A');
    chp++;
  }
}
*/
