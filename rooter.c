
Ä Area: Raytracing           [PCGnet] ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
  Msg#: 6636            Rec'd                        Date: 08-14-93  00:30
  From: Han-Wen Nienhuys                             Read: Yes    Replied: No 
    To: Peter Morris                                 Mark:                     
  Subj: british ray-tracing
ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ
[Op 08-09-93  20:40 schreef je aan Steve Burrows]
 PM> Start With Poli in 1 Variable
 PM> If Order of Poli is <= 2

   :
   :

abstract:
        solve polynomial p(x) if degree <= 2,
else:
        solve derivative dp/dx of polynomial , the roots of dp/dx are
the extremes of the p(x).  If two extrema x_n, x_(n+1) have different
sign, then search root between x_n and x_(n+1) (with binary chop)

 PM> I have never seen this method used (It may be a lot slower
 PM> than Newton-Raphson or others) But is has the advantage of
 PM> having NO loopholes.

An improvement can be made by taking x1 = 0.  In raytracing, you're
usually interested in the smallest positive root.

I have coded it, but it's not very efficient. This algorithm calculates
too much: it calculates not only the roots of the first polynomial, but
also of all of it derivatives. And the number of steps for x1->x2
typically is 40. It might help if I coded a cubic solver (Cardano) and a
quartic solver (Ferrari). 

The big advantages of this algorithm are:
        - It's easy to understand, knowledge of analysis isn't needed.
        - It's immune to numerical inaccuracies.

It could be optimized by (for example) using a better algorithm()
than binary search for searching a root. If you would have info on the
second order derivative, you could use Newton/Raphson for this.

groeten,
  /HaWee/

{2:284/102, hanwen@stack.urc.tue.nl}

Here is my source, I hope you people don't mind me posting it here. It
seems to work, but I haven't really checked it. It still took me some
time, but coding while still asleep is hard.
/*
 * pes.c -- polynomial equation solver
 * by Han-Wen Nienhuys. This is Public Domain.
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define sqr(a) (a)*(a)
#define MAXROOTS 10
#define EPSILON 1e-10
#define ABS(a) ((a < 0) ? -(a) : (a) )
#define LARGE 1e5

void print_sols(double *s, int nosols, double *coeff, int deg);
int             equation_solver(double *coeffs, int deg, double *roots);
void            print_poly(double *c, int d);
int             solve_cubic(double *coeffs, double *roots);
typedef double coefl[MAXROOTS];


void 
derivative(double *der, double *org, int deg)
{
    do {
 der[deg - 1] = deg * org[deg];
    } while (--deg);
}

int 
dcomp(const void *p1, const void *p2)
{
    double         *d1, *d2;

    d1 = (double *) p1;
    d2 = (double *) p2;

    if (*d1 < *d2)
 return -1;
    if (*d1 > *d2)
 return 1;
    return 0;
}

/*
 * find zero using binary chop, could be improved: use Newton/Raphson
 */
double 
find_zero(double coeffs[], int n, double x1, double x2)
{
    double          midval, fval1, mid;

    while (ABS(x2 - x1) > EPSILON) {

 fval1 = poly(x1, n, coeffs);
 mid = (x1 + x2) / 2.0;
 midval = poly(mid, n, coeffs);

 if (ABS(midval) < EPSILON)
     return mid;

 if ((fval1 > 0 && midval > 0) || (fval1 < 0 && midval < 0))
     x1 = mid;
 else
     x2 = mid;
    }

    return mid;
}

int 
equation_solver(double *coeffs, int deg, double *roots)
{
    coefl          der, der_roots, 
 extrema,  groots;

    int             noroots, i, no_der_roots;

    if (deg == 1) {
 *roots = -coeffs[0] / coeffs[1];
 return 1;
    }
    if (deg == 2) {
 double          d;

 d = sqr(coeffs[1]) - 4 * coeffs[0] * coeffs[2];
 if (d < 0)
     return 0;
 if (d == 0.0) {
     *roots = -coeffs[1] / (2 * coeffs[2]);
     return 1;
 }
 d = sqrt(d);
 roots[0] = (-coeffs[1] + d) / (2 * coeffs[2]);
 roots[1] = (-coeffs[1] - d) / (2 * coeffs[2]);
 return 2;
    }
    if (deg == 3) {
 return solve_cubic(coeffs, roots);
    }
    
    /* higher order eqs */
    derivative(der, coeffs, deg);
    no_der_roots = equation_solver(der, deg - 1, der_roots);

    qsort((void *) der_roots, no_der_roots, sizeof(double), dcomp);

    for (i = 0; i < no_der_roots; i++) {
 groots[i + 1] = der_roots[i];
    }

    no_der_roots += 2;

    groots[0] = -LARGE;
    groots[no_der_roots - 1] = LARGE;

    for (i = 0; i < no_der_roots; i++) {
 extrema[i] = poly(groots[i], deg, coeffs);
    }

    for (i = 0; i < no_der_roots; i++)
 printf("%d, x = %lf, f(x) = %lf\n", i, groots[i], extrema[i]);

    noroots = 0;

    for (i = 0; i < no_der_roots && ABS(extrema[i]) < EPSILON; i++)
 printf("root %lf\n", roots[noroots++] = groots[i]);


    for (; i < no_der_roots - 1; i++) {
 if (ABS(extrema[i + 1]) < EPSILON) {
     printf("root %lf\n", roots[noroots++] = groots[i + 1]);
     i++;
     continue;
 }
 if (extrema[i] * extrema[i + 1] < 0) {
     printf("root %lf\n", roots[noroots++] = find_zero(coeffs, deg, groots[i],
groots[i + 1]));
 }
    }

    return noroots;
}

void 
print_poly(double *c, int d)
{
    do {
 if (c[d]) {
     printf("%lf", *(c + d));
     if (d == 1)
  printf(" t + ");
     else if (d > 1) {
  printf(" t^%d + ", d);
     }
 }
    } while (d-- > 0);
    printf("\n");
}

void
print_sols(double *s, int nosols, double *coeff, int deg)
{
    int             i;
    if (nosols <= 0) {
 printf("no solutions\n");
 return;
    }
    printf("%d solution(s)\n", nosols);

    for (i = 0; i < nosols; i++)
 printf("%d: p(%lf) = %lf\n", i+1, s[i], poly(s[i], deg, coeff) );

    printf("\n");
}

main()
{
    int             deg, d;
    coefl          rootcoef, sols;

    while (1) {
 printf("degree: ");
 scanf("%d", &deg);
 if (deg <= 0)
     return;

 d = deg;
 do {
     printf("%d order coeff: ", d);
     scanf("%lf", &rootcoef[d]);
     printf("read %lf\n", rootcoef[d]);
 } while (d-- > 0);

 printf("poly: \n");
 print_poly(rootcoef, deg);

 d = equation_solver(rootcoef, deg, sols);

 print_sols(sols, d, rootcoef, deg);
    }    
}

/* solve an equation using Cardano's method. */
/* doesn't work with 2 solution eqs */
int 
solve_cubic(double *coeffs, double *roots)
{
    double          tcoeffs[3], a, lin, quaddiv3, cons;
    int             noroots, i;

    a = coeffs[3];
    for (i = 0; i < 4; i++)
 tcoeffs[i] = coeffs[i] / a;

    a = tcoeffs[2];

    lin = -sqr(a) / 3.0 + tcoeffs[1];
    cons = (a * a * a * 2) / 27.0 - a * tcoeffs[1] / 3.0 + tcoeffs[0];

    {
 double          p, q, u, v, d, theta;

 p = -lin / 3;
 q = cons / 2;

 d = 4 * q * q - 4 * p * p * p;

 if (d >= 0) { /* test, OK */
     d = sqrt(d);
     u = (-2 * q + d) / 2;
     v = (-2 * q - d) / 2;

     if (u < 0)
       u = -pow(-u, 1.0 / 3.0);
     else
       u = pow(u, 1.0/3.0);
     if (v < 0)   
       v = -pow(-v, 1.0/3.0);
     else
       v = pow(v, 1.0 / 3.0);

     *roots = u + v - a / 3;

     noroots = 1;
 } else { /* OK */
     a = a / 3;
     
     d = acos(-q / (p * sqrt(p) )) / 3;
     p = 2 * sqrt(p);

     *roots++ = p * cos(d) - a;
     d += 2*M_PI / 3;
     *roots++ = p * cos(d) - a;
     d += 2*M_PI / 3;
     *roots++ = p * cos(d) - a;

     noroots = 3;
 }
    }
    return noroots;
}

-!- FMail 0.96g+
 ! Origin: BBS Bennekom: Fractal Board 31-8389-15331 V32/V42bis (9:526/203.0)

