/* Procedural Colour Type Definitions */

typedef struct CheqVars
{
  MatDef *A, *B;
  Flt Size;
} CheqVars;

typedef struct TileVars
{
  MatDef *Face, *Grout;
  Flt Size;
  int GroutWidth;
  int TileWidth;
} TileVars;

typedef struct NoiseVars
{
  Flt Size;
  CFlt Varience;
  colour Dominant;
  MatDef *Material;
} NoiseVars;

typedef struct BumpyVars
{
  Flt XSize, YSize, ZSize;
  Flt Saturation;
  Flt Depth;
  int Terms;
  MatDef *Material;
} BumpyVars;

typedef struct BrickVars
{
  MatDef *A, *B;
  Flt Size;
} BrickVars;

typedef struct FadeRecord
{
  struct FadeRecord *NextRecord;
  Flt Depth;
  colour Colour;
  MatDef *Material;
} FadeRecord;

typedef struct FadeVars
{
  Vec Axis;
  Vec Origin;
  int Repeat;
  CFlt TotalDepth;
  FadeRecord *Records;
} FadeVars;

typedef struct Turb3dVars
{
  int Terms;
  Flt Depth;
  Flt Size;
  MatDef *Material;
} Turb3DVars;

typedef struct TurbVars
{
  int Terms;
  int Power;
  Flt XSize, YSize, ZSize;
  MatDef *AMaterial;
  MatDef *BMaterial;
} TurbVars;

typedef struct IntCol
{
  unsigned int IRed, IGreen, IBlue;

} IntCol;

typedef struct Palette
{
  IntCol Colours[256];
} Palette;

typedef struct ImageVars
{
  FILE *ImageFile;
  int Flip;
  unsigned int MaxCache, CurrCache, LineLength, LineHeight;
  ItemEntry *HashTable[128];
  ItemEntry *LRUList;
  Flt xpos, ypos, xsize, ysize, xscale, yscale;
  long HomePos;
  unsigned char Info;
  unsigned char Flags;
  Palette *ThePal;
} ImageVars;

typedef struct ICacheEntry
{
  ItemEntry *LRUPtr;
  ItemEntry *HashPtr;
  unsigned int xpos, ypos;
  void *ImageInfo;
} ICacheEntry;

typedef struct ImageNPalEntry
{
  unsigned char Red[8][8];
  unsigned char Green[8][8];
  unsigned char Blue[8][8];
} ImageNPalEntry;

typedef struct ImagePalEntry
{
  unsigned char Index[8][8];

} ImagePalEntry;

typedef struct ImageList
{
  char *FileName;
  ImageVars *VarPtr;
} ImageList;
