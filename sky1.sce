Light
   Position ( -200 100 -100 )
   Colour ( 1 1 1 )
   shape sphere
      center (-200 100 -100)
      radius 30
   ;
   samples 100
   min-samples 10
;
camera
   look-at ( 0 1.5 0 )
   drop-line (0 -1 0)
   origin ( 0 3 -10 )
   depth 0.8
   x-size 0.64
   y-size 0.48
   filter standard
;

background ( 0 0 1 )

image
   xsize  640
   ysize  480
;

object
   sphere
      center (0 1 0)
      radius 1.0
   ;
;

object
   box
      a (-100000 100 -1000000)
      b (100000 101 1000000)
   ;
   material
      turb
         power 9
         terms 9
         size 200
         material-a
            colour (1 1 1)
         ;
         material-b
            colour (1 1 1)
            specular-reflection 0.0
            diffuse-reflection 0.0
            transparancy 1.0
            refractive-index 1.0
         ;
      ;

   ;
;
