/*
  *************************************************************************
 **                                                                       **
 **                           Sphere Section                              **
 **                                                                       **
  *************************************************************************
*/


char   *SphName();
Sph    *SphRead();
void    SphPrint();
ItemEntry *SphIntersect(ray *Ray, Object *AnObj);
Vec    *SphNormal();
Vec    *SphInverse();
Box    *SphBound();
int     SphInOut();
Vec    *SphCentre();
Vec    *SphRandom();

void InitSphere(Prim *APrim)
{
  APrim->Name      = SphName;
  APrim->Read	   = (void *)SphRead;
  APrim->Print     = SphPrint;
  APrim->Intersect = SphIntersect;
  APrim->Normal    = SphNormal;
  APrim->Inverse   = SphInverse;
  APrim->Bound     = SphBound;
  APrim->InOut     = SphInOut;
  APrim->Centre	   = SphCentre;
  APrim->Random    = SphRandom;
}

char *SphName()
{
  return "SPHERE";
}

Sph *SphRead(FILE *inputfile)
{
  Sph *Sphere;
  char var[20];
  Vec Sub1;
  Sphere = (Sph *)AllocHeap(sizeof(Sph));
  SetVector(0.0, 0.0, 0.0, &(Sphere->Centre));
  SetVector(0.0, 0.0, 1.0, &(Sphere->Pole));
  SetVector(1.0, 0.0, 0.0, &(Sphere->Equator));
  Sphere->Radius = 1.0;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "CENTRE") == ZERO || strcmp(var, "CENTER") == ZERO)
    {
      ReadVec(inputfile,&(Sphere->Centre));
    }
    else if (strcmp(var, "RADIUS") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(Sphere->Radius));
    }
    else if (strcmp(var, "POLE") == ZERO)
    {
      VecUnit(ReadVec(inputfile, &Sub1),&(Sphere->Pole));
    }
    else if (strcmp(var, "EQUATOR") == ZERO)
    {
      VecUnit(ReadVec(inputfile, &Sub1), &(Sphere->Equator));
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Sphere\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);

  VecProduct(&(Sphere->Pole), &(Sphere->Equator),&(Sphere->VP));

  if (DEBUG == ON)
  {
    SphPrint(Sphere);
  }
  Sphere->RadSq = Sphere->Radius * Sphere->Radius;
  Sphere->CentDot = VecDot(&(Sphere->Centre), &(Sphere->Centre));
  return(Sphere);
}

void SphPrint(Sph *Sphere)
{
  printf("Sphere with centre at (" FltFmt ", " FltFmt ", " FltFmt ") and radius of " FltFmt "\n",
   Sphere->Centre.x,
   Sphere->Centre.y,
   Sphere->Centre.z,
   Sphere->Radius);
}

ItemEntry *SphIntersect(ray *Ray, Object *AnObj)
{
  ItemEntry *Ans=CreateList(), *Ans1=NULL;
  IntRec *AnInt;
  Sph *Sphere;
  Flt t1, t2;
  Flt a, b, c, d;
  Vec V;

  Sphere = (Sph *)AnObj->kdata;
  a = 2.0 * VecDot(&(Ray->Direction), &(Ray->Direction));
  VecSub(&(Ray->Origin), &(Sphere->Centre), &V);
  b = 2.0 * VecDot(&(Ray->Direction), &V);
  c = Sphere->CentDot + VecDot(&(Ray->Origin), &(Ray->Origin)) - (2.0 * VecDot(&(Ray->Origin), &(Sphere->Centre)) + Sphere->RadSq);

  d = b * b - (2.0 * a * c);
  if (d > 0.0)
  {
    d = sqrt(d);
    t1 = (-b - d) / a;
    t2 = (-b + d) / a;

    AnInt = AllocInt();
    Ans1 = LinkInToList(AnInt, Ans, AnInt->Parent);

    VecAdds(t1, &(Ray->Direction), &(Ray->Origin), &(AnInt->ModelHit));
    AnInt->Dirn	    = ENTRY;
    AnInt->Dist	    = t1;
    AnInt->HitObj   = AnObj;
    AnInt->WorldHit = AnInt->ModelHit;


    AnInt = AllocInt();

    LinkInToList(AnInt, Ans1, AnInt->Parent);

    VecAdds(t2, &(Ray->Direction), &(Ray->Origin), &(AnInt->ModelHit));
    AnInt->Dirn	    = EXIT;
    AnInt->Dist	    = t2;
    AnInt->HitObj   = AnObj;
    AnInt->WorldHit = AnInt->ModelHit;

  }
  return(Ans);
}

Vec *SphNormal(IntRec *A, Sph *Sphere)
{
  VecSub(&(A->ModelHit), &(Sphere->Centre), &(A->Normal));
  VecUnit(&(A->Normal), &(A->Normal));
  return (&(A->Normal));
}

Vec *SphInverse(IntRec *B, Sph *Sphere, Flt *u, Flt *v, Vec *Ans)
{
  Flt Eps;
  Vec Sn, Sub1, Sub2, Sub3;
  Vec *A;

  A = &(B->ModelHit);
  VecSub(A, &(Sphere->Centre),&Sn);
  VecUnit(&Sn, &Sn);
  VecNegate(&Sn, &Sub1);
  Eps = acos(VecDot(&Sub1, &(Sphere->Pole)));
  *v = Eps * Sphere->Radius;
  if (sin(Eps) < EFFECTIVE_ZERO) *u = 0.0;
  else
  {
    VecUnit(VecProduct(&(Sphere->Pole), &Sn, &Sub2), &Sub2);
    if (VecDot(VecProduct(&Sub2, &(Sphere->Equator), &Sub3), &(Sphere->Pole)) > 0.0)
    {
      *u = acos(VecDot(&(Sphere->Equator), &Sub2));
    }
    else
    {
      *u = (2.0 * PI) - acos(VecDot(&(Sphere->Equator), &Sub2));
    }
    *u *= Sphere->Radius;
    
  }
  return(A);
}

Box *SphBound(Sph *Sphere, Box *Bound)
{
  Bound->A.x = Sphere->Centre.x - Sphere->Radius;
  Bound->A.y = Sphere->Centre.y - Sphere->Radius;
  Bound->A.z = Sphere->Centre.z - Sphere->Radius;
  Bound->B.x = Sphere->Centre.x + Sphere->Radius;
  Bound->B.y = Sphere->Centre.y + Sphere->Radius;
  Bound->B.z = Sphere->Centre.z + Sphere->Radius;
  return(Bound);
}

int SphInOut(Sph *Sphere, Vec *A)
{
  Vec Sub1;
  VecSub(A, &(Sphere->Centre), &Sub1);
  if (VecDot(&Sub1, &Sub1) < Sphere->RadSq)
  {
    return -1; /* inside sphere */
  }
  else
  {
    return 0; /* outside sphere */
  }
}

Vec *SphCentre(Sph *Sphere, Vec *Cen)
{
  Cen->x = Sphere->Centre.x;
  Cen->y = Sphere->Centre.y;
  Cen->z = Sphere->Centre.z;
  return(Cen);
}

Vec *SphRandom(Sph *Sphere, Vec *Random)
{
    Flt u, v, theta, phi;
    Flt x, y, z, x1, y1, z1;
    u =  rand();
    u /= RAND_MAX;
    v =  rand();
    v /= RAND_MAX;
    theta = acos(1.0-2.0*u);
    phi   = 2.0 * PI * v;
    x = 0.0;
    y = 1.0;
    z = 0.0;
    x1 = x * cos(theta) - y * sin(theta);
    y1 = x * sin(theta) + y * cos(theta);
    z1 = z;
    x  = x1 * cos(phi) - z1 * sin(phi);
    y  = y1;
    z  = x1 * sin(phi) + z1 * cos(phi);
    Random->x = Sphere->Centre.x + Sphere->Radius * x;
    Random->y = Sphere->Centre.y + Sphere->Radius * y;
    Random->z = Sphere->Centre.z + Sphere->Radius * z;
    return (Random);
}
