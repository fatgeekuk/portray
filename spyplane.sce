
camera
  look-at (0.0 0.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (2.0 0.2 -4)
  depth 0.032
  x-size 0.032
  y-size 0.024
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 1024
  ysize 768
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;

object
   plane
      position (0 -1 0)
      normal (0 1 0)
      x-axis (1 0 0)
      y-axis (0 0 1)
   ;
;

object 
   implicit
      epsilon 0.01
      crosspoint 0.6
      bound
        a (-2 -2 -2)
        b ( 2  2  2)
      ;
      polar
        shrink-y 4.0
        height 2.0
        centre (0 0 0)
        zradius 1.7
      ;
      polar
        shrink-y 4.0
        shrink-x 4.0
        translate (-1 -0.2 0)
        height 2.0
        centre (0 0 0)
        zradius 1.7
      ;
      polar
        shrink-y 4.0
        shrink-x 4.0
        translate (1 -0.2 0)
        height 2.0
        centre (0 0 0)
        zradius 1.7
      ;
      polar
        centre (0 0 0)
        zradius 1
        shrink-x 5
        shrink-y 4
        height 2.0
        translate (0 0.2 -0.5)
      ;
      sum
         rotate-z -30.0
         translate (1 -0.20 0)
     
         tube
           shrink-y 6
           translate (0 -0.06 0)
           from (0 0 -3)
           to   (0 0  3)
           zradius 0.1
           height -3
         ;
         tube
           shrink-y 6
           from (0 0 -3)
           to   (0 0  3)
           zradius 0.1
           height -3
         ;
         tube
           shrink-y 6
           translate (0 0.06 0)
           from (0 0 -3)
           to   (0 0  3)
           zradius 0.1
           height -3
         ;
      ;
      sum
         rotate-z 30.0
         translate (-1 -0.20 0)
     
         tube
           shrink-y 6
           translate (0 -0.06 0)
           from (0 0 -3)
           to   (0 0  3)
           zradius 0.1
           height -3
         ;
         tube
           shrink-y 6
           from (0 0 -3)
           to   (0 0  3)
           zradius 0.1
           height -3
         ;
         tube
           shrink-y 6
           translate (0 0.06 0)
           from (0 0 -3)
           to   (0 0  3)
           zradius 0.1
           height -3
         ;
      ;
   ;
   material
      spec-colour (1 1 1)
      diff-colour (0 0 1)
      diffuse-reflection 0.6
      specular-reflection 0.4
   ;
;
