
//       stack o' balls

include colours.inc

camera
    look-from (6 12 6)
    Look-at (0 0 1)
    drop-line (0 0 -1)
    depth 0.64
    y-size 0.24
    x-size 0.32
//    filter adaptive quick sub-thresh 0.02 ;
    filter standard
;

image
   xsize 640
   ysize 480
;

light colour white position (-10 100 -30) ;
light colour white position (-20 20 100) ;

object
   plane
      position (-1000 -1000 0)
      normal (0 0 1)
      x-axis (1 0 0)
      y-axis (0 1 0)
   ;
   material
      chequers
         size 1
         material-a
            colour (1 1 1)
            diffuse-reflection 0.8
            specular-reflection 0.2
         ;
         material-b
            colour (0 0 0)
            spec-colour (1 1 1)
            diffuse-reflection 0.8
            specular-reflection 0.2
         ;
      ;
   ;
;

object
   implicit
      crosspoint 0.6
      bound a (-4 -4 -4) b (4 4 4) ;
      polar
         center (0 1 .85)
         height 1.0
         zradius 1.5
      ;
      polar
         center (.85 -.5 .85)
         height 1.0
         zradius 1.5
      ;
      polar
         center (-.85 -.5 .85)
         height 1.0
         zradius 1.5
      ;
      polar
         center (0 0 2.234)
         height 1.0
         zradius 1.5
      ;
   ;
   material
      colour red
      specular-reflection 0.3
      diffuse-reflection 0.7
      spec-colour (1 1 1)
      specular-power 200
   ;
;

