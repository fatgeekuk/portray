
camera
  look-at (0.0 1.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (0.0 3.0 -6)
  depth 0.022
  x-size 0.032
  y-size 0.024
//  filter standard
  filter adaptive quick
  ;
;

image
  xsize 640
  ysize 480
  calc-diffuse
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 10 0)
;

object
   plane
      position (0 -1 0)
      normal (0 1 0)
      x-axis (1 0 0)
      y-axis (0 0 1)
   ;
   material
      chequers
         material-a
            colour (1 1 1)
         ;
         material-b
            colour (0.2 0.2 0.2)
         ;
      ;
   ;
;
object 
   sphere
   
   ;
   material
      diffuse-reflection 0.8
      specular-reflection 0.2
      spec-colour (1 1 1)
      tran-colour (1 1 1)
      image
         file cloud848.tga
      ;
   ;
;
