
camera
  look-at (0.0 1.1 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (2.0 2.0 -3)
  depth 0.022
  x-size 0.032
  y-size 0.024
//  filter standard
  filter adaptive quick
  ;
;

image
  xsize 640
  ysize 480
;

light
  colour (1.0 1.0 1.0)
  position (0.0 100 -20)
;

object
   plane
      position (0 0 0)
      normal (0 1 0)
   ;
;
object
   torus
   ;
   z-rotate 90
   translate (0 1.3 0)
   material
      colour (0 0 0)
      spec-colour (1 1 1)
      specular-reflection 0.7
      diffuse-reflection 0.0
      named wheel-stuff
   ;
;
object
   torus
   ;
   z-rotate 90
   translate (-0.5 1.3 0)
   material
      called wheel-stuff
   ;
;
object
   csg
      subtract
      operand1
         cylinder
            from (0 1.3 0)
            to   (-0.5 1.3 0)
            radius 1.3
         ;
         material
            called wheel-stuff
         ; 
      ;
      operand2
         cylinder
            from (0.1 1.3 0)
            to   (-0.6 1.3 0)
            radius 0.7
         ;
      ;
   ; 
;

