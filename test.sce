include colours.inc

camera
  look-at (0.0 2.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (2.0 5.2 -13)
  depth 0.022
  x-size 0.032
  y-size 0.024
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 1024
  ysize 768
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
  shape sphere center (-30.0 100 -50) radius 20 ;
  samples 1024 min-samples 100
;

object
   plane
      position (0 0 4)
      normal (0 0 -1)
      x-axis (1 0 0)
      y-axis (0 1 0)
   ;
;

object
	sphere
		center (0 1 0)
		radius 1.0
	;
;

