Light
   Position ( -200 100 -100 )
   Colour ( 1 1 1 )
;
camera
   look-at ( 0 0 0 )
   drop-line (0 -1 0)
   origin ( 0 3 -10 )
   depth 0.8
   x-size 0.64
   y-size 0.48
   filter standard
;

background ( 0 0 1 )

image
   xsize  640
   ysize  480
;
object
   plane
      position (0 0 0)
      normal (0 1 0)
      x-axis (1 0 0)
      y-axis (0 0 1)
   ;
   material
      turb
         power 9
         terms 9
         size 0.2
         material-a
            turb
               power 3
               terms 9
               size 0.2
               material-a
                  colour (0.2 0.7 0.3)
               ;
               material-b
                  colour (0.4 0.3 0.2)
               ;
            ;
         ;
         material-b
            chequers
               material-a
                  colour (1 1 1)
               ;
               material-b
                  colour (0 0 0)
               ;
            ;
         ;
      ;

   ;
;
