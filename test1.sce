include colours.inc

camera
  look-at (0.0 0.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (0.0 0.0 -10)
  depth 0.022
  y-size 0.032
  x-size 0.024
//  filter standard
  filter adaptive quick
  ;
;

image
  ysize 1024
  xsize 768
;

light
  colour (1.0 1.0 1.0)
  position (20.0 100 -100)
;

object cylinder from (-2 5 0) to (-2 -5 0) radius 0.1 ; ;
object cylinder from (2 5 0) to (2 -5 -0) radius 0.1 ; ;
object cylinder from (-2 -5 0) to (0 -4.5 0) radius 0.1 ; ;
object cylinder from (2 -5 0) to (0 -4.5 0) radius 0.1 ; ;
object sphere centre (2 -5 0) radius 0.1 ; ;
object sphere centre (-2 -5 0) radius 0.1 ; ;
object sphere centre (0 -4.5 0) radius 0.1 ; ;

