
camera
   look-at (0.0 1.0 2.0 )
   drop-line (0 -1 0)
   origin ( 1 3.8 -20 )
   depth 1.8
   x-size 1.28
   y-size 0.96
//   filter standard
   filter adaptive quick sub-thresh 0.0005 ;
;

background ( 0 0 1 )

image
   xsize 800
   ysize 600
;
object
   sphere
      center (-2 3 -5)
      radius 0.4
   ;
   material colour (1 1 1) light ;
;
light
   position (-2 3 -5)
   colour (1 1 1)
   fall-off 2
;

object
   box
      a (-6 0 -28)
      b (6  -0.1 2)
   ;
   material
      colour (0.1 0.1 0.1)
      diffuse-reflection 0.3
      specular-reflection 0.7
      spec-colour (1 1 1)
      gloss 0.3
      gloss-samples 30
   ;
;


define
   object
      name frameside
      object
         translate (0 0 -1)
         csg
            intersect
            operand1
               csg
                  subtract
                  operand1
                     csg
                        union
                        operand1
                           box a ( 3 1 -1) b (-3 7  1) ;
                           material
            	              named glass
            	              transparancy 0.9
            	              specular-reflection 0.1
            	              diffuse-reflection 0.0
            	              spec-colour (1 1 1)
                              tran-colour (1 1 1)
            	              specular-power 100
            	              refractive-index 1.6
                           ;
                        ;
                        operand2
                           cylinder
                              from (-3 1 0)
            	              to   ( 3 1 0)
            	              radius 1.0
                           ;
                           material called glass ;
                        ;
                     ;
                  ;
                  operand2
                     csg
	                union
	                operand1
                           box a ( 4 1 -0.9) b (-4 8  0.9) ;
                           material called glass ;
	                ;
	                operand2
                           cylinder
                              from (-4 1 0)
            	              to   ( 4 1 0)
            	              radius 0.9
                           ;
                           material called glass ;
	                ;
      	             ;
	          ;
	       ;
            ;
            operand2
               box a (-6 -1 2) b ( 6 9 0) ;
	       material called glass ;
            ;
         ;
      ;
   ;
   object
      name frame
      object frameside ; translate (0 0 -0.1) ;
      object frameside ; y-rotate 180 translate (0 0 0.1) ;
   ;
   object
      name pic3
      object
         csg
	    intersect
	    operand1 frame ; ;
	    operand2
	       box a (-2.38 0 -2) b ( 2.38 4.82 2) ;
	       material called glass ;
	    ;
	 ;
      ;
      object
         translate (0 2 0)
         box
            a (-1.98 0.2  2.1)
            b ( 1.98 2.42 2.0)
         ;
         material
            image
               file 13b.tga
	       flip
               X-pos -1.98
               Y-pos  2.42
               X-size 3.96
               y-size 2.22
            ;
         ;
      ;
   ;
   object
      name pic2
      object
         csg
	    intersect
	    operand1 frame ; ;
	    operand2
	       box a (-1.59 0 -2) b ( 1.59 5.4 2) ;
	       material called glass ;
	    ;
	 ;
      ;
      object
         translate (0 1.2 0)
         box
            a (-1.19 0.2  0.1)
            b ( 1.19 3.8  0.0)
         ;
         material
            image
               file 22b.tga
      	       flip
               X-pos -1.19
               Y-pos  3.8
               X-size 2.38
               y-size 3.60
            ;
         ;
      ;
   ;
   object
      name pic1
      object
         csg
	    intersect
	    operand1 frame ; ;
	    operand2
	       box a (-1.865 0 -2) b ( 1.865 5.86 2) ;
	       material called glass ;
	    ;
	 ;
      ;
      object
         box
            a (-1.465 0.2  0.1)
            b ( 1.465 4.16  0.0)
         ;
         translate (0 1.3 0)
         material
            image
               file 23b.tga
      	       flip
               X-pos -1.465
               Y-pos  4.16
               X-size 2.93
               y-size 3.96
            ;
	 ;
      ;
   ;
;
object
   pic1
   ;
;
object
   translate (4 0 0)
   pic2
   ;
;
object
   translate (-4 0 0)
   pic3
   ;
;
