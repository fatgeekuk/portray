include colours.inc

camera
  look-at (0.0 2.0 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (2.0 5.2 -13)
  depth 0.022
  x-size 0.024
  y-size 0.032
//  filter standard
  filter adaptive 
  ;
;

background (0.7 0.7 1)
back-horizon (0 0 1.0)
back-up (0 1 0)

image
  xsize 768
  ysize 1024
;

light
  colour (1.0 1.0 1.0)
  position (-30.0 100 -50)
;

object
   plane
      position (0 -1 0)
      normal (0 1 0)
      x-axis (1 0 0)
      y-axis (0 0 1)
   ;
;

object 
   implicit
      epsilon 0.01
      crosspoint 0.3
      bound
        a (-2 -2 -2)
        b ( 2  6  2)
      ;
      tube
         from (0 0 0)
         to   (0 4 0)
         zradius 1.0
         height 1.0
      ;
      tube
         from (0 4 0)
         to (2 7 0)
         zradius 0.7
         height 1.0
      ;
      tube
         from (0 4 0)
         to (-2.5 6 0)
         zradius 0.7
         height 1.0
      ;
      noise
         amp 0.05
         terms 1
         freq 10
         shrink-y 0.2
      ;
   ;
   material
      diff-colour burlywood
   ;
;
