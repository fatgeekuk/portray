struct TGAHdr
{
  unsigned char IDLen;
  unsigned char ColMapInfo;
  char ImageType;
  char CMOriginLo;
  char CMOriginHi;
  char CMLengthLo;
  char CMLengthHi;
  char CMEntrySize;
  char XOriginLo;
  char XOriginHi;
  char YOriginLo;
  char YOriginHi;
  unsigned char WidthLo;
  unsigned char WidthHi;
  unsigned char HeightLo;
  unsigned char HeightHi;
  char Depth;
  char TGAInfo;
} TGAHdr;
