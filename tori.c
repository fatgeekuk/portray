/*
  *************************************************************************
 **                                                                       **
 **                           Torus Section                               **
 **                                                                       **
  *************************************************************************
*/

#include <math.h>

char   *TorName();
Tor    *TorRead();
void    TorPrint();
ItemEntry *TorIntersect(ray *Ray, Object *AnObj);
Vec    *TorNormal();
Vec    *TorInverse();
Box    *TorBound();
int     TorInOut();
Vec    *TorCentre();

void InitTorus(Prim *APrim)
{
  APrim->Name      = TorName;
  APrim->Read      = (void *)TorRead;
  APrim->Print     = TorPrint;
  APrim->Intersect = TorIntersect;
  APrim->Normal    = TorNormal;
  APrim->Inverse   = TorInverse;
  APrim->Bound     = TorBound;
  APrim->InOut     = TorInOut;
  APrim->Centre    = TorCentre;
  APrim->Random    = NULL;
}
char *TorName()
{
  return "TORUS";
}

Tor *TorRead(FILE *inputfile)
{
  Tor *Torus;
  char var[20];
  Vec Sub1, Centre, Axis;
  Torus = (Tor *)AllocHeap(sizeof(Tor));
  SetVector(1.0, 0.0, 0.0, &(Torus->Equator));
  SetVector(0.0, 0.0, 0.0, &Centre);
  SetVector(0.0, 1.0, 0.0, &Axis);
  Torus->MajRad = 1.0;
  Torus->MinRada = 0.3;
  Torus->MinRadb = 0.3;
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "CENTRE") == ZERO || strcmp(var, "CENTER") == ZERO)
    {
      ReadVec(inputfile,&Centre);
    }
    else if (strcmp(var, "RADIUS") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(Torus->MajRad));
    }
    else if (strcmp(var, "NORM-RADIUS") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(Torus->MinRada));
    }
    else if (strcmp(var, "PLANE-RADIUS") == ZERO)
    {
      fscanf(inputfile, " " FltFmt " ", &(Torus->MinRadb));
    }
    else if (strcmp(var, "EQUATOR") == ZERO)
    {
      VecUnit(ReadVec(inputfile, &Sub1), &(Torus->Equator));
    }
    else if (strcmp(var, "AXIS") == ZERO)
    {
      VecUnit(ReadVec(inputfile, &Sub1), &(Axis));
    }

    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Torus\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);

  Torus->rho = Torus->MinRadb * Torus->MinRadb / (Torus->MinRada*Torus->MinRada);
  Torus->a0 = 4.0 * Torus->MajRad * Torus->MajRad;
  Torus->b0 = Torus->MajRad * Torus->MajRad - Torus->MinRadb*Torus->MinRadb;

  if (DEBUG == ON)
  {
    TorPrint(Torus);
  }
  return(Torus);
}

void TorPrint(Tor *Torus)
{
  printf("Torus with radius of " FltFmt "\n",
   Torus->MajRad);
}

ItemEntry *TorIntersect(ray *Ray, Object *AnObj)
{
  ItemEntry *Ans=CreateList(), *Ans1=NULL;
  IntRec *AnInt;
  Tor *Torus;
  int nhits, i, swaps;
  Vec Cosines, RayOrigin;
  Flt rayspeed, f, l, t, g, q, m, u, C[5], r[5], t1;
  enum IntDirn Hit;
  rayspeed = VecLen(&(Ray->Direction));
  Cosines.x = Ray->Direction.x / rayspeed;
  Cosines.y = Ray->Direction.y / rayspeed;
  Cosines.z = Ray->Direction.z / rayspeed;
  RayOrigin.x = Ray->Origin.x ;
  RayOrigin.y = Ray->Origin.y ;
  RayOrigin.z = Ray->Origin.z ;

  Torus = (Tor *)AnObj->kdata;
  f = 1.0 - Cosines.y * Cosines.y;
  l = 2.0 * (RayOrigin.x * Cosines.x + RayOrigin.z * Cosines.z);
  t = RayOrigin.x * RayOrigin.x + RayOrigin.z * RayOrigin.z;
  g = f + Torus->rho * Cosines.y * Cosines.y;
  q = Torus->a0 / (g * g);
  m = (l + 2.0 * Torus->rho * Cosines.y * RayOrigin.y) / g;
  u = (t +       Torus->rho * RayOrigin.y * RayOrigin.y + Torus->b0) / g;
  C[4] = 1.0;
  C[3] = 2.0 * m;
  C[2] = m*m + 2.0*u - q*f;
  C[1] = 2.0*m*u - q*l;
  C[0] = u*u - q*t;
  nhits = SolveQuartic(C, r);
  if (nhits > 1)
  {
    if (r[0] > r[1]){ t1 = r[0]; r[0] = r[1]; r[1] = t1;}
    if (nhits > 3 && r[2] > r[3])
    {
       t1 = r[2]; r[2] = r[3]; r[3] = t1;
       if (r[0] > r[2]) 
       {
         t1 = r[0]; r[0] = r[2]; r[2] = t1;
         t1 = r[1]; r[1] = r[3]; r[3] = t1;
       }
    }
  }
  Hit = ENTRY;
  Ans1 = Ans;

  for (i=0; i<nhits; i++)
  {
    AnInt = AllocInt();
    VecAdds(r[i], &(Cosines), &RayOrigin, &(AnInt->ModelHit));
    AnInt->Dirn     = Hit;
    AnInt->Dist     = r[i]/rayspeed;
    AnInt->HitObj   = AnObj;
    VecAdds(r[i]/rayspeed, &(Ray->Direction), &(Ray->Origin), &(AnInt->WorldHit));
    if (Hit == ENTRY)
    {
      Hit = EXIT;
    }
    else
    {
      Hit = ENTRY;
    }
    Ans1 = LinkInToList(AnInt, Ans1, AnInt->Parent);
  }

  return(Ans);
}

#define EQN_EPS 1e-12

#define IsZero(x) ((x) > -EQN_EPS && (x) < EQN_EPS)

int SolveQuadric(Flt c[3], Flt s[2])
{
  Flt p, q, D;
  p = c[1] / (2 * c[2]);
  q = c[0] / c[2];
  D = p * p - q;
  if (IsZero(D))
  {
    s[0] = -p;
    return(1);
  }
  else if (D < 0)
  {
    return 0;
  }
  else if (D > 0)
  {
    Flt sqrt_D = sqrt(D);
    s[0] =   sqrt_D - p;
    s[1] = - sqrt_D - p;
    return 2;
  }
  return(0);
}

int SolveCubic(Flt c[4], Flt s[3])
{
  int i, num;
  Flt sub;
  Flt A, B, C;
  Flt sq_A, p, q;
  Flt cb_p, D;

  A = c[2] /* / c[3] */;
  B = c[1] /* / c[3] */;
  C = c[0] /* / c[3] */;

  sq_A = A * A;
  p = 1.0/3 * (-1.0/3 * sq_A + B);
  q = 1.0/2 * (2.0/27 * A * sq_A - 1.0 / 3 * A * B + C);

  cb_p = p*p*p;
  D = q * q + cb_p;

  if (IsZero(D))
  {
    if (IsZero(q))
    {
      s[0] = 0;
      num = 1;
    }
    else
    {
      Flt u = cbrt(-q);
      s[0] = 2 * u;
      s[1] = -u;
      num  = 2;
    }
  }
  else if (D < 0)
  {
    Flt phi = 1.0 / 3 * acos(-q / sqrt(-cb_p));
    Flt t = 2 * sqrt(-p);
    s[0] =  t * cos(phi);
    s[1] = -t * cos(phi+M_PI/3);
    s[2] = -t * cos(phi-M_PI/3);
    num = 3;
  }
  else
  {
    Flt sqrt_D = sqrt(D);
    Flt u = cbrt(sqrt_D - q);
    Flt v = -cbrt(sqrt_D + q);
    s[0] = u + v;
    num = 1;
  }
  sub = 1.0/3 * A;
  for (i = 0; i < num; i++)
    s[i] -= sub;

  return num;
}

int SolveQuartic(Flt c[5], Flt s[4])
{
  Flt coeffs[4];
  Flt z, u, v, sub;
  Flt A, B, C, D;
  Flt sq_A, p, q, r;
  int i, num;
  A = c[3]  /* / c[4] */;
  B = c[2]  /* / c[4] */;
  C = c[1]  /* / c[4] */;
  D = c[0]  /* / c[4] */;
 
  sq_A = A * A;
  p = -3.0 / 8 * sq_A + B;
  q = 1.0 / 8 * sq_A * A-1.0/2 * A * B + C;
  r = -3.0/256*sq_A * sq_A + 1.0/16*sq_A*B-1.0/4*A*C+D;
  if (IsZero(r))
  {
    coeffs[0] = q;
    coeffs[1] = p;
    coeffs[2] = 0;
    coeffs[3] = 1;
    num = SolveCubic(coeffs, s);
    s[num++] = 0;
  }
  else
  {
    coeffs[0] = 1.0/2 * r * p - 1.0/8 * q * q;
    coeffs[1] = -r;
    coeffs[2] = -1.0/2 * p;
    coeffs[3] = 1;

    SolveCubic(coeffs, s);

    z = s[0];

    u = z * z - r;
    v = 2 * z - p;

    if (IsZero(u))
       u = 0;
    else if (u > 0)
       u = sqrt(u);
    else
       return(0);

    if (IsZero(v))
       v = 0;
    else if (v > 0)
       v = sqrt(v);
    else
       return(0);

    coeffs[0] = z - u;
    coeffs[1] = q<0?-v:v;
    coeffs[2] = 1;
    
    num = SolveQuadric(coeffs, s);

    coeffs[0] = z + u;
    coeffs[1] = q<0?v:-v;
    coeffs[2] = 1;

    num += SolveQuadric(coeffs, s+num);
  }
  sub = 1.0 / 4 * A;
  for (i = 0; i < num; i++)
  {
    s[i] -= sub;
  };
  return num;
}


Vec *TorNormal(IntRec *A, Tor *Torus)
{
  Flt d, f;
  d=sqrt(A->ModelHit.x * A->ModelHit.x + A->ModelHit.z * A->ModelHit.z);
/*  f = 2.0*(d - Torus->MajRad)/(d * Torus->MinRadb * Torus->MinRadb);
  A->Normal.x = A->ModelHit.x * f;
  A->Normal.y = 2.0*A->ModelHit.y/(Torus->MinRada * Torus->MinRada);
  A->Normal.z = A->ModelHit.z * f;
  VecUnit(&(A->Normal), &(A->Normal));
*/
  A->Normal.x = A->ModelHit.x - (A->ModelHit.x * (Torus->MajRad/d));
  A->Normal.y = A->ModelHit.y;
  A->Normal.z = A->ModelHit.z - (A->ModelHit.z * (Torus->MajRad/d));
  VecUnit(&(A->Normal), &(A->Normal));
  return (&(A->Normal));
}

Vec *TorInverse(IntRec *B, Tor *Torus, Flt *u, Flt *v, Vec *Ans)
{
  Flt Eps, aps;
  Vec Sn, Sub1, Sub2;
  Vec *A;

  *u = 0.0;
  *v = 0.0;
  Sn = B->ModelHit;
  Sn.y = 0;
  Eps = VecLen(&Sn);
  VecScalar(1.0 / Eps, &Sn, &Sn);
  if (Sn.z >= 1.0) Sn.z = 1.0;
  if (Sn.z <= -1.0) Sn.z = -1.0;
  if (Sn.x > 0.0) *u = acos(Sn.z); 
             else *u = (2.0*PI)-acos(Sn.z);


  aps = B->ModelHit.y/Torus->MinRadb;
  if (aps >= 1.0) aps = 1.0;
  if (aps <= -1.0) aps = -1.0;
  aps =  asin(aps);
  if (aps > 0.0)
     if (Eps < Torus->MajRad) *v = aps;
                         else *v = PI - aps;
     else if (Eps > Torus->MajRad) *v = 2.0*PI - aps;
                              else *v = PI + aps;
 
  *u *= Torus->MajRad;
  *v *= Torus->MinRadb; 
  return(A);
}

Box *TorBound(Tor *Torus, Box *Bound)
{
  Bound->A.x = -Torus->MajRad - Torus->MinRadb;
  Bound->A.y = -Torus->MinRada;
  Bound->A.z = -Torus->MajRad - Torus->MinRadb;
  Bound->B.x = Torus->MajRad + Torus->MinRadb;
  Bound->B.y = Torus->MinRada;
  Bound->B.z = Torus->MajRad + Torus->MinRadb;
/*  Bound->A.x = -100.0;
  Bound->A.y = -100.0;
  Bound->A.z = -100.0;
  Bound->B.x = 100.0;
  Bound->B.y = 100.0;
  Bound->B.z = 100.0; */
  return(Bound);
}

int TorInOut(Tor *Torus, Vec *A)
{
  Vec Sub1;
  return 0;
}

Vec *TorCentre(Tor *Torus, Vec *Cen)
{
  Cen->x = 0.0;
  Cen->y = 0.0;
  Cen->z = 0.0;
  return(Cen);
}

