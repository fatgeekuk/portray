
camera
  look-at (0.0 1.1 0.0)
  drop-line (0.0 -1.0 0.0)
  look-from (2.0 2.0 -3)
  depth 0.022
  x-size 0.032
  y-size 0.024
//  filter standard
  filter adaptive quick
  ;
;

image
  xsize 640
  ysize 480
;

light
  colour (1.0 1.0 1.0)
  position (0.0 100 -20)
;

object
   plane
      position (0 0 0)
      normal (0 1 0)
   ;
;
object
   torus
   ;
   translate (-1 0 0)
;

object
   torus
   ;
   translate (1 0 0)
   shrink 3
;
object
   cylinder
      from (1 0 0)
      to   (0 0 0)
      radius 0.1
   ;
;
object 
   sphere
      centre (0.666 0 0)
      radius 0.13
   ;
;
