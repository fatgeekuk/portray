define object name tree 
object
compound
object cylinder from (0 0 0) to (0 2 0) radius1 1 radius2 0.8 ; material called branch-stuff ; ;
object translate (0 2 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 1.6 0) radius1 0.8 radius2 0.64 ; material called branch-stuff ; ;
object translate (0 1.6 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 1.28 0) radius1 0.64 radius2 0.512 ; material called branch-stuff ; ;
object translate (0 1.28 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 1.024 0) radius1 0.512 radius2 0.4096 ; material called branch-stuff ; ;
object translate (0 1.024 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 1.024 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 1.28 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 1.024 0) radius1 0.512 radius2 0.4096 ; material called branch-stuff ; ;
object translate (0 1.024 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 1.024 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 1.6 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 1.28 0) radius1 0.64 radius2 0.512 ; material called branch-stuff ; ;
object translate (0 1.28 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 1.024 0) radius1 0.512 radius2 0.4096 ; material called branch-stuff ; ;
object translate (0 1.024 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 1.024 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 1.28 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 1.024 0) radius1 0.512 radius2 0.4096 ; material called branch-stuff ; ;
object translate (0 1.024 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 1.024 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 2 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 1.6 0) radius1 0.8 radius2 0.64 ; material called branch-stuff ; ;
object translate (0 1.6 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 1.28 0) radius1 0.64 radius2 0.512 ; material called branch-stuff ; ;
object translate (0 1.28 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 1.024 0) radius1 0.512 radius2 0.4096 ; material called branch-stuff ; ;
object translate (0 1.024 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 1.024 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 1.28 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 1.024 0) radius1 0.512 radius2 0.4096 ; material called branch-stuff ; ;
object translate (0 1.024 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 1.024 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 1.6 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 1.28 0) radius1 0.64 radius2 0.512 ; material called branch-stuff ; ;
object translate (0 1.28 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 1.024 0) radius1 0.512 radius2 0.4096 ; material called branch-stuff ; ;
object translate (0 1.024 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 1.024 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 1.28 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 1.024 0) radius1 0.512 radius2 0.4096 ; material called branch-stuff ; ;
object translate (0 1.024 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
object translate (0 1.024 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.8192 0) radius1 0.4096 radius2 0.32768 ; material called branch-stuff ; ;
object translate (0 0.8192 0) z-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
object translate (0 0.8192 0) z-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.65536 0) radius1 0.32768 radius2 0.262144 ; material called branch-stuff ; ;
object translate (0 0.65536 0) x-rotate 30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
object translate (0 0.65536 0) x-rotate -30.0 compound
object cylinder from (0 0 0) to (0 0.524288 0) radius1 0.262144 radius2 0.2097152 ; material called branch-stuff ; ;
object translate (0 0.524288 0) z-rotate 30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
object translate (0 0.524288 0) z-rotate -30.0 sphere centre (0 0 0) radius 0.33554432 ; material called leaf-stuff ; ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
 ;
;
;
