/*
  *************************************************************************
 **                                                                       **
 **                        Triangle Section                               **
 **                                                                       **
  *************************************************************************
*/

char   *TriName();
Tri    *TriRead();
void    TriPrint();
ItemEntry *TriIntersect(ray *Ray, Object *AnObj);
Vec    *TriNormal();
Vec    *TriInverse();
Box    *TriBound();
int     TriInOut();
Vec    *TriCentre();


void InitTriangle(Prim *APrim)
{
  APrim->Name      = TriName;
  APrim->Read	   = (void *)TriRead;
  APrim->Print     = TriPrint;
  APrim->Intersect = TriIntersect;
  APrim->Normal    = TriNormal;
  APrim->Inverse   = TriInverse;
  APrim->Bound     = TriBound;
  APrim->InOut     = TriInOut;
  APrim->Centre    = TriCentre;
}

char *TriName()
{
  return "TRIANGLE";
}

Tri *TriRead(FILE *inputfile)
{
  Tri *Triangle;
  char var[20];
  Flt dist;
  Vec D, uAB, uBC, uCA, Sub1;
  Triangle = (Tri *)AllocHeap(sizeof(Tri));
  SetVector(0.0, 0.0, 0.0, &(Triangle->nA));
  SetVector(0.0, 0.0, 0.0, &(Triangle->nB));
  SetVector(0.0, 0.0, 0.0, &(Triangle->nC));
  do
  {
    fscanf(inputfile, " %s ", var);
    strupr(var);
    if (strcmp(var, "A") == ZERO)
    {
      ReadVec(inputfile, &(Triangle->A));
    }
    else if (strcmp(var, "NORMAL-A") == ZERO)
    {
      ReadVec(inputfile, &(Triangle->nA));
      VecUnit(&(Triangle->nA), &(Triangle->nA));
    }
    else if (strcmp(var, "NORMAL-B") == ZERO)
    {
      ReadVec(inputfile, &(Triangle->nB));
      VecUnit(&(Triangle->nB), &(Triangle->nB));
    }
    else if (strcmp(var, "NORMAL-C") == ZERO)
    {
      ReadVec(inputfile, &(Triangle->nC));
      VecUnit(&(Triangle->nC), &(Triangle->nC));
    }
    else if (strcmp(var, "B") == ZERO)
    {
      ReadVec(inputfile, &(Triangle->B));
    }
    else if (strcmp(var, "C") == ZERO)
    {
      ReadVec(inputfile, &(Triangle->C));
    }
    else if (strcmp(var, "Centre") == ZERO)
    {
      ReadVec(inputfile, &(Triangle->Centre));
    }
    else if (strcmp(var, ";") != ZERO)
    {
      printf("ERROR -- Invalid Variable (%s) encountered while reading Plane\n", var);
      exit(1);
    }
  } while (strcmp(var, ";") != ZERO);
  VecSub(&(Triangle->B), &(Triangle->A), &Sub1);
  VecUnit(&Sub1, &uAB);
  VecSub(&(Triangle->C), &(Triangle->B), &Sub1);
  VecUnit(&Sub1, &uBC);
  VecSub(&(Triangle->A), &(Triangle->C), &Sub1)
  VecUnit(&Sub1, &uCA);

/* Calculate Triangle->uAB */
/* find Distance Along AB that is at right-angles to C */
  VecSub(&(Triangle->C), &(Triangle->A), &Sub1);
  dist = VecDot(&uAB, &Sub1);

  printf ("ADist = " FltFmt "\n", dist);
/* Generate That Point */
  VecAdds(dist,&uAB, &(Triangle->A), &D);
/* Generate the unit vector Along the Perpendicular DC */
  VecSub(&(Triangle->C), &D, &Sub1);
  VecUnit(&Sub1, &(Triangle->uAB));
  Triangle->lAB = VecLen(&Sub1);


/* Calculate Triangle->uBC */
  VecSub(&(Triangle->A), &(Triangle->B), &Sub1);
  dist = VecDot(&uBC, &Sub1);
  printf ("BDist = " FltFmt "\n", dist);
  VecAdds(dist,&uBC, &(Triangle->B), &D);
  VecSub(&(Triangle->A), &D, &Sub1);
  VecUnit(&Sub1, &(Triangle->uBC));
  Triangle->lBC = VecLen(&Sub1);

/* Calculate Triangle->uCA */
  VecSub(&(Triangle->B), &(Triangle->C), &Sub1);
  dist = VecDot(&uCA, &Sub1);
  printf ("CDist = " FltFmt "\n", dist);
  VecAdds(dist,&uCA, &(Triangle->C), &D);
  VecSub(&(Triangle->B), &D, &Sub1);
  VecUnit(&Sub1, &(Triangle->uCA));
  Triangle->lCA = VecLen(&Sub1);

  Triangle->vAB = uAB;

/* Create surface Normal, and store it for any normals not specified */
  VecUnit(VecProduct(&uAB, &uCA, &Sub1), &(Triangle->Normal));
  if (VecLen(&(Triangle->nA)) < EFFECTIVE_ZERO)
  {
    Triangle->nA = Triangle->Normal;
  }
  if (VecLen(&(Triangle->nB)) < EFFECTIVE_ZERO)
  {
    Triangle->nB = Triangle->Normal;
  }
  if (VecLen(&(Triangle->nC)) < EFFECTIVE_ZERO)
  {
    Triangle->nC = Triangle->Normal;
  }


  if (DEBUG == ON)
  {
    TriPrint(Triangle);
  }
  return(Triangle);
}

void TriPrint(Tri *Triangle)
{
  printf("Triangle with position A (" FltFmt ", " FltFmt ", " FltFmt ") \n                       B (" FltFmt ", " FltFmt ", " FltFmt ") \n                       C (" FltFmt ", " FltFmt ", " FltFmt ") and \n               normal of (" FltFmt ", " FltFmt ", " FltFmt ")\n",
   Triangle->A.x,
   Triangle->A.y,
   Triangle->A.z,
   Triangle->B.x,
   Triangle->B.y,
   Triangle->B.z,
   Triangle->C.x,
   Triangle->C.y,
   Triangle->C.z,
   Triangle->Normal.x,
   Triangle->Normal.y,
   Triangle->Normal.z);
   PrintVec(&(Triangle->nA));
   PrintVec(&(Triangle->nB));
   PrintVec(&(Triangle->nC));
}

ItemEntry *TriIntersect(ray *Ray, Object *AnObj)
{
  ItemEntry *Ans = CreateList();
  IntRec *AnInt=NULL;
  enum IntDirn Dirn;
  Tri *Triangle;
  Flt t1, answer, u, v, w;
  Vec A, Normal, Sub1;

  Triangle = (Tri *)AnObj->kdata;
  Normal = Triangle->Normal;
  t1 = -VecDot(&Normal, &(Ray->Direction));
  if (t1 < EFFECTIVE_ZERO)
  {
    VecNegate(&Normal, &Normal);
    VecDot(&Normal, &(Ray->Direction));
    t1 = -t1;
    Dirn = EXIT;
  }
  else
    Dirn = ENTRY;

  VecSub(&(Ray->Origin), &(Triangle->A), &Sub1);
  answer = VecDot(&Normal, &Sub1) / t1;
  if (answer >= EFFECTIVE_ZERO)
  {
    VecAdds(answer, &(Ray->Direction), &(Ray->Origin), &A);
    VecSub(&A, &(Triangle->A),&Sub1);
    u = VecDot(&(Triangle->uAB), &Sub1);
    if (u >= 0.0)
    {
      VecSub(&A, &(Triangle->B),&Sub1);
      v = VecDot(&(Triangle->uBC), &Sub1);
      if (v >= 0.0)
      {
        VecSub(&A, &(Triangle->C),&Sub1);
        w = VecDot(&(Triangle->uCA), &Sub1);
        if (w >= 0.0)
        {
          AnInt = AllocInt();
          AnInt->ModelHit = A;
          AnInt->Dirn     = Dirn;
          AnInt->Dist     = answer;
          AnInt->HitObj   = AnObj;
          AnInt->WorldHit = AnInt->ModelHit;
          LinkInToList(AnInt, Ans, AnInt->Parent);
        }
      }
    }
  }
  return (Ans);
}

Vec *TriNormal(IntRec *A, Tri *Triangle)
{
  Flt u, v, w;
  Vec Sub1;
  VecSub(&(A->ModelHit), &(Triangle->A),&Sub1);
  u = VecDot(&(Triangle->uAB), &Sub1) / Triangle->lAB;
  VecSub(&(A->ModelHit), &(Triangle->B),&Sub1);
  v = VecDot(&(Triangle->uBC), &Sub1) / Triangle->lBC;
  VecSub(&(A->ModelHit), &(Triangle->C),&Sub1);
  w = VecDot(&(Triangle->uCA), &Sub1) / Triangle->lCA;
  SetVector(0.0, 0.0, 0.0, &(A->Normal));
  VecAdds(v, &(Triangle->nA), &(A->Normal), &(A->Normal));
  VecAdds(w, &(Triangle->nB), &(A->Normal), &(A->Normal));
  VecAdds(u, &(Triangle->nC), &(A->Normal), &(A->Normal));
  VecUnit(&(A->Normal), &(A->Normal));
  return (&(A->Normal));
}

Vec *TriInverse(IntRec *B, Tri *Triangle, Flt *u, Flt *v, Vec *Ans)
{
  Vec Sub1;
  Vec *A;

  A = &(B->ModelHit);

  VecSub(A, &(Triangle->A), &Sub1);
  *u = VecDot(&(Triangle->vAB), &Sub1);
  *v = VecDot(&(Triangle->uAB), &Sub1);
  return(A);
}

Box *TriBound(Tri *Triangle, Box *Bound)
{
  Bound->A.x = Triangle->A.x;
  Bound->A.y = Triangle->A.y;
  Bound->A.z = Triangle->A.z;
  Bound->B.x = Triangle->A.x;
  Bound->B.y = Triangle->A.y;
  Bound->B.z = Triangle->A.z;
  if (Bound->A.x > Triangle->B.x) Bound->A.x = Triangle->B.x;
  if (Bound->A.x > Triangle->C.x) Bound->A.x = Triangle->C.x;
  if (Bound->A.y > Triangle->B.y) Bound->A.y = Triangle->B.y;
  if (Bound->A.y > Triangle->C.y) Bound->A.y = Triangle->C.y;
  if (Bound->A.z > Triangle->B.z) Bound->A.z = Triangle->B.z;
  if (Bound->A.z > Triangle->C.z) Bound->A.z = Triangle->C.z;
  if (Bound->B.x < Triangle->B.x) Bound->B.x = Triangle->B.x;
  if (Bound->B.x < Triangle->C.x) Bound->B.x = Triangle->C.x;
  if (Bound->B.y < Triangle->B.y) Bound->B.y = Triangle->B.y;
  if (Bound->B.y < Triangle->C.y) Bound->B.y = Triangle->C.y;
  if (Bound->B.z < Triangle->B.z) Bound->B.z = Triangle->B.z;
  if (Bound->B.z < Triangle->C.z) Bound->B.z = Triangle->C.z;
  return(Bound);
}

int TriInOut(Tri *Triangle, Vec *A)
{
  Vec Sub1;
  VecSub(A, &(Triangle->A), &Sub1);
  if (VecDot(&(Triangle->Normal), &Sub1) > 0.0)
  {
    return 0;
  }
  else
  {
    return -1;
  }
}

Vec *TriCentre(Tri *Triangle, Vec *Cen)
{
  Cen->x = Triangle->Centre.x;
  Cen->y = Triangle->Centre.y;
  Cen->z = Triangle->Centre.z;
  return(Cen);
}
