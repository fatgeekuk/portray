
//       stack o' balls

include colours.inc

camera
    look-from (-3 5 -10)
    Look-at (0 1 1)
    drop-line (0 -1 0)
    depth 0.64
    y-size 0.24
    x-size 0.32
    filter adaptive quick sub-thresh 0.02 ;
//    filter standard
;

image
   xsize 1024
   ysize 768
;

light
	colour white
	shape sphere center (-10 100 -230) radius 15 ;
	samples 1000
	min-samples 20
	position (-10 100 -230)
;

object
   plane
      position (-1000 1 -1000)
      normal (0 1 0)
      x-axis (1 0 0)
      y-axis (0 0 1)
   ;
   material
         chequers
         size 1
         material-a
            colour (1 1 1)
            diffuse-reflection 0.8
            specular-reflection 0.2
         ;
         material-b
            colour (0 0 0)
            spec-colour (1 1 1)
            diffuse-reflection 0.8
            specular-reflection 0.2
         ;
      ;
   ;
;

object
	cylinder
		from (0 1 0)
		to (0 3 0)
		radius1 1
		radius2 0
	;
;

object
	cylinder
		from (-2 1 0)
		to (-2 2 0)
		radius1 0.5
		radius2 0
	;
	material
        	chequers
        		size 0.31415
        		material-a
        			colour (1 1 0)
        			diffuse-reflection 0.8
        			specular-reflection 0.2
        		;
        		material-b
        			colour (0 0 1)
        			spec-colour (1 1 1)
        			diffuse-reflection 0.8
			;
		;
	;
;

object
	cylinder
		from (2 1 0)
		to (2 2 0)
		radius1 0.5
		radius2 0
	;
	material
        	chequers
        		size 0.31415
        		material-a
        			colour (1 1 0)
        			diffuse-reflection 0.8
        			specular-reflection 0.2
        		;
        		material-b
        			colour (0 0 1)
        			spec-colour (1 1 1)
        			diffuse-reflection 0.8
			;
		;
	;
;

object
	cylinder
		from (0 1 -2)
		to (0 2 -2)
		radius1 0.5
		radius2 0
	;
	material
        	chequers
        		size 0.31415
        		material-a
        			colour (1 1 0)
        			diffuse-reflection 0.8
        			specular-reflection 0.2
        		;
        		material-b
        			colour (0 0 1)
        			spec-colour (1 1 1)
        			diffuse-reflection 0.8
			;
		;
	;
;

object
	cylinder
		from (0 1 2)
		to (0 2 2)
		radius1 0.5
		radius2 0
	;
	material
        	chequers
        		size 0.31415
        		material-a
        			colour (1 1 0)
        			diffuse-reflection 0.8
        			specular-reflection 0.2
        		;
        		material-b
        			colour (0 0 1)
        			spec-colour (1 1 1)
        			diffuse-reflection 0.8
			;
		;
	;
;

