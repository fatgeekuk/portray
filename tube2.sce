
//       stack o' balls

include colours.inc

camera
    look-from (-3 0 -14)
    Look-at (0 0 1)
    drop-line (0 -1 0)
    depth 0.64
    y-size 0.24
    x-size 0.32
//    filter adaptive quick sub-thresh 0.02 ;
    filter standard
;

image
   xsize 1024
   ysize 768
;

light colour white position (-10 100 -30) ;
light colour white position (-20 20 -100) ;

object
   plane
      position (-1000 -2 -1000)
      normal (0 1 0)
      x-axis (1 0 0)
      y-axis (0 0 1)
   ;
   material
      chequers
         size 1
         material-a
            colour (1 1 1)
            diffuse-reflection 0.8
            specular-reflection 0.2
         ;
         material-b
            colour (0 0 0)
            spec-colour (1 1 1)
            diffuse-reflection 0.8
            specular-reflection 0.2
         ;
      ;
   ;
;

object
   implicit
      crosspoint 0.5
      bound a (-4 -4 -4) b (4 4 4) ;
      tube from (-2 -1 0.1) to (2 -1 0.1) zradius 0.4 height 1.0 ;
      tube from (-2 1 0.1) to (2 1 0.1) zradius 0.4 height 1.0 ;
      tube from (-1 -2 -0.1) to (-1 2 -0.1) zradius 0.4 height 1.0 ;
      tube from ( 1 -2 -0.1) to ( 1 2 -0.1) zradius 0.4 height 1.0 ;
   ;
   material
      colour red
      specular-reflection 0.3
      diffuse-reflection 0.7
      spec-colour (1 1 1)
      specular-power 200
   ;
;

