/* VECTORS.C */

#include <stdio.h>
#include <stdlib.h>
#include "vectors.h"
#include "rend_0.h"
#include <string.h>


long VAddCnt=0, VLenCnt=0, VDotCnt=0, VSubCnt=0, VCombCnt=0, VAddsCnt=0, VScalCnt=0, VUnitCnt=0, VProdCnt=0, VNegCnt=0, RaysPerPixel=0, CacheTot=0, CacheHit=0, CacheMiss=0, ItemCount=0;

/* Vec *SetVector(Flt x, Flt y, Flt z, Vec *A)
{
  A->x = x;
  A->y = y;
  A->z = z;
  return(A);
}


Flt VecLen(Vec *A)
{
  Flt Answer;
  Answer = sqrt(A->x*A->x + A->y*A->y + A->z*A->z);
  VLenCnt++;
  return(Answer);
}
Flt VecDot(Vec *A, Vec *B)
{
  Flt Answer;
  Answer = (A->x * B->x + A->y * B->y + A->z * B->z);
  VDotCnt++;
  return(Answer);
}

Vec *VecAdd(Vec *A, Vec *B, Vec *C)
{
  C->x = A->x + B->x;
  C->y = A->y + B->y;
  C->z = A->z + B->z;
  VAddCnt++;
  return C;
}

Vec *VecSub(Vec *A, Vec *B, Vec *C)
{
  C->x = A->x - B->x;
  C->y = A->y - B->y;
  C->z = A->z - B->z;
  VSubCnt++;
  return C;
}

*/
Vec *VecComb(Flt a, Vec *A, Flt b, Vec *B, Vec *C)
{
  C->x = a * A->x + b * B->x;
  C->y = a * A->y + b * B->y;
  C->z = a * A->z + b * B->z;
  VCombCnt++;
  return C;
}

Vec *VecAdds(Flt a, Vec *A, Vec *B, Vec *C)
{
  C->x = a * A->x + B->x;
  C->y = a * A->y + B->y;
  C->z = a * A->z + B->z;
  VAddsCnt++;
  return C;
}

Vec *VecScalar(Flt a, Vec *A, Vec *C)
{
  C->x = a * A->x;
  C->y = a * A->y;
  C->z = a * A->z;
  VScalCnt++;
  return C;
}

Vec *VecUnit(Vec *A, Vec *C)
{
  Flt Length=VecLen(A);
  if (Length != 0.0)
  {
    C->x = A->x / Length;
    C->y = A->y / Length;
    C->z = A->z / Length;
  }
  else
  {
    C->x = 1.0;
    C->y = 0.0;
    C->z = 0.0;
    printf("Error - Division by Zero Error\n");
  }
  VUnitCnt++;
  return(C);
}

Vec *VecProduct(Vec *A, Vec *B, Vec *C)
{
  Vec Ans;
  Ans.x = (A->y * B->z) - (A->z * B->y);
  Ans.y = (A->z * B->x) - (A->x * B->z);
  Ans.z = (A->x * B->y) - (A->y * B->x);
  *C = Ans;
  VProdCnt++;
  return(C);
}

Vec *VecNegate(Vec *A, Vec *C)
{
  C->x = -A->x;
  C->y = -A->y;
  C->z = -A->z;
  VNegCnt++;
  return(C);
}

void PrintVec(Vec *A)
{
  printf("(" FltFmt " " FltFmt " " FltFmt ")", A->x, A->y, A->z);
}

Vec *ReadVec(FILE *inputfile, Vec *C)
{
  Flt x, y, z;
  if (fscanf(inputfile, " ( " FltFmt " " FltFmt " " FltFmt " )", &x, &y, &z) != 3)
  {
    printf("Error -- Unable to Read Vector\n");
    SetVector(0.0, 0.0, 0.0, C);
  }
  else
  {
    SetVector(x, y, z, C);
  }
  return C;
}

void WriteVec(FILE *OutputFile, Vec *A)
{
  fprintf(OutputFile, "(%g %g %g)", A->x, A->y, A->z);
}

ColDefn *SearchCols(char *Name)
{
  ColDefn *Ans;
  for (Ans = Colours; Ans != NULL; Ans = Ans->NextCol)
  {
    if (strcmp(Name, Ans->Name) == ZERO)
    {
      return(Ans);
    }
  }
  return(Ans);
}

colour *ReadCol(FILE *inputfile, colour *C)
{
  CFlt red, green, blue, Mult;
  int ch;
  char Buff[42];
  ColDefn *ACol;

  ch = getc(inputfile);
  while (ch == ' ' || ch == '\t' || ch == '\n') ch = getc(inputfile);

  if (ch == '(')
  {
    if (fscanf(inputfile, " " CFltFmt " " CFltFmt " " CFltFmt " )", &red, &green, &blue) != 3)
    {
      printf("Error -- Unable to Read Colour\n");
      SetColour(0.0, 0.0, 0.0, C);
    }
    else
    {
      C->red   = red;
      C->green = green;
      C->blue  = blue;
    }
  }
  else if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
  {
    Buff[0] = ch;
    fscanf(inputfile, "%40[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.]", &(Buff[1]));
    ACol = SearchCols(Buff);
    if (ACol != NULL)
    {
      *C = ACol->Value;
    }
    else
    {
      printf("Error -- Unable to find definition of Colour (%s)\n", Buff);
      exit(1);
    }
  }
  else
  {
    printf("Error -- Invalid Character at start of Colour Name\n");
    exit(1);
  }
  ch = getc(inputfile);
  if (ch == '*')
  {
    fscanf(inputfile, " " CFltFmt " ", &Mult);
    C->red   *= Mult;
    C->green *= Mult;
    C->blue  *= Mult;
  }
  else
  {
    if (ch != ungetc(ch, inputfile))
    {
      printf("Warning -- Unable to return char to inputstream\n");
    }
  }
  return C;
}

colour *ColourFilter(colour *A, colour *B, colour *C)
{
  C->red   = A->red   * B->red;
  C->green = A->green * B->green;
  C->blue  = A->blue  * B->blue;
  return C;
}

colour *ColourAddS(colour *A, CFlt b, colour *B, colour *C)
{
  C->red   = A->red   + b * B->red;
  C->green = A->green + b * B->green;
  C->blue  = A->blue  + b * B->blue;
  return C;
}

colour *ColourAdd(colour *A, colour *B, colour *C)
{
  C->red   = A->red   + B->red;
  C->green = A->green + B->green;
  C->blue  = A->blue  + B->blue;
  return C;
}

colour *ColourShade(CFlt a, colour *A, colour *C)
{
  C->red   = a * A->red;
  C->green = a * A->green;
  C->blue  = a * A->blue;
  return (C);
}

colour *SetColour(CFlt red, CFlt green, CFlt blue, colour *C)
{
  C->red   = red;
  C->green = green;
  C->blue  = blue;
  return(C);
}

colour *FadeColours(CFlt a, colour *A, colour *B, colour *C)
{
  CFlt b;
  b = 1.0 - a;
  C->red   = A->red   * b + B->red   * a;
  C->green = A->green * b + B->green * a;
  C->blue  = A->blue  * b + B->blue  * a;
  return(C);
}

void PrintCol(colour *A)
{
  printf("(" CFltFmt " " CFltFmt " " CFltFmt ")", A->red, A->green, A->blue);
}
