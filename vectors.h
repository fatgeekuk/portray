/* VECTORS.H */

#include <math.h>
#include <stdio.h>

typedef double Flt;
#define FltFmt "%lf"

typedef float CFlt;
#define CFltFmt "%f"

typedef struct pt
{
  Flt x, y, z;
} point;

typedef struct colour
{
  float red, green, blue;
} colour;

typedef point Vec;

typedef struct
{
  Vec Origin, Direction;
} ray;

#define SetVector(a,b,c,A) {(A)->x = a; (A)->y = b; (A)->z = c;}
#define VecLen(A) sqrt(((A)->x * (A)->x) + ((A)->y * (A)->y) + ((A)->z * (A)->z))
#define VecDot(A, B)  (((A)->x * (B)->x) + ((A)->y * (B)->y) + ((A)->z * (B)->z))
#define VecAdd(A, B, C)  {(C)->x = (A)->x + (B)->x;(C)->y = (A)->y + (B)->y;(C)->z = (A)->z + (B)->z;}
#define VecSub(A, B, C)  {(C)->x = (A)->x - (B)->x;(C)->y = (A)->y - (B)->y;(C)->z = (A)->z - (B)->z;}

/*
Vec *VecAdd(Vec *A, Vec *B, Vec *C);
Vec *VecSub(Vec *A, Vec *B, Vec *C);*/
Vec *VecAdds(Flt a, Vec *A, Vec *B, Vec *C);
Vec *VecComb(Flt a, Vec *A, Flt b, Vec *B, Vec *C);
Vec *VecScalar(Flt a, Vec *A, Vec *C);
Vec *VecUnit(Vec *A, Vec *C);
Vec *VecNegate(Vec *A, Vec *C);
Vec *VecProduct(Vec *A, Vec *B, Vec *C);
void PrintVec(Vec *A);
Vec *ReadVec(FILE *inputfile, Vec *C);
colour *ReadCol(FILE *inputfile, colour *C);
void WriteVec(FILE *outputfile, Vec *A);
colour *ColourAddS(colour *A, CFlt b, colour *B, colour *C);
colour *ColourAdd(colour *A, colour *B, colour *C);
colour *ColourFilter(colour *A, colour *B, colour *C);
colour *ColourShade(CFlt a, colour *A, colour *C);
colour *ColourFade(CFlt a, colour *A, colour *B, colour *C);
colour *SetColour(CFlt red, CFlt green, CFlt blue, colour *C);
void PrintCol(colour *C);
colour *FadeColours(CFlt a, colour *A, colour *B, colour *C);
struct ColDefn *SearchCols(char *Name);
